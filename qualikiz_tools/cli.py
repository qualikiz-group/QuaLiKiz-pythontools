# pylint: disable=import-outside-toplevel,line-too-long
"""
qualikiz_tools

Usage:
  qualikiz_tools <command> [<args>...]
  qualikiz_tools [-v | -vv] <command> [<args>...]

Options:
  -h --help                         Show this screen.
  [-v | -vv]                        Verbosity
  --version                         Show version.

Examples:
  qualikiz_tools create
  qualikiz_tools dump
  qualikiz_tools input
  qualikiz_tools launcher
  qualikiz_tools output
  qualikiz_tools plot

Help:
  For help using this tool, please open an issue on the Github repository:
  https://github.com/rdegges/skele-cli
"""
import os
import sys
from subprocess import call

from docopt import docopt

from qualikiz_tools import __version__ as VERSION


def main():
    """Main CLI entrypoint."""
    args = docopt(__doc__, version=VERSION, options_first=True)
    argv = [args["<command>"]] + args["<args>"]

    if args["-v"] >= 1:
        passing = ["-" + "v" * args["-v"]] + argv
    else:
        passing = argv

    if args["-v"] >= 2:
        print("qualikiz_tools received:")
        print("global arguments:")
        print(args)
        print("command arguments:")
        print(argv)
        print("passing:")
        print(passing)
        print()

    if args["<command>"] == "create":
        from qualikiz_tools.commands import create

        create.main(passing)
    elif args["<command>"] == "dump":
        from qualikiz_tools.commands import dump

        dump.main(passing)
    elif args["<command>"] == "input":
        from qualikiz_tools.commands import input as inp

        inp.main(passing)
    elif args["<command>"] == "launcher":
        from qualikiz_tools.commands import launcher

        launcher.main(passing)
    elif args["<command>"] == "output":
        from qualikiz_tools.commands import output

        output.main(passing)
    elif args["<command>"] == "plot":
        from qualikiz_tools.commands import plot

        plot.main(passing)
    elif args["<command>"] == "poll":
        from qualikiz_tools.commands import poll

        poll.main(passing)
    elif args["<command>"] == "hello":
        from qualikiz_tools.commands import hello

        hello.main(passing)
    elif args["<command>"] in ["help", None]:
        sys.exit(call([sys.executable, sys.argv[0], "--help"]))
    else:
        sys.exit(
            "%r is not a qualikiz_tools command. See 'qualikiz_tools help'."
            % args["<command>"]
        )


if __name__ == "__main__":
    main()
