from subprocess import call
import sys
import os
import getopt
import argparse
from pathlib import Path
import logging
import json

from IPython import embed
import xarray as xr

from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizBatch
from qualikiz_tools.plottools.plot_fluxlike import build_plot
from qualikiz_tools.misc.helper_classes import next_path
from qualikiz_tools.misc.fs_manipulation import is_qlk_netcdf

cli_description = """
Searches all sub-directories for QuaLiKiz output netCDF files (for example,
generated with `qualikiztool output to_netcdf`) and plot them. The plot
command provides different subcommands to plot different things of a
QuaLiKiz run, use `--help` to show them. Note that flags for the `plot` command
need to be given _before_ the subcommand, see the examples.
"""

cli_epilog = """
Examples:
    * Show global help

        qualikiz_tools plot --help

    * Show specific help

        qulikiz_tools plot fluxlike --help

    * Plot ITG heat flux not summing over all hydrogen species and dropping all non-hydrogen species:

        qualikiz_tools plot fluxlike --flux ef --mode ITG --sepH --dropnH .
"""


def gen_figure_save_path(output_basename, suffix=".png"):
    path = next_path("fig%s-*", search_path=os.path.abspath(os.getcwd()))
    path = path.replace("*", output_basename + suffix)
    return path


logger = logging.getLogger(__name__)


def main(argv):
    subcommand = argv[0]
    assert (
        subcommand == "plot"
    ), "Don't run this as script! Use the `qualikiz_tools` entry point"
    main_program_name = sys.argv[0].split("/")[-1]
    sub_program_name = f"{main_program_name} plot"
    plot_parser = argparse.ArgumentParser(
        prog=sub_program_name,
        description=cli_description,
        epilog=cli_epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    subparsers = plot_parser.add_subparsers(
        description="Plot fluxlike variables with spectra",
        required=True,
    )
    fluxlike_parser = subparsers.add_parser("fluxlike", help="a help")

    plot_parser.add_argument(
        "--headless",
        action="store_true",
        help="start HEADLESS mode: dump plot to file, bypassing GUI",
    )

    plot_parser.add_argument(
        "--outname",
        help="Name of the output files. Implies headless. Output figures are"
        " of the form fig-${outname}.png",
    )

    plot_parser.add_argument(
        "--myslice",
        type=str,
        default="{}",
        metavar="string",
        help="JSON string with {'input name': value}. Fill be used to constain"
        "the sliced space [default: unconstrained]",
    )

    fluxlike_parser.add_argument("path_list", nargs="*")

    fluxlike_parser.add_argument("--verbosity", "-v", action="count", default=0)

    fluxlike_parser.add_argument(
        "--reuse-netcdf",
        action="store_true",
        help="Re-use netCDFs that are found in the pointed-to folders",
    )

    fluxlike_parser.add_argument(
        "--norm",
        default="SI",
        type=str,
        choices=["SI", "GB"],
        help="The normalization used (SI or GB)",
    )
    fluxlike_parser.add_argument(
        "--flux",
        default="ef",
        type=str,
        help="The flux type to plot. These are all three-letter QuaLiKiz outputs"
        " (e.g. ef, pf, vc)",
    )
    fluxlike_parser.add_argument(
        "--mode",
        default="total",
        type=str,
        choices=["total", "ETG", "ITG", "TEM"],
        help="The mode to plot, (e.g. total, ETG, ITG or TEM)",
    )

    sepsumH_group = fluxlike_parser.add_mutually_exclusive_group()
    sepsumH_group.add_argument(
        "--sepH",
        action="store_false",
        default=False,
        help="Keep separate contributions of hydrogen isotopes",
    )
    sepsumH_group.add_argument(
        "--sumH",
        action="store_true",
        default=True,
        help="Sum separate contributions of hydrogen isotopes together",
    )

    keepdropH_group = fluxlike_parser.add_mutually_exclusive_group()
    keepdropH_group.add_argument(
        "--keepnH", action="store_true", default=True, help="Keep non-hydrogen isotopes"
    )
    keepdropH_group.add_argument(
        "--dropnH",
        action="store_true",
        default=False,
        help="Drop non-hydrogen isotopes",
    )

    # As this is a subcommand itself, don't pass all argv to the fluxlike_parser
    args = plot_parser.parse_args(argv[1:])

    path_list = args.path_list
    if len(path_list) == 0:
        raise Exception("Path list given is empty")
    elif len(path_list) == 1 and path_list[0] == "help":
        raise Exception("Use -h or --help")

    # Parse paths
    dataset_paths = []
    for path in path_list:
        found_ds = False
        rpath = Path(path)
        # Search for a nc file
        if is_qlk_netcdf(rpath):
            found_ds = True
            netcdf_path = rpath
        elif args.reuse_netcdf:
            # Try every file; performance seems good
            for ipath in rpath.iterdir():
                if is_qlk_netcdf(rpath):
                    found_ds = True
                    netcdf_path = ipath
                    break

        if not found_ds:
            # Assume we're trying to plot a QuaLiKizBatch instead
            batch = QuaLiKizBatch.from_dir(rpath)
            dss: list = batch.to_netcdf()
            assert len(dss) == 1
            ds: xr.Dataset = dss[0]
            dataset_paths.append(batch.netcdf_path)
        else:
            if args.verbosity >= 1:
                print(f"Found netCDF file {ipath}, plotting!")
            dataset_paths.append(netcdf_path)

    if len(dataset_paths) != len(path_list):
        raise Exception("Could not find all datasets!")

    sum_hydrogen = args.sumH or (not args.sepH)
    drop_non_hydrogen = args.dropnH or (not args.keepnH)

    datasets = []
    names = []
    for file_path in dataset_paths:
        ds: xr.Dataset = xr.open_dataset(file_path)
        name: str = os.path.splitext(file_path.name)[0]
        datasets.append(ds)
        names.append(name)

    slicestr = args.myslice.replace("'", '"')
    myslice = json.loads(slicestr)

    # Call the QuaLiKiz-pythontools general plotting methods
    headless = args.headless or not not args.outname
    if headless:
        fig = build_plot(
            datasets,
            args.flux,
            args.norm,
            instability_tag=args.mode,
            sum_hydrogen=sum_hydrogen,
            drop_non_hydrogen=drop_non_hydrogen,
            names=names,
            interactive=False,
            myslice=myslice,
        )
        logger.info(f"Headless mode detected, outname is '{args.outname}'")
        path = gen_figure_save_path(args.outname, suffix=".png")
        fig.savefig(path)
    else:
        logger.info("Interactive mode detected")
        build_plot(
            datasets,
            args.flux,
            args.norm,
            instability_tag=args.mode,
            sum_hydrogen=sum_hydrogen,
            drop_non_hydrogen=drop_non_hydrogen,
            names=names,
            interactive=True,
            myslice=myslice,
        )


if __name__ == "__main__":
    main(sys.argv)
