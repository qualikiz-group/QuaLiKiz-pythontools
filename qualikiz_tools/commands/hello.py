"""The hello command."""

import json


def main(args):
    print("Hello, world!")
    jsonified_dict = json.dumps(args, indent=2, sort_keys=True)
    print("You supplied the following options:", jsonified_dict)
