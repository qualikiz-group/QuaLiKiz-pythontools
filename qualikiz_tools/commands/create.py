# pylint: disable=import-outside-toplevel,line-too-long
"""
Usage:
  qualikiz_tools [-v | -vv] create [--as_batch <machine>] [--in_dir <directory>] [--binary_path <path>] [--name <name>] <target> [<parameter_json>]
  qualikiz_tools [-v | -vv] create --as_batch <machine> [--in_dir <directory>] [--binary_path <path>] regression
  qualikiz_tools [-v | -vv] create --as_batch <machine> [--in_dir <directory>] [--binary_path <path>] from_json <path_to_json>

Options:
    --in_dir <directory>            Create folder in this folder [default: .]
    --name <name>                   Name to give to the main folder of the run
    --binary_path <path>            Path to the QuaLiKiz binary
    --as_batch <machine>            Create a batch script for the specified machine
  -h --help                         Show this screen.
  [-v | -vv]                        Verbosity

Often used commands:
  qualikiz_tools create example
  qualikiz_tools create regression
  qualikiz_tools create from_json <path_to_json>

"""  # noqa: E501
from subprocess import call
from warnings import warn
from shutil import copyfile
from pathlib import Path
import sys
import os

from docopt import docopt
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools import __path__ as ROOT
from qualikiz_tools.qualikiz_io.qualikizrun import (
    overwrite_prompt,
    find_qualikiz_binary,
)
from qualikiz_tools.qualikiz_io.reference_runs import (
    find_reference_folder,
    generate_all_base_cases_standalone,
    base_tests,
)

this_file = Path(__file__).resolve()

if __name__ == "__main__":
    print(docopt(__doc__))


def main(args):
    args = docopt(__doc__, argv=args)

    if args["-v"] >= 2:
        print("create received:")
        print(args)
        print()

    parent_dir = Path(args["--in_dir"]).resolve()
    if args["-v"] >= 1:
        verbose = True
    else:
        verbose = False

    if args["--binary_path"] is None:
        try:
            binary_path = find_qualikiz_binary(this_file)
        except FileNotFoundError:
            binary_path = (this_file / "../../../../QuaLiKiz").resolve()
    else:
        binary_path = Path(args["--binary_path"]).resolve()

    name = args["--name"]
    kwargs = {}
    if args["<target>"] == "example":
        from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun

        if name is None:
            name = "example"
        binreldir = os.path.relpath(binary_path, start=os.path.join(parent_dir, name))
        run = QuaLiKizRun(parent_dir, name, binreldir, verbose=verbose)
        run.prepare()
    elif args["<target>"] == "example_batch":
        if name is None:
            name = "example_batch"
        rundir = parent_dir / name
        binreldir = os.path.relpath(binary_path, start=rundir)
        batch_folder = parent_dir / name
        if overwrite_prompt(batch_folder):
            os.makedirs(batch_folder, exist_ok=True)
        # 'Copy' through memory, in case we need to modify the script
        multirun_script = (this_file / "../../examples/multirun.py").resolve()
        with multirun_script.open() as ff:
            script = ff.readlines()
        with (batch_folder / "multirun.py").open("w") as ff:
            ff.writelines(script)
        templatepath = (
            this_file / "../../qualikiz_io/parameters_template.json"
        ).resolve()
        copyfile(templatepath, os.path.join(batch_folder, "parameters_template.json"))

    elif args["<target>"] == "regression":
        if args["--as_batch"] is None:
            raise Exception(
                "Must supply --as_batch for target `{!s}`".format(args["<target>"])
            )
        if args["--name"] is not None:
            warn("--name flag not used in target `regression`")
        try:
            _temp = __import__(
                "qualikiz_tools.machine_specific." + args["--as_batch"],
                fromlist=["Run", "Batch"],
            )
        except ModuleNotFoundError:
            raise NotImplementedError(
                "Machine {!s} not implemented yet".format(args["--as_batch"])
            )
        Run, Batch = _temp.Run, _temp.Batch

        generate_all_base_cases_standalone()

        from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

        runlist = []
        run_parent_dir = parent_dir / args["<target>"]
        casefile_path = find_reference_folder(this_file, "reference_parameters")
        for case in base_tests:
            json_path = casefile_path / (case + ".json")
            plan = QuaLiKizPlan.from_json(json_path)
            name = os.path.basename(case)
            rundir = run_parent_dir / name
            binreldir = os.path.relpath(binary_path, start=rundir)
            run = Run(
                run_parent_dir,
                name,
                binreldir,
                qualikiz_plan=plan,
                verbose=verbose,
                tasks=1,
            )
            runlist.append(run)

        if len(runlist) == 0:
            raise RuntimeError("Could not find any regression cases")
        batch = Batch(parent_dir, args["<target>"], runlist, **kwargs)
        batch.prepare()

    # if args['<target>'] == 'mini':
    #    create_mini(parent_dir)
    # elif args['<target>'] == 'performance':
    #    create_performance(parent_dir)
    elif args["<target>"] == "from_json":
        json_path = args["<parameter_json>"]
        if json_path is None:
            raise Exception(
                "Please supply a path to a JSON file. See 'qualikiz_tools create help'"
            )
        json_path = Path(json_path)
        from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

        plan = QuaLiKizPlan.from_json(json_path)

        from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun

        if name is None:
            name = json_path.name.split(".")[0]
        rundir = parent_dir / name
        binreldir = os.path.relpath(binary_path, start=rundir)
        if args["--as_batch"] is None:
            run = QuaLiKizRun(
                parent_dir, name, binreldir, qualikiz_plan=plan, verbose=verbose
            )
            run.prepare()
        else:
            try:
                _temp = __import__(
                    "qualikiz_tools.machine_specific." + args["--as_batch"],
                    fromlist=["Run", "Batch"],
                )
            except ModuleNotFoundError:
                raise NotImplementedError(
                    "Machine {!s} not implemented yet".format(args["--as_batch"])
                )
            Run, Batch = _temp.Run, _temp.Batch
            run = Run(parent_dir, name, binreldir, qualikiz_plan=plan, verbose=verbose)
            batch = Batch(parent_dir, name, [run], **kwargs)
            batch.prepare()
    elif args["<target>"] in ["help", None]:
        sys.exit(call([sys.executable, __file__, "--help"]))
    else:
        sys.exit(
            "%r is not a valid target. See 'qualikiz_tools create help'."
            % args["<target>"]
        )


# def create_mini(target_dir):
#    call([sys.executable, os.path.join(ROOT, 'examples', 'mini.py'), target_dir])


def create_performance(target_dir):
    call([sys.executable, os.path.join(ROOT, "examples", "performance.py"), target_dir])
