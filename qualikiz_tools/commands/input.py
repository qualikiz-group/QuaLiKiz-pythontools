# pylint: disable=line-too-long,import-outside-toplevel
"""
Usage:
  qualikiz_tools input [-v | -vv] [--version=<version>] generate <qualikiz_directory>
  qualikiz_tools input [-v | -vv] generate_ql <qualikiz_directory>
  qualikiz_tools input [-v | -vv] to_physical [(-o <path> | --output=<path>) (-l <reflen> | --lref=<reflen>) (-r <ro> | --ro=<ro>) (-a <rlcfs> | --rlcfs=<rlcfs>) --autor=<autor> --te=<te> --reduce --pure_tor --qn_ion=<iqn> --grad_qn_ion=<igqn> --zeff_ion=<izf> --grad_zeff_ion=<igzf>] [--] <parameters_path> [<zi> [<zi> ...]]
  qualikiz_tools input [-v | -vv] to_dimensionless [(-o <path> | --output=<path>) (-l <reflen> | --lref=<reflen>) (-r <ro> | --ro=<ro>) (-a <rlcfs> | --rlcfs=<rlcfs>) --autor=<autor> --te=<te> --reduce --pure_tor --qn_ion=<iqn> --grad_qn_ion=<igqn> --zeff_ion=<izf> --grad_zeff_ion=<igzf>] [--] <parameters_path> [<zi> [<zi> ...]]
  qualikiz_tools input [-v | -vv] help

  For example, create input binaries for QuaLiKiz batch or run contained in <target_path>
      qualikiz_tools input generate <target_path>

Options:
  --version=<version>               Version of QuaLiKiz to generate input for [default: current]

  -o <path>, --output=<path>        Path to which evaluations will be saved [default: output_pars]
  -l <reflen>, --lref=<reflen>      Reference length for gradient unnormalization
  -r <ro>, --ro=<ro>                Miplane averaged major radius of flux surface for collisionality inversion
  -a <rlcfs>, --rlcfs=<rlcfs>,      Midplane averaged minor radius of separatrix for unnormalization of radial coordinate
  --autor=<autor>                   Normalized toroidal rotation gradient used to separate out component due to second derivative of ion pressure
  --te=<te>                         Electron temperature input for Gyrobohm scaling
  --reduce                          Flag to specify reduction of output lists to length 1 if value is repeated [default: False]
  --pure_tor                        Flag to specify ExB shearing rate to be only from toroidal flow velocity [default: False]
  --qn_ion=<iqn>                    Toggles enforcement of quasineutrality, integer specifies which ion is modified to enforce it
  --grad_qn_ion=<igqn>              Toggles enforcement of gradient quasineutrality, integer specifies which ion is modified to enforce it
  --zeff_ion=<izf>                  Toggles enforcement of effective charge in input file, integer specifies which ion is modified to enforce it
  --grad_zeff_ion=<igzf>            Toggles enforcement of effective charge gradient in input file (zero if not in input file), integer specifies which ion is modified to enforce it

  -h --help                         Show this screen.
  [-v | -vv]                        Verbosity

Often used commands:
  qualikiz_tools input generate <target_path>

"""  # noqa: E501
from subprocess import call
import sys
import logging
from pathlib import Path
import json
import pprint

from docopt import docopt
import pandas as pd
import numpy as np
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.qualikizrun import (
    qlk_from_dir,
    FromSubdirFilter,
)

from qualikiz_tools.misc.conversion import (
    physical_to_dimensionless,
    dimensionless_to_physical,
    calc_puretor_absolute,
    calc_puretor_gradient,
    calc_epsilon_from_parts,
)

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    embed()
    print(docopt(__doc__))


def main(args):
    args = docopt(__doc__, argv=args)

    # Set loglevel based on amount of [-v]s
    logger.setLevel(logging.INFO)
    if args["-v"] == 1:
        logger.setLevel(logging.DEBUG)
    elif args["-v"] >= 2:
        logger.setLevel(logging.TRACE)  # pylint: disable=no-member # Dynamically added

    logger.trace("input received:")
    logger.trace(args)

    # Detect the type of path given; QuaLiKizBatch or QuaLiKizRun
    if args["generate"] is True or args["generate_ql"] is True:
        if args["<qualikiz_directory>"]:
            qlkdir = args["<qualikiz_directory>"]
            # Filter out messages that specific from_dir/from_file is undefined
            # input regeneration does not care about specific machine
            # methods, binaries are always the same.
            fil = FromSubdirFilter()
            input_logger = logging.getLogger("qualikiz_tools.qualikiz_io.qualikizrun")
            input_logger.addFilter(fil)

            # Get type of QuaLiKiz directory (Batch or Run)
            __, qlk_instance = qlk_from_dir(qlkdir)
            input_logger.removeFilter(fil)
            logger.debug(
                "Supplied dir %s is of type %s", qlkdir, qlk_instance.__class__
            )
            kwargs = {}
            if args["-v"] == 1:
                input_logger.setLevel(logging.INFO)
            elif args["-v"] >= 2:
                kwargs["dotprint"] = True
                kwargs["dotprint"] = True
                input_logger.setLevel(logging.DEBUG)
            if args["--version"] == "current":
                pass
            elif args["--version"] in ["2.4.0", "2.3.2", "2.3.1", "CEA_QuaLiKiz"]:
                from qualikiz_tools.qualikiz_io.legacy import convert_current_to

                def convert(inputdir):
                    convert_current_to(inputdir, target=args["--version"])

                kwargs["conversion"] = convert
            else:
                raise Exception("Unknown version {!s}".format(args["--version"]))

            kwargs["qlflux"] = args["generate_ql"]
            if kwargs["qlflux"]:
                logger.info("Generating QLflux input")

            qlk_instance.generate_input(**kwargs)
            logger.info("QuaLiKiz input generated!")
    elif args.get("to_dimensionless", False) or args.get("to_physical", False):
        param_path = args["<parameters_path>"]
        ipath = Path(param_path)
        if args["to_dimensionless"]:
            to_dimensionless = True
        elif args["to_physical"]:
            to_dimensionless = False
        else:
            raise Exception("huh?")
        inp = None
        if ipath.is_file():
            enforce_zeff_grad = bool(args["--grad_zeff_ion"])
            inp = parse_parameters(
                ipath,
                to_dimensionless,
                enforce_zeff_grad=enforce_zeff_grad,
                verbosity=args["-v"],
            )
        else:
            raise Exception(
                "Path {!s} is not a valid parameters file".format(param_path)
            )
        if inp is None:
            raise Exception("Could not parse {!s}".format(param_path))
        else:

            # Convert CLI args to kwargs for the functions
            zi = None if len(args["<zi>"]) == 0 else [float(el) for el in args["<zi>"]]
            kwargs = {
                "zi_in": zi,
                "te_in": float(args["--te"]) if args["--te"] else None,
                "lref_in": float(args["--lref"]) if args["--lref"] else None,
                "ro_in": float(args["--ro"]) if args["--ro"] else None,
                "rlcfs_in": float(args["--rlcfs"]) if args["--rlcfs"] else None,
                "autor_in": float(args["--autor"]) if args["--autor"] else None,
                "pure_tor": bool(args["--pure_tor"]),
                "qn_ion": int(args["--qn_ion"]) if args["--qn_ion"] else None,
                "grad_qn_ion": int(args["--grad_qn_ion"])
                if args["--grad_qn_ion"]
                else None,
                "zeff_ion": int(args["--zeff_ion"]) if args["--zeff_ion"] else None,
                "grad_zeff_ion": int(args["--grad_zeff_ion"])
                if args["--grad_zeff_ion"]
                else None,
                "verbose": args["-v"],
            }
            if args["-v"] >= 2:
                print("Kwargs passed to conversion script:")
                pprint.pprint(kwargs)
            outp = pd.DataFrame()
            if args["to_dimensionless"]:
                outp = physical_to_dimensionless(inp, **kwargs)
            elif args["to_physical"]:
                outp = dimensionless_to_physical(inp, **kwargs)
            else:
                raise Exception("Are you kidding me?")

            if not outp.empty:
                reduce = bool(args["--reduce"])
                write_to_file(args["--output"], outp, to_dimensionless, reduce)
            else:
                raise ValueError(
                    "Output results are empty, check inputs and try again!"
                )
    else:
        print("Invalid CLI arguments detected:")
        sys.exit(call([sys.executable, __file__, "--help"]))


def write_to_file(ofile, outp, to_dimensionless, reduce):
    dtag = "physical" if not to_dimensionless else "dimensionless"
    if ofile.endswith(".h5"):
        outp.to_hdf(ofile, dtag)
    else:
        ofile = ofile if ofile.endswith(".json") else ofile + ".json"
        jsondict = {}
        for key in list(outp.columns):
            if not np.all(np.isnan(outp[key])):
                temp = outp[key].values.tolist()
                jsondict[key] = (
                    [temp[0]]
                    if reduce and np.all(np.isclose(temp, np.nanmean(temp)))
                    else temp
                )
            else:
                logger.warning("Calculated values for %s are all NaNs!", key)
                jsondict[key] = [np.nan]
        with open(ofile, "w") as ff:
            json.dump(jsondict, ff, indent=4, sort_keys=True)
    print("Conversion complete!")


def parse_parameters(ipath, to_dimensionless, enforce_zeff_grad=None, verbosity=0):
    """Parse QuaLiKiz parameters file

    Args:
        ipath: Path instance to parse
        reverse: If true, convert dimensionless to physical
        enforce_zeff_grad: enforcement of effective charge gradient in input
            file (zero if not in input file), integer specifies which ion is
            modified to enforce it
    """
    if str(ipath.name).endswith(".h5"):
        temp = pd.read_hdf(str(ipath.resolve()))
        ind = temp.index
        inp = ind.to_frame()
    elif str(ipath.name).endswith(".json"):
        required_data = {
            "geometry": [
                "x",
                "Ro",
                "Rmin",
                "Bo",
                "q",
                "smag",
                "alpha",
                "Machtor",
                "Machpar",
                "Autor",
                "Aupar",
                "gammaE",
            ],
            "elec": ["T", "n", "At", "An"],
            "ions": ["T", "n", "At", "An", "Z", "A"],
        }
        temp = {}
        idata = None
        with open(str(ipath.resolve())) as jsonfile:
            idata = json.load(jsonfile)
        temp = idata["scan_dict"] if "scan_dict" in idata else idata
        iterables = []
        names = []
        for key, val in temp.items():
            names.append(key)
            iterables.append(val)
        itype = 1 if "scan_type" in idata and idata["scan_type"] == "hyperrect" else 0
        if itype == 0:
            ind = pd.MultiIndex.from_arrays(iterables, names=names)
        elif itype == 1:
            ind = pd.MultiIndex.from_product(iterables, names=names)
        inp = ind.to_frame()
        inp = inp.reset_index(drop=True)
        numvals = len(inp)
        if "scan_dict" in idata:
            for var in inp.columns.values:
                if var.startswith("ni"):
                    inp["norm" + var] = inp[var].values
                    inp = inp.drop(var, axis=1)
        if "xpoint_base" in idata:
            for field in required_data:
                if field == "ions":
                    for ii in range(0, len(idata["xpoint_base"][field])):
                        itag = "i{:d}".format(ii)
                        for var in required_data[field]:
                            nvar = "norm" + var + itag if var == "n" else var + itag
                            if nvar not in inp:
                                val = idata["xpoint_base"][field][ii][var]
                                print("numvals!", numvals)
                                inp[nvar] = (
                                    [val] * numvals
                                    if val is not None
                                    else [np.nan] * numvals
                                )
                else:
                    for var in required_data[field]:
                        nvar = var + "e" if field == "elec" else var
                        if nvar not in inp:
                            val = idata["xpoint_base"][field][var]
                            inp[nvar] = (
                                [val] * numvals
                                if val is not None
                                else [np.nan] * numvals
                            )
        if "x" in inp and "Rmin" in inp and "Ro" in inp and "q" in inp:
            epsilon = calc_epsilon_from_parts(inp["x"], inp["Rmin"], inp["Ro"])
            mtorflag = "Machtor" in inp and np.all(np.isfinite(inp["Machtor"]))
            mparflag = "Machpar" in inp and np.all(np.isfinite(inp["Machpar"]))
            atorflag = "Autor" in inp and np.all(np.isfinite(inp["Autor"]))
            aparflag = "Aupar" in inp and np.all(np.isfinite(inp["Aupar"]))
            gameflag = "gammaE" in inp and np.all(np.isfinite(inp["gammaE"]))
            if not mtorflag or not mparflag:
                abs_var = "Machpar" if mparflag else "Machtor"
                [Machtor, Machpar] = calc_puretor_absolute(
                    epsilon, inp["q"], **{abs_var: inp[abs_var]}
                )
                inp["Machtor"] = Machtor
                inp["Machpar"] = Machpar
                if verbosity >= 2:
                    print(inp["Machpar"].values)
            if not atorflag or not aparflag or not gameflag:
                grad_var = "gammaE"
                if atorflag:
                    grad_var = "Autor"
                if aparflag:
                    grad_var = "Aupar"
                [Aupar, Autor, gammaE] = calc_puretor_gradient(
                    epsilon, inp["q"], **{grad_var: inp[grad_var]}
                )
                inp["Autor"] = Autor
                inp["Aupar"] = Aupar
                inp["gammaE"] = gammaE
                if verbosity >= 2:
                    print(inp["Aupar"].values)
        if "scan_dict" in idata or "xpoint_base" in idata:
            for var in inp.columns.values:
                if var.startswith("ne") and to_dimensionless:
                    inp[var] = inp[var].values * 1.0e19
                if var.startswith("Ti"):
                    inp[var] = inp[var].values * 1.0e3
                if var.startswith("Te") and to_dimensionless:
                    inp[var] = inp[var].values * 1.0e3
        if enforce_zeff_grad is not None and "grad_Zeff" not in inp:
            inp["grad_Zeff"] = inp["x"] * 0.0 if "x" in inp else inp["r"] * 0.0
        if verbosity >= 1:
            print(inp.columns)
    else:
        raise Exception("Unknown file extension")
    return inp
