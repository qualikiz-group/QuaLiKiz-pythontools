# pylint: disable=line-too-long,import-outside-toplevel
"""
Usage:
  qualikiz_tools launcher [-v | -vv] [--stdout <path>] [--stderr <path>] [--no-clean] <command> <machine> <target_path>
  qualikiz_tools launcher [-v | -vv] help

  Launch a job using the machine-specific QuaLiKiz tools. In principle the 'bash' machine is machine-agnostic. It needs bash and mpirun at minimum. This command will create input binaries if they are missing.

Options:
  --version <version>               Version of QuaLiKiz to generate input for [default: current]
  --stdout <path>                   Path to put STDOUT. Default depends on <machine>.
  --stderr <path>                   Path to put STDERR. Default depends on <machine>.
  --no-clean                        Do not clean run folder before running
  -h --help                         Show this screen.
  [-v | -vv]                        Verbosity

Example command:
  qualikiz_tools launcher launch bash .

"""  # noqa: E501
from subprocess import call
import sys
import logging

from docopt import docopt

from qualikiz_tools.qualikiz_io.qualikizrun import (
    qlk_from_dir,
)

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    print(docopt(__doc__))


def main(args):
    args = docopt(__doc__, argv=args)

    # Set loglevel based on amount of [-v]s
    logger.setLevel(logging.INFO)
    if args["-v"] == 1:
        logger.setLevel(logging.DEBUG)
    elif args["-v"] >= 2:
        logger.setLevel(logging.TRACE)

    logger.trace("launcher received:")
    logger.trace(args)

    # Detect the type of path given; QuaLiKizBatch or QuaLiKizRun
    if args["<machine>"] not in ["help", None]:
        try:
            _temp = __import__(
                "qualikiz_tools.machine_specific." + args["<machine>"],
                fromlist=["Run", "Batch"],
            )
        except ModuleNotFoundError:
            raise NotImplementedError(
                "Machine {!s} not implemented yet".format(args["<machine>"])
            )
        Run, Batch = _temp.Run, _temp.Batch
        kwargs = {}
        if args["--stdout"] is not None:
            kwargs["stdout"] = args["--stdout"]
        if args["--stderr"] is not None:
            kwargs["stderr"] = args["--stderr"]
        if args["-v"] >= 1:
            kwargs["verbose"] = True
        __, qlk_instance = qlk_from_dir(
            args["<target_path>"], batch_class=Batch, run_class=Run, **kwargs
        )

        logger.debug(
            "Supplied dir %s is of type %s",
            args["<target_path>"],
            qlk_instance.__class__,
        )

    kwargs = {}
    help_command_given = args["<target_path>"] in ["help", None] or args[
        "<command>"
    ] in ["help", None]
    if args["<command>"] == "launch":
        if isinstance(qlk_instance, Run):
            qlk_instance.prepare(overwrite=False, overwrite_meta=False)
        elif isinstance(qlk_instance, Batch):
            qlk_instance.prepare(
                overwrite_batch=False,
                overwrite_batch_script=False,
                overwrite_meta=False,
            )
        else:
            raise Exception(
                "Unrecognized instance {!s} for machine {!s}".format(
                    qlk_instance, args["<machine>"]
                )
            )
        qlk_instance.generate_input()
        if args["--no-clean"]:
            logger.info("Not cleaning QuaLiKiz folder before running")
            qlk_instance.launch(clean=False)
        else:
            logger.info("Cleaning QuaLiKiz folder before running")
            qlk_instance.launch()
    elif help_command_given:
        sys.exit(call([sys.executable, __file__, "--help"]))
    else:
        sys.exit(
            "%r is not a valid target. See 'qualikiz_tools launcher help'."
            % args["<target_path>"]
        )
