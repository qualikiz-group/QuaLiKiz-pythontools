"""
Usage:
  qualikiz_tools dump <target_path>
  qualikiz_tools [-v | -vv] dump <target_path>

  Dump contents of target (binary) file to STDOUT in human-readable form

Options:
  -h --help                         Show this screen.
  [-v | -vv]                        Verbosity

"""
from subprocess import call
import sys
import os

from docopt import docopt


def main(args):
    args = docopt(__doc__, argv=args)

    if args["-v"] >= 2:
        print("create received:")
        print(args)
        print()

    # TODO: Check type of file. Currently only work for binary
    if args["<target_path>"]:
        args["<target_path>"] = os.path.abspath(args["<target_path>"])
        from qualikiz_tools.fs_manipulation.compare import bin_to_np

        print(bin_to_np(args["<target_path>"]))

    elif args["<target_path>"] in ["help", None]:
        sys.exit(call([sys.executable, __file__, "--help"]))
    else:
        sys.exit(
            "%r is not a valid target. See 'qualikiz_tools dump help'."
            % args["<target_path>"]
        )


if __name__ == "__main__":
    print(docopt(__doc__))
