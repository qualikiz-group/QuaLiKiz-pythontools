To automate scheduling with Airflow, you must first specify a home directory for the Airflow software, eg.:
export AIRFLOW_HOME=~/airflow

Then install airflow:
pip install apache-airflow

---- For the MARCONI environment only (for use in conjunction with Tensorflow):
     pip install --ignore_installed --user Werkzeug



Then initialize the airflow database (default sqlite):
airflow initdb

Copy the airflow.cfg file in this directory to your $AIRFLOW_HOME directory.

Ensure that the following settings in the new $AIRFLOW_HOME/airflow.cfg are correct:
load_examples = False
dags_are_paused_at_creation = False
dag_run_conf_overrides_params = True

And copy the DAG files from this directory to the airflow DAG repostiory:
cp ./*_dag.py $AIRFLOW_HOME/dags/

Then, open the file $AIRFLOW_HOME/dags/slurm_qualikiz_monitor_dag.py and set the fields in the 'params'
variable to specify your particular run. Details on each field are provided in the DAG file.

Run the following airflow command to check that the DAG files are in the right place and working:
airflow list_dags

It should display the following DAGs, among any others that you have from other projects:
slurm_qualikiz_monitor
slurm_qualikiz_submission





Simply run the scheduler to start the workflow:
airflow scheduler

Note that running the scheduler will lock up your terminal for the scheduler process. It is recommended
to leave the scheduler running and open another terminal to continue your work. DAGs will not be run if
the scheduler process is terminated.

The automated scheduler handles input generation (not input preparation!), code execution, and output
collection. It does this in strictly separate phases, as the time required to complete each phase can
vary significantly between different QuaLiKiz input configurations. The submission DAG tries to automate
and log the phase of each QuaLiKiz run as it goes, as well as take advantage of the standardized
QuaLiKiz run directory structure to auto-detect which phase a non-descript run is in.

By definition, this unfortunately means that the monitor must be run at least 3 times (one for each phase)
to fully complete even a single QuaLiKiz run.

Handling of QuaLiKiz batches is not automated at this time, use the following setting in the monitor DAG
to disable the auto-detect features (the batch execution will fail otherwise):
'is_batch': True





The custom SLURM monitor is pre-configured to run every hour on the hour after 2017-11-01 00:00:00 without
backfilling. Use the following command to determine the next run:
airflow next_execution slurm_qualikiz_monitor

This default 'start_date' and 'schedule_interval' can be changed by modifying their corresponding variables
inside the DAG file, 'slurm_qualikiz_monitor_dag.py', if the first execution is too far in the future.

Additionally, you can set 'schedule_interval' to '@once' to make the DAG run only upon receiving an external
trigger, such as one from the command line, which can be done as such:
airflow trigger_dag slurm_qualikiz_monitor -c <json string with user-defined params>

The parameters provided on the command line will override the defaults specified inside the DAG file itself.

