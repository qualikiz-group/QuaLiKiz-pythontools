# Airflow DAG for monitoring QuaLiKiz executions via SLURM queue submissions
# Developed by Aaron Ho (30/09/2019)

import datetime as dt
import getpass
import re
import subprocess
from pathlib import Path

from airflow import DAG, AirflowException
from airflow.models import Variable
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator

# This import is required for the run phase autodetection routine
try:
    from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun, QuaLiKizBatch
except ImportError:
    QuaLiKizRun = None
    QuaLiKizBatch = None
    print("QuaLiKiz-pythontools failed to import. Poor behaviour expected.")

# These are defaults properties of the DAG itself
#   Adjust start_date and schedule_interval to you needs before starting scheduler
default_args = {
    "owner": getpass.getuser(),
    "depends_on_past": False,
    "start_date": dt.datetime(2017, 11, 1),
    "catchup": False,
}
schedule_interval = "@once"
# These are defaults for variables used with the custom functions of this DAG
#   Adjust as necessary, they are automatically passed through to Operators via provide_context
params = {
    "monitor_id": None,  # Name of monitor configuration variable in Airflow database (REQUIRED!)
    # The DAG requires one of these two, all jobs to be executed must be contained by some root point
    "rootdir": None,  # Base directory containing all the jobs to be executed by this DAG
    "job_location_file": None,  # Text file containing locations of all jobs to be executed by this DAG
    # SLURM options
    "account": None,  # Account name, for computing clusters which require it
    "partition": None,  # Partition name, for computing clusters which require it
    "maxtime": None,  # Maximum wall time for SLURM job, for computing clusters which require it
    "nodes": 1,  # Number of nodes to occupy, recommended 1 for now
    # QuaLiKiz workflow configuration options
    "binary_path": None,  # QuaLiKiz binary file to be used
    "max_jobs_at_once": 1,  # Maximum number of jobs to submit to SLURM queue
    "skip_reconstruction": False,  # Flag to disable use of pythontools reconstruction in auto-detection phase
    "allow_retry": False,  # Flag to allow re-submission of jobs previously marked as failed
    #"preparation_script": None,  # Optional Python script file which prepares the directory tree - not yet implemented
    "generate_inputs": True,  # Specify input generation phase as part of workflow
    "execute_code": True,  # Specify code execution phase as part of workflow
    "collect_outputs": True,  # Specify output collection phase as part of workflow
}


dag = DAG(
    "slurm_qualikiz_monitor",
    default_args=default_args,
    params=params,
    schedule_interval=schedule_interval,
)

status_tags = {
    "none": "Untouched",
    "input": "Inputs generated",
    "execute": "Code executed",
    "output": "Outputs collected",
    "done": "Outputs verified",
}

def is_ready(**kwargs):
    print(kwargs["params"])
    monitor_id = kwargs["params"].get("monitor_id", None)
    rootdir = kwargs["params"].get("rootdir", None)
    jobfile = kwargs["params"].get("job_location_file", None)
    account = kwargs["params"].get("account", None)
    partition = kwargs["params"].get("partition", None)
    time = kwargs["params"].get("maxtime", None)
    if not isinstance(monitor_id, str):
        raise ValueError(
            "Forced failure because invalid monitor identification string was given!"
        )
    rdir = Path(rootdir) if isinstance(rootdir, str) else None
    jfile = Path(jobfile) if isinstance(jobfile, str) else None
    if not (rdir is not None and rdir.is_dir()) and not (jfile is not None and jfile.is_file()):
        raise ValueError(
            "Forced failure because invalid job directory specification was given!"
        )
    if account is None:
        raise ValueError("Forced failure because slurm account was not given!")
    if partition is None:
        raise ValueError("Forced failure because slurm partition was not given!")
    if time is None:
        raise ValueError(
            "Forced failure because maximum wall time for slurm job was not given!"
        )
    monitor_config = Variable.get(monitor_id, deserialize_json=True, default_var=None)
    next_task = (
        "load_monitor_config"
        if monitor_config is not None and monitor_config
        else "init_monitor_config"
    )
    return next_task

def initialize_monitor(**kwargs):
    rootdir = kwargs["params"].get("rootdir", None)
    jobfile = kwargs["params"].get("job_location_file", None)
    rdir = Path(rootdir) if isinstance(rootdir, str) else None
    jfile = Path(jobfile) if isinstance(jobfile, str) else None
    if rdir is None and jfile is not None:
        rdir = jfile.parent.resolve()
    if jfile is None and rdir is not None:
        lfile = rdir / "job_list.txt" 
        if lfile.is_file():
            jfile = lfile.resolve()
    monitor_config = {
        "job_location_file": str(jfile.resolve()),
        "rootdir": str(rdir.resolve()),
        "username": kwargs["params"].get("username", getpass.getuser()),
        "submission_database": {},
        "flags": {
            "input": kwargs["params"].get("generate_inputs", False),
            "execute": kwargs["params"].get("execute_code", False),
            "output": kwargs["params"].get("collect_outputs", False),
        },
    }
    job_locations = []
    if jfile is not None and jfile.is_file():
        mrdir = Path(monitor_config["rootdir"])
        with open(str(jfile.resolve()), "r") as xlog:
            for line in xlog:
                location = Path(line.strip())
                if location.is_dir():
                    job_locations.append(str(location.resolve()))
                elif (mrdir / location).is_dir():
                    job_locations.append(str((mrdir / location).resolve()))
    if len(job_locations) <= 0:
        job_locations = [str(rdir.resolve())]
    default_message = status_tags["none"]
    if not kwargs["params"].get("generate_inputs", True):
        default_message = status_tags["input"]
        if not kwargs["params"].get("execute_code", True):
            default_message = status_tags["execute"]
            # Used for output existence checking
            if not kwargs["params"].get("collect_outputs", True):
                default_message = status_tags["output"]
    monitor_config["status_database"] = dict.fromkeys(job_locations, default_message)
    Variable.set(
        kwargs["params"].get("monitor_id"), monitor_config, serialize_json=True
    )
    return monitor_config

def load_existing_monitor(**kwargs):
    monitor_id = kwargs["params"].get("monitor_id")
    print("Loading variable: %s..." % (monitor_id))
    monitor_config = Variable.get(monitor_id, deserialize_json=True)
    print("Loaded variable: %s" % (monitor_id))
    return monitor_config

def auto_detect_phase(target_directory, skip_recon):
    phase = None
    tdir = Path(target_directory)
    if skip_recon or QuaLiKizRun is None or QuaLiKizBatch is None:
        if (tdir / "qualikiz.batch").is_file():
            phase = "none"
            if (tdir / "igen_stdout.batch").is_file():
                phase = "input"
            if (tdir / "stdout.batch").is_file():
                phase = "execute"
            for item in tdir.glob("*.nc*"):
                if re.match(tdir.stem, str(item)):
                    phase = "output"
        else:
            raise OSError(
                "Required QuaLiKiz output infrastructure not found, slurm submission aborted!"
            )
    else:
        try:
            run_object = QuaLiKizRun.from_dir(str(tdir.resolve()))
            phase = "none"
            if run_object.inputbinaries_exist():
                phase = "input"
            if run_object.is_done():
                phase = "execute"
            for item in tdir.glob("*.nc*"):
                if re.search(tdir.stem, str(item)):
                    phase = "output"
        except OSError:
            try:
                batch_object = QuaLiKizBatch.from_dir(str(tdir.resolve()))
                phase = "none"
                if batch_object.inputbinaries_exist():
                    phase = "input"
                if batch_object.is_done():
                    phase = "execute"
                for item in tdir.glob("*.nc*"):
                    if re.search(tdir.stem, str(item)):
                        phase = "output"
            except OSError as e:
                print(repr(e))
                raise OSError(
                    "Required QuaLiKiz output infrastructure not found, slurm submission aborted!"
                )
    return phase

def check_monitor_consistency(**kwargs):
    new_monitor_config, existing_monitor_config = kwargs["task_instance"].xcom_pull(
        task_ids=["init_monitor_config", "load_monitor_config"]
    )
    monitor_config = (
        existing_monitor_config
        if isinstance(existing_monitor_config, dict)
        else new_monitor_config
    )
    iflag = kwargs["params"].get("generate_inputs", False)
    eflag = kwargs["params"].get("execute_code", False)
    oflag = kwargs["params"].get("collect_outputs", False)
    if "flags" not in monitor_config:
        monitor_config["flags"] = {"input": True, "execute": True, "output": True}
    print(monitor_config["flags"])
    if (
        monitor_config["flags"]["input"] != iflag
        or monitor_config["flags"]["execute"] != eflag
        or monitor_config["flags"]["output"] != oflag
    ):
        job_locations = []
        jobfile = monitor_config["job_location_file"]
        with open(jobfile, "r") as xlog:
            for line in xlog:
                location = Path(line.strip())
                if location.is_dir():
                    job_locations.append(str(location.resolve()))
                elif (Path(monitor_config["rootdir"]) / location).is_dir():
                    job_locations.append(str((Path(monitor_config["rootdir"]) / location).resolve()))
        if len(job_locations) <= 0:
            job_locations = [monitor_config["rootdir"]]
        default_message = status_tags["none"]
        if not iflag:
            default_message = status_tags["input"]
            if not eflag:
                default_message = status_tags["execute"]
                if not oflag:
                    # If output exists at this point, then this counts as verification
                    default_message = status_tags["done"]
        for jobloc in job_locations:
            if jobloc not in "submission_database":
                phase = auto_detect_phase(jobloc, skip_recon=False)
                monitor_config["status_database"][jobloc] = status_tags[phase]
        Variable.set(
            kwargs["params"].get("monitor_id"), monitor_config, serialize_json=True
        )
    return monitor_config

def define_next_submissions(**kwargs):
    monitor_config = kwargs["task_instance"].xcom_pull(task_ids="check_monitor_config")
    # Grab current queue to determine which jobs have finished
    queue_query = subprocess.check_output(
        ["squeue","-u", monitor_config["username"], "-o", "\"%i\""]
    )
    queue_query_results = queue_query.decode("utf-8").strip().split("\n")
    queue_query_results = (
        queue_query_results[1:] if len(queue_query_results) > 1 else []
    )
    current_queue = []
    for item in queue_query_results:
        pid = "".join(c for c in item.strip() if c.isdigit())
        current_queue.append(pid)
    print(current_queue)
    submission_database = monitor_config["submission_database"]
    donelist = [
        submission_database[key]
        for key in submission_database
        if key not in current_queue
    ]
    print(donelist)
    # Provide brief reporting
    status = monitor_config["status_database"]
    total = 0
    success_so_far = 0
    failed_so_far = 0
    for key in status:
        total += 1
        if status[key] == "Outputs collected":
            success_so_far += 1
        if status[key].startswith("FAILED") or status[key].startswith("ABORTED"):
            failed_so_far += 1
    print("Total number of jobs: {:d}".format(total))
    print("Completed jobs: {:d}".format(success_so_far))
    print("Failed jobs: {:d}".format(failed_so_far))
    # Define the next set of jobs to be run
    joblist = []
    non_submit_list = []
    target_status = status_tags["done"]
    if not kwargs["params"].get("collect_outputs", True):
        target_status = status_tags["execute"]
        if not kwargs["params"].get("execute_code", True):
            target_status = status_tags["input"]
            if not kwargs["params"].get("generate_inputs", True):
                target_status = status_tags["none"]
    for item in donelist:
        if item not in status:
            raise ValueError(
                "Run location {} not found in monitor, monitor data could be corrupted!".format(
                    item
                )
            )
        if kwargs["params"].get("allow_retry"):
            if status[item] == status_tags["output"]:
                non_submit_list.append(item)
            else:
                joblist.append(item)
        elif not status[item].startswith("FAILED") and not status[item].startswith(
            "ABORTED"
        ):
            if status[item] == status_tags["output"]:
                non_submit_list.append(item)
            else:
                joblist.append(item)
    default_message = status_tags["none"]
    if not kwargs["params"].get("generate_inputs", True):
        default_message = status_tags["input"]
        if not kwargs["params"].get("execute_code", True):
            default_message = status_tags["execute"]
            if not kwargs["params"].get("collect_outputs", True):
                default_message = status_tags["output"]
    was_modified = True
    while was_modified and len(submission_database) - len(donelist) + len(
        joblist
    ) < kwargs["params"].get("max_jobs_at_once"):
        was_modified = False
        for key, value in status.items():
            if key not in joblist:
                if kwargs["params"].get("allow_retry") and (
                    value.startswith("FAILED") or value.startswith("ABORTED")
                ):
                    joblist.append(key)
                    was_modified = True
                    break
                elif value == default_message:
                    joblist.append(key)
                    was_modified = True
                    break
    # Ensures there will always be max jobs in queue by distinguishing tasks which do not submit to queue
    joblist.extend(non_submit_list)
    # Pushes list of next jobs up to be submitted
    kwargs["task_instance"].xcom_push(key="nextjobs", value=joblist)
    kwargs["task_instance"].xcom_push(key="target", value=target_status)
    next_task = "trigger_new_submissions" if len(joblist) > 0 else "skip_trigger"
    return next_task

def submit_new_jobs(context, dag_run_obj):
    # Saving of new configuration done within submission DAG since it needs the new slurm job IDs
    joblist = context["task_instance"].xcom_pull(
        task_ids="create_new_jobs", key="nextjobs"
    )
    status = context["task_instance"].xcom_pull(
        task_ids="create_new_jobs", key="target"
    )
    if not isinstance(joblist,list):
        joblist = []
    dag_run_obj.payload = {
        "monitor_id": context["params"].get("monitor_id"),
        "log_submissions": True,
        "directory_list": joblist,
        "target_status": status,
        "skip_reconstruction": context["params"].get("skip_reconstruction"),
        "allow_retry": context["params"].get("allow_retry"),
        "account": context["params"].get("account"),
        "partition": context["params"].get("partition"),
        "maxtime": context["params"].get("maxtime"),
        "nodes": context["params"].get("nodes", 1),
        "binary_path": context["params"].get("binary_path"),
    }
    print(dag_run_obj.payload)
    return dag_run_obj

check_metadata = BranchPythonOperator(
    task_id="check_metadata", python_callable=is_ready, provide_context=True, dag=dag
)

init_monitor_config = PythonOperator(
    task_id="init_monitor_config",
    python_callable=initialize_monitor,
    provide_context=True,
    dag=dag,
)

load_monitor_config = PythonOperator(
    task_id="load_monitor_config",
    python_callable=load_existing_monitor,
    provide_context=True,
    dag=dag,
)

check_monitor_config = PythonOperator(
    task_id="check_monitor_config",
    python_callable=check_monitor_consistency,
    provide_context=True,
    trigger_rule="none_failed",
    dag=dag,
)

create_new_jobs = PythonOperator(
    task_id="create_new_jobs",
    python_callable=define_next_submissions,
    provide_context=True,
    dag=dag,
)

trigger_new_submissions = TriggerDagRunOperator(
    task_id="trigger_new_submissions",
    trigger_dag_id="slurm_qualikiz_submit",
    python_callable=submit_new_jobs,
    dag=dag,
)

skip_trigger = DummyOperator(task_id="skip_trigger", dag=dag)

end_task = DummyOperator(task_id="end_task", trigger_rule="none_failed", dag=dag)

check_metadata >> [
    init_monitor_config,
    load_monitor_config,
] >> check_monitor_config >> create_new_jobs >> [
    trigger_new_submissions,
    skip_trigger,
] >> end_task
