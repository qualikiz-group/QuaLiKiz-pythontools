import datetime as dt
import subprocess
import os
import time
import re
import copy
from pathlib import Path

from airflow import DAG
from airflow.models import Variable
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator

try:
    from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun, QuaLiKizBatch
except ImportError:
    QuaLiKizRun = None
    QuaLiKizBatch = None
    print("QuaLiKiz-pythontools failed to import. Poor behaviour expected.")

default_args = {
    "owner": os.getcwd(),
    "depends_on_past": False,
    "start_date": dt.datetime(2017, 11, 1),
    "catchup": False,
}
schedule_interval = None
params = {
    "monitor_id": None,
    "log_submissions": False,
    "directory_list": [],
    "is_batch": False,
    "target_status": "Outputs verified",
    "skip_reconstruction": False,
    "allow_retry": False,
    "binary_path": None,
}

dag = DAG(
    "slurm_qualikiz_submit",
    default_args=default_args,
    params=params,
    schedule_interval=schedule_interval,
)

status_tags = {
    "none": "Untouched",
    "input": "Inputs generated",
    "execute": "Code executed",
    "output": "Outputs collected",
    "done": "Outputs verified",
}
failure_tags = {
    "none": "Unknown state",
    "input": "Input generation",
    "execute": "Code execution",
    "output": "Output collection",
    "done": "Fake news!",
}

def submit_to_slurm(
    command,
    work_directory,
    stdout_redirect=None,
    stderr_redirect=None,
    use_airflow_kwargs=True,
    **kwargs
):
    wdir = os.path.abspath(work_directory)
    options = ["-D", wdir]
    if use_airflow_kwargs:
        account = kwargs["params"].get("account", None)
        partition = kwargs["params"].get("partition", None)
        time = kwargs["params"].get("maxtime", None)
        nodes = kwargs["params"].get("nodes", 1)
        if account is None:
            raise ValueError("Account not provided, slurm submission failed!")
        if partition is None:
            raise ValueError("Partition not provided, slurm submission failed!")
        if time is None:
            raise ValueError("Maximum wall time not provided, slurm submission failed!")
        options.extend(["-A", account, "-p", partition, "-t", time, "-n", repr(nodes)])
    if isinstance(stdout_redirect, str):
        options.extend(["-o", stdout_redirect])
    if isinstance(stderr_redirect, str):
        options.extend(["-e", stderr_redirect])
    bash_command = ["sbatch"]
    bash_command.extend(options)
    if isinstance(command, str):
        bash_command.append(command)
    elif isinstance(command, (list, tuple)):
        bash_command.extend(command)
    else:
        raise ValueError("Invalid submit command, slurm submission failed!")
    submission_stdout = subprocess.check_output(bash_command)
    stdout_list = submission_stdout.decode("utf-8").strip().split("\n")
    submission_result = stdout_list[-1].strip().split()
    slurm_job_id = submission_result[-1]
    return slurm_job_id

def submit_input_generation(target_directory, directory_type=None, **kwargs):
    job_id = "-1"
    if directory_type == "run":
        if not (target_directory / "input").is_dir():
            (target_directory / "input").mkdir()
        if not (target_directory / "meta").is_dir():
            (target_directory / "meta").mkdir()
    bfile = target_directory / "qualikiz_inputs.batch"
    with open(str(bfile.resolve()),"w") as ff:
        ff.write(
            "#!/bin/bash -l\n#SBATCH --job-name=igen_"
            + str(target_directory.stem)
            + "\n\nqualikiz_tools input generate .\n"
        )
    time.sleep(0.1)
    stdout_redirect = "igen_stdout.batch"
    stderr_redirect = "igen_stderr.batch"
    input_command = [str((target_directory / "qualikiz_inputs.batch").resolve())]
    subout = submit_to_slurm(
        input_command, target_directory, stdout_redirect, stderr_redirect, **kwargs
    )
    if re.match(r"^[0-9]+$", subout):
        job_id = subout
    return job_id  # Return as string, -1 indicates failure

def submit_code_execution(target_directory, directory_type=None, **kwargs):
    job_id = "-2"
    if directory_type == "run":
        if not (target_directory / "output").is_dir():
            (target_directory / "output").mkdir()
        if not (target_directory / "output" / "primitive").is_dir():
            (target_directory / "output" / "primitive").mkdir()
        if not (target_directory / "debug").is_dir():
            (target_directory / "debug").mkdir()
    elif directory_type == "batch":
        for item in target_directory.iterdir():
            if item.is_dir():
                if not (item / "output").is_dir():
                    (item / "output").mkdir()
                if not (item / "output" / "primitive").is_dir():
                    (item / "output" / "primitive").mkdir()
                if not (item / "debug").is_dir():
                    (item / "debug").mkdir()
    time.sleep(0.1)
    # These are the default redirected output filenames, not necessary to pass them to submission
    stdout_redirect = "stdout.batch"
    stderr_redirect = "stderr.batch"
    execute_command = [str((target_directory / "qualikiz.batch").resolve())]
    subout = submit_to_slurm(
        execute_command, target_directory, use_airflow_kwargs=False, **kwargs
    )
    if re.match(r"^[0-9]+$", subout):
        job_id = subout
    return job_id  # Return as string, -2 indicates failure

def submit_output_collection(target_directory, directory_type=None, **kwargs):
    job_id = "-3"
    bfile = target_directory / "qualikiz_outputs.batch"
    with open(str(bfile.resolve()), "w") as ff:
        ff.write(
            "#!/bin/bash -l\n#SBATCH --job-name=ocol_"
            + str(target_directory.stem)
            + "\n\nqualikiz_tools output to_netcdf --loopy .\n"
        )
    time.sleep(0.1)
    stdout_redirect = "ocol_stdout.batch"
    stderr_redirect = "ocol_stderr.batch"
    output_command = [str((target_directory / "qualikiz_outputs.batch").resolve())]
    subout = submit_to_slurm(
        output_command, target_directory, stdout_redirect, stderr_redirect, **kwargs
    )
    if re.match(r"^[0-9]+$", subout):
        job_id = subout
    return job_id  # Return as string, -3 indicates failure

def auto_detect_phase(target_directory, skip_recon, binary_path):
    phase = None
    dtype = None
    if skip_recon or QuaLiKizRun is None or QuaLiKizBatch is None:
        if (target_directory / "qualikiz.batch").is_file():
            phase = "none"
            dtype = "run" if (target_directory / "QuaLiKiz").is_file() else "batch"
            if (target_directory / "igen_stdout.batch").is_file():
                phase = "input"
            if (target_directory / "stdout.batch").is_file():
                phase = "execute"
            for item in target_directory.glob("*.nc*"):
                if re.search(str(target_directory.stem), str(item)):
                    phase = "output"
        else:
            raise OSError(
                "Required QuaLiKiz output infrastructure not found, slurm submission aborted!"
            )
    else:
        try:
            run_object = QuaLiKizRun.from_dir(str(target_directory.resolve()), binaryrelpath=binary_path)
            phase = "none"
            dtype = "run"
            if run_object.inputbinaries_exist(suppress_warning=True):
                phase = "input"
            if run_object.is_done():
                phase = "execute"
            for item in target_directory.glob("*.nc*"):
                if re.search(str(target_directory.stem), str(item)):
                    phase = "output"
        except OSError:
            try:
                rkwargs = {"binaryrelpath": binary_path}
                batch_object = QuaLiKizBatch.from_subdirs(str(target_directory.resolve()), run_kwargs=rkwargs)
                phase = "none"
                dtype = "batch"
                if batch_object.inputbinaries_exist(suppress_warning=True):
                    phase = "input"
                if batch_object.is_done():
                    phase = "execute"
                for item in target_directory.glob("*.nc*"):
                    if re.search(str(target_directory.stem), str(item)):
                        phase = "output"
            except OSError as e:
                print(repr(e))
                raise OSError(
                    "Required QuaLiKiz output infrastructure not found, slurm submission aborted!"
                )
    return phase, dtype

def submit_and_log(**kwargs):
    flog = kwargs["params"].get("log_submissions")
    monitor_id = kwargs["params"].get("monitor_id")
    monitor_config = (
        Variable.get(monitor_id, deserialize_json=True, default_var=None)
        if flog and monitor_id is not None
        else None
    )
    joblist = kwargs["params"].get("directory_list")
    target_phase = kwargs["params"].get("target_status")
    next_task = "skip_logging"
    if flog and not isinstance(monitor_config, dict):
        raise ValueError("Forced failure because monitor has not been instantiated!")
    submit_reference = dict()
    for item in joblist:
        tdir = Path(item)
        phase, dtype = auto_detect_phase(
            tdir,
            kwargs["params"].get("skip_reconstruction"),
            kwargs["params"].get("binary_path"),
        )
        status_in_db = copy.deepcopy(monitor_config["status_database"][item])
        if phase == "none":
            if status_tags[phase] == target_phase:
                submit_reference[item] = (None, status_tags[phase])
            elif status_in_db == status_tags[phase]:
                job_id_str = submit_input_generation(tdir, dtype, **kwargs)
                submit_reference[item] = (job_id_str, status_tags["input"])
                print("Input generation job submitted: {!s}".format(job_id_str))
            elif kwargs["params"].get("allow_retry"):
                if status_in_db.startswith("FAILED") or status_in_db.startswith("ABORTED"):
                    job_id_str = submit_input_generation(tdir, dtype, **kwargs)
                    submit_reference[item] = (job_id_str, status_tags["input"])
                    print("Input generation job re-submitted: {!s}".format(job_id_str))
                else:
                    pass
            else:
                submit_reference[item] = ("-1", "FAILED: " + failure_tag["input"])
        elif phase == "input":
            if status_tags[phase] == target_phase:
                submit_reference[item] = (None, status_tags[phase])
            elif status_in_db == status_tags[phase]:
                job_id_str = submit_code_execution(tdir, dtype, **kwargs)
                submit_reference[item] = (job_id_str, status_tags["execute"])
                print("Code execution job submitted: {!s}".format(job_id_str))
            elif kwargs["params"].get("allow_retry"):
                if status_in_db.startswith("FAILED") or status_in_db.startswith("ABORTED"):
                    job_id_str = submit_code_execution(tdir, dtype, **kwargs)
                    submit_reference[item] = (job_id_str, status_tags["execute"])
                    print("Code execution job re-submitted: {!s}".format(job_id_str))
                else:
                    pass
            else:
                submit_reference[item] = ("-2", "FAILED: " + failure_tags["execute"])
        elif phase == "execute":
            if status_tags[phase] == target_phase:
                submit_reference[item] = (None, status_tags[phase])
            elif status_in_db == status_tags[phase]:
                job_id_str = submit_output_collection(tdir, dtype, **kwargs)
                submit_reference[item] = (job_id_str, status_tags["output"])
                print("Output collection job submitted: {!s}".format(job_id_str))
            elif kwargs["params"].get("allow_retry"):
                if status_in_db.startswith("FAILED") or status_in_db.startswith("ABORTED"):
                    job_id_str = submit_output_collection(tdir, dtype, **kwargs)
                    submit_reference[item] = (job_id_str, status_tags["output"])
                    print("Output collection job re-submitted: {!s}".format(job_id_str))
                else:
                    pass
            else:
                submit_reference[item] = ("-3", "FAILED: " + failure_tags["output"])
        elif phase == "output":
            submit_reference[item] = (None, status_tags["done"])
            if status_in_db == status_tags["done"]:
                print("Full workflow already completed on directory")
        if item not in submit_reference:
            submit_reference[item] = ("-4", "ABORTED: " + failure_tags["none"])
            print("Directory state is indeterminate or ambiguous!")
    if flog:
        kwargs["task_instance"].xcom_push(
            key="submission_reference", value=submit_reference
        )
        next_task = "log_submissions"
    return next_task

def update_monitor(**kwargs):
    monitor_id = kwargs["params"].get("monitor_id")
    job_id_reference = kwargs["task_instance"].xcom_pull(
        task_ids="submit_job_list", key="submission_reference"
    )
    if isinstance(monitor_id, str):
        monitor_config = Variable.get(
            monitor_id, deserialize_json=True, default_var=None
        )
        if monitor_config is not None and "status_database" in monitor_config:
            stat_db = copy.deepcopy(monitor_config["status_database"])
            job_db = (
                copy.deepcopy(monitor_config["submission_database"])
                if "submission_database" in monitor_config
                else dict()
            )
            job_db_keys = list(job_db.keys())
            for key in job_db_keys:
                if job_db[key] in job_id_reference:
                    del job_db[key]
            for item in job_id_reference:
                if job_id_reference[item][0] is not None and not job_id_reference[item][
                    0
                ].startswith("-"):
                    job_db[job_id_reference[item][0]] = item
                if item in stat_db:
                    stat_db[item] = job_id_reference[item][1]
            monitor_config["status_database"] = stat_db
            monitor_config["submission_database"] = job_db
        Variable.set(monitor_id, monitor_config, serialize_json=True)
    return monitor_config

submit_job_list = BranchPythonOperator(
    task_id="submit_job_list",
    python_callable=submit_and_log,
    provide_context=True,
    dag=dag,
)

log_submissions = PythonOperator(
    task_id="log_submissions",
    python_callable=update_monitor,
    provide_context=True,
    dag=dag,
)

skip_logging = DummyOperator(task_id="skip_logging", dag=dag)

end_task = DummyOperator(task_id="end_task", dag=dag)

submit_job_list >> [log_submissions, skip_logging] >> end_task
