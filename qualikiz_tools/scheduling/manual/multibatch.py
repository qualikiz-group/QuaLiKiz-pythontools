#!/usr/bin/env python3
from itertools import product
from pathlib import Path
import logging
from os.path import relpath
import copy

from IPython import embed

from qualikiz_tools.machine_specific.slurm import Run, Batch

# Set defaults before importing anything else
# Setting dynamically is untested
Run.defaults["cores_per_node"] = 48  # For MARCONI A3/skl
Batch.defaults = {
    "partition": "skl_fua_prod",  # For production runs
    "repo": "FUA34_QLKNN11D",  # For our NN allocation
}

# Import other needed QuaLiKiz tools
import qualikiz_tools
from qualikiz_tools.misc.helper_classes import OODS
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

# Set up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# The binary is located in my home folder, e.g.
# /marconi/home/userexternal/kvandepl/working/QuaLiKiz/bin/QuaLiKiz_200914_de4ba633-intel-release-default-mpi.exe
qlk_bindir = Path.home() / "bin_archive"
qlk_binpath = qlk_bindir / "QuaLiKiz_QLKNN11D_hyper-intel-release-default-mpi.exe"
if not qlk_binpath.is_file():
    logger.warning('QuaLiKiz binary "{!s}" does not exist'.format(qlk_binpath))


# Use the default JSON as template for this batch
templatepath = "./template_megarun3.json"

# Load the default QuaLiKiz plan we will use as base
qualikiz_plan_base = QuaLiKizPlan.from_json(templatepath)

# We will use a physically relevant base scan
# We change our base plan programatically instead of with the JSON
qualikiz_plan_base["scan_dict"] = OODS(
    [
        ("Ati", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 10, 16]),
        ("Ate", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 10, 16]),
        ("Ane", [-5, -3, -1, 0, 0.5, 1, 1.5, 2, 2.5, 3, 5]),
        ("Ani0", [-15, -5, -1, 0, 0.5, 1, 1.5, 2, 3, 4, 9, 15]),
        # ('Ani0', np.linspace(-1, 3, 8).tolist()),
    ]
)
qualikiz_plan_base["scan_type"] = "hyperrect"  # Scan all points in the hyperrectangle
qualikiz_plan_base["xpoint_base"]["kthetarhos"] = [
    0.1,
    0.175,
    0.25,
    0.325,
    0.4,
    0.5,
    0.7,
    1.0,
    1.8,
    3,
    9,
    15,
    21,
    27,
    36,
    45,
]  # JETTO grid

# Define the "programatically defined parameters"
# Inner cube
batch_cube = OODS(
    [
        ("q", [0.66, 1, 1.33, 1.66, 2, 3.5, 5, 10]),
    ]
)

# Set up multiple batches.
rest_cube = OODS(
    [
        ("smag", (-1, -0.3, 0.1, 1.66, 0.7, 1, 1.5, 2, 2.75, 4)),
        ("x", (0.1, 0.25, 0.4, 0.55, 0.7, 0.85, 0.95)),
        ("Ti_Te_rel", (0.25, 0.5, 0.75, 1, 1.33, 1.66, 2.5)),
        ("dilution", (0, 0.1, 0.2, 0.3)),
        # ('smag', (1, )),
        # ('x', (0.55, )),
        # ('Ti_Te_rel', (1, )),
        # ('dilution', (0, 0.1, 0.2, 0.3)),
    ]
)

# Manually create the 'megacube' over multiple batches
mega_cube = OODS(
    [
        ("Nustar", (0.00001, 0.0001, 0.001, 0.01, 0.066, 0.033, 0.1, 0.215, 0.33, 0.66, 1)),
    ]
)

outer_var = "Nustar"
outer_vals = mega_cube.get(outer_var)
outer_val = outer_vals[0]  # The first megacube

# Save out batches as a subfolder of this one
batch_parent = Path("_".join([outer_var, str(outer_val)])).absolute()
if batch_parent.exists():
    raise Exception(
        "Folder in which we generate already exists. This is probably not what you meant! Please remove it and try again"
    )

# Use 20 nodes
nodes = 20

# And build our list of runs
batchlist = []
for ii, subcube_values in enumerate(product(*rest_cube.values_middle_first())):
    # Create name for the batch
    val_strings = list("{:0.2f}".format(v) for v in subcube_values)
    named_strings = list("_".join(foo) for foo in zip(rest_cube.keys(), val_strings))
    batch_name = "_".join(named_strings)
    batch_dir = batch_parent / batch_name  # For convenience

    runlist = []
    for ii, runcube_values in enumerate(product(*batch_cube.values())):
        # Create name for the run
        val_strings = list("{:0.2f}".format(v) for v in runcube_values)
        named_strings = list(
            "_".join(foo) for foo in zip(batch_cube.keys(), val_strings)
        )
        run_name = "_".join(named_strings)

        # Fill what changes every run
        # Set the order _explicitly_, so we know what sets what
        # Note that Ati, Ate, Ane, Ani0, and Nustar are set from the scan_dict itself
        # Use the explicit interfaces to be 100% sure1
        batch_qualikiz_plan = copy.deepcopy(qualikiz_plan_base)
        batch_qualikiz_plan["xpoint_base"]["q"] = runcube_values[
            list(batch_cube.keys()).index("q")
        ]
        batch_qualikiz_plan["xpoint_base"]["smag"] = subcube_values[
            list(rest_cube.keys()).index("smag")
        ]
        batch_qualikiz_plan["xpoint_base"]["x"] = subcube_values[
            list(rest_cube.keys()).index("x")
        ]
        tite = subcube_values[list(rest_cube.keys()).index("Ti_Te_rel")]
        dilution = subcube_values[list(rest_cube.keys()).index("dilution")]

        # Dilution is a bit tricky
        batch_qualikiz_plan["xpoint_base"]["ni0"] = 1 - dilution
        batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_normni_ion"] = 1
        if dilution == 0:
            batch_qualikiz_plan["xpoint_base"]["ni1"] = 0
            batch_qualikiz_plan["xpoint_base"]["Ani1"] = 0
            batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_An_ion"] = 0
        else:
            batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_An_ion"] = 1
            batch_qualikiz_plan["xpoint_base"].set_qn_normni_ion_n()
        # batch_qualikiz_plan['xpoint_base'].match_dilution(dilution)
        # Now set the vars that depend on other vars
        batch_qualikiz_plan["xpoint_base"]["Nustar"] = outer_val
        batch_qualikiz_plan["xpoint_base"]["Ti_Te_rel"] = tite

        run_parent = batch_name
        run_dir = batch_dir / run_name  # For convenience
        binreldir = Path(relpath(qlk_binpath, start=run_dir))
        run = Run(
            batch_dir,
            run_name,
            binreldir,
            qualikiz_plan=batch_qualikiz_plan,
            nodes=nodes,
        )
        runlist.append(run)
    # Implementation detail, we pass the directory the batch
    # _lives in_ and the name of the batch
    logger.info("Dimx of run is", run.qualikiz_plan.calculate_dimx())
    logger.info(
        "If runs are sequential, means {!s} MPI tasks taking {:.2f} minutes".format(
            run.tasks, len(runlist) * run.estimate_walltime(run.tasks) / 60
        )
    )
    batch = Batch(batch_parent, batch_name, runlist)
    batchlist.append(batch)

print(
    "If runs are sequential, means {!s} MPI tasks taking {:.2f} minutes per batch".format(
        run.tasks, len(runlist) * run.estimate_walltime(run.tasks) / 60
    )
)

run_dimx = run.qualikiz_plan.calculate_dimxn() / len(
    run.qualikiz_plan["xpoint_base"]["kthetarhos"]
)  # Single run dimx
batch_dimx = len(runlist) * run_dimx
print(
    "dimx={:.0f} per batch, for a total dimx={:.0f} for {:.0f} batches".format(
        batch_dimx, len(batchlist) * batch_dimx, len(batchlist)
    )
)

cpu_time_single_batch_single_core = len(runlist) * run.estimate_cputime(
    1
)  # In CPU seconds
cpu_time_single_batch_single_node = (
    cpu_time_single_batch_single_core / Run.defaults["cores_per_node"]
)  # In node-seconds

node_hours = len(batchlist) * cpu_time_single_batch_single_node / 3600
cpu_hours = node_hours * Run.defaults["cores_per_node"]
print(
    "If runs are sequential, meaning {:.2f} node hours, or {:.2f} CPU hours".format(
        node_hours, cpu_hours
    )
)

# Create links to scipts we need for a huge run
tools_root = Path(qualikiz_tools.__path__[0])
scheduling_dir = tools_root / "scheduling/manual"
if not scheduling_dir.is_dir():
    raise Exception(
        "QuaLiKiz-pythontools scheduling directory does not exists at {!s}! Cannot create links".format(
            scheduling_dir
        )
    )

needed_scripts = [
    "generate_input.py",
    "qualikiz_generate_input.batch",
    "launch_batches.py",
    "qualikiz_launch_batches.batch",
    "collect_output.py",
    "qualikiz_collect_output.batch",
]


batch_parent.mkdir(exist_ok=True)
for script_name in needed_scripts:
    script = scheduling_dir / script_name
    target_path = batch_parent / script_name
    if not script.is_file():
        raise Exception(
            "Script {!s} does not exist at {!s}. Cannot create link".format(
                script_name, script
            )
        )
    else:
        target_path.symlink_to(script)

# Prepare our batches, e.g. build the folder structure
for batch in batchlist:
    batch.prepare()

# resp = input('Generate input? [Y/n]')
# if resp == '' or resp == 'Y' or resp == 'y':
#    for batch in batchlist:
#        print('Generating input files', end='', flush=True)
#        batch.generate_input(dotprint=True)
#        print('\n')
