from pathlib import Path
import gc
import time
import datetime

import numpy as np
import xarray as xr
from dask.diagnostics import ProgressBar
from dask.distributed import performance_report
from qualikiz_tools.qualikiz_io.netcdf4 import get_info, get_variables, get_chunks, get_shapes, get_bytes

from qualikiz_tools.qualikiz_io.qualikizrun import (
    build_encoding,
    create_to_netcdf_kwargs,
)


def glue_paths_xarray(ds_paths, res_name):
    print("Collected {!s} paths to glue".format(len(paths)))
    dss = [xr.open_dataset(path) for path in paths]
    print("Datasets loaded, starting concat")
    newds = xr.concat(dss, dim="dimx")
    print("Concat finished, resetting dimx")
    newds["dimx"] = np.arange(0, len(newds["dimx"]))
    print("Saving to disk..")
    xarray_to_netcdf_kwargs = create_to_netcdf_kwargs()
    encoding = build_encoding(newds, n_dimx_chunks=len(ds_paths))
    newds.to_netcdf(res_name, encoding=encoding, **xarray_to_netcdf_kwargs)
    del newds
    gc.collect()


def glue_paths_dask(ds_paths, res_name):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
    print("[{!s}] Opening {!s} datasets".format(st, len(paths)))
    with ProgressBar():
        newds = xr.open_mfdataset(
            ds_paths, parallel=True, concat_dim="dimx", combine="nested"
        )
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
    print("[{!s}] Building encoding".format(st))
    xarray_to_netcdf_kwargs = create_to_netcdf_kwargs()
    # One chunk per file
    n_chunks = len(ds_paths)
    print("Using", n_chunks, "chunks")
    encoding = build_encoding(newds, n_dimx_chunks=n_chunks)
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
    print("[{!s}] Saving to disk".format(st))
    with ProgressBar():
        newds.to_netcdf(res_name, encoding=encoding, **xarray_to_netcdf_kwargs)
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
    print("[{!s}] done".format(st))
    return newds


if __name__ == "__main__":
    cwd = Path.cwd()
    cube_name = "_".join(cwd.name.split("_")[:-1])
    other_dilution_cube_name = cube_name + ".nc.1"
    zero_dilution_cube_name = cube_name + "_dilution_0.00.nc.1"
    # A note about chunks
    # Using largest case variable npol (dimx, ntheta, nions) float32       = 512 Bytes per dimx
    # dimx=1 for 512 B
    # dimx=2 for 1 kiB
    # dimx=245 for 122.5 kiB
    # dimx=490 for 245 kiB
    # dimx=980 for 490 kiB
    # dimx=1960 for 980 kiB
    # dimx=76666 for 37 MiB
    # dimx=127776 for 62.390625 MiB
    # Use smallest case variable efe_GB (dimx) float32 = 4 Bytes per       dimx
    # dimx=1 for 4B
    # dimx=49 for 196B
    # dimx=245 for 980B
    # dimx=256 for 1 kiB
    # dimx=1960 for 7.65625 kiB
    # dimx=15972 for 62.390625 kiB
    # dimx=76666 for 299.4765625 kiB
    # dimx=127776 for 499.125 kiB

    mode = 'distributed'
    mode = 'distributed_one_job'
    #mode = 'local'
    print('Running mode', mode)
    if mode == 'distributed':
        from dask.distributed import Client, LocalCluster

        # Each worker uses 100% CPU max
        cluster = LocalCluster(
           n_workers=48,  # Skylake Intel® Xeon® Platinum 8160 Processor
           threads_per_worker=2,  # Hyperthreading
           memory_limit=160e9 / 48,  # This is _per worker_
        )
        client = Client(cluster)
    elif mode == 'distributed_one_job':
        from dask.distributed import Client, LocalCluster

        # Each worker uses 100% CPU max
        cluster = LocalCluster(
           n_workers=1,  # Skylake Intel® Xeon® Platinum 8160 Processor
           threads_per_worker=96,  # Hyperthreading
           memory_limit=160e9,  # This is _per worker_
        )
        client = Client(cluster)
    else:
        from contextlib import contextmanager
        @contextmanager
        def performance_report(filename=None):
            yield None

    # Merge non-zero dilution
    paths = sorted(
        [
            path
            for path in cwd.glob("*.nc")
            if (
                not path.name.endswith("dilution_0.00.nc")
                and path.name not in [other_dilution_cube_name, zero_dilution_cube_name]
            )
        ]
    )
    with performance_report(filename="dask-report-nonzero.html"):
        glue_paths_dask(paths, other_dilution_cube_name)
    client.restart()

    # Merge zero dilution
    paths = sorted(
        [
            path
            for path in cwd.glob("*dilution_0.00.nc")
            if path.name not in [other_dilution_cube_name, zero_dilution_cube_name]
        ]
    )
    with performance_report(filename="dask-report-zero.html"):
        glue_paths_dask(paths, zero_dilution_cube_name)

    print("Gluing netCDF done!")
