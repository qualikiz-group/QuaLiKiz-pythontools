import os
from pathlib import Path

from IPython import embed

from qualikiz_tools.machine_specific.slurm import Batch

cwd = Path.cwd()
batch_paths = list(cwd.glob("*/qualikiz.batch"))

for ii, batch_file in enumerate(batch_paths):
    batch = Batch.from_batch_file(batch_file)
    if batch.inputbinaries_exist(suppress_warning=True) and batch.is_done():
        print(
            "{:40s} {:4d}/{:4d}: Run is done, skipping!".format(
                batch.name, ii + 1, len(batch_paths)
            )
        )
    elif batch.inputbinaries_exist(suppress_warning=True):
        # michele = mmarin00
        # Check running jobs to not double-launch
        my_jobs = Batch.get_running_jobs(user="mmarin00")
        my_jobs = Batch.get_running_jobs()
        batch_dir = batch.parent_dir / batch.name
        wdirs = my_jobs["WORK_DIR"]
        launch = True
        for wdir in wdirs:
            if batch_dir.samefile(wdir):
                print(
                    "{:40s} {:4d}/{:4d}: Still in queue, skipping!".format(
                        batch.name, ii + 1, len(batch_paths)
                    )
                )
                launch = False
                break
        if launch:
            print(
                "{:40s} {:4d}/{:4d}: Input binaries exist, job not in queue, job not done. Launching run!".format(
                    batch.name, ii + 1, len(batch_paths)
                )
            )
            batch.launch()
    else:
        print(
            "{:40s} {:4d}/{:4d}: Input binaries do not exist, skipping!".format(
                batch.name, ii + 1, len(batch_paths)
            )
        )
print(f"Done! Walked past {len(batch_paths)} batch paths")
