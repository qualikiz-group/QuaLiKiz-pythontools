import os
from pathlib import Path

from IPython import embed

from qualikiz_tools.machine_specific.slurm import Batch

cwd = Path.cwd()
batch_paths = list(cwd.glob("*/qualikiz.batch"))

for ii, batch_file in enumerate(batch_paths):
    batch = Batch.from_batch_file(batch_file)
    if batch.inputbinaries_exist(suppress_warning=True):
        print(
            "{:40s} {:4d}/{:4d}: Binaries already exist!".format(
                batch.name, ii + 1, len(batch_paths)
            )
        )
    else:
        print(
            "{:40s} {:4d}/{:4d}: Generating binaries!".format(
                batch.name, ii + 1, len(batch_paths)
            )
        )
        batch.generate_input()
print(f"Done! Walked past {len(batch_paths)} batch paths")
