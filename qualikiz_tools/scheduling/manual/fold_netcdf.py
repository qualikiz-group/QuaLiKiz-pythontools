import os
import psutil
import gc
import datetime
from collections import OrderedDict
from itertools import chain

import dask.dataframe as dd
import xarray as xr
from IPython import embed
import numpy as np
from qualikiz_tools.qualikiz_io.outputfiles import (
    squeeze_dataset,
    split_ion_coordinates,
)

from functions import *


dss = []
full_cube = OrderedDict(
    [
        ("Ati", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 9, 14]),
        ("Ate", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 9, 14]),
        ("Ane", [-5, -3, -1, 0, 0.5, 1, 1.5, 2, 2.5, 3, 5]),
        ("Ani0", [-15, -5, -1, 0, 0.5, 1, 1.5, 2, 3, 4, 9, 15]),
        ("q", [0.66, 1, 1.33, 1.66, 2, 3, 4, 5, 10]),
        ("smag", [-1, 0.1, 0.4, 0.7, 1, 1.5, 2, 2.75, 4]),
        ("x", [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]),
        ("Ti_Te0", [0.25, 0.5, 0.75, 1, 1.33, 1.66, 2.5]),
        ("normni0", (1 - np.array((0, 0.1, 0.2, 0.3))).tolist()),
        #    ('Nustar', [0, 1.00E-04, 1.00E-03, 1.00E-02, 1.00E-01]),
    ]
)

part_cube = OrderedDict(
    [
        ("Ati", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 9, 14]),
        ("Ate", [0, 2, 2.75, 3.5, 4.25, 5, 5.75, 6.5, 7.5, 9, 14]),
        ("Ane", [-5, -3, -1, 0, 0.5, 1, 1.5, 2, 2.5, 3, 5]),
        ("Ani0", [-15, -5, -1, 0, 0.5, 1, 1.5, 2, 3, 4, 9, 15]),
    ]
)

supercube_vars = ["Nustar"]

chunksize_dict = dict(
    [
        ("Ati", 11),
        ("Ate", 11),
        ("Ane", 11),
        ("Ani0", 12),
        ("q", 1),
        ("smag", 1),
        ("x", 1),
        ("Ti_Te0", 1),
        ("normni0", 1),
        ("Nustar", 1),
    ]
)

# files = [
#    'smag-1_x0.3_Ti_Te_rel0.25_dilution0.nc',
#    'smag4_x0.9_Ti_Te_rel1_dilution0.2.nc',
# ]
files = sorted(
    [
        path
        for path in os.listdir()
        if os.path.isfile(path)
        and (path.endswith(".nc.1") or path.endswith(".nc"))
        and path.startswith("cleaned_")
    ]
)
# files = files[:10]

# ds_new = xr.Dataset(coords=full_cube)
# ds_new['nions'] = ds['nions']
# runcube_dims = ['Ati', 'Ate', 'Ane', 'Ani0']
# runcube_sizes = [ds_new.dims[var] for var in runcube_dims]

new_dims = xr.Dataset(part_cube)
new_dim_names = ["Ati", "Ate", "Ane", "Ani0"]

# ds_new = fold_subcube(ds, new_dim_names, new_dims)
# super_dims = [(dim, full_cube[dim]) for dim in full_cube if dim not in ds_new.dims]
# ds_new.coords.update(OrderedDict(super_dims))
# ds_new = new_dims.copy()
ds = xr.open_dataset(files[0])
fname = "folded.nc.1"
for var in supercube_vars:
    uniq = np.unique(ds[var])
    if len(uniq) != 1:
        raise Exception("Cannot make superdim {!s}".format(var))
    full_cube[var] = uniq
ds_new = xr.Dataset(coords=full_cube)
ds_new.attrs = ds.attrs
ds_new.to_netcdf(fname)
data_vars = [var for var in ds.data_vars if var not in ["gam_GB", "ome_GB"]]
for ii, var_name in enumerate(data_vars):
    log("Starting var {:2s}/{:2s}: {!s}", (str(ii + 1), str(len(data_vars)), var_name))

    # sizes = [len(full_cube[dim]) for dim in full_cube.keys()]
    ds_full = xr.Dataset(coords=full_cube)
    ds_full.coords["nions"] = ds["nions"]

    if var_name in ds_full.dims:
        new_var_name = var_name + "_orig"
    else:
        new_var_name = var_name

    dims = list(full_cube.keys())
    chunksizes = [chunksize_dict[dim] for dim in dims]
    if "nions" in ds[var_name].dims:
        dims += ["nions"]
        chunksizes += [1]

    arr = np.full([ds_full.dims[dim] for dim in dims], np.nan)
    mold = xr.DataArray(arr, dims={dim: ds_full[dim] for dim in dims})
    ds_full[new_var_name] = mold

    # singleton_dims = [dim for dim in mold.dims if dim not in ds[var_name].dims]

    # for dim in singleton_dims:
    #    new_var = fold_1dscan_variable(ds[dim], ds['q'], new_dim_names, new_dims)
    #    print("hey!")
    #    ds_full[dim + '_orig'] = new_var
    for jj, file in enumerate(files):
        ds = xr.open_dataset(file)
        new_var = fold_1dscan_variable(
            ds[var_name],
            ds["q"],
            new_dim_names,
            new_dims,
            supercube_new_dims=supercube_vars,
        )
        fold_with_mold(ds, new_var, ds_full[new_var_name])

    log("Mold filled, writing to disk")
    ds_full.to_netcdf(
        fname,
        "a",
        encoding={var_name: {"chunksizes": chunksizes, "zlib": True, "complevel": 1}},
    )
    process = psutil.Process(os.getpid())
    log("Using {!s} MiB", process.memory_info().rss / 1024 ** 2)
log("Done")
