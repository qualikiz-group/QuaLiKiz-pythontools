import datetime
from IPython import embed
import xarray as xr
import numpy as np
from collections import OrderedDict


def curtime():
    now = datetime.datetime.now()
    return now.strftime("%d-%m-%y %H:%M:%S")


def log(msg, fmt=None):
    if fmt is None:
        fmt = []
    elif isinstance(fmt, (tuple, list)):
        pass
    else:
        fmt = [fmt]
    print("[" + curtime() + "] " + msg.format(*fmt))


def fold_variable(var, new_dim_names, new_dims, verbosity=0):
    # This seems to work
    if verbosity >= 1:
        print("Doing var", var.name)
    # Check if we have this fold case coded yet
    if any(dim not in ("dimx", "nions") for dim in var.dims):
        raise NotImplementedError(
            "Folding of {!s} with dims {!s}".format(var.name, var.dims)
        )
    if "nions" in var.dims:
        nion_scan = var["nions"]
    else:
        nion_scan = [-1]
    ion_vars = []
    for ion in nion_scan:
        if ion != -1:
            subvar = var.sel(nions=ion)
        else:
            subvar = var
        # Warning! list(new_dims.dims.values()) is not ordered! Nor is new_dims.dims!
        shape = [len(new_dims[dim]) for dim in new_dim_names]
        resh = subvar.values.reshape(shape)
        data = xr.DataArray(resh, dims=new_dim_names)
        if ion != -1:
            data = data.expand_dims({"nions": [ion]}, axis=-1)
        ion_vars.append(data)
    if ion == -1:
        q_var = data
    else:
        q_var = xr.concat(ion_vars, "nions")
    return q_var


def fold_1dscan_variable(
    var, scan_var, subcube_new_dim_names, subcube_new_dims, supercube_new_dims=None
):
    # This seems to work
    q_vars = []
    for ii, qx in enumerate(np.unique(scan_var)):
        sub_var = var.where(scan_var == qx, drop=True)
        q_var = fold_variable(sub_var, subcube_new_dim_names, subcube_new_dims)
        # By convention, nions comes after the other fold dims
        if "nions" in q_var.dims:
            new_idx = -2
        else:
            new_idx = -1
        q_var = q_var.expand_dims({"q": [qx]}, axis=new_idx)
        q_vars.append(q_var)
    new_var = xr.concat(q_vars, scan_var.name)
    if supercube_new_dims is not None:
        new_var.expand_dims(supercube_new_dims)
    new_var.name = var.name
    return new_var


def fold_with_mold(ds, var, mold):
    singleton_dims = [dim for dim in mold.dims if dim not in var.dims]
    sel = {}
    for dim in singleton_dims:
        val = np.unique(ds[dim])
        if len(val) == 1:
            sel[dim] = val[0]
        else:
            raise Exception(
                "Dim {!s} is not singleton. Mismatch between var and mold".format(dim)
            )
    try:
        mold.loc[dict(sel)] = var
    except ValueError:
        mismatch = set(mold.dims).symmetric_difference(
            set(var.dims).union(singleton_dims)
        )
        if len(mismatch) >= 1:
            raise Exception(
                "Var {!s} does not fit in mold. Spurious dims: {!s}".format(
                    var.name, mismatch
                )
            )
        else:
            raise


def fold_subcube(ds, new_dim_names, new_dims):
    # This seems to work
    if not isinstance(new_dim_names, (list, tuple)):
        raise Exception(
            "new_dim_names should be an ordered iteratable, not {!s}".format(
                type(new_dim_names)
            )
        )
    ds_new = new_dims.copy()
    for var_name in ds.data_vars:
        if var_name in ds.dims:
            continue
        if var_name in ["gam_GB", "ome_GB"]:
            continue
        new_var = fold_1dscan_variable(ds[var_name], ds["q"], new_dim_names, new_dims)
        if var_name in ds_new.dims:
            ds_new[var_name + "_orig"] = new_var
        else:
            ds_new[var_name] = new_var
    return ds_new
