import gc
import os
import time
import datetime
from pathlib import Path

import psutil
from IPython import embed

from qualikiz_tools.machine_specific.slurm import Batch


def print_process_info():
    process = psutil.Process()
    mem_info = process.memory_info()
    print(
        "Using {!s} MiB of Resident Set Size (non-swap physical memory)".format(
            mem_info.rss / 1024 ** 2
        )
    )
    cpu_times = process.cpu_times()
    time_dict = {}
    for varname in ["user", "system", "iowait"]:
        time_dict[varname] = getattr(cpu_times, varname)
    print(
        "Spend {user}s in userspace, {system}s in kernel and {iowait}s waiting on blocking IO".format(
            **time_dict
        )
    )


cwd = Path.cwd()
batch_paths = list(cwd.glob("*/qualikiz.batch"))

print("Starting output collection")
for ii, batch_file in enumerate(batch_paths):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
    batch = Batch.from_batch_file(batch_file)
    netcdf_file = batch_file.parent / (batch.name + ".nc")
    lock_file = batch_file.parent / 'LOCK'
    if lock_file.exists():
        print(
            "[{:s}] {:40s} {:4d}/{:4d}: Is locked, skipping!".format(
                st, batch.name, ii + 1, len(batch_paths)
            )
        )
    elif not batch.is_done():
        print(
            "[{:s}] {:40s} {:4d}/{:4d}: Run is not done, skipping!".format(
                st, batch.name, ii + 1, len(batch_paths)
            )
        )
    elif netcdf_file.exists():
        print(
            "[{:s}] {:40s} {:4d}/{:4d}: netCDF already exists!".format(
                st, batch.name, ii + 1, len(batch_paths)
            )
        )
    else:
        lock_file.touch()
        print(
            "[{:s}] {:40s} {:4d}/{:4d}: Job is done and netCDF does not exists. Collecting!".format(
                st, batch.name, ii + 1, len(batch_paths)
            )
        )
        print_process_info()
        try:
            # Set some defaults to enable "expert mode"
            # e.g. multiprocess, parallel and glue rundirs on dimx
            # We'll end up generating just a single netCDF file for the batch
            batch.to_netcdf(
                mode="glue_dimx",
                clean=True,
                n_processes="max",
                overwrite_runs=False,
                overwrite_batch=False,
                verbosity=0,
            )
        except Exception as ee:
            print("Converting {:40s} failed! Got {!s}".format(batch.name, ee))
        lock_file.unlink()

    # Print memory and CPU statistics. We might have a memory leak!
    gc.collect()
    print_process_info()

print(f"Done! Walked past {len(batch_paths)} batch paths")
