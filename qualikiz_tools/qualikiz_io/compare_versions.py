import os
from collections import OrderedDict
from itertools import cycle, combinations, chain
import shutil
from datetime import date
from pathlib import Path
import logging
from typing import Optional, Mapping, Sequence

import numpy as np
import xarray as xr
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib
from IPython import embed  # pylint: disable=unused-import # noqa: F401

matplotlib.use("pdf")
import matplotlib.pyplot as plt  # pylint: disable=wrong-import-position # noqa: E402

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def is_flux(var):
    fluxes = ["ef", "pf", "vf", "df", "vt", "vr", "vc", "chie", "ven", "ver", "vec"]
    return any(var.startswith(flux) for flux in fluxes)


def is_grow(var):
    return any(var.startswith(name) for name in ["ome", "gam"])


def is_norm(var, norm):
    return var.endswith("_" + norm)


# def is_interesting(rel, rel_mean, found_in_ds, found_in_ref):
#    yes_rel_mean = rel_mean != 1
#    yes_found_in_ds = found_in_ds != 0
#    yes_found_in_ref = found_in_ref != 0
#    #yes_large_rel_mean = (rel_mean - 1).apply(abs) > 0.02
#    not_all_nan = (~rel.isnull()).any()
#    return (yes_found_in_ds | yes_found_in_ref) & not_all_nan & yes_rel_mean


def rel_difference(val, val_ref):
    rel = (val - val_ref).map(np.abs) / val_ref.map(np.abs)
    return rel


def collect_reference_batch(batchdir, target_dir, overwrite=False):
    """Collect relevant IO for saving as reference run

    Args:
      - batchdir: Path to the batch folder to collect
      - targetdir: Path to where to save the collected IO

    Kwargs:
      - overwrite: Overwrite the targetdir if it already exists
    """
    batchdir = Path(batchdir)
    target_dir = Path(target_dir)
    if not batchdir.exists():
        raise ValueError("Source batch directory {!s} does not exist!".format(batchdir))

    elif not target_dir.exists():
        os.mkdir(target_dir)

    today = date.today()
    for parameter in batchdir.glob("*/parameters.json"):
        run_source_dir = parameter.parent
        run_target_dir = target_dir / run_source_dir.name

        if not run_target_dir.is_dir():
            run_target_dir.mkdir()
        elif len(list(run_target_dir.iterdir())) == 0:
            # Folder is empty, no worries
            pass
        elif overwrite:
            for file in run_target_dir.iterdir():
                if file.is_dir():
                    shutil.rmtree(file)
                else:
                    os.remove(file)
        else:
            raise ValueError(
                "Given batchdir '{!s}' with subdir '{!s}' would overwrite non-empty '{!s}', but overwrite is {!s}.".format(
                    batchdir, run_source_dir.name, run_target_dir, overwrite
                )
            )
        collect_reference_run(run_source_dir, run_target_dir)


def collect_reference_run(
    rundir,
    target_dir,
    debug_folder="debug",
    output_folder="output",
    primitive_folder="output/primitive",
    parameters_file="parameters.json",
    overwrite=False,
):
    """Collect QuaLiKiz files needed for reference regression cases

    Collects the following files from a finished QuaLiKiz run:
        - parameters.json
        - debug/*.dat
        - output/*.dat
        - output/primitives/cftrans.dat
        - output/primitives/*modeshift.dat
        - output/primitives/*modewidth.dat
    The folder structure will be re-constructed in the target_dir
    """

    rundir = Path(rundir)
    target_dir = Path(target_dir)

    logger.info("Putting %s in %s", rundir, target_dir)

    if not rundir.exists():
        raise ValueError("Source run directory {!s} does not exist!".format(rundir))

    # Check if the last output that is always generate by QuaLiKiz exists
    last_output = rundir / output_folder / "vfi_GB.dat"
    if not last_output.is_file():
        logger.warning("%s not found! Did the run finish?", last_output)

    if not target_dir.is_dir():
        target_dir.mkdir()
    elif len(list(target_dir.iterdir())) == 0:
        # Folder is empty, no worries
        pass
    elif overwrite:
        for file in target_dir.iterdir():
            if file.is_dir():
                shutil.rmtree(file)
            else:
                os.remove(file)
    else:
        raise ValueError(
            "Given rundir '{!s}' would overwrite non-empty '{!s}', but overwrite is {!s}.".format(
                rundir, target_dir, overwrite
            )
        )

    # Find paths to all the dirs we need
    source_debug_dir = rundir / debug_folder
    source_output_dir = rundir / output_folder
    source_primitive_dir = rundir / primitive_folder
    source_parameters_path = rundir / parameters_file
    target_debug_dir = target_dir / debug_folder
    target_output_dir = target_dir / output_folder
    target_primitive_dir = target_dir / primitive_folder
    target_parameters_path = target_dir / parameters_file

    # Recreate the QuaLiKiz directory structure
    target_debug_dir.mkdir()
    target_output_dir.mkdir()
    target_primitive_dir.mkdir()

    # Copy all debug files; contains the input
    for file in source_debug_dir.glob("*.dat"):
        shutil.copy(file, target_debug_dir)

    # Copy all regular output. Useful for debugging
    for file in source_output_dir.glob("*.dat"):
        shutil.copy(file, target_output_dir)

    # Save a few primitives for debugging
    for file in chain(
        source_primitive_dir.glob("*modeshift.dat"),
        source_primitive_dir.glob("*modewidth.dat"),
    ):
        shutil.copy(file, target_primitive_dir)

    # Copy the used parameter file for reference
    shutil.copy(source_parameters_path, target_parameters_path)

    # Copy compiler info
    shutil.copy(source_debug_dir / "compile_info.csv", target_debug_dir)


# def dimx_is_okay(okay_all, x):
#    return all([bool(var.values) for var in okay_all.sel(dimx=x).data_vars.values()])

styles = [
    {"linestyle": ":", "marker": "v"},
    {"linestyle": "--", "marker": "^"},
    {"linestyle": "-.", "marker": "<"},
]


def plot_growth_vars(dss, ref_version, x, var):
    ds_ref = dss[ref_version]
    fig = plt.figure()
    ax = plt.subplot(111)

    def labelmaker(version, nsol):
        ds_label = "numsol={:d}, {!s}".format(int(nsol), version)
        return ds_label

    for (version, ds), style in zip(dss.items(), cycle(styles)):
        for nsol in ds_ref.numsols:
            ds_label = labelmaker(version, nsol)
            subds = ds[var].where(ds[var] != 0).sel(dimx=x, numsols=nsol)
            subds.plot.line(ax=ax, x="dimn", label=ds_label, **style)
    ax.relim()
    ax.autoscale()
    ax.legend()
    ax.set_title(
        "dimx = {:d} \n kthetarhos = {!s}".format(int(x), ds.kthetarhos.values)
    )
    return fig


def plot_flux_vars(dss, ref_version, varname):
    ds_ref = dss[ref_version]
    fig = plt.figure()
    ax = plt.subplot(111)

    def labelmaker(version, nion):
        if nion >= 0:
            ds_label = "nion={:d}, {!s}".format(int(nion), version)
        else:
            ds_label = "{!s}".format(version)
        return ds_label

    if "nions" in ds_ref[varname].dims:
        spe = ds_ref.nions
    else:
        spe = [-1]
    for (version, ds), style in zip(dss.items(), cycle(styles)):
        for nion in spe:
            if nion >= 0:
                var = ds[varname].sel(nions=nion)
            else:
                var = ds[varname]
            ds_label = labelmaker(version, nion)
            var.plot(ax=ax, label=ds_label, **style)
    ax.relim()
    ax.autoscale()
    ax.legend()
    return fig


def load_dss(case, plot_versions):
    dss = OrderedDict()
    for version in plot_versions:
        ds = xr.open_dataset(os.path.join(version, case + ".nc"))
        dss[version] = ds
    return dss


_normalization_values = ["GB", "SI", "both"]


def process_case(
    case: str,
    dss: Mapping[str, xr.Dataset],
    ref_version: str,
    normalization: Optional[str] = "both",
    equiv_rel_diff_bound: Optional[float] = 1e-2,
    save_pdf: Optional[bool] = False,
    pdf_dir: Optional[Path] = None,
    show_plot: Optional[bool] = False,
    on_different_vars: Optional[str] = "warn",
):
    """Determines if datasets are (binary/numerically) equivalent

    Args:
        case:                 Name of the current case that is compared
        dss:                  (Ordered) dictionary the datasets to compare. The keys
                              are used as names
        ref_version:          Version defined as the reference. Must be in dss

    Keyword Args:
        normalization:        Normalization in which to do comparisions
        equiv_rel_diff_bound: rel_difference for which two sets are numerically equivalent
        save_pdf:             Save non-equivalence plots to pdf
        pdf_dir:              Directory to save PDF report in.
        show_plot:            Interatively show non-equivalence plots
        on_different_vars:    What to do when variables in dataset differ from those in
                              the reference. Can be 'warn' or 'raise'

    """
    if pdf_dir is None:
        pdf_dir = Path.cwd()
    else:
        pdf_dir = Path(pdf_dir)

    if not isinstance(dss, Mapping):
        raise TypeError(f"Arg dss is {type(dss)}, should be a mapping")

    if normalization not in _normalization_values:
        raise ValueError(f"Kwarg normalization should be in {_normalization_values}")
    else:
        if normalization == "SI":
            drop_norm = "GB"
        if normalization == "GB":
            drop_norm = "SI"
        else:
            drop_norm = ""

    # Drop variables not normalized with given normalization
    for ds_name in dss:
        drop_vars = (var for var in dss[ds_name].data_vars if is_norm(var, drop_norm))
        dss[ds_name] = dss[ds_name].drop_vars(drop_vars)

    if ref_version in dss:
        ds_ref = dss[ref_version]
    else:
        raise ValueError(
            "Reference version {!s} not in dss with keys {!s}".format(
                ref_version, dss.keys()
            )
        )

    if not isinstance(ds_ref, xr.Dataset):
        raise TypeError(f"Dss contains instances of {type(ds_ref)}, not xr.Dataset")

    var_not_in_ds = OrderedDict()
    extra_var_in_ds = OrderedDict()

    def diff_msg(msg):
        if on_different_vars == "raise":
            raise ValueError(msg)
        elif on_different_vars == "warn":
            logger.warning(msg)
        else:
            ValueError("Unknown on_different_vars={on_different_vars}")

    # Check if the compared runs have all variables in the reference run
    # If not, print a diff_msg
    for version, ds in dss.items():
        var_not_in_ds[version] = set(ds_ref.data_vars) - set(ds.data_vars)
        extra_var_in_ds[version] = set(ds.data_vars) - set(ds_ref.data_vars)
        if not len(extra_var_in_ds[version]) == 0:
            logger.warning(
                "Variables %s are in %s but not in reference",
                extra_var_in_ds[version],
                version,
            )
        if not len(var_not_in_ds[version]) == 0:
            diff_msg(
                "Variables {!s} are in reference but not in {!s}".format(
                    var_not_in_ds[version], version
                )
            )

    # Drop all variables from reference that are not in ds
    for version, not_in_ds in var_not_in_ds.items():
        if len(not_in_ds) != 0:
            logger.warning("Dropping %r from reference", not_in_ds)
            dss[ref_version] = ds_ref = dss[ref_version].drop_vars(not_in_ds)

    # Drop variables from ds that are not in ref
    for version, extra_in_ds in extra_var_in_ds.items():
        if len(extra_in_ds) != 0:
            logger.warning("Dropping %s from version %r", extra_in_ds, version)
            dss[version] = dss[version].drop_vars(extra_in_ds)

    # Append implicit defaults if missing from ds
    for version, extra_in_ds in extra_var_in_ds.items():
        if "rhomax" not in dss[version]:
            dss[version].coords["rhomax"] = 1.0
        if "rhomin" not in dss[version]:
            dss[version].coords["rhomin"] = 0.0

    # Check if the Dataset are `xr.equal`, in other words, binary equivalent
    bin_equiv = OrderedDict()
    for v0, v1 in combinations(dss.keys(), 2):
        ds0 = dss[v0]
        ds1 = dss[v1]
        # Compare variables that should not change physics
        var = "simple_mpi_only"
        if ds0[var] != ds1[var]:
            logger.warning(
                "var '%s' in '%s' is %f, and %f in '%s'. This can result in"
                "slightly different results depending on CPU and compiler!",
                var,
                v0,
                ds0[var],
                ds1[var],
                v1,
            )
            ds0 = ds0.drop_vars(var)
            ds1 = ds1.drop_vars(var)
        different_input = not ds0.coords.to_dataset().equals(ds1.coords.to_dataset())
        if different_input:
            logger.warning(
                "Coordinates/inputs unequal of version %s do not agree with version %s"
                "! Will result in non-equivalence!",
                v0,
                v1,
            )
        bin_equiv[(v0, v1)] = ds0.equals(ds1)

        dss[v0] = ds0
        dss[v1] = ds1
    ds_ref = dss[ref_version]

    if all(bin_equiv.values()):
        print("All versions for case {!s} are binary equivalent!".format(case))
        # If a version is binary equivalent, it is also numerically equivalent
        num_equiv = OrderedDict()
        for version in dss.keys():
            if version != ref_version:
                num_equiv[(ref_version, version)] = True
        return bin_equiv, num_equiv

    print("------")
    print("{!s} is not binary equivalent!! Generating report".format(case))
    print("------")
    for versions, is_equiv in bin_equiv.items():
        if is_equiv:
            print("{!s} is binary equivalent with {!s}".format(*versions))
        else:
            print("{!s} is not binary equivalent with {!s}".format(*versions))

    case_stats = OrderedDict()
    for version, ds in dss.items():
        # Do not compare reference with itself
        if ds is ds_ref:
            continue

        # Calculate absolute and relative error the matrixy way
        ds_stats = case_stats[version] = pd.DataFrame()
        is_close = xr.apply_ufunc(np.isclose, ds, ds_ref)
        all_dims_is_close = is_close.all(
            dim=["dimn", "numsols", "nions", "ecoefs", "numicoefs", "ntheta"]
        )
        # This fails if there are non-flux vars in the datasets!
        all_vars_is_close = np.array(
            [var.values for var in all_dims_is_close.data_vars.values()]
        )
        rel = rel_difference(ds, ds_ref).where((ds_ref != 0) & (ds != 0))
        abs_diff = (ds - ds_ref).map(np.abs)
        # okay = xr.where(rel > 1e-2, False, True);
        # okay_all = okay.all(dim=['dimn', 'numsols', 'nions']);
        # dimx_okay = [dimx_is_okay(okay_all, x) for x in okay_all.dimx]

        found_in_ds = ((ds_ref == 0) & (ds != 0)).sum(dim=["dimn", "numsols", "nions"])
        found_in_ref = ((ds_ref != 0) & (ds == 0)).sum(dim=["dimn", "numsols", "nions"])
        diff_in_ds_ref = (found_in_ds > 0) | (found_in_ref > 0)
        for var in ds_ref.data_vars:
            logger.debug("Comparing version %s variable %s", version, var)
            if ds[var].isnull().any():
                logger.warning("Variable %s in compared dataset has NaNs!", var)
            if ds_ref[var].isnull().any():
                logger.warning("Variable %s in reference dataset has NaNs!", var)
            if (rel[var] > equiv_rel_diff_bound).any() or (diff_in_ds_ref[var]).any():
                logger.debug(
                    "Relative difference is %s, outside relative difference bound of %s",
                    rel[var].values,
                    equiv_rel_diff_bound,
                )

                # Save maximum absolute and relative error over all dimensions
                ds_stats.at[var] = None
                ds_stats.at[var, "max_rel_diff"] = rel[var].max().values
                ds_stats.at[var, "max_abs_diff"] = abs_diff[var].max().values

                # Indexes in dimx of values in one dataset but not in the other
                diff_in_ds_ref_xs = ds[var].dimx[diff_in_ds_ref[var]].values
                in_ds_not_in_ref = found_in_ds[var].sel(dimx=diff_in_ds_ref_xs).values
                in_ref_not_in_ds = found_in_ref[var].sel(dimx=diff_in_ds_ref_xs).values
                found_in_ds.sel(dimx=diff_in_ds_ref_xs).values()
                ds_stats.at[var, "diff_in_ds_ref_xs"] = object()
                ds_stats.at[var, "diff_in_ds_ref_xs"] = diff_in_ds_ref_xs
                ds_stats.at[var, "in_ds_not_in_ref"] = object()
                ds_stats.at[var, "in_ds_not_in_ref"] = in_ds_not_in_ref
                ds_stats.at[var, "in_ref_not_in_ds"] = object()
                ds_stats.at[var, "in_ref_not_in_ds"] = in_ref_not_in_ds

    if save_pdf:
        sane_filename = case.replace(".", "_").replace("/", "_")
        pdf_path = pdf_dir / f"{sane_filename}.pdf"
        print("Saving PDF report in {!s}".format(pdf_path))
        pdf_doc = PdfPages(pdf_path)

    merged_case_stats = pd.concat(case_stats, sort=False).unstack(0)
    if save_pdf or show_plot:
        for var in merged_case_stats.index:
            if ds[var].dims == ("dimx",) or ds[var].dims == ("dimx", "nions"):
                fig = plot_flux_vars(dss, ref_version, var)
                if save_pdf:
                    pdf_doc.savefig(fig)
                if show_plot:
                    plt.show()
                plt.close(fig)
            elif ds[var].dims == ("dimx", "dimn", "numsols"):
                look_at_x = set()
                for interesting in merged_case_stats.loc[
                    var, "diff_in_ds_ref_xs"
                ].values:
                    look_at_x = look_at_x.union(set(interesting))
                for xx in sorted(list(look_at_x)):
                    fig = plot_growth_vars(dss, ref_version, xx, var)
                    if save_pdf:
                        pdf_doc.savefig(fig)
                    if show_plot:
                        plt.show()
                    plt.close(fig)
            else:
                if save_pdf:
                    pdf_doc.close()
                raise Exception(var, "unexpected! Has dims", ds[var].dims)

    # Check numeric equivalence
    num_equiv = OrderedDict()
    for version in dss.keys():
        if version != ref_version:
            if (
                version not in merged_case_stats.columns.get_level_values(1)
                or merged_case_stats.loc[:, (slice(None), version)].empty
            ):
                print(
                    "{!s} is numerically equivalent to {!s}".format(
                        ref_version, version
                    )
                )
                num_equiv[(ref_version, version)] = True
            else:
                print(
                    "{!s} is not numerically equivalent to {!s}".format(
                        ref_version, version
                    )
                )
                num_equiv[(ref_version, version)] = False
        else:
            num_equiv[(ref_version, version)] = True

    # If not all numerically equivalent, print a report
    if not all(num_equiv.values()):
        merged_case_stats.drop("diff_in_ds_ref_xs", axis="columns", inplace=True)
        # pd.set_option('display.max_colwidth', -1)
        # pd.set_option('display.width', -1)
        pd.set_option("display.expand_frame_repr", False)
        # pd.set_option('display.max_columns', None)
        print()
        print(merged_case_stats)
    # If it is numerically equivalent and you reach this, some vars are not binary equivalent
    # Non-general? loopy-loopy printing of these variables
    else:
        print()
        for version, ds in dss.items():
            if version != ref_version:
                non_bin = []
                for varname in ds_ref.data_vars:
                    if not ds_ref[varname].equals(ds[varname]):
                        non_bin.append(varname)
                if len(non_bin) > 0:
                    print(
                        "{!s} has non-binary equivalent variables {!s}".format(
                            version, non_bin
                        )
                    )
                else:
                    print(
                        "Strange, all data_vars of {!s} are equal, what about coordinates?".format(
                            version
                        )
                    )
                    for coordname in ds_ref.coords:
                        if not ds_ref[coordname].equals(ds[coordname]):
                            print(
                                "Coord {!s} of {!s} is not equal, that should not happen!".format(
                                    coordname, version
                                )
                            )

    print()
    if save_pdf:
        pdf_doc.close()

    return bin_equiv, num_equiv


if __name__ == "__main__":
    collect_reference_run(
        "../../runs/regression_07125f96/GASTD_base_case_withcol", "test", overwrite=True
    )
