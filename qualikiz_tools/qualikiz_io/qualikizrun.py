import os
import warnings
import shutil
import multiprocessing as mp
import logging
from functools import partial
import json
import copy
from pathlib import Path
import errno
from typing import Optional

import numpy as np
import xarray as xr
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.inputfiles import (
    QuaLiKizPlan,
    json_dump_kwargs,
    QuaLiKizQLFluxScan,
)
from qualikiz_tools.qualikiz_io.outputfiles import (
    merge_many_lazy_snakes,
    merge_many_orthogonal,
    add_dims,
    outputdir,
    output_qlfluxdir,
    debug_qlfluxdir,
    primitivedir,
    primitive_qlfluxdir,
    debugdir,
    inputdir,
    python_debugdir,
    qualikiz_folder_to_xarray,
)

from qualikiz_tools import __file__ as root_init
from qualikiz_tools import netcdf4_engine, HAS_NETCDF4

root = Path(root_init).parent.resolve()
logger = logging.getLogger(__name__)

SPECIAL_FROM_DIR_UNDEFINED_MSG = "Specialized from_dir method not defined"
SPECIAL_FROM_FILE_UNDEFINED_STARTMSG = "No from_file function defined for "

threads_per_task = (
    1  # QuaLiKiz uses a single thread per MPI task, EXCEPT on the master task (rank=0)
)

warnings.simplefilter("always", UserWarning)


class FromSubdirFilter(logging.Filter):
    """Logging filter to ignore no specific from_dir available messages"""

    def filter(self, record):
        if record.msg == SPECIAL_FROM_DIR_UNDEFINED_MSG:
            return False
        elif record.msg.startswith(SPECIAL_FROM_FILE_UNDEFINED_STARTMSG):
            return False
        return True


class QuaLiKizRun:
    """Defines everything needed for a single run of QuaLiKiz

    Attributes:
        parameterspath: Default path where the parameters json is
        outputdir:      Relative path to the output folder
        primitivedir:   Relative path to the primitive output folder
        debugdir:       Relative path to the debug folder
        inputdir:       Relative path to the input folder
        default_stdout: Default name to write STDOUT to
        default_stderr: Default name to write STDERR to
    """

    parameterspath = "parameters.json"
    metadatapath = "metadata.json"
    qlfluxpath = "impurities_qlflux.json"
    # Save these paths from outputfiles in the class for backwards compatibility
    outputdir = outputdir
    output_qlfluxdir = output_qlfluxdir
    debug_qlfluxdir = debug_qlfluxdir
    primitivedir = primitivedir
    primitive_qlfluxdir = primitive_qlfluxdir
    debugdir = debugdir
    inputdir = inputdir
    python_debugdir = python_debugdir

    default_stderr = "stderr.run"
    default_stdout = "stdout.run"

    def __init__(
        self,
        parent_dir,
        name,
        binaryrelpath,
        qualikiz_plan=None,
        impurities_qlflux=None,
        stdout=None,
        stderr=None,
        verbose=False,
        metadata=None,
    ):
        """Initialize an empty QuaLiKiz run
        Args:
            parent_dir:    Parent of where the run folder will be.
            name:          The name of the QuaLiKiz Run. This will be the
                           name of the folder that will be generated
            binaryrelpath: The name of the binary that needs to be run.
                           Usually a path relative to the run folder.

        Kwargs:
            qualikiz_plan: The QuaLiKizPlan, usually read from json. Will
                           load the parameter_template by default
            impurities_qlflux: A dictionairy containing impurities to be scanned
                over using QuaLiKiz in QL mode, this translates to "typei == 0"
            stdout:        Path to the STDOUT file.
            stderr:        Path to the STDERR file.
            verbose:       Print verbose information when initializing.
        """
        self.rundir = Path(parent_dir) / name

        if verbose:
            print("Creating new QuaLiKiz run in {!s}".format(self.rundir))

        if stdout is None:
            self.stdout = QuaLiKizRun.default_stdout
        else:
            self.stdout = stdout

        if stderr is None:
            self.stderr = QuaLiKizRun.default_stderr
        else:
            self.stderr = stderr

        self.binaryrelpath = Path(binaryrelpath) if binaryrelpath is not None else None

        # Load the default parameters if no plan is defined
        if qualikiz_plan is None:
            logger.warning("No QuaLiKiz Plan given, using default parameters")
            templatepath = root / "qualikiz_io/parameters_template.json"
            if templatepath.is_file():
                qualikiz_plan = QuaLiKizPlan.from_json(templatepath)
            else:
                raise FileNotFoundError(
                    errno.ENOENT, os.strerror(errno.ENOENT), str(templatepath)
                )
        else:
            qualikiz_plan = copy.deepcopy(qualikiz_plan)
        self.qualikiz_plan = qualikiz_plan

        # Deepcopy given "qlflux" impurities
        if impurities_qlflux is None:
            self.impurities_qlflux = {}
        else:
            self.impurities_qlflux = copy.deepcopy(impurities_qlflux)

        if metadata is None:
            metadata = {}
        self.metadata = metadata

    def prepare(self, overwrite=None, overwrite_meta=None, overwrite_imp=None):
        """Write all Run folders to file
        This will generate a folders for each run. Note that this will not
        generate the input files, just the skeleton needed. For large runs
        the input generation can take a while. Generate input binaries using
        the generate_input function.

        Kwargs:
            overwrite:  Overwrite the directory if it exists. Prompts the
                          user by default.
        """

        rundir = self.rundir

        create_folder_prompt(rundir, overwrite=overwrite)

        self._create_output_folders(rundir)
        (rundir / self.inputdir).mkdir(exist_ok=True)
        # Check if the binary we are trying to link to exists
        if True:
            #absbinpath = rundir.joinpath(f"{self.binaryrelpath}").resolve()
            absbinpath = rundir / self.binaryrelpath
        else:
            absbinpath = rundir / self.binaryrelpath
        if not absbinpath.exists():
            logger.warning(
                "Warning! Binary at "
                + str(absbinpath.resolve())
                + " does not "
                + "exist! Run will fail!"
            )
        # Create link to binary
        binarybasepath = self.binaryrelpath.name
        try:
            (rundir / binarybasepath).symlink_to(self.binaryrelpath)
        except FileExistsError:
            pass

        # Create a parameters file
        self.qualikiz_plan.to_json(rundir / self.parameterspath)

        # Create an type0 impurities file
        imp_path = rundir / self.qlfluxpath
        overwrite_imp = overwrite_prompt(imp_path, overwrite_imp)
        if overwrite_imp:
            with open(imp_path, "w") as ff:
                json.dump(self.impurities_qlflux, ff, **json_dump_kwargs)

        overwrite_meta = overwrite_prompt(self.meta_path, overwrite_meta)
        if overwrite_meta:
            with open(self.meta_path, "w") as ff:
                json.dump(self.metadata, ff, **json_dump_kwargs)

    def _create_output_folders(self, path):
        """Create the output folders"""
        (path / self.outputdir).mkdir(exist_ok=True, parents=True)
        (path / self.primitivedir).mkdir(exist_ok=True, parents=True)
        (path / self.debugdir).mkdir(exist_ok=True, parents=True)
        (path / self.python_debugdir).mkdir(exist_ok=True, parents=True)
        (path / self.output_qlfluxdir).mkdir(exist_ok=True, parents=True)
        (path / self.primitive_qlfluxdir).mkdir(exist_ok=True, parents=True)
        (path / self.debug_qlfluxdir).mkdir(exist_ok=True, parents=True)

    def generate_input(self, dotprint=False, conversion=None, qlflux=False):
        """Generate the input binaries for a QuaLiKiz run

        Kwargs:
            dotprint:   Print a dot after each generation. Used for debugging.
            conversion: Function will be called as conversion(input_dir). Can
                        be used to convert input files to older version.
            qlflux: Use the QuaLiKizQLFluxScan to build a
                scan with QuaLiKiz in QL mode.
        """
        path = Path(self.rundir)
        parameterspath = path / Path(self.parameterspath)
        plan = QuaLiKizPlan.from_json(parameterspath)
        if plan != self.qualikiz_plan:
            logger.warning(
                "%s changed on disk! Overwriting in-memory plan", parameterspath
            )
            self.qualikiz_plan = plan
        if not self.meta_path.is_file():
            logger.warning(
                "Legacy QuaLiKizRun detected, generating empty metadata file"
            )
            with open(self.meta_path, "w") as fp:
                json.dump(dict(), fp, indent=0)
                fp.write("\n")
        with open(self.meta_path, "r") as ff:
            meta = json.load(ff)

        if meta != self.metadata:
            logger.warning(
                "%s changed on disk! Overwriting in-memory metadata", self.meta_path
            )
            self.metadata = meta

        if qlflux and self.impurities_qlflux:
            # We modify temporarily the QuaLiKiz base to have QLFlux scan ions
            # We work on a copy (as we've loaded from disk) so no need to
            # Save "the old" plan, it is unchanged on disk
            idx = self.impurities_qlflux["ion_index"]
            ion_template = plan["xpoint_base"]["ions"][idx]
            new_ions = []
            for Ai, Zi in zip(
                self.impurities_qlflux["Ai"], self.impurities_qlflux["Zi"]
            ):
                ion = copy.deepcopy(ion_template)
                ion["A"] = Ai
                ion["Z"] = Zi
                ion["type"] = 0
                ion["n"] = 1e-5
                new_ions.append(ion)

            plan["xpoint_base"]["ions"].extend(new_ions)
            del new_ions

            # We set our QN explicitly above. Just to be sure, put these to False
            plan["xpoint_base"]["options"]["check_qn"] = False
            plan["xpoint_base"]["options"]["set_qn_normni"] = False
            plan["xpoint_base"]["options"]["set_qn_An"] = False

            # Create qlflux output directories
            (path / self.output_qlfluxdir).mkdir(exist_ok=True, parents=True)
            (path / self.primitive_qlfluxdir).mkdir(exist_ok=True, parents=True)

        elif qlflux:
            raise RuntimeError(
                f"qlflux is {qlflux}, but impurities_qlflux of run {repr(self)} is {self.impurities_qlflux}"
            )
        input_binaries, meta_unfolded = plan.setup(metadata=meta)
        inputdir = Path(self.rundir) / Path(self.inputdir)

        if dotprint:
            print(".", end="", flush=True)
        inputdir.mkdir(exist_ok=True)
        for name, value in input_binaries.items():
            filepath = inputdir / (name + ".bin")
            with open(filepath, "wb") as file_:
                pretty_value = str(value[:3])
                if len(value) > 4:
                    pretty_value = pretty_value.replace("])", ", ...])")
                if filepath.is_absolute():
                    relpath = filepath.relative_to(self.rundir)
                else:
                    relpath = filepath
                logger.debug("Writing %s to %s", pretty_value, relpath)
                value.tofile(file_)

        python_debugdir = self.rundir / self.python_debugdir
        python_debugdir.mkdir(exist_ok=True)
        for scan_name, scan_metas in meta_unfolded.items():
            for meta_name, meta_vals in scan_metas.items():
                name = "meta_{!s}_{!s}".format(scan_name, meta_name)
                with (python_debugdir / (name + ".dat")).open("w") as file_:
                    np.array(meta_vals).tofile(file_, sep="\n")

        if conversion is not None:
            conversion(inputdir)

    def inputbinaries_exist(self, suppress_warning=False):
        """Check if the input binaries exist
        Currently only checks for Ro.bin. Change this if the QuaLiKiz
        input files ever change!

        Returns:
            True if the input binaries exist
        """
        input_binary = self.rundir / self.inputdir / "Ro.bin"
        exist = True
        if not input_binary.exists():
            if not suppress_warning:
                logger.warning(
                    "Warning! Input binary at %s does not "
                    "exist! Run will fail! Please generate input binaries!",
                    self.rundir,
                )
            exist = False
        return exist

    def estimate_walltime(self, cores):
        """Estimate the walltime needed to run
        This directely depends on the CPU time needed and cores needed to run.
        Currently uses worst-case estimate.

        Args:
            cores: The amount of physical cores to use

        Returns:
            Estimated walltime in seconds
        """
        cputime = self.estimate_cputime(cores)
        return cputime / cores

    def estimate_cputime(self, cores):
        """Estimate the cpu time needed to run
        Currently just uses a worst-case assumtion. In reality cpus_per_dimxn
        should depend on the dimxn per core. It also depends on the amount of
        stable points in the run, which is not known a-priori.

        Args:
            cores: The amount of physical cores to use

        Returns:
            Estimated cputime in seconds
        """
        dimxn = self.qualikiz_plan.calculate_dimxn()
        rot_on = self.qualikiz_plan["xpoint_base"]["rot_flag"]
        cpus_per_dimxn = 0.8 * (1 + rot_on * 4)
        return dimxn * cpus_per_dimxn

    def calculate_tasks(self, cores, HT=False, threads_per_core=2):
        """Calulate the amount of MPI tasks needed based on the cores used

        Args:
            cores: The amount of physical cores to use

        Kwargs:
            HT: Flag to use HyperThreading. By default False.
            threads_per_core: Amount of threads per core when hyperthreading.
                              Usually 2, but can be 3 or 4 for KNL nodes

        Returns:
            Tasks to use to run this QuaLiKizRun
        """
        if not HT:
            threads_per_core = 1

        threads = cores * threads_per_core
        tasks, remainder = divmod(threads, threads_per_task)
        tasks = int(tasks)
        if remainder != 0:
            logger.warning(
                "%d cores using %d threads per core not evenly divisible"
                "over %d threads per tasks. Using %d tasks",
                cores,
                threads_per_core,
                threads_per_task,
                tasks,
            )
        return tasks

    @classmethod
    def from_dir(
        cls,
        dir,
        binaryrelpath=None,
        stdout=default_stdout,
        stderr=default_stderr,
        verbose=False,
    ):
        """Reconstruct Run from directory
        Try to reconstruct the Run from a directory. Gives a warning if
        STDOUT and STDERR cannot be found on their given or default location.

        Args:
            dir: Root directory of the Run

        Kwargs:
            binarylinkpath: Path to the link pointing to the QuaLiKiz binary
            stdout:         Where to look for the STDOUT file
            stderr:         Where to look for the STDERR file

        Returns:
            Reconstructed QuaLiKizRun
        """
        rundir = Path(dir)
        name = rundir.name
        parent_dir = rundir.parent.absolute()

        planpath = rundir / cls.parameterspath
        qualikiz_plan = QuaLiKizPlan.from_json(planpath)
        # We assume the binary is named something with 'QuaLiKiz' in it
        if binaryrelpath is None:
            for file in rundir.glob("*QuaLiKiz*"):
                try:
                    binaryrelpath = os.readlink(str((rundir / file)))
                except OSError:
                    logger.warning("Failed to read %s", str((rundir / file)))
                else:
                    break
        if binaryrelpath is None:
            logger.warning(
                "Could not find link to QuaLiKiz binary. Please supply 'binaryrelpath'"
            )

        meta_path = rundir / cls.metadatapath
        if meta_path.is_file():
            with open(meta_path) as f_:
                meta = json.load(f_)
        else:
            logger.warning("Could not find metadata JSON file, assuming no metadata")
            meta = {}

        ql_path = rundir / cls.qlfluxpath
        if ql_path.is_file():
            qlflux = QuaLiKizQLFluxScan.from_json(ql_path)
        else:
            logger.warning(
                "Could not find impurities_qlflux JSON file, assuming no QL scan"
            )
            qlflux = QuaLiKizQLFluxScan()
        # binarybasepath = os.path.basename(binaryrelpath)
        # binaryrelpath = os.readlink(os.path.join(rundir, binarybasepath))
        return QuaLiKizRun(
            parent_dir,
            name,
            binaryrelpath,
            qualikiz_plan=qualikiz_plan,
            impurities_qlflux=qlflux,
            stdout=stdout,
            stderr=stderr,
            metadata=meta,
        )

    def relink_binary(self, new_path):
        new_path = Path(new_path)
        try:
            relpath = new_path.relative_to(self.rundir)
        except ValueError:
            relpath = (
                "../"
                * len(Path(self.rundir).parents)
                / new_path.relative_to(Path(new_path.root))
            )

        self.binaryrelpath = relpath
        bin_link = self.rundir / "QuaLiKiz"
        bin_link.unlink()
        if not new_path.is_file():
            logger.warning("Given path '%s' does not exist", new_path)
        Path(self.rundir / "QuaLiKiz").symlink_to(new_path)

    def clean(self):
        """Cleans run folder to state before it was run"""
        runcounter = self.rundir / "runcounter.dat"
        if runcounter.is_file():
            counter = np.loadtxt(runcounter)
            if counter >= 1:
                logger.warning("Cleaning directory with runcounter >= 1")
            runcounter.unlink()

        suffix = ".dat"
        for dir in [self.outputdir, self.primitivedir, self.debugdir]:
            self._clean_suffix(self.rundir / dir, suffix)

        for path in [self.stdout, self.stderr]:
            if (self.rundir / path).exists():
                (self.rundir / path).unlink()

    @classmethod
    def _clean_suffix(cls, dir, suffix):
        """Removes all files with suffix in dir"""
        for file in dir.iterdir():
            if str(file).endswith(suffix) and (dir / file).exists():
                (dir / file).unlink()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            attrs = self.__dict__.copy()
            other_attrs = other.__dict__.copy()
            equal = True
            for name in ["binaryrelpath"]:
                self_path = attrs.pop(name)
                other_path = other_attrs.pop(name)
                equal &= str(self_path) == str(other_path)
            for name in ["rundir"]:
                self_path = attrs.pop(name)
                other_path = other_attrs.pop(name)
                equal &= str(self_path.resolve()) == str(other_path.resolve())
            for name in ["stderr", "stdout"]:
                self_path = self.rundir / attrs.pop(name)
                other_path = self.rundir / other_attrs.pop(name)
                equal &= str(self_path.resolve()) == str(other_path.resolve())
            return attrs == other_attrs and equal
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        return NotImplemented

    def to_netcdf(self, **kwargs):
        """Convert the output and debug to netCDF"""
        return run_to_netcdf(self.rundir, **kwargs)

    def is_done(self):
        last_output = self.rundir / "output/vfi_GB.dat"
        return last_output.is_file()

    @property
    def netcdf_path(self):
        """Path to the generated netCDF file of this Run"""
        return self.rundir / (self.rundir.name + ".nc")

    @property
    def meta_path(self):
        return self.rundir / self.metadatapath

    def __str__(self):
        mod = str(self.__class__.__module__)
        name = str(self.__class__.__name__)
        return "{!s}.{!s}({!s})".format(
            mod.replace("qualikiz_tools.", ""),
            name,
            self.rundir,
        )


class QuaLiKizBatch:
    """A collection of QuaLiKiz Runs

    This class is used to define a collection of QuaLiKiz Runs. This is more
    or less equivalent with a batch script, but with path awareness and some
    extra smarts built-in.

    Attributes:
        batchinfofile:  The default name of batchinfo file. Used to store
                        batch metadata
        scriptname:     The default name of the sbatch scipt file.
        default_stdout: Default name to write STDOUT to
        default_stderr: Default name to write STDERR to
        run_class:      The class of the underlying QuaLiKiz run
        parent_dir:     Directory this Batch's will live in
        name:           Name of the Batch. Will be used the name the folder
        runlist:        QuaLiKiz Runs (of any type) contained in this Batch
    """

    batchinfofile = "batchinfo.json"
    scriptname = "qualikiz.batch"

    default_stderr = "stderr.batch"
    default_stdout = "stdout.batch"

    run_class = QuaLiKizRun

    def __init__(self, parent_dir, name, runlist, stdout=None, stderr=None):
        """Initialize a batch
        Args:
            parent_dir: Parent directory of the batch directory.
            name:       Name of the batch. This will also be the folder name
            runlist:    A list of QuaLiKizRuns contained in this batch

        Kwargs:
            stdout:     File to write stdout to. By default 'stdout.batch'
            stderr:     File to write stderr to. By default 'stderr.batch'
        """
        self.parent_dir = Path(parent_dir)
        self.name = name
        self.runlist = runlist

        if stdout is None:
            self.stdout = QuaLiKizBatch.default_stdout
        else:
            self.stdout = stdout

        if stderr is None:
            self.stderr = QuaLiKizBatch.default_stderr
        else:
            self.stderr = stderr

    def prepare(
        self,
        overwrite_batch=None,
        overwrite_runs=False,
        overwrite_batch_script=False,
        overwrite_meta=None,
    ):
        """Prepare the batch and runs to be submitted
        This function writes necessary files and folders for the batch
        to run correctly. Note that this does not generate the input files,
        as that might take a while. You can generate those with the
        generate_input function.

        Keyword arguments:
            overwrite_batch: Flag to overwrite the batch folder if it
                               already exists. Prompts the user by default.
            overwrite_runs:  Flag to overwrite the runs folders if they
                               already exist. False by default.
        """
        batchdir = self.parent_dir / self.name
        batchpath = batchdir / self.scriptname
        create_folder_prompt(batchdir, overwrite=overwrite_batch)
        if overwrite_batch_script and batchpath.exists():
            batchpath.unlink()
        if not batchpath.is_file():
            if hasattr(self, "to_batch_file"):
                self.to_batch_file(
                    batchpath, overwrite_batch_script=overwrite_batch_script
                )
            else:
                logger.warning(
                    "No to_batch_file function defined for %s, creating empty script.",
                    self,
                )
                with open(batchdir / self.scriptname, "w") as file_:
                    file_.write("")
        # Create link to python scripts
        for run in self.runlist:
            run.prepare(overwrite=overwrite_runs, overwrite_meta=overwrite_meta)

    def generate_input(
        self, dotprint=False, processes=1, conversion=None, qlflux=False
    ):
        """Generate the input files for all runs

        Keyword arguments:
            dotprint:   Print a dot after each generation. Used for debugging.
            processes:  Amount of processes used to generate. Defaults to 1.
                        Set this to 'max' to autodetect.
            conversion: Function will be called as conversion(input_dir). Can
                        be used to convert input files to older version.
            qlflux: See QuaLiKizRun.generate_input
        """
        if processes == 1:
            for run in self.runlist:
                run.generate_input(
                    dotprint=dotprint, conversion=conversion, qlflux=qlflux
                )
        else:
            if processes == "max":
                tasks = min((mp.cpu_count(), len(self.runlist)))
            else:
                tasks = processes

            pool = mp.Pool(processes=tasks)
            pool.map(
                partial(
                    QuaLiKizRun.generate_input,
                    dotprint=dotprint,
                    conversion=conversion,
                    qlflux=qlflux,
                ),
                self.runlist,
            )
        if dotprint:
            print()

    def inputbinaries_exist(self, **kwargs):
        return all([run.inputbinaries_exist(**kwargs) for run in self.runlist])

    @classmethod
    def from_dir_recursive(cls, searchdir):
        """Reconstruct batch from directory tree
        Walks from the given path until it finds a file named
        QuaLiKizBatch.scriptname, and tries to reconstruct the
        batch from there.

        Args:
            searchdir: The path to search

        Returns:
            batchlist: A list of batches found
        """
        batchlist = []
        for dirpath, __, filenames in os.walk(searchdir):
            if QuaLiKizBatch.scriptname in filenames:
                batchlist.append(QuaLiKizBatch.from_subdirs(dirpath))
        return batchlist

    @classmethod
    def list_from_dir(
        cls, dir, scriptname=None, verbose=False, run_kwargs=None, batch_kwargs=None
    ):
        if batch_kwargs is None:
            batch_kwargs = {}
        if run_kwargs is None:
            run_kwargs = {}
        if scriptname is None:
            scriptname = cls.scriptname

        batchlist = []
        for subpath in dir.iterdir():
            if subpath.is_dir():
                batch = cls.from_dir(
                    subpath,
                    scriptname=scriptname,
                    verbose=verbose,
                    run_kwargs=run_kwargs,
                    batch_kwargs=batch_kwargs,
                )
                batchlist.append(batch)
        return batchlist

    @classmethod
    def from_dir(cls, dir, *args, **kwargs):
        logger.warning(SPECIAL_FROM_DIR_UNDEFINED_MSG)
        return cls.from_subdirs(dir, *args, **kwargs)

    @classmethod
    def from_subdirs(
        cls,
        batchdir,
        *args,
        scriptname=None,
        verbose=False,
        run_kwargs=None,
        batch_kwargs=None,
    ):
        """Reconstruct batch from a directory
        This function assumes that the name of the batch can be
        determined by the given batchdir. If the batch was created
        with the functions contained in this module, it should always
        be succesfully re-contructed.

        Args:
            batchdir:   The top directory of the batch

        Kwargs:
            scriptname: name of the script to search for. Defaults to qualikiz.batch.

        Returns:
            qualikizbatch: The reconstructed batch
        """
        if batch_kwargs is None:
            batch_kwargs = {}
        if run_kwargs is None:
            run_kwargs = {}
        if scriptname is None:
            scriptname = cls.scriptname

        batch_kwargs["verbose"] = verbose
        run_kwargs["verbose"] = verbose

        batchdir = Path(batchdir).resolve()
        # The name should be the same as the directory name given
        parent_dir = batchdir.parent
        name = batchdir.name
        # qualikizbatch = QuaLiKizBatch.__new__(cls)
        # qualikizbatch.parent_dir = os.path.abspath(parent_dir)
        # qualikizbatch.name = name
        batchscript_path = batchdir / scriptname
        try:
            batch = cls.from_batch_file(batchscript_path, **batch_kwargs)
        except (AttributeError, FileNotFoundError, NotImplementedError) as ee:
            if isinstance(ee, AttributeError):
                warn_msg = SPECIAL_FROM_FILE_UNDEFINED_STARTMSG + "{!s}".format(cls)
            elif isinstance(ee, FileNotFoundError):
                warn_msg = "No batch file found for {!s}".format(cls)
            elif isinstance(ee, NotImplementedError):
                warn_msg = "from_batch_file not implemented for {!s}".format(cls)
            logger.warning("%s, falling back to subdirs", warn_msg)
            runlist = cls.runlist_from_subdirs(batchdir, **run_kwargs)
            batch = cls(parent_dir, name, runlist)

        return batch

    @classmethod
    def runlist_from_subdirs(cls, batchdir, verbose=False, **kwargs):
        runlist = []
        # Try to find the contained runs, they are usually in one of the children
        try:
            runlist = [cls.run_class.from_dir(batchdir, **kwargs)]
        except FileNotFoundError:
            if verbose:
                print(
                    "Could not reconstruct run from '{!s}'. Maybe from its subfolders?".format(
                        batchdir
                    )
                )
            for subpath in batchdir.iterdir():
                rundir = batchdir / subpath
                if rundir.is_dir():
                    try:
                        logger.info("Trying %s", rundir)
                        run = cls.run_class.from_dir(rundir, **kwargs)
                        logger.info(
                            "Reconstructed %s from '%s'.", run.__class__, rundir
                        )
                    except OSError as ee:
                        print(ee.__class__)
                        print(ee)
                        pass
                    else:
                        runlist.append(run)
        if len(runlist) == 0:
            raise OSError("Could not reconstruct runlist from subdirs")
        return runlist

    def to_netcdf(
        self,
        mode: Optional[str] = "glue_dimx",
        clean: Optional[bool] = None,
        n_processes: Optional[int] = 1,
        verbosity: Optional[int] = 0,
        overwrite_runs: Optional[bool] = None,
        overwrite_batch: Optional[bool] = None,
        run_kwargs: Optional[dict] = None,
        gluedim: Optional[str] = None,
        loopy: Optional[bool] = False,
    ):
        """Convert QuaLiKizBatch output to netcdf

        This function converts the output contained in the output and debug
        folders to netcdf. The :py:class:`QuaLiKizRun` instances in the
        :py:attr:`QuaLiKizBatch.runlist` will be converted to netCDF using
        :py:func:`QuaLiKizRun.to_netcdf`. Optionally, the produced netCDFs can
        be glued together afterwards. By default, Runs will be glued along the
        ``dimx`` axis, and the Runs netCDF files will be removed. Influence
        this behaviour with the ``mode`` kwargs:

        * ``mode="glue_dimx"``: Glue the datasets along the ``dimx`` dimension
        * ``mode="glue_orthogonal"``: Glue arbritrairy orthogonalized datasets
          together along any non-shared dimension. Tries to autodetect how to glue
        * ``mode="glue_snake"``: Glue orthogonalized datasets together that are
          only different by a single dimension. Tries to autodetect how to glue
        * ``mode="noglue"``: Do not try to glue Runs together; leaves behind the
          Run netCDFs

        Datasets are compressed the "Karel way" by default, see :py:func:`run_to_netcdf`
        They are saved one level above the batch folder, in the
        :py:attr:`QuaLiKizBatch.parent_dir`.

        Keyword Args:
            mode:            What to do after netcdfizing runs. Set 'glue_*' to
                             glue datasets together, or "noglue" to not.
            clean:           Remove netcdf files generated by
                             :py:func:`QuaLiKizRun.to_netcdf` when done.
            n_processes:     Amount of processes used to generate. Set this to
                             ``max`` to autodetect.
            verbosity:       Verbosity of the function. Will be passed to children
            overwrite_runs:  Overwrite pre-existing Run netCDFs
            overwrite_batch: Overwrite pre-existing Batch netCDFs
            run_kwargs:      Kwargs to pass to run_to_netcdf
            gluedim:         Dimension added to Run netCDFs to act as dimension
                             to be glued over
            loopy:           Concatenate Run netCDFs in a loop instead of a single
                             operation

        Returns:
            A list of xarray :py:class:`xarray:xarray.Dataset`
        """
        if run_kwargs is None:
            run_kwargs = {}

        if clean is None:
            if mode in ["noglue"]:
                clean = False
            else:
                clean = True

        if verbosity < 0:
            logger.setLevel(logging.WARNING)
        elif verbosity < 1:
            logger.setLevel(logging.INFO)
        elif verbosity < 2:
            logger.setLevel(logging.DEBUG)

        if verbosity >= 1:
            verbose = True
        else:
            verbose = False

        # Analyse type of job
        is_gluejob = mode.startswith("glue_")
        is_multirun = len(self.runlist) > 1

        # Check if this is a run and a batch in one
        is_runbatch = False
        if not is_multirun:
            run = self.runlist[0]
            if run.rundir == self.my_dir:
                is_runbatch = True

        # First, look for existing netCDF files
        # A 'joblist' is a list of runfolders to netCDFize
        joblist = []  # jobs that still need to be netcdfized
        for run in self.runlist:
            if not overwrite_prompt(run.netcdf_path, overwrite_runs):
                logger.warning(
                    "User does not want to overwrite %s, keeping it",
                    run.netcdf_path,
                )
            else:
                joblist.append(run.rundir)
        logger.info("Found {:d} QuaLiKizRuns to be netCDFized".format(len(joblist)))

        # Try to detect amount of CPU's
        if n_processes == "max":
            n_processes = min((mp.cpu_count(), len(self.runlist)))
            logger.debug("Multiprocessing detected %s CPUs", mp.cpu_count())

        logger.debug("Using %s processes to convert %s runs", n_processes, len(joblist))
        if n_processes == 1:
            for job in joblist:
                logger.debug('netCDFizing "%s"', job)
                run_to_netcdf(job, **run_kwargs)
        else:
            # Seems this implementation leaks memory?
            # pool = mp.Pool(processes=tasks)
            # pool.map(partial(run_to_netcdf, **run_kwargs), joblist)
            # Run each job[run] in a single process
            processes = {}
            for job in joblist:
                logger.debug('netCDFizing "%s"', job)
                p = mp.Process(target=run_to_netcdf, args=(job,), kwargs=run_kwargs)
                p.start()
                processes[job] = p
                logger.debug('job "%s launched"', job)
            for job, process in processes.items():
                process.join()
            logger.debug("All parallel run convert jobs returned")

        if self.netcdf_path.exists():
            if is_runbatch:
                overwrite_okay = True
            elif not is_gluejob:
                # Nothing will be overwritten
                overwrite_okay = None
            elif not is_multirun:
                # This is not a multirun, so we just move the run onto the Batch
                overwrite_okay = True
            else:
                # Query the user if its okay to overwrite
                overwrite_okay = overwrite_dialog(self.netcdf_path, overwrite_batch)

        logger.debug("All %d runs netCDFized", len(self.runlist))
        if not is_gluejob:
            # Just return the generated runs
            logger.info("Gluing is not needed, return all generated runs")
            return [xr.open_dataset(run.netcdf_path) for run in self.runlist]

        if not is_multirun:
            # Move the single run onto the Batch if overwriting is okay
            if not self.netcdf_path.exists() or overwrite_okay:
                if is_runbatch:
                    # The run netCDF _is_ the batch netCDF, so always overwrite
                    run_path = self.netcdf_path
                else:
                    run_path = self.runlist[0].netcdf_path
                self.runlist[0].netcdf_path.rename(run_path)
                logger.info("Gluing is not needed, return generated run")
                return [xr.open_dataset(run_path)]
            # Remove the batch netCDF if overwriting is okay
            logger.warning(
                "User does not want to overwrite %s. Cannot move run!",
                self.runlist[0].netcdf_path,
            )

        if self.netcdf_path.exists() and overwrite_okay:
            # Remove the Batch file; we want to put out glued cube there
            self.netcdf_path.unlink()
        elif not self.netcdf_path.exists():
            # netcdf_path doesn't exist, no worries!
            pass
        else:
            raise Exception(
                f"Cannot use loopy={loopy}, mode={mode} without overwriting"
                f"{self.netcdf_path}",
            )

        # Now we have the hypercubes. Let's find out which dimensions
        # we're missing and glue the datasets together
        # As we return earlier, this is always a gluejob, and always a multirun
        if loopy:
            logger.debug("Gluing netCDFs loopy with mode %r", mode)
            if mode == "glue_dimx":
                batch_ds = None
                for ii, run in enumerate(self.runlist):
                    logger.info(
                        "Loading and concatenating netCDF of run %s (%d/%d)",
                        run.rundir.name,
                        ii,
                        len(self.runlist) - 1,
                    )
                    ds = xr.open_dataset(run.netcdf_path, engine=netcdf4_engine)
                    if gluedim is not None:
                        if gluedim in ds.attrs:
                            ds.coords[gluedim] = ds.attrs[gluedim]
                        ds = add_dims(ds, [gluedim])
                    if batch_ds is None:
                        batch_ds = ds
                    else:
                        batch_ds = xr.concat([batch_ds, ds], dim="dimx")
                # Overwrite dimx to be a unique number.
                batch_ds["dimx"] = np.arange(0, len(batch_ds["dimx"]))
            else:
                raise NotImplementedError("Loopy mode " + mode)
        else:
            logger.debug("Gluing netCDFs matrixy with mode %r", mode)
            dss = []
            for ii, run in enumerate(self.runlist):
                logger.debug(
                    "Lazy loading netCDF of run %s (%d/%d)",
                    run.netcdf_path.name,
                    ii,
                    len(self.runlist) - 1,
                )
                ds = xr.open_dataset(run.netcdf_path, engine=netcdf4_engine)
                if gluedim is not None:
                    if gluedim in ds.attrs:
                        ds.coords[gluedim] = ds.attrs[gluedim]
                    ds = add_dims(ds, [gluedim])
                dss.append(ds)

            logger.info("Start merging of loaded netCDFs")
            if mode == "glue_orthogonal":
                newds = merge_many_orthogonal(dss)
            elif mode == "glue_snake":
                # This writes to disk
                batch_ds = merge_many_lazy_snakes(
                    self.netcdf_path(), dss, verbose=verbose
                )
                logger.info("Merge many lazy snakes finished, return generated Dataset")
                return [newds]
            elif mode == "glue_dimx":
                batch_ds = xr.concat(dss, dim="dimx")
                # Overwrite dimx to be a unique number.
                batch_ds["dimx"] = np.arange(0, len(batch_ds["dimx"]))
            else:
                raise NotImplementedError("Matrixy mode " + mode)

        logger.info(f"Writing netCDF to file {self.netcdf_path}")
        encoding = build_encoding(batch_ds)
        xarray_to_netcdf_kwargs = create_to_netcdf_kwargs()
        batch_ds.to_netcdf(
            self.netcdf_path, encoding=encoding, **xarray_to_netcdf_kwargs
        )
        if clean:
            for run in self.runlist:
                logger.debug(f"Cleaning {run.netcdf_path}")
                run.netcdf_path.unlink()

        return [batch_ds]

    def clean(self):
        """Remove all output"""
        for run in self.runlist:
            run.clean()
        batchdir = self.parent_dir / self.name
        for path in [
            batchdir / self.batchinfofile,
            batchdir / self.stdout,
            batchdir / self.stderr,
        ]:
            if path.exists():
                path.unlink()

    def is_done(self, verbosity=0):
        """Check if job is done running
        Returns:
            True if job is done
        """
        if verbosity <= 0:
            logger.setLevel(logging.WARNING)
        elif verbosity <= 1:
            logger.setLevel(logging.INFO)

        done = True
        for run in self.runlist:
            run_is_done = run.is_done()
            if run_is_done:
                logger.info("Run %s is done", run)
            else:
                logger.info("Run %s is not done", run)
            done &= run.is_done()
        return done

    @property
    def netcdf_path(self):
        """Path to the generated netCDF file of this Batch"""
        return self.parent_dir / self.name / f"{self.name}.nc"

    @property
    def my_dir(self):
        """The directory this Batch saves its files in"""
        return self.parent_dir / self.name

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            attrs = self.__dict__.copy()
            other_attrs = other.__dict__.copy()
            equal = True
            for name in ["stderr", "stdout"]:
                self_path = self.parent_dir / self.name / str(attrs.pop(name))
                other_path = self.parent_dir / self.name / str(other_attrs.pop(name))
                equal &= str(self_path.resolve()) == str(other_path.resolve())
            equal &= equal_ignore_order(
                attrs.pop("runlist"), other_attrs.pop("runlist")
            )
            return attrs == other_attrs and equal
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        return NotImplemented

    def __str__(self):
        mod = str(self.__class__.__module__)
        name = str(self.__class__.__name__)
        return "{!s}.{!s}({!s})".format(
            mod.replace("qualikiz_tools.", ""),
            name,
            self.parent_dir / self.name,
        )


def equal_ignore_order(a, b):
    """Use only when elements are neither hashable nor sortable!"""
    unmatched = list(b)
    for element in a:
        try:
            unmatched.remove(element)
        except ValueError:
            return False
    return not unmatched


def overwrite_dialog(path, overwrite=None):
    """Ask user to overwrite an existing path

    If overwrite is not None, just return what
    overwrite was.
    """
    if overwrite is None:
        resp = input(path.stem + " exists, overwrite? [Y/n]")
        if resp == "" or resp == "Y" or resp == "y":
            overwrite = True
        else:
            overwrite = False
    return overwrite


def overwrite_prompt(path, overwrite=None):
    """Prompt user if file/path can be overwritten

    Kwargs:
        overwrite: If None, prompt user. If True, overwrite and if False,
                   throw Exception. None by default

    Returns:
        True if user wants to overwrite
    """
    if path.is_file() or path.is_dir():
        overwrite = overwrite_dialog(path, overwrite)
        if overwrite:
            print("overwriting ", path)
            if path.is_dir():
                for subname in path.iterdir():
                    subpath = path / subname
                    if subpath.is_dir():
                        shutil.rmtree(subpath)
                    else:
                        if subpath.exists():
                            subpath.unlink()
            else:
                if path.exists():
                    path.unlink()
    else:
        overwrite = True
    return overwrite


def create_folder_prompt(path, overwrite=None):
    """Overwrite folder prompt

    Kwargs:
        overwrite: If None, prompt user. If True, overwrite and if False,
                   throw Exception. None by default
    """
    if overwrite_prompt(path, overwrite=overwrite):
        path.mkdir(exist_ok=True, parents=True)


_netcdf4_chunk_mold = {
    "numsols": 1,
    "numicoefs": 7,
    "ntheta": 64,
    "nions": 1,
    "ecoefs": 13,
    "dimn": 1,
}
_netcdf4_chunk_mold["kthetarhos"] = _netcdf4_chunk_mold["dimn"]

_netcdf4_zlib = True
_netcdf4_complevel = 1
_netcdf4_shuffle = False
_netcdf4_fletcher32 = True
_netcdf4_least_signicant_digit = None
_netcdf4_float_format = "float32"
_netcdf4_int_format = "int32"
_netcdf4_int_FillValue = -999999999
_intlist = [
    "numicoefs",
    "ecoefs",
    "nions",
    "dimn",
    "dimx",
    "ntheta",
    "numsols",
    "typei",
    "coll_flag",
    "maxpts",
    "maxruns",
    "typee",
    "verbose",
    "integration_routine",
    "simple_mpi_only",
    "write_primi",
    "rot_flag",
]


def build_encoding(
    ds: xr.Dataset,
    n_dimx_chunks: Optional[int] = None,
    dimx_per_chunk: Optional[int] = None,
    group_in_chunk: Optional[list] = None,
    bytes_per_chunk: Optional[float] = 1024**2,
):
    """Build encoding for a given :py:class:`xarray:xarray.Dataset`

    When written to disk, xarray encodes the variables contained
    in the Dataset. When using the netcdf4 engine, each variable
    can have a separate encoding. This builds a homogeneous encoding
    for all variables in the given Dataset. It tries to smartly chunk
    for you. See
    http://davis.lbl.gov/Manuals/HDF5-1.8.7/Advanced/Chunking/index.html
    for more information.

    Args:
        ds: Dataset that encoding will be generated for. Will loop over all
            variables.keys()

    Keyword args:
        n_dimx_chunks: Number of chunks to make in the dimx dimension.
            Mututally exclusive with dimx_per_chunk
        dimx_per_chunk: Number of chunks to make in the dimx dimension
            Mututally exclusive with n_dimx_chunks
        bytes_per_chunk: Number of bytes in a single HDF5 chunk. Will be met
            on best-effort basis, erring on the large side.
    """
    # Determine the size of chunks the user wants in dimx if it exists
    # As this is usually a big scan dimension, we treat it separately
    if "dimx" in ds.dims:
        if n_dimx_chunks and dimx_per_chunk:
            # n_dimx_chunks _or_ dimx_per_chunk
            raise RuntimeError("Give either n_dimx_chunks or dimx_per_chunk, not both")
        elif n_dimx_chunks and not dimx_per_chunk:
            # Split dimx over n_dimx_chunks
            dimx_size = int(np.ceil(ds.dims["dimx"] / n_dimx_chunks))
        elif not n_dimx_chunks and dimx_per_chunk:
            # Do what the user wants
            dimx_size = dimx_per_chunk
        elif not n_dimx_chunks and not dimx_per_chunk:
            # Single chunk for dimx
            dimx_size = ds.dims["dimx"]

    if group_in_chunk is None:
        group_in_chunk = [
            var for var in ["Ati", "Ate", "At", "Ane", "Ani", "An"] if var in ds.dims
        ]

    encoding = {}
    # allvars = list(base.variables.keys())
    for varname in ds.variables:
        # Build chunksizes dependent on use case and what's in the var
        # Walk the dimension with known chunking _first_
        chunksizes = {}
        valsize = ds[varname].dtype.itemsize  # Size of single value in bytes
        ordered_dims = (dim not in _netcdf4_chunk_mold for dim in ds[varname].dims)
        for dimname in (y for x, y in sorted(zip(ordered_dims, ds[varname].dims))):
            if dimname == "dimx":
                size = dimx_size
                logger.debug(
                    "Size for dim %s of %s is %s, as %s is dimx_size=%s",
                    dimname,
                    varname,
                    size,
                    dimname,
                    dimx_size,
                )
            elif dimname in _netcdf4_chunk_mold:
                size = _netcdf4_chunk_mold[dimname]
                logger.debug(
                    "Size for dim %s of %s is %s, as %s is in" " _netcdf4_chunk_mold",
                    dimname,
                    varname,
                    size,
                    dimname,
                )
            else:
                # This is probably a folded dataset. Efficient chunking depends
                # on the usecase see http://davis.lbl.gov/Manuals/HDF5-1.8.7/Advanced/Chunking/index.html
                # Rule of thumb: use a (few) MB per chunk. Put data queried
                # together in a chunk; e.g. the gradients
                chunksize_so_far = valsize * np.prod(
                    [val for val in chunksizes.values()]
                )
                is_room = chunksize_so_far < bytes_per_chunk
                is_requested = dimname in group_in_chunk
                if is_room and is_requested:
                    # Try to make as large as possible chunks
                    size = ds.dims[dimname]
                elif is_room and not is_requested:
                    if any(dim in group_in_chunk for dim in ds[varname].dims):
                        # There will be a big chunk later; make this one small
                        size = 1
                    else:
                        # There will not be a big chunk and there is space
                        # Make this one big
                        size = ds.dims[dimname]
                elif not is_room and is_requested:
                    # There is no space for bigger chunks anymore, but the user
                    # wants it. Make a small chunk and raise a warining
                    logger.info("Cannot make a large chunk for %s", dimname)
                    size = 1
                else:
                    # There is no space for bigger chunks anymore, but the user
                    # Just make small chunks
                    size = 1
                logger.debug(
                    "Size for dim %s of %s is %s, as is_room=%s and"
                    " is_requested=%s. Chunksize before this addition=%s",
                    dimname,
                    varname,
                    size,
                    is_room,
                    is_requested,
                    chunksize_so_far,
                )
            chunksizes[dimname] = size
        # For xarray, chuncksizes is given as a tuple in dimensions
        chunksizes: tuple = tuple(chunksizes[dim] for dim in ds[varname].dims)

        encoding[varname] = {
            "zlib": _netcdf4_zlib,
            "complevel": _netcdf4_complevel,
            "shuffle": _netcdf4_shuffle,
            "fletcher32": _netcdf4_fletcher32,
            "least_significant_digit": _netcdf4_least_signicant_digit,
            "chunksizes": chunksizes,
        }
        if varname in _intlist:
            encoding[varname]["dtype"] = _netcdf4_int_format
            encoding[varname]["_FillValue"] = _netcdf4_int_FillValue
        else:
            encoding[varname]["dtype"] = _netcdf4_float_format

    return encoding


def create_to_netcdf_kwargs(allow_fallback_netcdf3=False):
    kwargs = {
        "compute": True,
        "invalid_netcdf": False,
    }

    if not allow_fallback_netcdf3:
        kwargs.update(
            {
                "format": "NETCDF4",
                "engine": "netcdf4",
            }
        )
        return kwargs


def run_to_netcdf(
    path,
    runmode="dimx",
    overwrite=None,
    genfromtxt=False,
    keepfile=True,
    extra_squeeze=None,
    Te_var="Te",
    verbosity=0,
    metareldir=python_debugdir,
    allow_fallback_netcdf3=False,
):
    """Convert a QuaLiKizRun to netCDF

    This will write a netCDF file using xarray containing all IO from the
    QuaLiKiz run. By default, variables will be encoded in a heuristically
    efficient way, meaning that the following kwargs will be passed to the backend:

    * zlib: True; Compress dataset with zlib for faster loading(!) and smaller files
    * complevel: 1; Use a single complevel. Higher level cost longer to compress
      for small size savings. When not shuffling, load times are similar.
    * shuffle: False; Do not shuffle dataset. Shuffling saves less space and
      increases load time, presumably because QuaLiKiz run data is usually ordered.
    * fletcher32: True; Activate Fletcher32 HDF5 checksum algorithm is activated
      to detect errors.
    * chunksizes: custom; Chunked dataset on disk using length one in nions,
      numsols, and dimn. The rest will be one big chunk to avoid too small chunks
    * least_signicant_digit: Do not truncate. Little use, we come from ASCII and
      go to float32. Truncating gives lossy compression for little space saving
    * dtype: custom; Use float32 for most vars, int32's for selected vars.
    * _FillValue: custom; -999999999 for int32's (IMAS convention)

    See :py:meth:`xarray:xarray.Dataset.to_netcdf` and
    :netcdf4:`nc4.Dataset.createVariable <#netCDF4.Dataset.createVariable>`.

    Args:
        path: Path of the run folder to netcdfize. Should contain the debug,
            output and output/primitive folders.

    Keyword Args:
        runmode: Runmode of netcdfizing. If orthogonal, fold the dataset as
            an hyperrectangle. Any dimx will extract the values in a 1D array.
        overwrite: Overwrite existing netcdf file. Prompt user by default
        genfromtxt: Use genfromtxt instead of loadtxt. Slower and loads
            unreadable values as nan
        keepfile: Keep read ASCII files. Highy recommended!
        extra_squeeze: List of coordinates to move to data_vars, see `squeeze_dataset`
        Te_var: Which variable to use as Te dimension e.g. Nustar
        verbose: Enable verbose printouts.
        metareldir: Directory to metadata files. Relative to path.
        allow_fallback_netcdf3: Allow xarray to fall back to netcdf3 if netcdf4
            module is not available
    """
    if verbosity >= 1:
        verbose = True
    else:
        verbose = False

    xarray_to_netcdf_kwargs = create_to_netcdf_kwargs(allow_fallback_netcdf3)

    path = Path(path)
    netcdf_path = (path / f"{path.name}.nc").resolve()
    if overwrite_prompt(netcdf_path, overwrite=overwrite):
        ds = qualikiz_folder_to_xarray(
            path,
            runmode=runmode,
            genfromtxt=genfromtxt,
            keepfile=keepfile,
            squeeze=False,
            extra_squeeze=extra_squeeze,
            Te_var=Te_var,
            verbose=verbose,
            metareldir=metareldir,
        )
        encoding = build_encoding(ds)
        if HAS_NETCDF4:
            ds.to_netcdf(netcdf_path, encoding=encoding, **xarray_to_netcdf_kwargs)
        elif not allow_fallback_netcdf3:
            raise Exception(
                "HAS_NETCDF4={!s} and allow_fallback_netcdf3={!s}, abort!".format(
                    HAS_NETCDF4, allow_fallback_netcdf3
                )
            )
        else:
            ds.to_netcdf(netcdf_path, encoding=encoding, **xarray_to_netcdf_kwargs)
    else:
        ds = xr.open_dataset(netcdf_path)
    return ds


def qlk_from_dir(
    target_dir,
    batch_class=QuaLiKizBatch,
    run_class=QuaLiKizRun,
    prioritize_batch=True,
    verbosity=0,
    **kwargs,
):
    """Convert a directory on disk to a QuaLiKizRun/Batch

    Has fallbacks in place if the default given `batch_class` and `run_class`
    do not work

    Args:
        target_dir: Directory on disk to try and reconstruct
          :py:class:`QuaLiKizRun`/:py:class:`QuaLiKizBatch` from

    Keyword Args:
        batch_class: Class that will be used by default to reconstruct a Batch
        run_class: Class that will be used by default to reconstruct a Run
        prioritize_batch: Exhaust all Batch options before trying Run reconstruction
        verbosity: Verbosity of this function. Will set verbose of calls to underlying functions
        **kwargs: Extra arguments will be passed to :py:func:`batch_class.from_dir`
           and/or :py:func:`run_class.from_dir`

    Returns:
        A tuple with the global type of the class ("run", "batch") and an instance of
        the class given by batch_class or run_class

    Raises:
        Exception if all fallback methods are exhausted
    """  # noqa: E501

    target_dir = Path(target_dir).resolve()
    if verbosity <= 0:
        kwargs["verbose"] = False
    else:
        logger.setLevel(logging.DEBUG)
        kwargs["verbose"] = True

    # Find indications if this is a batch
    has_batch_from_file = hasattr(batch_class, "from_file")
    if not has_batch_from_file:
        logger.debug("Given 'batch_class' does not have a 'from_file'")

    script_exists = (target_dir / batch_class.scriptname).resolve().exists()
    if not has_batch_from_file:
        logger.debug("Script %s does not exist", (target_dir / batch_class.scriptname))

    if script_exists and has_batch_from_file:
        # This is a specific type of Batch. Use its from_file
        logger.debug("Trying to reconstruct Batch from file")
        qlk_instance = batch_class.from_file(target_dir, **kwargs)
        return ("batch", qlk_instance)
    elif prioritize_batch:
        logger.debug(
            "Prioritizing Batch above run. Trying to reconstruct Batch from dir"
        )
        qlk_instance = batch_class.from_dir(target_dir, **kwargs)
    else:
        # No script existing or no method defined. Is this a run?
        logger.debug("Trying to reconstruct run")
        try:
            qlk_instance = run_class.from_dir(target_dir, **kwargs)
        except:
            # Not a run nor a specific Batch. Last chance, a from_dir Batch
            pass
        else:
            # If this works, we have a run
            return ("run", qlk_instance)

    logger.info("Trying to reconstruct Batch from dir")
    qlk_instance = batch_class.from_dir(target_dir, **kwargs)
    return ("batch", qlk_instance)


def find_qualikiz_binary(startdir=None, binary_name=None):
    """Walk up the tree to find a QuaLiKiz binary or use env variable"""
    candidate = None
    if "QUALIKIZ_BIN" in os.environ:
        candidate = Path(os.environ["QUALIKIZ_BIN"])
    elif binary_name is None and startdir is None:
        # No search information given
        raise FileNotFoundError(
            "Either define startdir and binary_name, "
            "or define the 'QUALIKIZ_BIN' environment variable"
        )
    elif binary_name is None:  # And startdir is not None
        binary_name = "QuaLiKiz"
        startdir = Path(startdir)
        candidate = startdir / binary_name
    elif startdir is None:  # And binary_name is not None
        startdir = Path.cwd()
        candidate = startdir / binary_name
    elif candidate is None:  # All not None
        candidate = startdir / binary_name

    if candidate.is_file():
        return candidate
    elif startdir == Path("/"):
        raise FileNotFoundError("Reached file system root, but no binary found")
    else:
        return find_qualikiz_binary(startdir.parent, binary_name=binary_name)
