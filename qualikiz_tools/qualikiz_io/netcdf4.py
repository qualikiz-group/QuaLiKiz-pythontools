""" Convinience functions for working with the netCDF4 library"""
from pathlib import Path
import logging
from typing import Optional
from itertools import product

import numpy as np
from netCDF4 import (
    Dataset,
)  # pylint: disable=preferred-module,no-name-in-module # noqa: F401,E501
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.outputfiles import _possible_dims

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)


def get_info(
    path, var_return_mode: Optional[str] = "first", raise_on_missing_dim=False
):
    """Find variables in netCDF4.Dataset for each possible dimension

    List of dimensions is based on a v2.8.1 phys_meth=1 run, see
    :py:attr:`_possible_dims`
    """
    ds_nc = Dataset(path)

    netcdf_variables = get_variables(
        ds_nc,
        var_return_mode=var_return_mode,
        raise_on_missing_dim=raise_on_missing_dim,
    )
    netcdf_chunks = get_chunks(ds_nc, raise_on_missing_dim=raise_on_missing_dim)

    # Only 1D variable sizes are interesting, otherwise you can self-calculate
    netcdf_shapes = get_shapes(ds_nc, raise_on_missing_dim=raise_on_missing_dim)
    for kk in list(netcdf_shapes.keys()):
        if len(netcdf_shapes[kk]) == 1:
            netcdf_shapes[kk[0]] = netcdf_shapes.pop(kk)[0]
        else:
            netcdf_shapes.pop(kk)

    # Get the theoretical in-memory sizes
    netcdf_bytes = get_bytes(ds_nc, raise_on_missing_dim=raise_on_missing_dim)
    result_dict = {
        "chunks": netcdf_chunks,
        "variables": netcdf_variables,
        "shapes": netcdf_shapes,
        "bytes": netcdf_bytes,
    }

    return result_dict


def _vardim_iterator():
    """ Iterate over all expected dimensions and acceptance criteria """
    # F F
    # T F
    # T T
    for use_coord, use_dim in product([False, True], repeat=2):
        if use_dim and not use_coord:
            continue
        logger.debug(
            "Trying to find variable with use_coord=%s and use_dim=%s",
            use_coord,
            use_dim,
        )
        for dims in _possible_dims:
            yield dims, use_dim, use_coord


def _do_variable_search(
    ds_nc, container, container_putter, var_return_mode: Optional[str] = "first"
):
    for dims, use_dim, use_coord in _vardim_iterator():
        for varname, var in ds_nc.variables.items():
            if hasattr(var, "coordinates"):
                # This is an xarray data variable
                is_valid = True
            elif dims == (varname,):
                # This is an xarray dimension
                is_valid = use_dim
            else:
                # This is an xarray coordinate
                is_valid = use_coord

            if is_valid and dims == var.dimensions:
                # If we can use this var and it matches the dimensions we are looking for
                is_valid = False
                if var_return_mode == "first" and dims not in container:
                    # We are in mode first, and have not found this var yet
                    container_putter(container, dims, varname, var)
                elif var_return_mode == "first" and dims in container:
                    # We are in mode first, and already added this var. Stop looping
                    break
                else:
                    container_putter(container, dims, varname, var)


def _put_variable(container, dims, varname, var):
    """ Put the get_variables variable into a container """
    if dims not in container:
        container[dims] = []
    container[dims].append(varname)


def get_variables(
    ds_nc: Dataset,
    var_return_mode: Optional[str] = "first",
    raise_on_missing_dim=False,
):
    """Grab netCDF variables in dataset based on their dimensions

    See :py:func:`get_info` for more information.

    Args:
        ds_nc: A netCDF4.Dataset to be analyzed

    Keyword Args:
        var_return_mode: When ``"first"``, return the first found matching
            variable, otherwise return all.
        raise_on_missing_dim: When True, raise an :py:class:`Exception` if not
            all expected dimensions have a matching found variable

    Returns:
        A dictionary with keys the dimension tuples and values a list of
        variables mathing this dimension. These lists are length one if
        ``var_return_mode`` is ``"first"``.

    Raises:
        Exception: If ``raise_on_missing_dim`` is True and not all
            expected dimensions have a matching found variable
    """
    netcdf_variables = {}
    logger.debug("Trying to find variables matching expected dimensions")
    _do_variable_search(
        ds_nc, netcdf_variables, _put_variable, var_return_mode=var_return_mode
    )

    # Check if we found 'm all
    _found_dims_okay(netcdf_variables.keys(), raise_on_missing_dim=raise_on_missing_dim)
    return netcdf_variables


def _put_chunks(container, dims, varname, var):
    """ Put the get_chunks variable into a container """
    chunk: list = var.chunking()
    container[dims] = chunk


def get_chunks(
    ds_nc: Dataset,
    raise_on_missing_dim=False,
):
    """Grab on-disk chunk sizes in dataset based on their dimensions

    See :py:func:`get_info` for more information. Assume all variables
    sharing a dimension have the same chunks sizes.

    Args:
        ds_nc: A netCDF4.Dataset to be analyzed

    Keyword Args:
        raise_on_missing_dim: When True, raise an :py:class:`Exception` if not
            all expected dimensions have a matching found variable

    Returns:
        A dictionary with keys the dimension tuples

    Raises:
        Exception: If ``raise_on_missing_dim`` is True and not all
            expected dimensions have a matching found variable
    """
    netcdf_chunks = {}
    _do_variable_search(ds_nc, netcdf_chunks, _put_chunks, var_return_mode="first")

    # Check if we found 'm all
    _found_dims_okay(netcdf_chunks.keys(), raise_on_missing_dim=raise_on_missing_dim)
    return netcdf_chunks


def _put_shapes(container, dims, varname, var):
    shape: list = var.shape
    container[dims] = shape


def get_shapes(
    ds_nc: Dataset,
    raise_on_missing_dim=False,
):
    """Grab on-disk variable sizes in dataset based on their dimensions

    See :py:func:`get_info` for more information. Assume all variables
    sharing a dimension have the same variable sizes.

    Args:
        ds_nc: A netCDF4.Dataset to be analyzed

    Keyword Args:
        raise_on_missing_dim: When True, raise an :py:class:`Exception` if not
            all expected dimensions have a matching found variable

    Returns:
        A dictionary with keys the dimension tuples

    Raises:
        Exception: If ``raise_on_missing_dim`` is True and not all
            expected dimensions have a matching found variable
    """
    netcdf_shapes = {}
    _do_variable_search(ds_nc, netcdf_shapes, _put_shapes, var_return_mode="first")

    # Check if we found 'm all
    _found_dims_okay(netcdf_shapes.keys(), raise_on_missing_dim=raise_on_missing_dim)
    return netcdf_shapes


def _put_bytes(container, dims, varname, var):
    dtype_size: int = var.dtype.itemsize  # In Bytes
    minimal_blksizes = [
        ln for name, ln in zip(var.dimensions, var.shape) if name != "dimx"
    ]
    minimal_blkbytes = np.prod(minimal_blksizes)
    container[dims] = int(np.ceil(minimal_blkbytes * dtype_size))  # Bytes


def get_bytes(
    ds_nc: Dataset,
    raise_on_missing_dim=False,
    dimx=1,
):
    """Grab theoretical in-memory variable sizes in dataset based on their dimensions

    See :py:func:`get_info` for more information. Assume all variables
    sharing a dimension have the same variable sizes.

    Args:
        ds_nc: A netCDF4.Dataset to be analyzed

    Keyword Args:
        raise_on_missing_dim: When True, raise an :py:class:`Exception` if not
            all expected dimensions have a matching found variable

    Returns:
        A dictionary with keys the dimension tuples

    Raises:
        Exception: If ``raise_on_missing_dim`` is True and not all
            expected dimensions have a matching found variable
    """
    netcdf_bytes = {}

    _do_variable_search(
        ds_nc, netcdf_bytes, _put_bytes, var_return_mode="first"
    )  # Bytes assuming dimx=1

    for kk, vv in netcdf_bytes.items():
        netcdf_bytes[kk] = vv * dimx
    netcdf_bytes["assume_dimx="] = dimx

    # Check if we found 'm all
    _found_dims_okay(netcdf_bytes.keys(), raise_on_missing_dim=raise_on_missing_dim)

    return netcdf_bytes


def _found_dims_okay(found_dims, raise_on_missing_dim=False):
    found_dims = list(found_dims)
    missing_dims = set(_possible_dims) - set(found_dims)
    if len(missing_dims) != 0:
        msg = "Not all dimensions found {!s} are missing"
        logger.warning(msg, missing_dims)
        if raise_on_missing_dim:
            raise Exception(msg, missing_dims)
