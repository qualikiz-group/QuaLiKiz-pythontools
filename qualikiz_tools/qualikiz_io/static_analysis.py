from pathlib import Path
import json

from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.outputfiles import _possible_subsets


def _find_ndims(el):
    ndims = 0
    for attr in el.attributes:
        if "dimension" in attr:
            ndims = attr.count(":")
    return ndims


def _get_qlk_datamap(qlk_dir):
    """ Static code analysis to get QuaLiKiz types """
    qlk_dir = Path(qlk_dir).resolve()
    if not qlk_dir.exists():
        raise FileNotFoundError(f"Could not find QuaLiKiz folder at '{str(qlk_dir)}'")

    # Read QuaLiKiz source files using f90wrap
    from f90wrap.parser import read_files

    # src_files = (qlk_dir / "src").glob("*.f90")
    src_files = [qlk_dir / "src/kind.f90", qlk_dir / "src/datcal.f90"]
    qlk_src = read_files(str(path) for path in src_files)

    modules = {el.name: el for el in qlk_src.modules}
    variables = _get_user_defined_types(modules["kind"])
    more_variables = _get_hard_coded_variable_sizes(modules["datcal"])
    variables.update(more_variables)
    return variables


def _get_user_defined_types(kind_module):
    """Loop over user-defined types of the kind module of QuaLikiz

    The kind module defines the "kinds" for parameters used throughout QuaLiKiz
    similar to datcal and datmat, the usage might have been diluted over the
    years. Right now it defines the types, and the unit numbers for STDOUT
    and STDERR. Use f90wrap, which has an internal fortran file parser,
    to parse the files.
    """
    vars = {}
    is_normalized = []
    for qlk_type in kind_module.types:
        for el in qlk_type.elements:
            sub_el = {
                "name": el.orig_name,
                "supertype": qlk_type.orig_name,
            }

            if el.name == "normalization":
                # This means all vars in the Type have a SI and GB version
                is_normalized.append(qlk_type.orig_name)
                continue

            # Find which subset the variable belongs to
            for subset in _possible_subsets:
                if subset in qlk_type.name:
                    sub_el["subset"] = subset
                    break

            if "subset" not in sub_el:
                raise Exception(
                    f"Could not find subset" f"'{el.name}' in '{qlk_type.name}'!"
                )

            # For output files, find with which phys_meth it will be outputted
            if subset in ["output", "primi"]:
                if "meth" in qlk_type.name:
                    sub_el["meth"] = int(qlk_type.name.split("_")[3])
                else:
                    raise Exception(
                        f"Could not find output meth belonging to"
                        f"'{el.name}' in '{qlk_type.name}'!"
                    )

            # For output files, find if it is a sepvar or a multi-mode var
            if subset in ["output", "primi"]:
                if "sep" in qlk_type.name:
                    sub_el["sep"] = bool(qlk_type.name.split("_")[5])
                else:
                    sub_el["sep"] = False

            # Fortran data type
            sub_el["type"] = el.type

            # Array size
            sub_el["ndims"] = _find_ndims(el)

            vars[sub_el["name"]] = sub_el

    # Set if a variable as both GB and SI versions:
    for key in vars:
        if vars[key]["supertype"] in is_normalized:
            vars[key]["normalized"] = True
        else:
            vars[key]["normalized"] = False

    # Loop over hard-coded elements
    for el in kind_module.elements:
        if el.name in ["sgl", "dbl"]:
            vars[el.name] = {
                "name": el.name,
                "type": el.type,
                "subset": "hardcoded",
            }
            # The values depend on your compiler here, so cannot parse.
            # Best we can do is grab the lines and save that
            with Path(kind_module.filename).open() as f_:
                for ii, line in enumerate(f_):
                    if ii == el.lineno:
                        var, val = line.split("=", 1)
                        # Double check
                        if el.orig_name == var.split("::")[1].strip():
                            vars[el.name]["value"] = val.strip()
    return vars


def _get_hard_coded_variable_sizes(datcal_module):
    """Get hard-coded variable sizes

    Many work-arrays and convinience arrays have their shapes pre-allocated, the
    shapes of these arrays can be found in the QuaLikiz datcal module
    """
    datcal = datcal_module
    vars = {}
    for el in datcal.elements:
        if el.name in ["numicoefs", "numecoefs", "ntheta"]:
            sub_el = {
                "name": el.name,
                "subset": "hardcoded",
                "sep": False,
                "type": el.type,
                "ndims": _find_ndims(el),
                "supertype": None,
                "value": el.value,
            }
            vars[sub_el["name"]] = sub_el

    return vars


def _dump_qlk_datamap(qlk_dir=None, target_path=None):
    """ Dump a map of data types generated from QuaLiKiz source """
    import qualikiz_tools
    import subprocess

    if qlk_dir is None:
        qlk_dir = Path(__file__, "../../../..").resolve()
    else:
        qlk_dir = Path(qlk_dir)

    if target_path is None:
        target_path = qlk_datamap
    else:
        target_path = Path(target_path)

    cmd = f"git -C {str(qlk_dir)} rev-parse HEAD"
    outp = subprocess.check_output(cmd, shell=True)
    qlk_hash = outp.decode().strip()

    js = {
        "_metadata": {
            "qualikiz_tools_version": qualikiz_tools.__version__,
            "qualikiz_version": qlk_hash,
        }
    }

    variables = _get_qlk_datamap(qlk_dir)
    js.update(variables)
    json.dump(js, target_path.open("w"), indent=2)
    return target_path
    # from pprint import pprint
    # pprint(variables, stream=None, width=200, indent=1, compact=True)
