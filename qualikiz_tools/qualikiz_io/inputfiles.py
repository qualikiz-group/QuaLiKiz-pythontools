"""
Copyright Dutch Institute for Fundamental Energy Research (2016-2017)
Contributors: Karel van de Plassche (karelvandeplassche@gmail.com)
License: CeCILL v2.1
"""
import array
import copy
import json
import itertools
from collections import OrderedDict
from warnings import warn
import os
import re
from operator import eq as _eq
import logging
import random

import numpy as np

from qualikiz_tools.misc.conversion import (
    calc_te_from_nustar,
    calc_nustar_from_parts,
    calc_zeff,
    calc_puretor_absolute,
    calc_puretor_gradient,
    calc_epsilon_from_parts,
    calc_no_pol_gammaE,
    calc_no_pol_Machtor,
    calc_no_pol_Autor,
    calc_puretor_Machpar_from_Machtor,
    calc_puretor_Aupar_from_Autor_Machtor,
)

logger = logging.getLogger("qualikiz_tools")
logger.setLevel(logging.INFO)


def json_serializer(obj):
    if isinstance(obj, np.ndarray):
        return obj.tolist()
    elif isinstance(obj, np.integer):
        return int(obj)
    raise ValueError("Could not serialize %s" % obj)


json_dump_kwargs = {"indent": 4, "default": json_serializer}


def allequal(lst):
    return lst[1:] == lst[:-1]


class Particle(OrderedDict):
    """Particle (ion or electron)"""

    in_args = ["T", "n", "At", "An", "type", "anis", "danisdr"]

    def __init__(self, **kwargs):
        """Initialize a Particle.
        Usually it is better to create an Electron or Ion instead.

        Kwargs:
            T :       Temperature in keV
            n :       Density in 10^19 m^-3 for electrons, relative factor to
                      electron denisity for ions
            At:       Normalized logarithmic temperature gradient
                      A_t = - (R/T) * (dT/dr)
            An:       Normalized logarithmic density gradient
                      A_n = - (R/n) * (dn/dr)
            type: 1:  Active
                  2:  Adiabatic
                  3:  Passing at ion scales
            anis:     Temperature anisotropy T_perp / T_para at LFS
            danisdr:  Radial gradient of temperature anisotropy

        Kwargs (ion only):
            Ai: Ion mass in amu
            Zi: Ion charge in e
        """
        super().__init__()
        if len(kwargs) != 0:
            for arg in self.in_args:
                self[arg] = kwargs.pop(arg)
        if len(kwargs) > 0:
            warn("Unrecognized kwargs {!s}".format(kwargs.keys()))


class Electron(Particle):
    def __init__(self, **kwargs):
        """ See Particle.__init__ """
        super().__init__(**kwargs)


class Ion(Particle):
    in_args = ["A", "Z"]

    def __init__(self, **kwargs):
        """ See Particle.__init__ """
        super().__init__()
        if len(kwargs) != 0:
            for arg in self.in_args + super().in_args:
                self[arg] = kwargs.pop(arg)
        if len(kwargs) > 0:
            warn("Unrecognized kwargs {!s}".format(kwargs.keys()))


class IonList(list):
    """Convenient wrapper for a list of Ions.

    Setting this list with a key value sets all ions contained
    inside. Similairly, getting with a key value gets all ions
    inside.

        args:
            list of Ions
    """

    def __init__(self, *args):
        super().__init__(args)

    def __getitem__(self, key):
        if isinstance(key, (int, slice)):
            return super().__getitem__(key)
        if key in Ion.in_args + Particle.in_args:
            valuelist = [ion[key] for ion in self]
            if allequal(valuelist):
                return self[0][key]
            else:
                raise Exception(
                    "Unequal values for ion key '" + key + "'= " + str(valuelist)
                )
        else:
            raise NotImplementedError("getting of " + key)

    def __setitem__(self, key, value):
        for ion in self:
            ion[key] = value
        return self


class QuaLiKizXpoint(OrderedDict):
    """A single xpoint in a QuaLiKiz run.

    Typically a QuaLiKiz run scans over multiple xpoints. This class
    contains multiple dicts sorted by meaning. Those dicts are:
        elec:      an Electron that describes the electrons in the plasma
        ions:      an IonList with all ions contained in the plasma
        meta:      a Meta instance with all values that don't change for different
                   radial points
        special:   a Special instance with all values that need special treatment
                   when writing to binary
        geometric: a Geometry instance with all values that change for
                   different radial points
        options:   information about different rescalings, assumptions, etc.

    """

    def __init__(self, **kwargs):
        """Initialize a single QuaLiKizXpoint
        Usually this point is part of a scan. Initialize an instance
        of QuaLiKizXpoint to use as base for the scan. This base_point
        can then be used as argument for an QuaLiKizPlan.

        Kwargs:
            kthetarhos:  The wave spectrum to be scanned
            electrons:   An Electron instance describing the electron
                         population
            ions:        An IonList instance describing the ion population
            all kwargs described in the Meta, Special and Geometry classes
        """
        super().__init__()

        if len(kwargs) != 0:
            kthetarhos = kwargs.pop("kthetarhos")
            ions = kwargs.pop("ions")
            dimn = len(kthetarhos)
            nions = len(ions)
            self["elec"] = kwargs.pop("electrons")
            self["ions"] = ions
            if "nag_relacc1" in kwargs:
                warn("Legacy keyword nag_relacc1 found, assuming you meant relacc1")
                kwargs["relacc1"] = kwargs.pop("nag_relacc1")
            if "cub_reqrelacc_strict" in kwargs:
                warn(
                    "Legacy keyword cub_reqrelacc_strict found, assuming you meant relacc1"
                )
                kwargs["relacc1"] = kwargs.pop("cub_reqrelacc_strict")
            if "cub_reqabsacc_strict" in kwargs:
                warn(
                    "Legacy keyword cub_reqabsacc_strict found, assuming you meant absacc1"
                )
                kwargs["absacc1"] = kwargs.pop("cub_reqabsacc_strict")
            if "nag_relacc2" in kwargs:
                warn("Legacy keyword nag_relacc2 found, assuming you meant relacc2")
                kwargs["relacc2"] = kwargs.pop("nag_relacc2")
            if "cub_reqrelacc_loose" in kwargs:
                warn(
                    "Legacy keyword cub_reqrelacc_loose found, assuming you meant relacc2"
                )
                kwargs["relacc2"] = kwargs.pop("cub_reqrelacc_loose")
            if "cub_reqabsacc_loose" in kwargs:
                warn(
                    "Leabsy keyword cub_reqabsacc_loose found, assuming you meant absacc2"
                )
                kwargs["absacc2"] = kwargs.pop("cub_reqabsacc_loose")
            self["meta"] = self.Meta(
                **{
                    name: kwargs.pop(name)
                    for name in self.Meta.in_args
                    if name in kwargs
                }
            )
            self["special"] = self.Special(kthetarhos=kthetarhos)
            self["geometry"] = self.Geometry(
                **{
                    name: kwargs.pop(name)
                    for name in self.Geometry.in_args
                    if name in kwargs
                }
            )
            self["options"] = self.Options(
                **{
                    name: kwargs.pop(name)
                    for name in self.Options.in_args
                    if name in kwargs
                }
            )
        if len(kwargs) > 0:
            warn("unrecognized params: %s. Ignoring" % ", ".join(kwargs.keys()))

    def get_other_non_trace_ions(self, ion_index):
        if ion_index > len(self["ions"]) - 1:
            warn(
                " ".join(
                    [
                        "Ion",
                        str(ion_index),
                        "out of range for IonList length",
                        str(len(self["ions"])),
                        "Changing Ion",
                        str(len(self["ions"]) - 1),
                        "instead",
                    ]
                )
            )
            ion_index = len(self["ions"]) - 1
        ions = [
            ion
            for ii, ion in enumerate(self["ions"])
            if ion["type"] != 3 and ii != ion_index
        ]
        return ion_index, ions

    def set_qn_normni_ion_n(self):
        """ Set density of nth ion to maintian quasineutrality """
        var_ion, ions = self.get_other_non_trace_ions(
            self["options"]["set_qn_normni_ion"]
        )
        var_normni = (1 - sum(ion["n"] * ion["Z"] for ion in ions)) / self["ions"][
            var_ion
        ]["Z"]

        if var_normni < 0 or var_normni > 1:
            raise Exception(
                "Quasineutrality results in unphysical n_0/n_e = "
                + str(var_normni)
                + " with Z = "
                + str([ion["Z"] for ion in self["ions"]])
                + " and n = "
                + str([ion["n"] for ion in self["ions"]])
            )
        if var_normni == 0:
            raise Exception(
                "Quasineutrality results in 0 density for ion {!s}".format(var_ion)
            )

        self["ions"][var_ion]["n"] = var_normni

    def set_qn_An_ion_n(self):
        """ Set density gradient of nth ion to maintian quasineutrality """
        var_ion, ions = self.get_other_non_trace_ions(self["options"]["set_qn_An_ion"])
        Z_var_ion = self["ions"][var_ion]["Z"]
        n_var_ion = self["ions"][var_ion]["n"]
        if Z_var_ion == 0 or n_var_ion == 0:
            raise Exception(
                "Z = {:.0f} and n = {:.0f} for ion {:d}. Unable to"
                " set Ani to match quasineutrality".format(
                    Z_var_ion, n_var_ion, var_ion
                )
            )
        # Check non-normalized ions
        for ion in ions:
            for var in ["n", "An", "Z"]:
                if ion[var] is None:
                    raise Exception(
                        'Ion "{!s}" has a variable "{!s}" which is None'.format(
                            ion, var
                        )
                    )

        var_An = (
            self["elec"]["An"] - sum(ion["n"] * ion["An"] * ion["Z"] for ion in ions)
        ) / (Z_var_ion * n_var_ion)
        self["ions"][var_ion]["An"] = var_An

    def check_quasi(self):
        """ Check if quasineutrality is maintained """
        ions = list(filter(lambda x: x["type"] != 3, self["ions"]))
        quasicheck = abs(sum(ion["n"] * ion["Z"] for ion in ions) - 1)
        ions = list(filter(lambda x: x["type"] != 3, self["ions"]))
        quasicheck_grad = abs(
            sum(ion["n"] * ion["An"] * ion["Z"] for ion in ions) - self["elec"]["An"]
        )
        quasitol = 1e-5
        if quasicheck > quasitol:
            raise Exception("Quasineutrality violated!")
        if quasicheck_grad > quasitol:
            raise Exception("Quasineutrality gradient violated!")

    def match_zeff(self, zeff):
        """ Adjust ni1 to match the given Zeff """
        if len(self["ions"]) > 1:
            ions = filter(lambda x: x["type"] != 3, self["ions"][2:])
            sum1 = sum(ion["n"] * ion["Z"] ** 2 for ion in ions)
            ions = filter(lambda x: x["type"] != 3, self["ions"][2:])
            sum2 = sum(ion["n"] * ion["Z"] for ion in ions) * self["ions"][0]["Z"]
            n1 = (zeff - self["ions"][0]["Z"] - sum1 + sum2) / (
                self["ions"][1]["Z"] ** 2 - self["ions"][1]["Z"] * self["ions"][0]["Z"]
            )
            if n1 < 0 or n1 > 1:
                raise Exception(
                    "Zeff= "
                    + str(zeff)
                    + " results in unphysical n_1/n_e = "
                    + str(n1)
                    + " with Z = "
                    + str([ion["Z"] for ion in self["ions"]])
                    + " and n = "
                    + str([ion["n"] for ion in self["ions"]])
                )
            self["ions"][1]["n"] = n1
            self.set_qn_normni_ion_n()  # Set ion n for
            # self.normalize_gradient()
        # Sanity check
        # print ('Zeff = ' + str(zeff))
        # print ([ion['n'] for ion in self['ions']])
        # print (np.isclose(self.calc_zeff(), zeff))

    def calc_zeff(self):
        """ Calculate Zeff """
        ions = list(filter(lambda x: x["type"] != 3, self["ions"]))
        for ion in ions:
            for var in ["n", "Z"]:
                if ion[var] is None:
                    raise Exception(
                        'Non-trace ion "{!s}" has a variable "{!s}" which is None'.format(
                            ion, var
                        )
                    )

        zeff = calc_zeff(ions)
        return zeff

    def match_nustar(self, nustar):
        """ Set Te to match the given Nustar """
        # First set everything needed for nustar: Zeff, Ne, q, Ro, Rmin, x
        zeff = self.calc_zeff()
        Te = calc_te_from_nustar(
            zeff,
            self["elec"]["n"],
            nustar,
            self["geometry"]["q"],
            self["geometry"]["Ro"],
            self["geometry"]["Rmin"],
            self["geometry"]["x"],
        )
        if len(Te) > 1:
            raise Exception("Why is Te so long?")
        self["elec"]["T"] = Te[0]

        # nustar_calc = c1 / Te ** 2 * (c2 + np.log(Te))
        # Sanity check
        # print(np.isclose(nustar_calc, nustar))

    def calc_nustar(self):
        """ Calculate Nustar """
        zeff = self.calc_zeff()
        nustar = calc_nustar_from_parts(
            zeff,
            self["elec"]["n"],
            self["elec"]["T"],
            self["geometry"]["q"],
            self["geometry"]["Ro"],
            self["geometry"]["Rmin"],
            self["geometry"]["x"],
        )
        return nustar

    def match_tite(self, tite):
        """ Set all Ions temperature to match the given Ti/Te """
        self["ions"]["T"] = tite * self["elec"]["T"]

    def match_Ati(self):
        """Set all Ions temperature gradients equal to that of the main ion"""
        for ion in range(len(self["ions"][0:])):
            self["ions"][ion]["At"] = self["ions"][0]["At"]
        # self['ions'][1]['At'] = self['ions'][0]['At']
        # self['ions'][2]['At'] = self['ions'][0]['At']

    def calc_tite(self):
        """ Calculate Ti/Te. Raises exception if undefined """
        for ion in self["ions"][0:]:
            if ion["T"] != self["ions"][0]["T"]:
                raise Exception("Ions have non-equal temperatures")
        return self["ions"][0]["T"] / self["elec"]["T"]

    def match_epsilon(self, epsilon):
        """ Set x to match the given epsilon """
        self["geometry"]["x"] = (
            self["geometry"]["Ro"] * epsilon / self["geometry"]["Rmin"]
        )

    def calc_epsilon(self):
        """ Calculate epsilon """
        return calc_epsilon_from_parts(
            self["geometry"]["x"], self["geometry"]["Rmin"], self["geometry"]["Ro"]
        )

    def match_dilution(self, dilution):

        # Get and check impurities
        impurities = filter(lambda x: x["type"] != 3, self["ions"][1:])
        main_impurity = next(impurities)
        other_impurities = list(impurities)
        main_impurity_idx = self["ions"].index(main_impurity)
        if (
            self["options"]["set_qn_normni"]
            and self["options"]["set_qn_normni_ion"] == main_impurity_idx
        ):
            raise Exception(
                "Option set_qn_normni with set_qn_normni_ion = {!s} will overwrite setting requested dilution on ion {!s}".format(
                    self["options"]["set_qn_normni_ion"], main_impurity_idx
                )
            )

        # Get and check main ion
        main = self["ions"][0]
        if main["type"] == 3:
            raise Exception("Main ion has type 3, does not make any sense!")

        # Calculate needed main impurity density
        normni_main_imp = (
            dilution
            - sum(impurity["Z"] * impurity["n"] for impurity in other_impurities)
        ) / main_impurity["Z"]

        if normni_main_imp < 0 or normni_main_imp > 1:
            raise Exception(
                "Dilution of {!s} results in unphysical n_1 = {!s} with Z = {!s} normni = {!s} ".format(
                    normni_main_imp,
                    dilution,
                    [ion["Z"] for ion in self["ions"]],
                    [ion["n"] for ion in self["ions"]],
                )
            )
        self["ions"][main_impurity_idx]["n"] = normni_main_imp

    def calc_dilution(self):
        """ Calculate dilution of the main ion """
        if self["ions"][0]["Z"] != 1.0:
            raise NotImplementedError(
                "Calculating dilution with non-hydrogenic main ion. Z0 = {!s}".format(
                    self["ions"]["Z"][0]
                )
            )
        impurities = filter(lambda x: x["type"] != 3, self["ions"][1:])
        dilution = sum(impurity["Z"] * impurity["n"] for impurity in impurities)
        return dilution

    def set_puretor(self):
        epsilon = self.calc_epsilon()
        q = self["q"]
        abs_var = self["puretor_abs_var"]
        grad_var = self["puretor_grad_var"]

        if not self["rot_flag"]:
            old_level = logger.level
            logger.setLevel(logging.ERROR)
        [Machtor, Machpar] = calc_puretor_absolute(
            epsilon, q, **{abs_var: self[abs_var]}
        )
        [Aupar, Autor, gammaE] = calc_puretor_gradient(
            epsilon, q, **{grad_var: self[grad_var]}
        )
        if not self["rot_flag"]:
            logger.setLevel(old_level)

        self["Machtor"] = Machtor
        self["Machpar"] = Machpar
        self["Aupar"] = Aupar
        self["Autor"] = Autor
        self["gammaE"] = gammaE

    def match_rot(self):
        """ Set rotation variable to match the other given quantities """
        rec_rot_val, rec_rot_var = self.calc_rot()
        if rec_rot_var is not None:
            self[rec_rot_var] = rec_rot_val
            self["Machpar"] = calc_puretor_Machpar_from_Machtor(
                Machtor=self["Machtor"],
                epsilon=self.calc_epsilon(),
                q=self["q"],
            )
            self["Aupar"] = calc_puretor_Aupar_from_Autor_Machtor(
                Autor=self["Autor"],
                Machtor=self["Machtor"],
                epsilon=self.calc_epsilon(),
                q=self["q"],
                smag=self["smag"],
            )

    def calc_rot(self):
        """Recalculate one toroidal rotation variable if the recalc_rot_var option is set to True

        First checks if there is at least one nonzero rotation variable (gammaE, Machtor or Autor).
        If so, checks if gammaE, Machtor or Autor is undetermined (equal to zero) in this order.
        If so, recalculates this variable from the other two if those do not equal zero.
        If not, recalculates gammaE.
        Returns the recalculated variable's value and name.
        Functions independently from assume_pure_rot

        The calculations use equation (31) from
        https://gitlab.com/qualikiz-group/QuaLiKiz-documents/-/blob/master/reports/rotation_variables.pdf
        assuming there is no poloidal rotation and no curvature in ion density and temperature.
        """
        logger.warning("Pending recheck of implemented formulas")
        rec_rot_val = None
        rec_rot_var = None
        if self["Machtor"] == 0 and self["gammaE"] == 0 and self["Autor"] == 0:
            logger.warning(
                "Machtor, gammaE and Autor are zero! Skipping recalculation of rotation."
            )
        else:
            Ani = np.array([ion["An"] for ion in self["ions"]])
            Ati = np.array([ion["At"] for ion in self["ions"]])
            pi = np.array(
                [
                    ion["T"] * 1e3 * ion["n"] * self["elec"]["n"] * 1e19
                    for ion in self["ions"]
                ]
            )

            if (
                self["Machtor"] is None
                and self["gammaE"] is not None
                and self["Autor"] is not None
            ):
                rec_rot_val = calc_no_pol_Machtor(
                    eps=self.calc_epsilon(),
                    q=self["q"],
                    smag=self["smag"],
                    Bo=self["Bo"],
                    r=self["x"] * self["Rmin"],
                    Ro=self["Ro"],
                    Ani=Ani,
                    Ati=Ati,
                    pi=pi,
                    ne=self["elec"]["n"] * 1e19,
                    Ane=self["elec"]["An"],
                    gammaE=self["gammaE"],
                    Autor=self["Autor"],
                )
                rec_rot_var = "Machtor"
            elif (
                self["Autor"] is None
                and self["Machtor"] is not None
                and self["gammaE"] is not None
            ):
                rec_rot_val = calc_no_pol_Autor(
                    eps=self.calc_epsilon(),
                    q=self["q"],
                    smag=self["smag"],
                    Bo=self["Bo"],
                    r=self["x"] * self["Rmin"],
                    Ro=self["Ro"],
                    Ani=Ani,
                    Ati=Ati,
                    pi=pi,
                    ne=self["elec"]["n"] * 1e19,
                    Ane=self["elec"]["An"],
                    gammaE=self["gammaE"],
                    Mtor=self["Machtor"],
                )
                rec_rot_var = "Autor"
            else:
                Machtor = 0 if self["Machtor"] is None else self["Machtor"]
                Autor = 0 if self["Autor"] is None else self["Autor"]
                rec_rot_val = calc_no_pol_gammaE(
                    eps=self.calc_epsilon(),
                    q=self["q"],
                    smag=self["smag"],
                    Bo=self["Bo"],
                    r=self["x"] * self["Rmin"],
                    Ro=self["Ro"],
                    Ani=Ani,
                    Ati=Ati,
                    pi=pi,
                    ne=self["elec"]["n"] * 1e19,
                    Ane=self["elec"]["An"],
                    Mtor=Machtor,
                    Autor=Autor,
                )
                rec_rot_var = "gammaE"

        return rec_rot_val, rec_rot_var

    class Options(OrderedDict):
        """ Wraps options for normalization, assumptions, etc."""

        in_args = OrderedDict(
            [
                ("set_qn_normni", True),
                ("set_qn_normni_ion", 0),
                ("set_qn_An", True),
                ("set_qn_An_ion", 0),
                ("check_qn", True),
                ("x_eq_rho", False),
                ("recalc_Nustar", True),
                ("recalc_Ti_Te_rel", False),
                ("assume_tor_rot", True),
                ("puretor_abs_var", "Machtor"),
                ("puretor_grad_var", "gammaE"),
                ("recalc_rot_var", False),
                ("recalc_Ati", False),
            ]
        )

        def __init__(self, **kwargs):
            """Initialize Options class
            kwargs:
                set_qn_normni:    Flag to set ion concentration to maintain
                                  quasineutrality
                set_qn_normni_ion:Index of ion to adjust if set_qn_normni is True
                set_qn_An:        Flag to set ion gradient to maintain
                                  quasineutrality
                set_qn_An_ion:    Index of ion to adjust if set_qn_An is True
                check_qn:         Flag for maintaining quasineutrality of gradients
                x_eq_rho:         Flag to keep rho and x equal if set with __setitem__
                recalc_Nustar:    Flag to recalculate Nustar after scanning over
                                  values. Needed when setting Nustar and either
                                  Zeff, ne, q, Ro, Rmin, x, rho, ni, ni0 or ni1
                recalc_Ti_Te_rel: Flag to recalculate Ti after setting Te
                assume_tor_rot:   Assume pure toroidal rotation. Auto-calculate
                                  Autor, Machpar, and Aupar from puretor_abs_var and
                                  puretor_grad_var
                puretor_abs_var:  Variable name to use as fixed for pure toroidal
                                  rotation absolute value
                puretor_grad_var: Variable name to use as fixed for pure toroidal
                                  rotation gradient
                recalc_rot_var:   Assuming there is no poloidal rotation and no ion density and temperature curvature,
                                  recalculte either Machtor, Autor or gammaE and adjusts Machpar and Aupar
                recalc_Ati:       Flag to set all ion temperature gradients equal to that of the main ion
            """

            super().__init__()
            for arg, default in self.in_args.items():
                self[arg] = kwargs.pop(arg, default)
            assert len(kwargs) == 0, "unrecognized params: %s" % ", ".join(
                kwargs.keys()
            )

    class Meta(OrderedDict):
        """ Wraps variables that stay constant during the QuaLiKiz run """

        in_args = OrderedDict(
            [
                ("phys_meth", 2),
                ("coll_flag", True),
                ("rot_flag", 0),
                ("verbose", True),
                ("separateflux", False),
                ("write_primi", True),
                ("numsols", 3),
                ("relacc1", 1e-3),
                ("relacc2", 2e-2),
                ("absacc1", 0),
                ("absacc2", 0),
                ("maxruns", 1),
                ("maxpts", 5e5),
                ("timeout", 60),
                ("ETGmult", 1),
                ("collmult", 1),
                ("rhomin", 0),
                ("rhomax", 1),
                ("simple_mpi_only", 0),
                ("integration_routine", 1),
            ]
        )

        def __init__(self, **kwargs):
            """Initialize Meta class
            kwargs:
                phys_meth:    Flag for additional calculation of output parameters
                coll_flag:    Flag for collisionality
                rot_flag:     Flag for rotation [0, 1, 2]
                verbose:      Flag for level of output verbosity
                separateflux: Flag for toggling output of separate
                              ITG, TEM, ETG fluxes
                write_primi:  Flag to write QuaLiKiz primitives to file
                numsols:      Number of requested solutions
                relacc1:      Strict relative accuracy, usually for 1D integrals
                relacc2:      Loose relative accuracy, usually for 2D integrals
                absacc1:      Strict absolute accuracy, usually for 1D integrals
                absacc2:      Loose absolute accuracy, usually for 2D integrals
                maxruns:      Number of runs jumping directly to Newton between
                              contour checks
                maxpts:       Number of integrant evaluations done in 2D integral
                timeout:      Upper time limit [s] for wavenumber/scan point
                              solution finding
                ETGmult:      Multpliers for ETG saturation level
                collmult:     Multiplier for collisionality
            """
            super().__init__()
            for arg, default in self.in_args.items():
                self[arg] = kwargs.pop(arg, default)
            assert len(kwargs) == 0, "unrecognized params: %s" % ", ".join(
                kwargs.keys()
            )

    class Special(OrderedDict):
        """ Wraps variables that need special convertion to binary"""

        def __init__(self, **kwargs):
            """Initialize Special class
            kwargs:
                kthetarhos: Wave spectrum input
            """
            super().__init__()
            if len(kwargs) != 0:
                self["kthetarhos"] = kwargs.pop("kthetarhos")
            if len(kwargs) > 0:
                warn("Unrecognized kwargs {!s}".format(kwargs.keys()))

    class Geometry(OrderedDict):
        """ Wraps variables that change per scan point """

        in_args = [
            "x",
            "rho",
            "Ro",
            "Rmin",
            "Bo",
            "q",
            "smag",
            "alpha",
            "Machtor",
            "Autor",
            "Machpar",
            "Aupar",
            "gammaE",
        ]

        def __init__(self, **kwargs):
            """Initialize Geometry class
            kwargs:
                x:       [m] Radial normalized coordinate
                rho:     [-] Normalized toroidal flux coordinate
                Ro:      [m] Major radius
                Rmin:    [m] Minor radius
                Bo:      [T] Magnetic field at magnetic axis
                q:       Local q-profile value
                smag:    Local magnetic shear s def rq'/q
                alpha:   Local MHD alpha

            Might be overwritten if assume_tor_rot is True:
                Machtor: Normalized toroidal velocity
                Autor:   Toroidal velocity gradient
                Machpar: Normalized parallel velocity
                Aupar:   Parallel velocity gradient
                gammaE:  Normalized perpendicular ExB flow shear
            """
            super().__init__()
            if len(kwargs) != 0:
                for key in self.in_args:
                    self[key] = kwargs.pop(key)
            if len(kwargs) > 0:
                warn("Unrecognized kwargs {!s}".format(kwargs.keys()))

    def __getitem__(self, key):
        """Get value from nested dict
        Use this method to get a value in the QuaLiKizRun class.
        It adds some extra abstraction for the multi-layered structure
        of the QuaLiKizRun class. You can get a specific internal variable,
        or get an Electron variable by appending 'e', or get all Ions
        by appending 'i'. You can also get a specific Ion with
        'i#', for example 'i1'
        """
        if key == "Zeff":
            return self.calc_zeff()
        elif key == "Nustar":
            return self.calc_nustar()
        elif key == "Ti_Te_rel":
            return self.calc_tite()
        elif key == "epsilon":
            return self.calc_epsilon()
        elif key in self.Geometry.in_args:
            return self["geometry"].__getitem__(key)
        elif key in ["kthetarhos"]:
            return self["special"].__getitem__(key)
        elif key in self.Meta.in_args:
            return self["meta"].__getitem__(key)
        elif key in self.Options.in_args:
            return self["options"].__getitem__(key)
        elif key in ["geometry", "special", "meta", "options", "ions", "elec"]:
            return super().__getitem__(key)
        elif key in Particle.in_args:
            ionval = self["ions"].__getitem__(key)
            elecval = self["elec"].__getitem__(key)
            if ionval == elecval:
                return elecval
            else:
                raise Exception(
                    "Unequal values for ion/elec key '"
                    + key
                    + "'= "
                    + str((ionval, elecval))
                )
        elif key.endswith("i") or (key[-1].isdigit() and key[-2] == "i"):
            if key[-1].isdigit():
                ionnumber = int(key[-1])
                key = key[:-2]
                return self["ions"][ionnumber].__getitem__(key)
            else:
                key = key[:-1]
                return self["ions"].__getitem__(key)
        elif key.endswith("e"):
            key = key[:-1]
            return self["elec"].__getitem__(key)
        else:
            raise NotImplementedError("getting of " + key)

    def __setitem__(self, key, value):
        """Set value in nested dict
        Use this method to set a value in the QuaLiKizRun class.
        It adds some extra abstraction for the multi-layered structure
        of the QuaLiKizRun class. You can set a specific internal variable,
        or set an Electron variable by appending 'e', or set all Ions
        by appending 'i'. You can also set a specific Ion with
        'i#', for example 'i1'
        """
        if key == "Zeff":
            self.match_zeff(value)
        elif key == "Nustar":
            self.match_nustar(value)
        elif key == "Ti_Te_rel":
            self.match_tite(value)
        elif key == "epsilon":
            self.match_epsilon(value)
        elif key == "dilution":
            self.match_dilution(value)
        elif key in self.Geometry.in_args:
            self["geometry"].__setitem__(key, value)
        elif key in ["kthetarhos"]:
            self["special"].__setitem__(key, value)
        elif key in self.Meta.in_args:
            self["meta"].__setitem__(key, value)
        elif key in self.Options.in_args:
            self["options"].__setitem__(key, value)
        elif key in ["geometry", "special", "meta", "options", "ions", "elec"]:
            super().__setitem__(key, value)
        elif key in Particle.in_args:
            self["ions"].__setitem__(key, value)
            self["elec"].__setitem__(key, value)
        elif key.endswith("i") or (key[-1].isdigit() and key[-2] == "i"):
            if key[-1].isdigit():
                ionnumber = int(key[-1])
                key = key[:-2]
                if key not in Particle.in_args and key not in Ion.in_args:
                    raise NotImplementedError("setting of " + key + "=" + str(value))
                self["ions"][ionnumber][key] = value
            else:
                key = key[:-1]
                if key not in Particle.in_args and key not in Ion.in_args:
                    raise NotImplementedError("setting of " + key + "=" + str(value))
                self["ions"].__setitem__(key, value)

            if (key not in Ion.in_args) and (key not in Particle.in_args):
                raise NotImplementedError("setting of " + key + "=" + str(value))
        elif key.endswith("e"):
            key = key[:-1]
            if key not in Particle.in_args:
                raise NotImplementedError("setting of " + key + "=" + str(value))
            self["elec"].__setitem__(key, value)
        else:
            raise NotImplementedError("setting of " + key + "=" + str(value))

    def __eq__(self, other):
        """od.__eq__(y) <==> od==y.  Comparison to another OD is order-sensitive
        while comparison to a regular mapping is order-insensitive.

        """
        if isinstance(other, QuaLiKizXpoint):
            is_equal = True
            for key, val in self.items():
                if not all(map(_eq, val, other[key])):
                    is_equal = False
            return is_equal
        return dict.__eq__(self, other)


class QuaLiKizPlan(OrderedDict):
    """Defines how to generate QuaLiKiz input files from a QuaLiKizXpoint base

    This class can be used to define a scan plan, in other words, over which
    values will be scanned in the QuaLiKiz run. This is given in the form of
    a xpoint base and a strategy how the exact points will be generated
    from this base. Usually this is a line or its N-D equivalent the edges of
    a hyperrectangle, or a hyperrectangle itself.
    """

    def __init__(self, **kwargs):
        """Initialize the QuaLiKizPlan

        kwargs:
            scan_dict:   Dictionary with as keys the names of the variables to
                         be scanned and as values the values to be scanned.
                         Use an OrderedDict to conserve ordering.
            scan_type:   How the points are generated. Currently accepts
                         'hyperedge', 'hyperrect', 'parallel', and 'mixed'.
            mixed_dict:  Dictionary of variables to use in 'hyperrect' as keys
                         and variables to use as corresponding 'parallel' as
                         values.
            xpoint_base: The QuaLiKizXpoint used as base for the generation
        """
        super().__init__()
        if len(kwargs) != 0:
            self["scan_dict"] = kwargs.pop("scan_dict")
            self["scan_type"] = kwargs.pop("scan_type")
            self["mixed_dict"] = kwargs.pop("mixed_dict", None)
            self["xpoint_base"] = kwargs.pop("xpoint_base")
        if len(kwargs) > 0:
            warn("Unrecognized kwargs {!s}".format(kwargs.keys()))

    def calculate_dimx(self):
        """Calculate the amount of xpoints, also known as dimx

        This depends on the scan_type
        """
        lenlist = [len(range) for range in self["scan_dict"].values()]
        if self["scan_type"] == "hyperedge":
            dimx = int(np.sum(lenlist))
        elif self["scan_type"] == "hyperrect":
            dimx = int(np.prod(lenlist))
        elif self["scan_type"] == "parallel":
            if lenlist[:-1] == lenlist[1:]:
                dimx = int(lenlist[0])
            else:
                raise Exception(
                    "scan_dict lists of unequal length: {!s}".format(lenlist)
                )
        elif self["scan_type"] == "mixed":
            if "mixed_dict" in self and self["mixed_dict"]:
                mixed_keys = []
                for key in self["mixed_dict"]:
                    if key in self["scan_dict"]:
                        mixed_keys.append(key)
                    else:
                        raise Exception(
                            "mixed_dict entry not in scan_dict: {!s}".format(key)
                        )
                lenlist = [len(self["scan_dict"][key]) for key in mixed_keys]
                dimx = int(np.prod(lenlist))
            else:
                raise Exception("mixed_dict not provided or is empty!")
        else:
            raise Exception("Unknown scan_type '" + self["scan_type"] + "'")
        return dimx

    def calculate_dimxn(self):
        """Calculate dimxn"""
        kthetarhos = self["xpoint_base"]["special"]["kthetarhos"]
        return self.calculate_dimx() * len(kthetarhos)

    def edge_generator(self, xlike_meta=None):
        """Generates the points on the edge of a hyperrectangle"""
        if xlike_meta is None:
            xlike_meta = {}
        intersec = [x[0] for x in self["scan_dict"].values()]
        meta_intersec = {
            scan_name: {meta_name: meta_vals[0]}
            for scan_name, scan_metas in xlike_meta.items()
            for meta_name, meta_vals in scan_metas.items()
        }
        # yield intersec
        numscan = -1
        for ii, (scan_name, values) in enumerate(self["scan_dict"].items()):
            for jj, value in enumerate(values):
                numscan += 1
                point = copy.deepcopy(intersec)
                meta = copy.deepcopy(meta_intersec)
                point[ii] = value
                # if point != intersec:
                if scan_name in meta_intersec:
                    for meta_name in meta_intersec[scan_name]:
                        meta[scan_name][meta_name] = xlike_meta[scan_name][meta_name][
                            jj
                        ]
                if "dimx" in meta_intersec:
                    for meta_name in meta_intersec["dimx"]:
                        meta["dimx"][meta_name] = xlike_meta["dimx"][meta_name][numscan]

                yield point, meta

    def parallel_generator(self, xlike_meta=None):
        if xlike_meta is None:
            xlike_meta = {}
        self["scan_dict"][list(self["scan_dict"].keys())[0]]
        num_par = len(self["scan_dict"])
        points = num_par * [None]
        dimx = self.calculate_dimx()
        point = len(self["scan_dict"].keys()) * [None]
        meta = {}
        for ii in range(dimx):
            for jj, (scan_name, scan_values) in enumerate(self["scan_dict"].items()):
                point[jj] = scan_values[ii]
                if scan_name in xlike_meta:
                    for meta_name in xlike_meta[scan_name]:
                        if scan_name not in meta:
                            meta[scan_name] = {}
                        meta[scan_name][meta_name] = xlike_meta[scan_name][meta_name][
                            ii
                        ]
                if "dimx" in xlike_meta:
                    meta["dimx"] = {}
                    for meta_name in xlike_meta["dimx"]:
                        meta["dimx"][meta_name] = xlike_meta["dimx"][meta_name][ii]
            yield point, meta

    def rect_generator(self, xlike_meta=None):
        if xlike_meta is None:
            xlike_meta = {}
        dimx = self.calculate_dimx()
        indices = [
            [(ii, val) for ii, val in enumerate(vals)]
            for vals in self["scan_dict"].values()
        ]
        indices = [list(range(len(vals))) for vals in self["scan_dict"].values()]
        values = list(self["scan_dict"].values())
        scan_values = len(indices) * [None]
        for num_x, scan_indices in enumerate(itertools.product(*indices)):
            for ii, idx in enumerate(scan_indices):
                scan_values[ii] = values[ii][idx]
            meta = {}
            for ii, scan_name in enumerate(self["scan_dict"].keys()):
                if scan_name in xlike_meta:
                    for meta_name in xlike_meta[scan_name]:
                        if scan_name not in meta:
                            meta[scan_name] = {}
                        meta[scan_name][meta_name] = xlike_meta[scan_name][meta_name][
                            scan_indices[ii]
                        ]
                if "dimx" in xlike_meta:
                    meta["dimx"] = {}
                    for meta_name in xlike_meta["dimx"]:
                        meta["dimx"][meta_name] = xlike_meta["dimx"][meta_name][num_x]
            yield scan_values, meta

    def mixed_generator(self, xlike_meta=None):
        """Combines the parallel and hyperrect input generators

        Requires the mixed_dict argument to be specified in addition to the scan_type argument. Generates
        parallel variable changes alongside a hyperrectangular scan.
        """
        if xlike_meta is None:
            xlike_meta = {}
        dimx = self.calculate_dimx()
        keys = list(self["scan_dict"])
        indices = []
        values = []
        rect_indices = {}
        for key in self["mixed_dict"]:
            rect_indices[key] = keys.index(key)
        for key in self["scan_dict"]:
            if key in self["mixed_dict"]:
                indices.append(list(range(len(self["scan_dict"][key]))))
                values.append(list(self["scan_dict"][key]))
            else:
                for rect_key in self["mixed_dict"]:
                    if key in self["mixed_dict"][rect_key]:
                        indices.append([-1 - rect_indices[rect_key]])
                        values.append(list(self["scan_dict"][key]))
                        break
        for num_x, scan_indices in enumerate(itertools.product(*indices)):
            actual_scan_indices = list(scan_indices)
            scan_values = len(indices) * [None]
            for row in range(len(scan_indices)):
                if scan_indices[row] < 0:
                    actual_scan_indices[row] = scan_indices[-1 - scan_indices[row]]
            for ii, idx in enumerate(actual_scan_indices):
                scan_values[ii] = values[ii][idx]
            meta = {}
            for ii, scan_name in enumerate(self["scan_dict"].keys()):
                if scan_name in xlike_meta:
                    for meta_name in xlike_meta[scan_name]:
                        if scan_name not in meta:
                            meta[scan_name] = {}
                        meta[scan_name][meta_name] = xlike_meta[scan_name][meta_name][
                            actual_scan_indices[ii]
                        ]
                if "dimx" in xlike_meta:
                    meta["dimx"] = {}
                    for meta_name in xlike_meta["dimx"]:
                        meta["dimx"][meta_name] = xlike_meta["dimx"][meta_name][num_x]
            yield scan_values, meta

    def extract_dimx_meta(self, metadata):
        metasplit = r"^@(\w+).(\w+)"
        xlike_meta = OrderedDict()
        for key, val in metadata.items():
            if re.match(metasplit, key):
                # print(key, val)
                _, metavar, metaname, _ = re.split(metasplit, key)
                if metavar not in self["scan_dict"] and metavar != "dimx":
                    warn(
                        "Warning! {!s} not in scan_dict, "
                        "cannot attach metadata".format(metavar)
                    )
                elif metavar == "dimx" and len(val) != self.calculate_dimx():
                    ex_str = (
                        "Length of metadata {!s} attached to {!s} "
                        "not the same length {!s} != {!s}".format(
                            key, metavar, len(val), self.calculate_dimx()
                        )
                    )
                    raise Exception(ex_str)
                elif (metavar != "dimx") and (
                    len(val) != len(self["scan_dict"][metavar])
                ):
                    ex_str = (
                        "Length of metadata {!s} attached to {!s} "
                        "not the same length {!s} != {!s}".format(
                            key, metavar, len(val), len(self["scan_dict"][metavar])
                        )
                    )
                    raise Exception(ex_str)
                else:
                    if metavar not in xlike_meta:
                        xlike_meta[metavar] = OrderedDict()
                    xlike_meta[metavar][metaname] = val
            else:
                warn(
                    "Warning! {!s} does not match regex {!s}, "
                    "ignoring".format(key, metasplit)
                )
        return xlike_meta

    def setup(self, metadata=None):
        """Set up the QuaLiKiz scan

        Pass the binary generator the correct generator depending on the
        scan_type
        """
        if metadata is None:
            metadata = {}
        xlike_meta = self.extract_dimx_meta(metadata)
        metasplit = r"^@(\w+).(\w+)"
        new_xlike_meta = OrderedDict()
        for key, val in metadata.items():
            if re.match(metasplit, key):
                _, metavar, metaname, _ = re.split(metasplit, key)
                if metavar not in new_xlike_meta:
                    new_xlike_meta[metavar] = OrderedDict()
                new_xlike_meta[metavar][metaname] = val
        if len(new_xlike_meta) != len(xlike_meta):
            warn("Some metadata fields are not dimx0like or failed parsing.")
        names = list(self["scan_dict"].keys())

        if self["scan_type"] == "hyperedge":
            bytes, meta = self.setup_scan(
                names, self.edge_generator(xlike_meta=xlike_meta)
            )
        elif self["scan_type"] == "hyperrect":
            bytes, meta = self.setup_scan(
                names, self.rect_generator(xlike_meta=xlike_meta)
            )
        elif self["scan_type"] == "parallel":
            bytes, meta = self.setup_scan(
                names, self.parallel_generator(xlike_meta=xlike_meta)
            )
        elif self["scan_type"] == "mixed":
            if "mixed_dict" in self and self["mixed_dict"]:
                for key in self["mixed_dict"]:
                    if not (key in self["scan_dict"] and self["scan_dict"][key]):
                        raise Exception(
                            "Unable to perform mixed scan: {!s} not in scan_dict!".format(
                                key
                            )
                        )
                    if self["mixed_dict"][key] is not None:
                        for parallel_key in self["mixed_dict"][key]:
                            if not (
                                parallel_key in self["scan_dict"]
                                and self["scan_dict"][parallel_key]
                            ):
                                raise Exception(
                                    "Unable to perform mixed scan: {!s} not in scan_dict!".format(
                                        parallel_key
                                    )
                                )
                            if len(self["scan_dict"][parallel_key]) != len(
                                self["scan_dict"][key]
                            ):
                                raise Exception(
                                    "Unable to perform a mixed scan: the lengths of {!s} and {!s} are not equal!".format(
                                        key, parallel_key
                                    )
                                )
                bytes, meta = self.setup_scan(
                    names, self.mixed_generator(xlike_meta=xlike_meta)
                )
            else:
                warn("Warning! mixed_dict entry not found, defaulting to hyperrect!")
                bytes, meta = self.setup_scan(
                    names, self.rect_generator(xlike_meta=xlike_meta)
                )
        else:
            raise Exception("Unknown scan_type '" + self["scan_type"] + "'")
        return bytes, meta

    def _sanity_check_setup(self, scan_names):
        """ Check if the order of scan_names is correct """
        if len(scan_names) == 0:
            raise Exception("Scan list is empty!")
        try:
            index = scan_names.index("Zeff")
        except ValueError:
            pass
        else:
            if any(name in scan_names[index:] for name in ["ni", "ni0", "ni1"]):
                warn("Warning! ni will overwrite Zeff!")
            if any(name.startswith("ni") for name in scan_names[index:]):
                warn("Warning! ni not taken into account while setting Zeff")
        try:
            index = scan_names.index("Nustar")
        except ValueError:
            pass
        else:
            if any(
                name in scan_names[index:]
                for name in [
                    "Zeff",
                    "ne",
                    "q",
                    "Ro",
                    "Rmin",
                    "x",
                    "rho",
                    "ni",
                    "ni0",
                    "ni1",
                ]
            ):
                warn(
                    "Warning! Set Zeff, ne, q, Ro, Rmin, x, ni"
                    + "and rho before setting Nustar"
                )
        try:
            index = scan_names.index("Ti_Te_rel")
        except ValueError:
            pass
        else:
            if any(name in scan_names[index:] for name in ["Te", "Nustar"]):
                warn("Warning! Set Te before setting Ti_Te_rel")

        if "dilution" in scan_names and "normni" in scan_names:
            raise Exception(
                "Cannot scan dilution and normni at the same time, they are equivalent!"
            )

        for name in scan_names:
            for subclass in [QuaLiKizXpoint.Meta, QuaLiKizXpoint.Options]:
                if name in subclass.in_args:
                    raise Exception(
                        "scan name {!s} in group {!s} cannot be scanned".format(
                            name, subclass.__name__
                        )
                    )
            if name == "kthetarhos":
                raise Exception("kthetarhos cannot be scanned")

    def setup_scan(self, scan_names, scan_list):
        """Set up a QuaLiKiz scan

        scan_names should be the names of the parameters being scanned over.
        This is a list with the same length of list-like objects generated
        by scan_list. Scan_list should be a generator (or list of lists)
        that generates the values matching the values of the scan_names.
        """
        self._sanity_check_setup(scan_names)
        dimxpoint = copy.deepcopy(self["xpoint_base"])
        if (
            any(name in scan_names for name in ["Nustar", "Te", "T"])
            and dimxpoint["options"]["recalc_Nustar"]
        ):
            warn(
                "Warning! Nustar, Te or T in scan and Nustar is being recalculated from base!"
            )
        if (
            ("T" in scan_names) or any(name.startswith("Ti") for name in scan_names)
        ) and dimxpoint["options"]["recalc_Ti_Te_rel"]:
            warn(
                "Warning! Ti*, T or Ti_Te_rel in scan and Ti_Te_rel is being recalculated from base!"
            )

        # Initialize all the arrays that will eventually be written to file
        dimx = self.calculate_dimx()
        dimn = len(dimxpoint["special"]["kthetarhos"])
        nions = len(dimxpoint["ions"])

        bytes = dict(
            zip(
                QuaLiKizXpoint.Geometry.in_args,
                [array.array("d", [0] * dimx) for i in range(13)],
            )
        )
        bytes.update(
            dict(
                zip(
                    [x + "e" for x in Electron.in_args],
                    [array.array("d", [0] * dimx) for i in range(7)],
                )
            )
        )
        dimxi = dimx * nions
        bytes.update(
            dict(
                zip(
                    [x + "i" for x in Electron.in_args + Ion.in_args],
                    [array.array("d", [0] * dimxi) for i in range(9)],
                )
            )
        )
        calc = {"dimx": dimx, "dimn": dimn, "nions": nions}

        # Put the three numbers we already calculated in an array
        for key, value in calc.items():
            bytes[key] = array.array("d", [value])

        # Rename what we call 'ni' to what QuaLiKiz calls 'normni'
        bytes["normni"] = bytes.pop("ni")

        numscan = -1
        meta_unfolded = {}
        # Iterate over the scan_list, each next() should provide a list-like
        # object with as many entries as we have different parameters
        for scan_values, meta in scan_list:
            numscan += 1
            # Set the dimxn point value to the value in the list.
            if dimxpoint["options"]["recalc_Nustar"]:
                nustar = dimxpoint.calc_nustar()
            if dimxpoint["options"]["recalc_Ti_Te_rel"]:
                Ti_Te_rel = dimxpoint.calc_tite()
            nustar_idx = None
            titerel_idx = None
            for ii, (scan_name, scan_value) in enumerate(zip(scan_names, scan_values)):
                if scan_name == "Nustar":
                    nustar_idx = ii
                elif scan_name == "Ti_Te_rel":
                    titerel_idx = ii
                else:
                    dimxpoint[scan_name] = scan_value

            if dimxpoint["options"]["assume_tor_rot"]:
                dimxpoint.set_puretor()
            elif dimxpoint["options"]["recalc_rot_var"]:
                dimxpoint.match_rot()
            if dimxpoint["options"]["x_eq_rho"]:
                dimxpoint["geometry"].__setitem__("rho", dimxpoint["x"])
            if dimxpoint["options"]["set_qn_normni"]:
                dimxpoint.set_qn_normni_ion_n()
            if dimxpoint["options"]["set_qn_An"]:
                dimxpoint.set_qn_An_ion_n()
            if dimxpoint["options"]["recalc_Nustar"]:
                dimxpoint.match_nustar(nustar)
            if nustar_idx is not None:
                dimxpoint.match_nustar(scan_values[nustar_idx])
            if titerel_idx is not None:
                dimxpoint.match_tite(scan_values[titerel_idx])
            if dimxpoint["options"]["recalc_Ti_Te_rel"]:
                dimxpoint.match_tite(Ti_Te_rel)
            if dimxpoint["options"]["recalc_Ati"]:
                dimxpoint.match_Ati()
            if dimxpoint["options"]["check_qn"]:
                dimxpoint.check_quasi()

            for scan_name in meta:
                if scan_name not in meta_unfolded:
                    meta_unfolded[scan_name] = {}
                for meta_name in meta[scan_name]:
                    if meta_name not in meta_unfolded[scan_name]:
                        meta_unfolded[scan_name][meta_name] = []
                    meta_unfolded[scan_name][meta_name].append(
                        meta[scan_name][meta_name]
                    )

            # Now iterate over all the values in the xpoint dict and add them
            # to our array
            for name, value in dimxpoint["geometry"].items():
                bytes[name][numscan] = value
            for name, value in dimxpoint["elec"].items():
                bytes[name + "e"][numscan] = value

            # Note that the ion array is in C ordering, not F ordering
            for jj, ion in enumerate(dimxpoint["ions"]):
                for name, value in ion.items():
                    if name == "n":
                        name = "normn"
                    bytes[name + "i"][jj * dimx + numscan] = value

        # Some magic because electron type is a QuaLiKizRun constant
        bytes["typee"] = array.array("d", [bytes["typee"][0]])

        for name, value in dimxpoint["special"].items():
            bytes[name] = array.array("d", value)

        for name, value in dimxpoint["meta"].items():
            bytes[name] = array.array("d", [value])
        return bytes, meta_unfolded

    def to_json(self, filename):
        """Dump the QuaLiKiz plan to json file

        The QuaLiKiz plan, including the xpoint base, can be fully
        recontructed later using the from_json function
        """
        with open(filename, "w") as file_:
            json.dump(self, file_, **json_dump_kwargs)

    @classmethod
    def from_json(cls, filename):
        """Load the QuaLiKiz plan from json

        Reconstruct the QuaLiKiz plan based on the given json file.
        Backwards compatibility is not guaranteed, so preferably
        generate the json with the same version as which you load it
        with.
        """
        with open(filename, "r") as file_:
            data = json.load(file_, object_pairs_hook=OrderedDict)
            scan_dict = data.pop("scan_dict")
            scan_type = data.pop("scan_type")
            mixed_dict = data.pop("mixed_dict", None)

            kthetarhos = data["xpoint_base"]["special"].pop("kthetarhos")
            data["xpoint_base"].pop("special")
            elec = Electron(**data["xpoint_base"].pop("elec"))
            ionlist = []
            for ion in data["xpoint_base"]["ions"]:
                ionlist.append(Ion(**ion))
            ions = IonList(*ionlist)
            data["xpoint_base"].pop("ions")
            dict_ = {}
            for dicts in data["xpoint_base"].values():
                dict_.update(dicts)

            xpoint_base = QuaLiKizXpoint(
                kthetarhos=kthetarhos, electrons=elec, ions=ions, **dict_
            )
            return QuaLiKizPlan(
                scan_dict=scan_dict,
                scan_type=scan_type,
                mixed_dict=mixed_dict,
                xpoint_base=xpoint_base,
            )

    @classmethod
    def from_defaults(cls):
        return cls.from_json(
            os.path.join(os.path.dirname(__file__), "parameters_template.json")
        )

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.keys() != other.keys():
                return False
            if self["scan_dict"].keys() != other["scan_dict"].keys():
                return False
            equal = True
            for key in self.keys():
                if key != "scan_dict":
                    equal &= self[key] == other[key]
            for key in self["scan_dict"].keys():
                equal &= np.array_equal(self["scan_dict"][key], other["scan_dict"][key])
            return equal
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        return NotImplemented


class QuaLiKizQLFluxScan(OrderedDict):
    @classmethod
    def from_json(cls, filename):
        """Load the QLScan from json"""
        with open(filename, "r") as file_:
            data = json.load(file_, object_pairs_hook=OrderedDict)
        return cls(data)

    def to_json(self, filename):
        """Dump the QLScan to json file"""
        with open(filename, "w") as file_:
            json.dump(self, file_, **json_dump_kwargs)
