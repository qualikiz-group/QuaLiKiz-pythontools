from warnings import warn
import os
from subprocess import (
    Popen,
    PIPE,
    STDOUT,
    check_output,
    CompletedProcess,
    SubprocessError,
)
from subprocess import run as sprun
import logging
import getpass

import numpy as np
import pandas as pd
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.machine_specific.bash import Run as BashRun
from qualikiz_tools.machine_specific.system import Batch as SystemBatch
from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun, QuaLiKizBatch
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SlurmError(SubprocessError):
    def __init__(self, message, errors):
        super().__init__(message)


class Run(BashRun):
    def __init__(
        self, parent_dir, name, binaryrelpath, stdout=None, stderr=None, **kwargs
    ):

        if stdout is None:
            stdout = QuaLiKizRun.default_stdout
        if stderr is None:
            stderr = QuaLiKizRun.default_stderr
        super().__init__(
            parent_dir, name, binaryrelpath, stdout=stdout, stderr=stderr, **kwargs
        )


class Batch(SystemBatch):
    """Defines a batch job

    This class uses the OpenMP/MPI parameters as defined by Edison,
    but could in principle be extented to support more machines.

    Class Variables:
        - attr:             All possible attributes as defined by Edison
        - sbatch:           Names of attributes as they are in the sbatch file
        - shell:            The shell to use for sbatch scripts. Usually bash
    """

    # pylint: disable=too-many-instance-attributes
    attr = [
        "nodes",
        "maxtime",
        "partition",
        "tasks_per_node",
        #'vcores_per_task',
        "filesystem",
        "name",
        "repo",
        "stderr",
        "stdout",
        "qos",
    ]
    sbatch = [
        "nodes",
        "time",
        "partition",
        "ntasks-per-node",
        #'cpus-per-task',
        "license",
        "job-name",
        "account",
        "error",
        "output",
        "qos",
    ]
    shell = "/bin/bash"
    run_class = Run
    defaults = {"stdout": "stdout.batch", "stderr": "stderr.batch"}

    def __init__(
        self,
        parent_dir,
        name,
        runlist,
        maxtime=None,
        stdout=None,
        stderr=None,
        safetytime=1.5,
        style="sequential",
        **kwargs
    ):
        """Initialize Edison batch job

        Args:
            - parent_dir:     Name of the folder of the Batch
            - name:           Name of the Batch job
            - runlist: List of Run instances included in the Batch job

        Kwargs:
            - stdout:     File to write stdout to. By default 'stdout.batch'
            - stderr:     File to write stderr to. By default 'stderr.batch'
            - filesystem: The default filesystem to use. Usually SCRATCH
            - partition:  Partition to run on, for example 'debug'. By default
                          'regular'
            - qos:        Priority in the queue. By default 'normal'
            - repo:       The default repo to bill hours to. Usually None
            - HT:         Hyperthreading on/off. Default=True
            - vcores_per_task: Amount of cores to use per task
            - safetytime: An extra factor that will be used in the calculation
                          of requested runtime. 1.5x by default
            - style:      How to glue the different runs together. Currently
                          only 'sequential' is used
            - maxtime:    Maximum time for a job. Guesstimated by default.


        Calculated:
            - threads_per_core: amount of OMP threads per physical core
            - threads_per_node: amount of OMP threads per compute node
            - sockets_per_node: Amount of sockets in one compute node
            - cores_per_socket: Amount of physical CPU cores in one socket
            - cores_per_node:   Amount of physical CPU cores in one node
        """
        # Fill (needed) attribute with defaults or None
        kwargs["maxtime"] = maxtime
        if "nodes" in kwargs:
            warn(
                "nodes passed to Batch.__init__. Will be ignored in favor of Run.nodes"
            )
        for attribute in self.attr:
            if attribute != "nodes":
                if attribute in kwargs:
                    setattr(self, attribute, kwargs[attribute])
                elif attribute in self.defaults:
                    setattr(self, attribute, self.defaults[attribute])
                else:
                    setattr(self, attribute, None)

        super().__init__(
            parent_dir, name, runlist, stdout=self.stdout, stderr=self.stderr
        )

        if style == "sequential":
            if self.maxtime is None:
                task_array = np.array([run.tasks for run in self.runlist])
                cores_per_node = self.run_class.defaults["cores_per_node"]
                nodes_array = np.array([run.nodes for run in self.runlist])
                cores_array = cores_per_node * nodes_array
                if any(cores_array != task_array):
                    warn(
                        "Warning! More than 1 task per physical core! Walltime might be inaccurate"
                    )

                totwallsec = np.sum(
                    [
                        run.estimate_walltime(run.nodes * cores_per_node)
                        for run in self.runlist
                    ]
                )
                totwallsec *= safetytime
                mm, ss = divmod(totwallsec, 60)
                hh, mm = divmod((mm + 1), 60)

                # TODO: generalize for non-edison machines
                if self.partition == "debug" and (hh >= 1 or mm >= 30):
                    warn("Walltime requested too high for debug partition")
                self.maxtime = "%d:%02d:%02d" % (hh, mm, ss)
        else:
            raise NotImplementedError("Style {!s} not implemented yet.".format(style))

    @property
    def nodes(self):
        return max([run.nodes for run in self.runlist])

    def launch(self, clean=True, on_input_not_exist="raise"):
        """Launch a Batch using the Slurm system

        Kwargs:
          - clean: Clean all run folders before launching
          - on_input_not_exist: What to do if input does not exist

        Returns:
          - job_id: The Slurm ID of the launched job. None if failed
        """
        if not self.inputbinaries_exist():
            msg = "QuaLiKiz input binaries do not exists"
            if on_input_not_exist == "raise":
                raise Exception(msg)
            else:
                logger.warning(msg)

        if clean:
            self.clean()

        batch_dir = os.path.join(self.parent_dir, self.name)

        cmd = " ".join(["cd", batch_dir, "&&", "sbatch", self.scriptname])
        logger.info('Running command "%s"', cmd)
        out = check_output(cmd, shell=True)
        response = out.strip().decode("ascii")
        logger.info('Response "%s" from slurm', response)

        if response.startswith("Submitted batch job "):
            job_id = response.replace("Submitted batch job ", "")
            job_id = int(job_id)
        else:
            job_id = None

        return job_id

    # Add new specific arguments to general method
    # pylint: disable=arguments-differ
    def to_batch_file(self, script_path, overwrite_batch_script=False, **kwargs):
        """Writes sbatch script to file

        Args:
            - path: Path of the sbatch script file.
        """
        if os.path.isfile(script_path) and not overwrite_batch_script:
            raise OSError("Script path '{!s}' already exists".format(script_path))
        sbatch_lines = ["#!" + self.shell + " -l\n"]
        for attr, sbatch in zip(self.attr, self.sbatch):
            value = getattr(self, attr)
            if value is not None:
                line = "#SBATCH --" + sbatch + "=" + str(value) + "\n"
                sbatch_lines.append(line)

        sbatch_lines.append("\nexport OMP_NUM_THREADS=2\n\n")

        # Write sruns to file
        batchdir = os.path.join(self.parent_dir, self.name)
        for ii, run_instance in enumerate(self.runlist):
            sbatch_lines.append(
                '\necho "Starting job {:d}/{:d}"'.format(ii + 1, len(self.runlist))
            )
            sbatch_lines.append("\n" + run_instance.to_batch_string(batchdir))
        sbatch_lines.append('\necho "All jobs done!"\n')

        with open(script_path, "w") as file:
            file.writelines(sbatch_lines)

    # Add new specific arguments to general method
    # pylint: disable=arguments-differ
    @classmethod
    def from_batch_file(cls, path, **kwargs):
        """ Reconstruct sbatch from sbatch file """
        srun_strings = []
        batch_dict = {}
        with open(path, "r") as file:
            for line in file:
                if line.startswith("#SBATCH --"):
                    line = line.lstrip("#SBATCH --")
                    name, value = line.split("=")
                    value = str_to_number(value.strip())
                    if name in cls.sbatch:
                        batch_dict[cls.attr[cls.sbatch.index(name)]] = value
                        # setattr(new, cls.attr[cls.sbatch.index(name)], value)
                if line.startswith(cls.run_class.runstring):
                    srun_strings.append(line)

        # try:
        #    getattr(new, 'repo')
        # except AttributeError:
        #    setattr(new, 'repo', None)

        # new.vcores_per_node = new.tasks_per_node * new.vcores_per_task
        batch_dir = os.path.dirname(os.path.abspath(path))
        batch_name = os.path.basename(batch_dir)
        batch_parent = os.path.dirname(batch_dir)
        nodes = batch_dict.pop("nodes")
        try:
            runlist = []
            for srun_string in srun_strings:
                runlist.append(
                    cls.run_class.from_batch_string(srun_string, batch_dir, nodes=nodes)
                )
        except FileNotFoundError:
            raise Exception(
                "Could not reconstruct run from string: {!s}".format(srun_string)
            )

        check_vars = {}
        for var in ["tasks_per_node", "name"]:
            if var in batch_dict:
                check_vars[var] = batch_dict.pop(var)

        batch = Batch(batch_parent, batch_name, runlist, **batch_dict)
        return batch

    @classmethod
    def from_dir(cls, batchdir, run_kwargs=None, batch_kwargs=None, **kwargs):
        if batch_kwargs is None:
            batch_kwargs = {}
        if run_kwargs is None:
            run_kwargs = {}
        path = os.path.join(batchdir, cls.scriptname)
        try:
            new = cls.from_batch_file(path, **batch_kwargs)
        except FileNotFoundError:
            logger.warning("%s not found! Falling back to subdirs", path)
            new = cls.from_subdirs(batchdir, run_kwargs=run_kwargs)
        return new

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        return NotImplemented

    @staticmethod
    def get_all_running_jobs():
        return Batch.get_running_jobs(user="all")

    @staticmethod
    def get_running_jobs(user=None):
        cmd = "squeue "
        if user is None:
            user = getpass.getuser()
        if user != "all":
            cmd += "-u {!s} ".format(user)

        # See `man squeue`
        # -o: -o <output_format>, --format=<output_format>
        #
        # squeue -i format descriptions for `slurm 20.02.2` (Marconi). Shortened
        # Note that opposed to the docs, it's not a minimum fields size, but a maximum
        #
        # %a: Account associated with the job.
        # %A: Job  id.   This will have a unique value for each element of job arrays.
        # %B: Executing (batch) host.
        # %D: Number  of  nodes  allocated  to  the job or the minimum number of nodes
        #     required by a pending job.
        # %g: Group name of the job.
        # %j: Job or job step name.  (Valid for jobs and job steps)
        # %k: Comment associated with the job.  (Valid for jobs only)
        # %l: Time  limit  of  the job or job step in days-hours:minutes:seconds.
        # %M: Time  used  by  the  job or job step in days-hours:minutes:seconds.
        # %N: List of nodes allocated to the job or job step. # Printed by %R as well
        # %P: Partition of the job or job step.
        # %q: Quality of service associated with the job.
        # %r: The reason a job is in its current state.
        # %R: For  pending  jobs: the reason a job is waiting for execution is printed
        #     within parenthesis.  For terminated jobs with failure: an explanation as
        #     to  why the job failed is printed within parenthesis.  For all other job
        #     states: the list of allocate nodes.  See the JOB  REASON  CODES  section
        #     below for more information.  (Valid for jobs only)
        # %T: Job state in extended form.  See the JOB STATE CODES section below for a
        #     list of possible states.  (Valid for jobs only)
        # %u: User name for a job or job step.  (Valid for jobs and job steps)
        # %V: The job's submission time.
        # %Z: The job's working directory.
        squeue_fmt = (
            "%8A;%12u;%45j;%11M;%15a;%13P;%6r;%20R;%13q;%11B;%5D;%12g;%7k;%11l;%T;%Z"
        )
        cmd += '-o "{!s}"'.format(squeue_fmt)

        out: CompletedProcess = sprun(cmd, stdout=PIPE, stderr=PIPE, shell=True)

        # squeue always has exit code 0
        if out.stderr != b"":
            msg = 'Command "{!s}" failed with stderr "{!s}"'.format(cmd, out.stderr)
            raise SlurmError(msg, out.stderr)

        # Convert shell strings to a list of regular python3 strings
        queue_strings = out.stdout.decode("UTF-8")
        queue_strings = queue_strings.split("\n")

        headers = [field.strip() for field in queue_strings[0].split(";")]

        if "JOBID" not in headers:
            raise Exception("Could not find job-id, abort!")

        jobs = {}

        for jobstring in queue_strings[1:]:
            if jobstring != "":
                # Parse a string which contains ; separated values
                # Use the first field as unique ID
                logger.debug("Parsing {!r}", jobstring)
                job_parts = jobstring.split(";")
                job_uid = job_parts[0]
                logger.debug("UID for this job is {!r}", job_uid)
                job_id = int(job_uid)
                job_vars = {
                    head: job_part.strip()
                    for head, job_part in zip(headers[1:], job_parts[1:])
                }
                jobs[job_id] = job_vars

        jobs_frame = pd.DataFrame(jobs).T
        jobs_frame.index.names = (headers[0],)
        return jobs_frame


def str_to_number(string):
    """ Convert a string in a float or int if possible """
    try:
        value = float(string)
    except ValueError:
        value = string
    else:
        if value.is_integer:
            value = int(value)
    return value
