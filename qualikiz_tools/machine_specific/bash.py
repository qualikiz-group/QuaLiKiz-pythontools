import subprocess
import multiprocessing as mp
import os
import stat
import logging
import re
from pathlib import Path

from IPython import embed

from qualikiz_tools.setup_logging import connect_formatter
from qualikiz_tools.machine_specific.system import Run as SystemRun
from qualikiz_tools.machine_specific.system import Batch as SystemBatch
from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun

logger = logging.getLogger(__name__)
connect_formatter(logger)


def get_num_threads():
    """Returns amount of threads/virtual cores on current system"""
    return mp.cpu_count()


def get_vcores():
    """Get a list of virtual cores from /proc/cpuinfo"""
    with open("/proc/cpuinfo", "r") as file_:
        vcores = []
        for line in file_:
            if line.startswith("physical id"):
                phys_id = int(line.split(":")[1])
            if line.startswith("core id"):
                core_id = int(line.split(":")[1])
                vcores.append((phys_id, core_id))
    num_vcores = len(vcores)
    if num_vcores != get_num_threads():
        raise Exception(
            "Amount of threads != amount of virtual cores. Probably error in reading /proc/cpuinfo"
        )
    return vcores


def get_num_cores():
    """Get the amount of physical cores of the current machine"""
    try:
        numcores = len(set(get_vcores()))
    except FileNotFoundError:
        # Assume we are on macOS
        cmd = "sysctl -a | grep machdep.cpu | grep core_count"
        res = subprocess.check_output(cmd, shell=True).decode("UTF-8")
        numcores = int(re.findall(r":\s(\d+)", res)[0])
    return numcores


class Run(SystemRun):
    """ Defines the run command """

    runstring = "mpirun"
    defaults = {
        "cores_per_node": get_num_cores(),
        "threads_per_core": 2,
        "HT": False,
        "nodes": 1,
    }

    def __init__(
        self,
        parent_dir,
        name,
        binaryrelpath,
        qualikiz_plan=None,
        stdout=None,
        stderr=None,
        verbose=False,
        nodes=1,
        HT=False,
        tasks=None,
        **kwargs
    ):
        """Initializes the Run class

        Args:
            parent_dir:     Directory the run lives in
            name:           Name of the run. Will also be the folder name
            binaryrelpath:  Path of the binary relative to the run dir
        Kwargs:
            qualikiz_plan:  The QuaLiKizPlan instance used to generate input
            HT:             Use hyperthreading. [Default: True]
            stdout:         Standard target of redirect of STDOUT [default: terminal]
            stderr:         Standard target of redirect of STDERR [default: terminal]
            verbose:        Verbose output while creating the Run [default: False]
            **kwargs:       kwargs past to superclass
        """
        if stdout is None:
            stdout = "STDOUT"
        if stderr is None:
            stderr = "STDERR"
        super().__init__(
            parent_dir,
            name,
            binaryrelpath,
            qualikiz_plan=qualikiz_plan,
            stdout=stdout,
            stderr=stderr,
            verbose=verbose,
            **kwargs
        )

        self.nodes = nodes
        for varname in ["HT", "threads_per_core"]:
            if varname in self.defaults:
                setattr(self, varname, self.defaults[varname])
            else:
                setattr(self, varname, None)
        if self.nodes is None:
            nodes = self.defaults["nodes"]
        ncores = self.defaults["cores_per_node"] * nodes
        tasks_full = self.calculate_tasks(
            ncores, HT=self.HT, threads_per_core=self.threads_per_core
        )
        if tasks is None:
            self.tasks = tasks_full
        else:
            if tasks != tasks_full:
                logger.warning(
                    "Using %s task(s), but nodes support %s tasks",
                    tasks,
                    tasks_full,
                )
            self.tasks = tasks

    # Add new specific arguments to general method
    def to_batch_string(self, batch_dir):  # pylint: disable=arguments-differ
        """Create string to include in batch job

        This string will be used in the batch script file that runs the jobs.

        Args:
            batch_dir: Directory the batch script lives in. Needed to
                       generate the relative paths.
        """
        paths = []
        for path in [self.stdout, self.stderr]:
            if os.path.isabs(path):
                pass
            else:
                path = os.path.normpath(
                    os.path.join(os.path.relpath(self.rundir, batch_dir), path)
                )
            paths.append(path)
        if self.binaryrelpath is None:
            raise FileNotFoundError(
                "No binary rel path specified, could not find link to QuaLiKiz binary in {!s}".format(
                    self.rundir
                )
            )

        string = " ".join(
            [
                self.runstring,
                "-n",
                str(self.tasks),
                "-wdir",
                os.path.normpath(os.path.relpath(self.rundir, batch_dir)),
                "./" + os.path.basename(self.binaryrelpath),
            ]
        )
        if self.stdout != "STDOUT":
            string += " > " + paths[0]
        if self.stderr != "STDERR":
            string += " 2> " + paths[1]
        return string

    # Add new specific arguments to general method
    # pylint: disable=arguments-differ
    @classmethod
    def from_batch_string(cls, string, batchdir, **kwargs):
        """Reconstruct the Run from a batch string

        Reverse of to_batch_string. Used to reconstruct the run from a batch script.

        Args:
            string:     The string to parse
            batchdir:   The directory of the containing batch script

        Returns:
            The reconstructed Run instance
        """
        split = string.split(" ")
        dict_ = {}

        tasks = int(split[2])
        rundir = os.path.join(batchdir, split[4])
        binary_name = split[5].strip()
        binaryrelpath = os.readlink(os.path.join(rundir, binary_name))
        paths = []
        for path_index in [7, 9]:
            try:
                path = split[path_index]
            except IndexError:
                path = None
            else:
                if not os.path.isabs(path):
                    path = os.path.relpath(os.path.join(batchdir, path), rundir)
            paths.append(path)
        return cls.from_dir(rundir, stdout=paths[0], stderr=paths[1], **kwargs)

    @classmethod
    def from_dir(cls, dirpath, **kwargs):
        stdout = kwargs.pop("stdout", None)
        stderr = kwargs.pop("stderr", None)
        nodes = kwargs.pop("nodes", None)
        parameterspath = os.path.join(dirpath, cls.parameterspath)
        qualikiz_run = QuaLiKizRun.from_dir(
            dirpath, stdout=stdout, stderr=stderr, **kwargs
        )
        parent_dir = os.path.dirname(qualikiz_run.rundir)
        name = os.path.basename(qualikiz_run.rundir)
        if qualikiz_run.stdout == QuaLiKizRun.default_stdout:
            qualikiz_run.stdout = None
        if qualikiz_run.stderr == QuaLiKizRun.default_stderr:
            qualikiz_run.stderr = None
        return cls(
            parent_dir,
            name,
            qualikiz_run.binaryrelpath,
            stdout=qualikiz_run.stdout,
            stderr=qualikiz_run.stderr,
            qualikiz_plan=qualikiz_run.qualikiz_plan,
            nodes=nodes,
            **kwargs
        )

    def launch(self, clean=True):
        """Launch QuaLiKizRun using mpirun

        Special variables self.stdout == 'STDOUT' and self.stderr == 'STDERR'
        will output to terminal.
        """
        self.inputbinaries_exist()
        # Check if batch script is generated
        if clean:
            self.clean()

        cmd = " ".join(
            [
                "cd",
                self.rundir,
                "&&",
                self.runstring,
                "-n",
                str(self.tasks),
                "./" + os.path.basename(self.binaryrelpath),
            ]
        )
        if self.stdout == "STDOUT":
            stdout = None
        else:
            stdout = open(os.path.join(self.rundir, self.stdout), "w")
        if self.stderr == "STDERR":
            stderr = None
        else:
            stderr = open(os.path.join(self.rundir, self.stderr), "w")
        subprocess.check_call(cmd, shell=True, stdout=stdout, stderr=stderr)


class Batch(SystemBatch):
    """Defines a batch job

    Class Variables:
        shell:            The shell to use for batch scripts.
                          Tested only with bash
        run_class:        class that represents the runs contained in batch
    """

    shell = "/bin/bash"
    run_class = Run

    def __init__(
        self,
        parent_dir,
        name,
        runlist,
        stdout=None,
        stderr=None,
        style="sequential",
        verbose=False,
    ):
        """Initialize batch job

        Args:
            parent_dir:     Directory the batch lives in
            name:           Name of the batch. Will also be the folder name
            runlist:        List of runs contained in this batch

        Kwargs:
            stdout:         Standard target of redirect of STDOUT [default: terminal]
            stderr:         Standard target of redirect of STDERR [default: terminal]
            style:          How to glue the different runs together. Currently
                            only 'sequential' is used
            verbose:        Verbose output while creating the Run [default: False]
            **kwargs:       kwargs past to superclass

        Kwargs:
            - stdout:     File to write stdout to. By default 'stdout.batch'
            - stderr:     File to write stderr to. By default 'stderr.batch'
        """

        if not all([run.__class__ == self.run_class for run in runlist]):
            raise Exception("Runs are not of class {!s}".format(self.run_class))
        if stdout is None:
            stdout = "STDOUT"
        if stderr is None:
            stderr = "STDERR"
        super().__init__(parent_dir, name, runlist, stdout=stdout, stderr=stderr)

        if style == "sequential":
            pass
        else:
            raise NotImplementedError("Style {!s} not implemented yet.".format(style))

    # pylint: disable=arguments-differ
    def to_batch_file(self, path, overwrite_batch_script=False):
        """Writes batch script to file

        Args:
            path:       Path of the sbatch script file.
        """
        if len(self.runlist) == 0:
            raise RuntimeError("Runlist is empty, cannot generate batch file")
        if overwrite_batch_script:
            try:
                os.remove(path)
            except FileNotFoundError:
                pass

        batch_lines = ["#!" + self.shell + "\n\n"]

        # Write sruns to file
        batch_lines.append("export OMP_NUM_THREADS=2\n")
        batch_lines.append("export OMP_STACKSIZE=100000\n\n")
        batch_lines.append(
            'echo "Starting job {:d}/{:d}"\n'.format(1, len(self.runlist))
        )
        batch_lines.append(self.runlist[0].to_batch_string(os.path.dirname(path)))
        for ii, run in enumerate(self.runlist[1:]):
            batch_lines.append(
                ' &&\necho "Starting job {:d}/{:d}"'.format(ii + 2, len(self.runlist))
            )
            batch_lines.append(" &&\n" + run.to_batch_string(os.path.dirname(path)))

        if overwrite_batch_script:
            try:
                os.remove(path)
            except FileNotFoundError:
                pass

        with open(path, "w") as file:
            file.writelines(batch_lines)

        st = os.stat(path)
        os.chmod(path, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

    @classmethod
    def from_batch_file(cls, path, **kwargs):
        """ Reconstruct batch from batch file """
        name = os.path.basename(os.path.dirname(path))
        parent_dir = os.path.dirname(os.path.dirname(path))
        run_strings = []
        with open(path, "r") as file:
            for line in file:
                if line.startswith(Run.runstring):
                    run_strings.append(line)

        runlist = []
        for run_string in run_strings:
            runlist.append(
                Run.from_batch_string(run_string, os.path.join(parent_dir, name))
            )
        batch = Batch(parent_dir, name, runlist, **kwargs)

        return batch

    @classmethod
    def from_dir(cls, dirpath, *args, **kwargs):
        return cls.from_subdirs(dirpath, *args, **kwargs)

    def launch(self, clean=True):
        """Launch QuaLiKizBatch using a batch script with mpirun"""
        dirname = Path().absolute().name
        self.inputbinaries_exist()
        # Check if batch script is generated
        batchdir = self.parent_dir / self.name
        script_path = batchdir / self.scriptname
        if not os.path.exists(script_path):
            logger.warning("Batch script does not exist! Generating.. in %s", batchdir)
            self.to_batch_file(os.path.join(script_path))

        if clean:
            self.clean()

        cmd = f"cd {batchdir} && bash {self.scriptname}"
        if not batchdir.exists():
            raise OSError("Trying to run from non-existent batch directory", batchdir)
        if self.stdout == "STDOUT":
            stdout = None
        else:
            stdout = open(os.path.join(batchdir, self.stdout), "w")
        if self.stderr == "STDERR":
            stderr = None
        else:
            stderr = open(os.path.join(batchdir, self.stderr), "w")
        subprocess.check_call(cmd, shell=True, stdout=stdout, stderr=stderr)
