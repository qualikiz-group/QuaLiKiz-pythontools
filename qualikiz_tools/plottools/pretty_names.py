"""
Copyright Dutch Institute for Fundamental Energy Research (2016-2017)
Contributors: Karel van de Plassche (karelvandeplassche@gmail.com)
License: CeCILL v2.1
"""
from pathlib import Path
from os import path
import collections
import logging

from IPython import embed

logger = logging.getLogger(__name__)

plot_styles = {
    "gam_GB": ["Growth Rate [GB]", "wavenumber", r"growth rate [$\sqrt{T_e/mi}/a$]"],
    "gam_SI": ["Growth Rate [SI]", "wavenumber", r"growth rate [$\sqrt{T_e/mi}/a$]"],
    "ome_GB": ["Frequencies [GB]", "wavenumber", r"frequency [?]"],
    "ome_SI": ["Frequencies [SI]", "wavenumber", r"frequency [$s^-1$]"],
    "ief_GB": [
        "Ion Heat Conductivity [GB]",
        "ions",
        r"heat conductivity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "ief_SI": ["Ion Heat Flux [SI]", "ions", r"heat flux [$W/m^2$]"],
    "eef_GB": [
        "Electron Heat Conductivity [GB]",
        "electrons",
        r"heat conductivity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "eef_SI": ["Electron Heat Flux [SI]", "electrons", r"heat flux [$W/m^2$]"],
    "eefETG_SI": ["Electron Scale Heat Flux [SI]", "electrons", r"heat flux [$W/m^2$]"],
    "ipf_GB": [
        "Ion Particle Diffusivity [GB]",
        "ions",
        r"particle diffusifity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "ipf_SI": ["Ion Particle Flux [SI]", "ions", r"particle flux [$m^-2 s^-1$]"],
    "epf_GB": [
        "Electron Particle Diffusivity [GB]",
        "electrons",
        r"particle diffusifity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "epf_SI": [
        "Electron Particle Flux [GB]",
        "electrons",
        r"particle flux [$m^-2 s^-1$]",
    ],
    "epfETG_SI": [
        "Electron Scale Particle Flux [GB]",
        "electrons",
        r"particle flux [$m^-2 s^-1$]",
    ],
    "ivf_GB": [
        "Ion Momentum Diffusivity [GB]",
        "ions",
        r"momentum diffusifity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "ivf_SI": ["Ion Momentum Flux [SI]", "ions", r"momentum flux [$N s m^-2 s^-1$]"],
    "evf_GB": [
        "Electron Momentum Diffusivity [GB]",
        "electrons",
        r"momentum diffusifity [$\sqrt{mi}T_e^{1.5}/(q_e^2B^2a)$]",
    ],
    "evf_SI": [
        "Electron Momentum Flux [SI]",
        "electrons",
        r"momentum flux [$N s m^-2 s^-1$]",
    ],
}

prims = {
    "gam": ["growth rate", "SI", "GB"],
    "ome": ["frequencies", "SI", "GB"],
    "ef": ["heat", "conductivity", "flux"],
    "efETG": ["heat", "conductivity", "flux"],
    "pf": ["particle", "diffusivity", "flux"],
    "pfETG": ["particle", "diffusivity", "flux"],
    "vf": ["momentum", "diffusivity", "flux"],
    "df": ["", "diffusivity", None],
    "vt": ["particle", "thermopinch", None],
    "vr": ["particle", "rotodiffusion pinch", None],
    "vc": ["particle", "compressebility pinch", None],
    "chie": ["heat", "conductivity", None],
    "ven": ["heat", "thermopinch", None],
    "ver": ["heat", "rotodiffusion pinch", None],
    "vec": ["heat", "compressebility pinch", None],
    "ecoefs": ["math", "math", None],
    "ck": ["math", "math", None],
    "cek": ["math", "math", None],
    "cftrans": ["math", "math", None],
    "npol": ["math", "math", None],
}

oldbase_translate = [
    ("one", "two"),
]


def listify(*args: tuple, **kwargs: dict):
    return list(args)


def plotWrapper(file_path, label: str):
    file_path = Path(file_path)
    return (file_path, label)


AnalyzedBase = collections.namedtuple(
    "AnalyzedBase", "primitive ionelec mode unit label"
)


def analyze_basename(basename: str, label=""):
    """Analyze a given basename, the name of a path without the suffix"""
    assert isinstance(basename, str)
    base, __, unit = basename.partition("_")

    if unit == "" or unit is None:
        unit = "?"

    # Analyze modes and variable names
    potential_mode = base[-3:]
    if potential_mode in ["TEM", "ITG", "ETG", ""]:
        if potential_mode == "TEM":
            mode = "TEM"
            base = base[:-3]
            ionelec = "?"
        elif potential_mode == "ITG":
            mode = "ITG"
            base = base[:-3]
            ionelec = "?"
        elif potential_mode == "ETG":
            mode = "ETG"
            base = base[:-3]
            ionelec = "?"
        elif potential_mode == "":
            mode = "total"
    else:
        mode = "total"

    # We stripped the mode (TEM/ITG/ETG) here
    if base == "primitive":
        # The primitive folder
        prim = base
        ionelec = "NaN"
        mode = "NaN"
        unit = "NaN"
        label = "Primitive folder"
    elif base in ["ome", "gam"]:
        # A spectrum, so this typically depends on dimn and dimx
        prim = base
        ionelec = "ie"
    elif base.endswith("e") or base.endswith("i"):
        # The default case. Probably something like "efi_GB"
        # Look for the electron/ion identifier
        ionelec = base[-1]
        prim = base[:-1]
    else:
        prim = base
        ionelec = "?"
        unit = "?"
        logger.info(
            f"Found case {basename}, unsure how to continue the analysis, skipping!"
        )

    logger.info(
        f"Basename analyzer found prim={prim} ionelec={ionelec} mode={mode} unit={unit} label={label}"
    )
    assert ionelec in [
        "i",
        "e",
        "ie",
        "?",
        "NaN",
    ], f"ionelec {ionelec } not part of allowed ionelec values"
    assert mode in [
        "",
        "total",
        "TEM",
        "ITG",
        "ETG",
        "?",
        "NaN",
    ], f"mode {mode } not part of allowed mode values"
    assert unit in [
        "SI",
        "GB",
        "cm",
        "?",
        "NaN",
    ], f"unit {unit} not part of allowed mode values"
    assert isinstance(label, str)
    assert (
        prim in prims or prim == "primitive"
    ), f"Prim {prim} can not be found in dict of prims qualikiz_tools.plottools.pretty_names.prims"

    analyzed_base = AnalyzedBase(prim, ionelec, mode, unit, label)
    return analyzed_base


def create_title_from_analyzed_base(analyzed_base: AnalyzedBase):
    prim, ionelec, mode, unit, label = analyzed_base
    logger.info(
        f"Title creator found prim={prim} ionelec={ionelec} mode={mode} unit={unit} label={label}"
    )

    title = ""
    if prim in ("gam", "ome"):
        title = prims[prim][0] + " "
        if unit == "SI":
            title += prims[prim][1]
        elif unit == "GB":
            title += prims[prim][2]
    elif not unit == "SI" and not unit == "GB":
        # We are not sure what to do if the suffix is not SI nor GB, just skip it for mow
        logger.info(f"Skipping base {analyzed_base}")
        ionelec = "?"
        mode = "?"
        title = prim
    else:
        # Start build the title
        if ionelec == "i":
            title += "Ion "
        elif ionelec == "e":
            title += "Electron "

        title += prims[prim][0] + " "  # e.g. Particle or Heat

        if unit == "SI":
            title += str(prims[prim][1]) + " [SI]"
        elif unit == "GB":
            title += str(prims[prim][2]) + " [GB]"

    # plot_element: dict = plotWrapper(file_path, title)
    return title


def parse_file_name(file_path: Path):
    temp = file_path.name
    suff = file_path.suffix
    name = file_path.stem
    analyzed_base = analyze_basename(name)
    analyzed_base = create_title_from_analyzed_base(analyzed_base)
    return analyzed_base


def parse_file_list_dir(file_list_dir: Path):
    if not file_list_dir.exists():
        raise Exception(f"Given directory {file_list_dir} does not exist!")

    files = Path(file_list_dir).glob("*")
    file_list = list(files)
    if not len(file_list):
        raise Exception(f"Given directory {file_list_dir} is empty!")

    output = {}
    for file_path in file_list:
        assert file_path.exists()
        plot_element = parse_file_name(file_path)
        name = file_path.stem
        if name in output:
            output[name].append(plot_element)
        else:
            output[name] = listify(plot_element)

    assert len(file_list) == len(
        output
    ), f"Could not read every file in {file_list_dir}!"
    return output
