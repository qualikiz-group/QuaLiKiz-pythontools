import sys

import numpy as np
import xarray as xr
import matplotlib as mpl
from IPython import embed  # pylint: disable=unused-import # noqa: F401

mpl.use("tkAgg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt

from matplotlib import gridspec
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib.widgets import Button
from matplotlib import cm

from qualikiz_tools.plottools.discrete_slider import DiscreteSlider

# pylint: enable=wrong-import-position


def get_x_y(ds, A, Z, scanning_Ani_ion1):

    # Get all dimxs for which the requested impurity Z and A matches
    ds_imp = ds.isel(nions=1)
    ds_sub = ds.isel(dimx=np.isclose(ds_imp["Zi"], Z) & np.isclose(ds_imp["Ai"], A))

    # Get unique Ani and Zeff values for these points
    if scanning_Ani_ion1:
        Ani = np.unique(ds_sub["Ani"].isel(nions=1))
    else:
        Ani = np.unique(ds_sub["Ani"].isel(nions=0))
    Zeff = np.unique(ds_sub["Zeff"])
    ninorm = np.unique(ds_sub["normni"].isel(nions=1))

    return (Ani, Zeff, ninorm)


def read_data_from_netcdf(netcdf_name):
    # Asume the netCDF is already generated
    ds = xr.open_dataset(netcdf_name)

    return ds


def tick_function(Zeff, ninorm, Z, ticks):

    #    This is not ideal, but I do not know how to control the number of ticks in the plots. Might not be very general
    #    print(ticks)
    max_ninorm = max(ninorm)
    ninorm = np.arange(6) / 5 * max_ninorm
    #    f_space = interp1d(Zeff, ninorm, fill_value = 'extrapolate')
    #    print(ninorm)
    #    new_ticks = np.array(f_space(ticks))
    #    print(new_ticks)
    #    new_ticks = list(map(lambda tick: round(tick*Z,4), new_ticks))
    #    print(new_ticks)

    new_ticks = list(map(lambda ninorm: round(ninorm * Z, 4), ninorm))

    return new_ticks


def plot_Zeff_super_scan(
    netcdf_name,
    Ani_min=-20,
    Ani_max=20,
    Zeff_min=1,
    Zeff_max=5,
    fontx=12,
    fonty=12,
    ion=1,
    GB_SI="GB",
    dilution_max=1.0,
    verbosity=2,
    flux_type="particle",
    mode="normal",
    scanning_Ani_ion1=True,
):

    # Read data
    # Open the netcdf file with xarray and read the data needed.

    fig = plt.figure()
    ds = read_data_from_netcdf(netcdf_name)
    if verbosity >= 2:
        print("loaded dataset")

    class ImpurityFigure:
        gs = gridspec.GridSpec(
            16,
            21,
            wspace=0.5,
            hspace=0.05,
            left=0.05,
            right=0.95,
            bottom=0.05,
            top=0.95,
        )
        axes = {
            "Qi": plt.subplot(gs[2:-3, 2:8]),
            "Colorbar Qi": plt.subplot(gs[2:-3, 9]),
            "Qe": plt.subplot(gs[2:-3, 13:-2]),
            "Colorbar Qe": plt.subplot(gs[2:-3, -1]),
            "Q tot button": plt.subplot(gs[0, :2]),
            "Q ITG button": plt.subplot(gs[0, 3:5]),
            "Q TEM button": plt.subplot(gs[0, 6:8]),
            "Q ETG button": plt.subplot(gs[0, 9:11]),
            "Slider A": plt.subplot(gs[-1, :10]),
            "Slider Z": plt.subplot(gs[-1, 12:-1]),
        }

        def __init__(self, fig, ds, fontx, fonty):

            # Initialize variables to be plotted

            self.Z = []
            self.fig = fig
            self.ds = ds

            to_drop = []
            for name, var in self.ds.data_vars.items():
                if var.dims != ("dimx",) and var.dims != ("dimx", "nions"):
                    to_drop.append(name)

            self.ds = self.ds.drop(to_drop)

            #            embed()

            normni_min = 1 - dilution_max

            # Should introduce 'yvaraible' to switch between Zeff and ninorm in the plot. Probably not. Maybe later something more advanced.
            #            print(self.ds)

            np.set_printoptions(threshold=sys.maxsize)

            #            print(self.ds['Zeff'].values)
            #            print(self.ds.isel(nions=1)['Ani'].values)
            #            print(self.ds.isel(nions=1)['normni'].values)

            # Stuff I tryed and did not work. might want to try again once I have time to make this better.

            #            self.ds = self.ds.where(self.ds.isel(nions=1)['normni'] < normni_max, drop = True)
            #            self.ds = self.ds.isel(dimx=(self.ds_ion1['normni'] < normni_max),nions=[0,1])

            #            self.ds = self.ds.isel(dimx=(self.ds_ion0['normni'] >= normni_max))
            #            self.ds = self.ds.where(self.ds.normni[:,0] < normni_max, drop = True)

            #            self.ds = self.ds.isel(dimx=(self.ds_ion1['normni'] < normni_max))

            #            self.ds_test = self.ds_ion0.__set_item__(self.ds_ion1,coords={'nions':('nions'),1})

            #            self.ds_ion0 = self.ds_ion0.expand_dims('nions')
            #            self.ds_ion1 = self.ds_ion1.expand_dims('nions')

            #            self.ds_ion1.coords['nions'] = np.asarray([1])

            #            self.ds_ion0.assign_coords(nions=0)
            #            self.ds_ion1.assign_coords(nions=1)
            #            self.ds_ion1 = self.ds_ion1.coords
            #            self.ds_ion1.coords['nions' = 1]

            #            self.ds_ion0.assign_coords({'nions':(0)})
            #            self.ds_ion1.assign_coords({'nions':(1)})

            #            self.ds_test = self.ds_ion0.merge(self.ds_ion1, join = 'left')

            #            self.ds = self.ds.sel(dimx = self.ds.dimx.where(self.ds.normni.sel(nions=0) > normni_max, drop = True).values)

            #            self.ds.dimx.values = np.arange(len(self.ds.dimx.values))
            #            self.ds = self.ds.sel(dimx = mask.dimx.values)

            print(Zeff_max)
            print(normni_min)

            self.ds_Qi = self.ds["efi_GB"]
            self.ds_Qe = self.ds["efe_GB"]

            self.ds_Qi = self.ds_Qi.loc[
                (self.ds_Qi.isel(nions=0)["normni"] >= normni_min)
            ]
            self.ds_Qi = self.ds_Qi.loc[(self.ds_Qi["Zeff"] <= Zeff_max)]

            self.ds_Qi_ion0 = self.ds_Qi.isel(nions=0)
            self.ds_Qi_ion1 = self.ds_Qi.isel(nions=1)

            # Using a mask to retain information about ninorm

            mask_ds = self.ds["efi_GB"]

            if flux_type == "heat":

                if GB_SI == "GB":
                    self.ds_Qi_tot = self.ds["efi_GB"]
                    self.ds_Qe_tot = self.ds["efe_GB"]
                    self.ds_Qi_ITG = self.ds["efiITG_GB"]
                    self.ds_Qe_ITG = self.ds["efeITG_GB"]
                    self.ds_Qi_TEM = self.ds["efiTEM_GB"]
                    self.ds_Qe_TEM = self.ds["efeTEM_GB"]
                    self.ds_Qe_ETG = self.ds["efeETG_GB"]
                elif GB_SI == "SI":
                    self.ds_Qi_tot = self.ds["efi_SI"]
                    self.ds_Qe_tot = self.ds["efe_SI"]
                    self.ds_Qi_ITG = self.ds["efiITG_SI"]
                    self.ds_Qe_ITG = self.ds["efeITG_SI"]
                    self.ds_Qi_TEM = self.ds["efiTEM_SI"]
                    self.ds_Qe_TEM = self.ds["efeTEM_SI"]
                    self.ds_Qe_ETG = self.ds["efeETG_SI"]

            if flux_type == "particle":

                if GB_SI == "GB":
                    self.ds_Qi_tot = self.ds["pfi_GB"]
                    self.ds_Qe_tot = self.ds["pfe_GB"]
                    self.ds_Qi_ITG = self.ds["pfiITG_GB"]
                    self.ds_Qe_ITG = self.ds["pfeITG_GB"]
                    self.ds_Qi_TEM = self.ds["pfiTEM_GB"]
                    self.ds_Qe_TEM = self.ds["pfeTEM_GB"]
                    self.ds_Qe_ETG = self.ds["efeETG_GB"]
                elif GB_SI == "SI":
                    self.ds_Qi_tot = self.ds["pfi_SI"]
                    self.ds_Qe_tot = self.ds["pfe_SI"]
                    self.ds_Qi_ITG = self.ds["pfiITG_SI"]
                    self.ds_Qe_ITG = self.ds["pfeITG_SI"]
                    self.ds_Qi_TEM = self.ds["pfiTEM_SI"]
                    self.ds_Qe_TEM = self.ds["pfeTEM_SI"]
                    self.ds_Qe_ETG = self.ds["efeETG_SI"]

            self.ds_Qi_tot.load()

            fluxes = [
                self.ds_Qi_tot,
                self.ds_Qi_ITG,
                self.ds_Qi_TEM,
                self.ds_Qe_tot,
                self.ds_Qe_ITG,
                self.ds_Qe_TEM,
                self.ds_Qe_ETG,
            ]
            new_fluxes = []

            self.A = np.unique(self.ds_Qi_ion1["Ai"])

            for single_A in self.A:
                self.ds_A = self.ds_Qi_ion1.loc[(self.ds_Qi_ion1["Ai"] == single_A)]
                self.Z.append(np.unique(self.ds_A["Zi"]))

            ds_Qi, A, Z = self.ds_Qi, self.A, self.Z

            # Should add something that automatically detects if it is ion flux or electron flux

            # Kills the A and Z scan that only have one value in Zeff, since they cannot be plotted
            # Just building the mask now

            mask_ds = mask_ds.loc[(mask_ds.isel(nions=0)["normni"] >= normni_min)]
            mask_ds = mask_ds.loc[(mask_ds["Zeff"] <= Zeff_max)]

            def mask_single_Zeff_lines(mask_ds, A, Z):
                for single_A in A:
                    A_index = int(np.where(A == single_A)[0])
                    for single_Z in Z[A_index]:
                        mask_ds_A_Z = mask_ds.loc[
                            (
                                (mask_ds.isel(nions=1)["Zi"] == single_Z)
                                & (mask_ds.isel(nions=1)["Ai"] == A[A_index])
                            )
                        ]
                        print("processing " + str(single_A) + " " + str(single_Z))
                        Ani, Zeff, ninorm = get_x_y(
                            mask_ds_A_Z, single_A, single_Z, scanning_Ani_ion1
                        )
                        if len(Ani) == len(mask_ds_A_Z.values):
                            print("deleating data because cannot plot a line in 2D")
                            mask_ds = mask_ds.loc[
                                (mask_ds.isel(nions=1)["Zi"] != single_Z)
                                & (mask_ds.isel(nions=1)["Ai"] != single_A)
                            ]

                return mask_ds

            mask_ds = mask_single_Zeff_lines(mask_ds, A, Z)

            # here kill if second ion is Deuterium or Hydrogen (no impurities! Red and blu D, not useful)

            mask_ds = mask_ds.loc[mask_ds.isel(nions=1)["Zi"] != 1]

            print(
                "I am deleating the second ion if the charge is one, do not want to do red and blu deuterium"
            )

            new_fluxes = []

            for flux in fluxes:
                flux = flux.sel(dimx=mask_ds.dimx)
                new_fluxes.append(flux)

            # Applying the mask

            self.ds_Qi_tot = new_fluxes[0]
            self.ds_Qi_ITG = new_fluxes[1]
            self.ds_Qi_TEM = new_fluxes[2]
            self.ds_Qe_tot = new_fluxes[3]
            self.ds_Qe_ITG = new_fluxes[4]
            self.ds_Qe_TEM = new_fluxes[5]
            self.ds_Qe_ETG = new_fluxes[6]

            #            self.ds = self.ds.stack(index=('dimx','nions')).isel(index=mask).unstack('index')

            #            self.ds = self.ds.stack(index=('dimx','nions')).isel(index=np.tile((self.ds_ion1['normni'] < normni_max),2)).unstack('index')
            #            self.ds = self.ds.stack(index=('dimx','nions')).isel(index=np.tile((self.ds.isel(nions=0)['normni'] < normni_max),2)).unstack#            self.ds_ion1 = self.ds_ion1.isel(dimx=(self.ds_ion0['normni'] >= normni_max))

            #            self.ds = self.ds.isel(dimx=(self.ds_ion1['normni'] < normni_max))
            #            self.ds = self.ds.isel(dimx=ds_mask)

            #            for var in ds.data_vars.dat():

            # MODIFY HERE, NEED TO TAKE UPDATED A AND Z

            self.A = np.unique(self.ds_Qi_tot.isel(nions=1)["Ai"])
            self.Z = []

            for single_A in self.A:
                self.ds_A = self.ds_Qi_tot.isel(nions=1).loc[
                    (self.ds_Qi_tot.isel(nions=1)["Ai"] == single_A)
                ]
                self.Z.append(np.unique(self.ds_A["Zi"]))

            # Kills the A and Z scan that only have one value in Zeff, since they cannot be plotted

            # NEED TO DETECT WHICH IS Y AND WHICH IS X IN THE TWO CASES

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot, self.A[0], self.Z[0][0], scanning_Ani_ion1
            )
            self.x = self.Ani
            self.y = self.Zeff

            #           Only the Z and A selected. Here 0 0 because we are initializing

            self.Qi = self.ds_Qi_tot.copy(deep=True)
            self.Qe = self.ds_Qe_tot.copy(deep=True)

            #           Should probably not be self.

            self.val_A = self.A[0]
            self.val_Z = self.Z[0][0]

            self.A_index = 0
            self.Z_index = 0

            self.ds_Qi_A = self.Qi.loc[(self.Qi.isel(nions=1)["Ai"] == self.A[0])]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
            ]
            self.ds_Qe_A = self.Qe.loc[(self.Qi.isel(nions=1)["Ai"] == self.A[0])]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
            ]

            # Adding options to have ratio or sum of ions

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[0][0])
                    ]
                )
            #           Need a common variable changed on the run which stores tot, ITG, ETG ecc depending on which one is shown

            self.val_A = self.A[0]
            self.val_Z = self.Z[0][0]

            self.A_index = 0
            self.Z_index = 0
            # working

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            #            z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))

            self.cmap = cm.get_cmap("plasma", 256)
            self.fontx = fontx
            self.fonty = fonty

            #           add levels and norm with ITG and ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(r"Ions, tot, " + GB_SI)
            self.countour_axis_Qe.set_title(r"Electrons, tot " + GB_SI)

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.slider_A_axis = self.axes["Slider A"]
            self.slider_A = DiscreteSlider(
                self.slider_A_axis,
                "A",
                self.A[0],
                self.A[-1],
                allowed_vals=self.A,
                valinit=self.A[0],
            )
            self.slide_A = self.slider_A.on_changed(self.update_A)

            self.slider_Z_axis = self.axes["Slider Z"]
            self.slider_Z = DiscreteSlider(
                self.slider_Z_axis,
                "Z",
                self.Z[0][0],
                self.Z[0][-1],
                allowed_vals=self.Z[0],
                valinit=self.Z[0][0],
            )
            self.slide_Z = self.slider_Z.on_changed(self.update_Z)

        def show_Q_tot(self):

            self.Qi = self.ds_Qi_tot.copy(deep=True)
            self.Qe = self.ds_Qe_tot.copy(deep=True)

            self.A_index = int(np.where(self.A == self.val_A)[0])
            self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]
            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[A_index][Z_index])
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[A_index][Z_index])
                    ]
                )

            self.axes["Qi"] = plt.subplot(self.gs[2:-3, 2:8])
            self.axes["Qe"] = plt.subplot(self.gs[2:-3, 13:-2])
            self.axes["Colorbar Qi"] = plt.subplot(self.gs[2:-3, 9])
            self.axes["Colorbar Qe"] = plt.subplot(self.gs[2:-3, -1])

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            # add levels and norm with ITG and ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.axes["Colorbar Qi"].cla()
            self.axes["Colorbar Qe"].cla()

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            self.countour_axis_Qi.cla()
            self.countour_axis_Qe.cla()
            self.countour_axis_Qi = fig.add_subplot(self.gs[2:-3, 2:8])
            self.countour_axis_Qe = fig.add_subplot(self.gs[2:-3, 13:-2])

            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(r"Ions, tot, " + GB_SI)
            self.countour_axis_Qe.set_title(r"Electrons, tot, " + GB_SI)
            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.fig.canvas.draw_idle()

        # And bla bla to update slide_Z. Slider does not change though

        def show_Q_ITG(self):

            self.Qi = self.ds_Qi_ITG.copy(deep=True)
            self.Qe = self.ds_Qe_ITG.copy(deep=True)

            self.A_index = int(np.where(self.A == self.val_A)[0])
            self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]
            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )

            self.axes["Qi"] = plt.subplot(self.gs[2:-3, 2:8])
            self.axes["Qe"] = plt.subplot(self.gs[2:-3, 13:-2])
            self.axes["Colorbar Qi"] = plt.subplot(self.gs[2:-3, 9])
            self.axes["Colorbar Qe"] = plt.subplot(self.gs[2:-3, -1])

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            # add levels and norm with ITG and ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.axes["Colorbar Qi"].cla()
            self.axes["Colorbar Qe"].cla()

            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(r"Ions, ITG, " + GB_SI)
            self.countour_axis_Qe.set_title(r"Electrons, ITG, " + GB_SI)

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            self.countour_axis_Qi = fig.add_subplot(self.gs[2:-3, 2:8])
            self.countour_axis_Qe = fig.add_subplot(self.gs[2:-3, 13:-2])

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.fig.canvas.draw_idle()

        def show_Q_TEM(self):

            self.Qi = self.ds_Qi_TEM.copy(deep=True)
            self.Qe = self.ds_Qe_TEM.copy(deep=True)

            self.A_index = int(np.where(self.A == self.val_A)[0])
            self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]
            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )

            z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
            z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            self.axes["Qi"] = plt.subplot(self.gs[2:-3, 2:8])
            self.axes["Qe"] = plt.subplot(self.gs[2:-3, 13:-2])
            self.axes["Colorbar Qi"] = plt.subplot(self.gs[2:-3, 9])
            self.axes["Colorbar Qe"] = plt.subplot(self.gs[2:-3, -1])

            # add levels and norm with ITG and ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.axes["Colorbar Qi"].cla()
            self.axes["Colorbar Qe"].cla()

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            self.countour_axis_Qi.cla()
            self.countour_axis_Qe.cla()
            self.countour_axis_Qi = fig.add_subplot(self.gs[2:-3, 2:8])
            self.countour_axis_Qe = fig.add_subplot(self.gs[2:-3, 13:-2])

            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(r"Ions, TEM, " + GB_SI)
            self.countour_axis_Qe.set_title(r"Electrons, TEM, " + GB_SI)

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.fig.canvas.draw_idle()

        def show_Q_ETG(self):

            # Fills ETG ions with zeros

            self.ds_Qi_ETG = self.ds_Qi_tot

            self.Qi = self.ds_Qi_ETG.copy(deep=True)
            self.Qi.values = np.full_like(self.ds_Qi_tot, 0)
            self.Qe = self.ds_Qe_ETG.copy(deep=True)

            self.A_index = int(np.where(self.A == self.val_A)[0])
            self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]
            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qi = np.full_like(z_Qi, 0)
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qi = np.full_like(z_Qi, 0)
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            self.axes["Qi"] = plt.subplot(self.gs[2:-3, 2:8])
            self.axes["Qe"] = plt.subplot(self.gs[2:-3, 13:-2])
            self.axes["Colorbar Qi"] = plt.subplot(self.gs[2:-3, 9])
            self.axes["Colorbar Qe"] = plt.subplot(self.gs[2:-3, -1])

            # add levels and norm with ITG and ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.axes["Colorbar Qi"].cla()
            self.axes["Colorbar Qe"].cla()

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            self.countour_axis_Qi.cla()
            self.countour_axis_Qe.cla()

            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(r"Ions, ETG, " + GB_SI)
            self.countour_axis_Qe.set_title(r"Electrons, ETG, " + GB_SI)

            self.countour_axis_Qi = fig.add_subplot(self.gs[2:-3, 2:8])
            self.countour_axis_Qe = fig.add_subplot(self.gs[2:-3, 13:-2])

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.fig.canvas.draw_idle()

        # And bla bla to update slide_A. Slider does not change though

        def update_A(self, val_A):

            self.val_A = val_A.copy()
            self.A_index = int(np.where(self.A == self.val_A)[0])

            # Find the value in rho that correspond to the val and substitute the data with those

            if self.val_Z not in self.Z[self.A_index]:
                self.val_Z = self.Z[self.A_index][0]
                self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            # later do something with self.Qi and self.Qe so they store the correct variable when you change tot - TEM - ITG
            # Will need to update this to have tot, ITG, TEM, ETG

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            title_Qi = self.countour_axis_Qi.get_title()
            title_Qe = self.countour_axis_Qe.get_title()

            self.countour_axis_Qi.cla()
            self.countour_axis_Qe.cla()

            #            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            #            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(title_Qi)
            self.countour_axis_Qe.set_title(title_Qe)

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            #            print('ninorm e')
            #            print(self.ninorm)
            #            print('prima di entrare')
            #            print(ticks1_Qi)
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.slider_A_axis = self.axes["Slider A"]
            self.slider_Z_axis = self.axes["Slider Z"]

            self.slider_Z_axis.clear()
            self.slider_Z.disconnect(self.slide_Z)
            self.slider_Z_axis = self.axes["Slider Z"]
            self.slider_Z = DiscreteSlider(
                self.slider_Z_axis,
                "Z",
                self.Z[self.A_index][0],
                self.Z[self.A_index][-1],
                allowed_vals=self.Z[self.A_index],
                valinit=self.val_Z,
            )

            self.slide_A = self.slider_A.on_changed(self.update_A)
            self.slide_Z = self.slider_Z.on_changed(self.update_Z)

            self.fig.canvas.draw_idle()

        def update_Z(self, val_Z):

            self.val_Z = val_Z.copy()

            self.A_index = int(np.where(self.A == self.val_A)[0])
            self.Z_index = int(np.where(self.Z[self.A_index] == self.val_Z)[0])

            self.Ani, self.Zeff, self.ninorm = get_x_y(
                self.ds_Qi_tot,
                self.A[self.A_index],
                self.Z[self.A_index][self.Z_index],
                scanning_Ani_ion1,
            )
            self.x = self.Ani
            self.y = self.Zeff

            self.ds_Qi_A = self.Qi.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qi_A_Z = self.ds_Qi_A.isel(nions=ion).loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]
            self.ds_Qe_A = self.Qe.loc[
                (self.Qi.isel(nions=1)["Ai"] == self.A[self.A_index])
            ]
            self.ds_Qe_A_Z = self.ds_Qe_A.loc[
                (self.ds_Qi_A.isel(nions=1)["Zi"] == self.Z[self.A_index][self.Z_index])
            ]

            if mode == "ratio":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    / self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )
                self.ds_Qi_A_Z.values = np.nan_to_num(self.ds_Qi_A_Z.values)
            elif mode == "sum":
                self.ds_Qi_A_Z = (
                    self.ds_Qi_A.isel(nions=0).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                    + self.ds_Qi_A.isel(nions=1).loc[
                        (
                            self.ds_Qi_A.isel(nions=1)["Zi"]
                            == self.Z[self.A_index][self.Z_index]
                        )
                    ]
                )

            z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
            z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))

            if self.ds_Qi_A_Z["Zeff"].values[0] == self.ds_Qi_A_Z["Zeff"].values[1]:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.y), len(self.x))
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.y), len(self.x))
            else:
                z_Qi = self.ds_Qi_A_Z.values.reshape(len(self.x), len(self.y)).T
                z_Qe = self.ds_Qe_A_Z.values.reshape(len(self.x), len(self.y)).T

            # Find the value in rho that correspond to the val and substitute the data with those

            self.levels_Qi = MaxNLocator(nbins=20).tick_values(z_Qi.min(), z_Qi.max())
            self.levels_Qe = MaxNLocator(nbins=20).tick_values(z_Qe.min(), z_Qe.max())
            self.norm_Qi = BoundaryNorm(self.levels_Qi, ncolors=self.cmap.N, clip=True)
            self.norm_Qe = BoundaryNorm(self.levels_Qe, ncolors=self.cmap.N, clip=True)

            self.countour_axis_Qi = self.axes["Qi"]
            self.countour_axis_Qe = self.axes["Qe"]

            title_Qi = self.countour_axis_Qi.get_title()
            title_Qe = self.countour_axis_Qe.get_title()

            self.countour_axis_Qi.cla()
            self.countour_axis_Qe.cla()

            #            self.countour_axis_Qi2 = self.countour_axis_Qi.twinx()
            #            self.countour_axis_Qe2 = self.countour_axis_Qe.twinx()

            self.countour_axis_Qi.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qi.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qi2.set_ylabel(r"dilution [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_ylabel(r"$ Z_{eff} $ [-]", fontsize=self.fonty)
            self.countour_axis_Qe.set_xlabel(
                r"$ R/L_{n_{i}} $ [-]", fontsize=self.fontx
            )
            self.countour_axis_Qe2.set_ylabel(r"dilution [-]", fontsize=self.fonty)

            self.countour_axis_Qi.set_title(title_Qi)
            self.countour_axis_Qe.set_title(title_Qe)

            self.cf_Qi = self.countour_axis_Qi.contourf(
                self.x, self.y, z_Qi, cmap=self.cmap, levels=self.levels_Qi
            )
            self.cf_Qe = self.countour_axis_Qe.contourf(
                self.x, self.y, z_Qe, cmap=self.cmap, levels=self.levels_Qe
            )

            self.cb_Qi = fig.colorbar(
                self.cf_Qi,
                norm=self.norm_Qi,
                cax=self.axes["Colorbar Qi"],
                cmap=self.cmap,
                values=self.levels_Qi,
            )
            self.cb_Qe = fig.colorbar(
                self.cf_Qe,
                norm=self.norm_Qe,
                cax=self.axes["Colorbar Qe"],
                cmap=self.cmap,
                values=self.levels_Qe,
            )

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            ticks1_Qi = self.countour_axis_Qi.get_yticks()
            self.countour_axis_Qi2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qi)
            )

            ticks1_Qe = self.countour_axis_Qe.get_yticks()
            self.countour_axis_Qe2.set_yticklabels(
                tick_function(self.Zeff, self.ninorm, self.val_Z, ticks1_Qe)
            )

            self.fig.canvas.draw_idle()

    class ButtonShowQTot(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_tot(self, event):
            self.impurity_figure.show_Q_tot()

    class ButtonShowQITG(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_ITG(self, event):
            self.impurity_figure.show_Q_ITG()

    class ButtonShowQTEM(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_TEM(self, event):
            self.impurity_figure.show_Q_TEM()

    class ButtonShowQETG(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_ETG(self, event):
            self.impurity_figure.show_Q_ETG()

    if verbosity >= 2:
        print("Creating base figure")
    impurity_figure = ImpurityFigure(fig, ds, 12, 12)
    if verbosity >= 2:
        print("Created base figure")

    callback_Q_tot = ButtonShowQTot(impurity_figure)
    callback_Q_ITG = ButtonShowQITG(impurity_figure)
    callback_Q_TEM = ButtonShowQTEM(impurity_figure)
    callback_Q_ETG = ButtonShowQETG(impurity_figure)

    if verbosity >= 2:
        print("Created buttons")

    bshow_Q_tot = Button(impurity_figure.axes["Q tot button"], "show Q tot")
    bshow_Q_ITG = Button(impurity_figure.axes["Q ITG button"], "show Q ITG")
    bshow_Q_TEM = Button(impurity_figure.axes["Q TEM button"], "show Q TEM")
    bshow_Q_ETG = Button(impurity_figure.axes["Q ETG button"], "show Q ETG")

    if verbosity >= 2:
        print("Put buttons on axis")

    bshow_Q_tot.on_clicked(callback_Q_tot.show_Q_tot)
    bshow_Q_ITG.on_clicked(callback_Q_ITG.show_Q_ITG)
    bshow_Q_TEM.on_clicked(callback_Q_TEM.show_Q_TEM)
    bshow_Q_ETG.on_clicked(callback_Q_ETG.show_Q_ETG)

    if verbosity >= 2:
        print("Callbacks attached")

    plt.show()


def plot_A_lines(netcdf_name, flux, Z, Ani, Zeff, fontx=12, fonty=12):

    fig, ax = plt.subplots()
    ds = read_data_from_netcdf(netcdf_name)

    #   later select the closest Zeff

    ds_Ani = ds.where(ds["Ani"] == Ani, drop=True)
    ds_Ani_Zeff = ds_Ani.where(ds["Zeff"] == Zeff, drop=True)
    ds_Ani_Zeff_Z = ds_Ani_Zeff.isel(nions=1).where(
        ds_Ani_Zeff.isel(nions=1)["Zi"] == Z, drop=True
    )

    x = ds_Ani_Zeff_Z["Ai"].values
    y = ds_Ani_Zeff_Z[flux].values

    x, y = zip(*sorted(zip(x, y)))

    title = "scan"
    y_label = r"heat flux"
    x_label = r"mass"
    ax.set(xlabel=x_label, ylabel=y_label, title=title)
    ax.xaxis.label.set_size(13)
    ax.yaxis.label.set_size(13)
    ax.title.set_size(15)
    ax.errorbar(x, y)

    fig.show()


def plot_Z_lines(netcdf_name, flux, A, Ani, Zeff, fontx=12, fonty=12):

    fig, ax = plt.subplots()
    ds = read_data_from_netcdf(netcdf_name)

    ds_Ani = ds.where(ds["Ani"] == Ani, drop=True)
    ds_Ani_Zeff = ds_Ani.where(ds["Zeff"] == Zeff, drop=True)
    ds_Ani_Zeff_Z = ds_Ani_Zeff.isel(nions=1).where(
        ds_Ani_Zeff.isel(nions=1)["Ai"] == A, drop=True
    )

    x = ds_Ani_Zeff_Z["Zi"].values
    y = ds_Ani_Zeff_Z[flux].values

    x, y = zip(*sorted(zip(x, y)))

    title = "scan"
    y_label = r"heat flux"
    x_label = r"charge"
    ax.set(xlabel=x_label, ylabel=y_label, title=title)
    ax.xaxis.label.set_size(13)
    ax.yaxis.label.set_size(13)
    ax.title.set_size(15)
    ax.errorbar(x, y)

    fig.show()


def plot_Z_all_lines(netcdf_name):

    # Read data
    # Open the netcdf file with xarray and read the data needed.

    fig = plt.figure()
    ds = read_data_from_netcdf(netcdf_name)

    class ImpurityFigure:
        gs = gridspec.GridSpec(
            16,
            21,
            wspace=0.5,
            hspace=0.05,
            left=0.05,
            right=0.95,
            bottom=0.05,
            top=0.95,
        )
        axes = {
            "Qi": plt.subplot(gs[2:-4, 2:9]),
            "Qe": plt.subplot(gs[2:-4, 13:-1]),
            "Q tot button": plt.subplot(gs[0, :2]),
            "Q ITG button": plt.subplot(gs[0, 3:5]),
            "Q TEM button": plt.subplot(gs[0, 6:8]),
            "Q ETG button": plt.subplot(gs[0, 9:11]),
            "Slider Ani": plt.subplot(gs[-2, 1:10]),
            "Slider Zeff": plt.subplot(gs[-2, 12:-1]),
            "Slider A": plt.subplot(gs[-1, 1:-1]),
        }

        def __init__(self, fig, ds, fontx, fonty):

            # Initialize variables to be plotted

            self.Z = []
            self.fig = fig

            self.ds = ds
            self.ds_ion1 = ds.isel(nions=1)

            self.A = np.unique(self.ds_ion1["Ai"])

            self.fontx = fontx
            self.fonty = fonty

            #           Only the Z and A selected. Here 0 0 because we are initializing

            self.ds_Qi_tot = ds["efi_GB"]
            self.ds_Qe_tot = ds["efe_GB"]

            self.Qi = self.ds_Qi_tot.copy(deep=True)
            self.Qe = self.ds_Qe_tot.copy(deep=True)

            #           the arrays for Zeff and Ani could be different for some impurities and so not been plottablea
            #           could be necessary to update the A variable to get the correct sliders for Zeff. Could also pu zeroes there

            self.A = np.unique(self.ds_ion1["Ai"])
            for single_A in self.A:
                ds_A = self.ds_ion1.where(self.ds_ion1["Ai"] == single_A, drop=True)
                self.Z.append(np.unique(ds_A["Zi"]))

            self.val_A = self.A[0]
            self.val_Z = self.Z[0][0]

            self.A_index = 0

            self.Ani, self.Zeff, self.ninorm = get_x_y(self.ds, self.val_A, self.val_Z)

            self.val_Ani = self.Ani[0]
            self.val_Zeff = self.Zeff[0]

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            # order is wrong need to take x as before and reorder it

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            # add levels and norm with ITG and ETG

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.fig.canvas.draw_idle()

            #           Should probably not be self.

            #            Need a common variable changed on the run which stores tot, ITG, ETG ecc depending on which one is shown
            #            add levels and norm with ITG and ETG

            self.slider_Ani_axis = self.axes["Slider Ani"]
            self.slider_Ani = DiscreteSlider(
                self.slider_Ani_axis,
                "Ani",
                self.Ani[0],
                self.Ani[-1],
                allowed_vals=self.Ani,
                valinit=self.Ani[0],
            )
            self.slide_Ani = self.slider_Ani.on_changed(self.update_Ani)

            self.slider_Zeff_axis = self.axes["Slider Zeff"]
            self.slider_Zeff = DiscreteSlider(
                self.slider_Zeff_axis,
                "Zeff",
                self.Zeff[0],
                self.Zeff[-1],
                allowed_vals=self.Zeff,
                valinit=self.Zeff[0],
            )
            self.slide_Zeff = self.slider_Zeff.on_changed(self.update_Zeff)

            self.slider_A_axis = self.axes["Slider A"]
            self.slider_A = DiscreteSlider(
                self.slider_A_axis,
                "A",
                self.A[0],
                self.A[-1],
                allowed_vals=self.A,
                valinit=self.A[0],
            )
            self.slide_A = self.slider_A.on_changed(self.update_A)

        def show_Q_tot(self):

            self.ds_Qi_tot = ds["efi_GB"]
            self.ds_Qe_tot = ds["efe_GB"]

            self.Qi = self.ds_Qi_tot.copy(deep=True)
            self.Qe = self.ds_Qe_tot.copy(deep=True)

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            #            self.axes['Qi'] = plt.subplot(self.gs[2:-3,2:9])
            #            self.axes['Qe'] = plt.subplot(self.gs[2:-3,13:-1])

            # add levels and norm with ITG and ETG

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.fig.canvas.draw_idle()

        def show_Q_ITG(self):

            self.ds_Qi_ITG = ds["efiITG_GB"]
            self.ds_Qe_ITG = ds["efeITG_GB"]

            self.Qi = self.ds_Qi_ITG.copy(deep=True)
            self.Qe = self.ds_Qe_ITG.copy(deep=True)

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            #            self.axes['Qi'] = plt.subplot(self.gs[2:-3,2:9])
            #            self.axes['Qe'] = plt.subplot(self.gs[2:-3,13:-1])

            # add levels and norm with ITG and ETG

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.fig.canvas.draw_idle()

        def show_Q_TEM(self):

            self.ds_Qi_TEM = ds["efiTEM_GB"]
            self.ds_Qe_TEM = ds["efeTEM_GB"]

            self.Qi = self.ds_Qi_TEM.copy(deep=True)
            self.Qe = self.ds_Qe_TEM.copy(deep=True)

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            #            self.axes['Qi'] = plt.subplot(self.gs[2:-3,2:9])
            #            self.axes['Qe'] = plt.subplot(self.gs[2:-3,13:-1])

            # add levels and norm with ITG and ETG

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.fig.canvas.draw_idle()

        def show_Q_ETG(self):

            self.ds_Qi_ETG = ds["efi_GB"]
            self.ds_Qe_ETG = ds["efeETG_GB"]

            self.Qi = self.ds_Qi_ETG.copy(deep=True)
            self.Qe = self.ds_Qe_ETG.copy(deep=True)

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            #            self.axes['Qi'] = plt.subplot(self.gs[2:-3,2:9])
            #            self.axes['Qe'] = plt.subplot(self.gs[2:-3,13:-1])

            # add levels and norm with ITG and ETG

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.fig.canvas.draw_idle()

        def update_Ani(self, val_Ani):

            self.val_Ani = val_Ani.copy()
            self.Ani_index = int(np.where(self.Ani == self.val_Ani)[0])

            # Find the value in rho that correspond to the val and substitute the data with those

            #            from IPython import embed
            #            embed()
            #            ds_Ani = self.Qi.where(self.Qi.isel(nions=1)['Ani'] == self.val_Ani, drop = True)

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            # to plot both ions need a way to mask one ion with the other one

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.fig.canvas.draw_idle()

        def update_Zeff(self, val_Zeff):

            self.val_Zeff = val_Zeff.copy()
            self.Zeff_index = int(np.where(self.Zeff == self.val_Zeff)[0])

            # Find the value in rho that correspond to the val and substitute the data with those

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            self.fig.canvas.draw_idle()

        # Might be able to slide both Z and A with an option

        def update_A(self, val_A):

            self.val_A = val_A.copy()
            self.A_index = int(np.where(self.A == self.val_A)[0])

            # Find the value in rho that correspond to the val and substitute the data with those

            if self.val_Z not in self.Z[self.A_index]:
                self.val_Z = self.Z[self.A_index][0]
                Ani, Zeff, ninorm = get_x_y(self.ds, self.val_A, self.val_Z)
            else:
                Ani, Zeff, ninorm = get_x_y(self.ds, self.val_A, self.val_Z)

            change_slider = False
            change_Zeff = False

            if not (np.array_equal(Zeff, self.Zeff)):
                if not self.val_Zeff in Zeff:
                    self.val_Zeff = Zeff[0]
                    change_Zeff = True
                self.Zeff = Zeff
                change_slider = True

            ds_Qi_Ani = self.Qi.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qi_Ani_Zeff = ds_Qi_Ani.loc[(ds_Qi_Ani["Zeff"] == self.val_Zeff)]
            ds_Qi_Ani_Zeff_A = ds_Qi_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            ds_Qe_Ani = self.Qe.loc[(self.Qi.isel(nions=1)["Ani"] == self.val_Ani)]
            ds_Qe_Ani_Zeff = ds_Qe_Ani.loc[(ds_Qe_Ani["Zeff"] == self.val_Zeff)]
            ds_Qe_Ani_Zeff_A = ds_Qe_Ani_Zeff.loc[
                (ds_Qi_Ani_Zeff.isel(nions=1)["Ai"] == self.val_A)
            ]

            #            self.x = self.Z[self.A_index]
            self.x = ds_Qi_Ani_Zeff_A.isel(nions=1)["Zi"]
            self.y_Qi0 = ds_Qi_Ani_Zeff_A.isel(nions=0).values
            self.y_Qi1 = ds_Qi_Ani_Zeff_A.isel(nions=1).values
            self.y_Qe = ds_Qe_Ani_Zeff_A.values

            #            ds_Ani_Zeff = ds_Ani.where(ds_Ani['Zeff'] == self.val_Zeff, drop = True)
            #            ds_Ani_Zeff_Z = ds_Ani_Zeff.isel(nions=1).where(ds_Ani_Zeff.isel(nions=1)['Ai'] == self.val_A, drop = True)

            x = np.copy(self.x)
            if isinstance(x, np.ndarray):
                self.x, self.y_Qi0 = zip(*sorted(zip(x, self.y_Qi0)))
                self.x, self.y_Qi1 = zip(*sorted(zip(x, self.y_Qi1)))
                self.x, self.y_Qe = zip(*sorted(zip(x, self.y_Qe)))

            self.axes["Qi"].cla()
            self.axes["Qe"].cla()

            self.axes["Qi"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qi"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qi"].set_title(r"Ions")
            self.axes["Qe"].set_ylabel(r"heat flux", fontsize=self.fonty)
            self.axes["Qe"].set_xlabel(r"Z", fontsize=self.fontx)
            self.axes["Qe"].set_title(r"Electrons")

            self.axes["Qi"].autoscale(axis="y")
            self.axes["Qe"].autoscale(axis="y")

            self.axes["Qi"].errorbar(self.x, self.y_Qi0, marker="s")
            self.axes["Qi"].errorbar(self.x, self.y_Qi1, marker="s")
            self.axes["Qe"].errorbar(self.x, self.y_Qe, marker="s")

            #            if self.val_Z not in self.Z[self.A_index]:
            #                self.val_Z = self.Z[self.A_index][0]
            #                Ani, Zeff = get_x_y(self.ds, self.val_A, self.val_Z)
            #            else:
            #                Ani, Zeff = get_x_y(self.ds, self.val_A, self.val_Z)

            #            embed()

            #            print(Zeff)
            #            print(self.Zeff)
            #            print(self.val_Zeff)

            #            if not(np.array_equal(Zeff, self.Zeff)):
            #                if not self.val_Zeff in Zeff:
            #                    self.val_Zeff = Zeff[0]
            #                    print('Hey Ray')
            #                self.Zeff = Zeff

            if change_slider == True:

                self.slider_Zeff_axis.clear()
                self.slider_Zeff.disconnect(self.slide_Zeff)
                self.slider_Zeff_axis = self.axes["Slider Zeff"]
                # Probably don t need this
                if change_Zeff:
                    self.slider_Zeff = DiscreteSlider(
                        self.slider_Zeff_axis,
                        "Zeff",
                        self.Zeff[0],
                        self.Zeff[-1],
                        allowed_vals=self.Zeff,
                        valinit=self.Zeff[0],
                    )
                else:
                    self.slider_Zeff = DiscreteSlider(
                        self.slider_Zeff_axis,
                        "Zeff",
                        self.Zeff[0],
                        self.Zeff[-1],
                        allowed_vals=self.Zeff,
                        valinit=self.val_Zeff,
                    )
                self.slide_Zeff = self.slider_Zeff.on_changed(self.update_Zeff)
                # Possibly wrong
                self.slide_A = self.slider_A.on_changed(self.update_Zeff)

            self.slide_A = self.slider_A.on_changed(self.update_A)

            self.fig.canvas.draw_idle()

    class ButtonShowQTot(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_tot(self, event):
            self.impurity_figure.show_Q_tot()

    class ButtonShowQITG(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_ITG(self, event):
            self.impurity_figure.show_Q_ITG()

    class ButtonShowQTEM(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_TEM(self, event):
            self.impurity_figure.show_Q_TEM()

    class ButtonShowQETG(object):
        def __init__(self, impurity_figure):
            self.impurity_figure = impurity_figure

        def show_Q_ETG(self, event):
            self.impurity_figure.show_Q_ETG()

    impurity_figure = ImpurityFigure(fig, ds, 12, 12)

    callback_Q_tot = ButtonShowQTot(impurity_figure)
    callback_Q_ITG = ButtonShowQITG(impurity_figure)
    callback_Q_TEM = ButtonShowQTEM(impurity_figure)
    callback_Q_ETG = ButtonShowQETG(impurity_figure)

    bshow_Q_tot = Button(impurity_figure.axes["Q tot button"], "show Q tot")
    bshow_Q_ITG = Button(impurity_figure.axes["Q ITG button"], "show Q ITG")
    bshow_Q_TEM = Button(impurity_figure.axes["Q TEM button"], "show Q TEM")
    bshow_Q_ETG = Button(impurity_figure.axes["Q ETG button"], "show Q ETG")

    bshow_Q_tot.on_clicked(callback_Q_tot.show_Q_tot)
    bshow_Q_ITG.on_clicked(callback_Q_ITG.show_Q_ITG)
    bshow_Q_TEM.on_clicked(callback_Q_TEM.show_Q_TEM)
    bshow_Q_ETG.on_clicked(callback_Q_ETG.show_Q_ETG)

    plt.show()


def plot_A_all_lines(netcdf_name, flux, A, Ani, Zeff, fontx=12, fonty=12):

    fig, ax = plt.subplots()
    ds = read_data_from_netcdf(netcdf_name)

    ds_Ani = ds.where(ds["Ani"] == Ani, drop=True)
    ds_Ani_Zeff = ds_Ani.where(ds["Zeff"] == Zeff, drop=True)
    ds_Ani_Zeff_A = ds_Ani_Zeff.where(ds["A"] == A, drop=True)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        path = sys.argv[1]
    else:
        path = "/home/marinm/Work/QuaLiKiz/runs/Impurities_scan/impurities_scan2.nc"
    if len(sys.argv) > 2:
        output_type = sys.argv[2]
    else:
        output_type = "GB"
    if len(sys.argv) > 3:
        Zeff_max = sys.argv[3]
    else:
        Zeff_max = 5
    if len(sys.argv) > 4:
        dilution_max = sys.argv[4]
    else:
        dilution_max = 0.9

#    plot_Zeff_super_scan(path, Ani_min=-20, Ani_max=20, Zeff_min=1, Zeff_max=Zeff_max, fontx=12, fonty=12, ion=0, GB_SI=output_type, dilution_max = dilution_max)

Zeff_max, dilution_max, output_type = 5, 0.1, "GB"

if __name__ == "__main__":

    plot_Zeff_super_scan(
        path,
        Ani_min=-20,
        Ani_max=20,
        Zeff_min=1,
        Zeff_max=Zeff_max,
        fontx=12,
        fonty=12,
        ion=0,
        GB_SI=output_type,
        dilution_max=dilution_max,
        flux_type="heat",
        mode="normal",
        scanning_Ani_ion1=False,
    )


# if __name__ == '__main__':
#    plot_Z_all_lines('/home/marinm/Work/QuaLiKiz/runs/Impurities_scan/Impurities_scan.nc')
