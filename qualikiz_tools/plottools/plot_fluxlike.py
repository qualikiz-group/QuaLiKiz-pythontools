from itertools import cycle, chain
import logging
import re

import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib import gridspec
from IPython import embed

from qualikiz_tools.qualikiz_io.outputfiles import (
    squeeze_dataset,
    xarray_to_pandas,
)
import qualikiz_tools.plottools.pretty_names as pretty_names

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def determine_scandim(ds):
    scan_dims = [
        coord
        for name, coord in ds.coords.items()
        if name not in ds.dims
        and "dimx" in coord.dims
        and len(np.unique(coord)) > 1
        and name != "phi"
    ]
    # new_dims = OrderedDict([(dim.name, np.unique(dim.values)) for dim in ortho_dims])
    if len(scan_dims) == 0:
        logger.info("No scan dim found, falling back to dimx as scan_dim")
        scan_dim = ds["dimx"]
    elif len(scan_dims) == 1:
        logger.info(
            "Non-ambiguous scan_dim found, making something plottable out of it "
        )
        scan_dim = scan_dims[0]
        if scan_dim.dims == ("dimx",):
            pass
        elif scan_dim.dims == ("dimx", "nions") or scan_dim.dims == (
            "set",
            "dimx",
            "nions",
        ):
            can_squeeze = True
            for ii in scan_dim["dimx"]:
                unique_vals = np.unique(scan_dim.sel(**{"dimx": ii}))
                unique_vals = unique_vals[~np.isnan(unique_vals)]
                can_squeeze &= len(unique_vals) == 1
            if can_squeeze:
                try:
                    scan_dim = scan_dim.sel(nions=0, set=0)
                except ValueError:
                    scan_dim = scan_dim.sel(nions=0)
            else:
                print("Warning! Ions unsqueezable. Behaviour untested!")
                scan_dim = ds["dimx"]
        else:
            print("Scan_dim dims are {!s}. Not sure what to do".format(scan_dim.dims))
            scan_dim = ds["dimx"]

    elif len(scan_dims) > 1:
        logger.warning(
            "Warning! More than 1 scanned variable! Behaviour untested! Falling back to dimx as scan_dim and keep fingers crossed!"
        )
        scan_dim = ds["dimx"]
    return scan_dim


def plot_any_spectrum(freqlike_table, name, colors, linestyles, ax):
    assert isinstance(freqlike_table, pd.DataFrame)
    for run_num, df in freqlike_table.groupby(
        axis="columns", level="set"
    ):  # loop over all runs
        initialized_plot = False
        for dimx, df in df.groupby(level="dimx"):  # Loop over all dimx
            table = freqlike_table.loc[(dimx, slice(None), slice(None)), run_num]
            if (table != 0).any().any():
                # We found a spectrum! Plot it
                if (df != 0).any().any():
                    logger.debug(
                        f"Found a non-zero spectrum for dimx={dimx}, run_num={run_num}, setting up initial plot"
                    )
                    ax = update_grow(
                        dimx,
                        name,
                        colors,
                        linestyles,
                        ax=ax,
                        freqlike_table=freqlike_table,
                        run_num=run_num,
                        initial_plot=True,
                    )
                    initialized_plot = True
            if initialized_plot:
                break
    if not initialized_plot:
        # The table is fully empty for all runs and all dimx.
        # We will never plot anything!
        logger.debug(f"Initial plot for '{name}' fully empty")
        ax.text(
            0.5,
            0.5,
            "Fully empty for all runs and dimx",
            horizontalalignment="center",
            verticalalignment="center",
        )
    return ax


def sort_axis_labels(ax):
    """Sort the axis legend labels"""
    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)
    return ax


def update_grow(
    dimx_val,
    name,
    colors,
    linestyles,
    ax=None,
    freqlike_table=None,
    run_num=-1,
    initial_plot=False,
):
    """Update growrates in a plot

    Args:
        dimx_val: The value in the dimx variable to plot
        name: The name of the variable to plot. Will be searched for in freqlike_table.
        colors: The different colors to use for the plot
        linestyles: The different linestyles to use for the plot

    Kwargs:
        ax: The matplotlib axis to plot on
        freqlike_table: The table to search data in
        run_num: The number of the run to plot [-1 for all]
        initial_plot: If this is the first plot on the axis, yes or no
    """
    # Input sanitation
    run_num = int(run_num)
    assert run_num >= 0 or run_num == -1
    dimx_val = float(dimx_val)
    assert isinstance(dimx_val, float)
    assert isinstance(name, str)
    assert isinstance(colors, dict)
    assert len(colors) >= 0
    assert isinstance(linestyles, dict)
    assert len(linestyles) >= 0
    assert isinstance(ax, plt.Axes)
    assert isinstance(freqlike_table, pd.DataFrame)
    assert isinstance(run_num, int)
    assert isinstance(initial_plot, bool)

    if initial_plot:
        # If no ax is given, we assume that this is an "initial plot" setup run
        logger.debug(f"Creating initial plot for dimx_val={dimx_val} and name='{name}'")
    else:
        logger.debug(f"Plotting slider plot for dimx_val={dimx_val} and name='{name}'")

    if len(freqlike_table) == 0:
        # The table is fully empty for all runs and all dimx.
        # We will never plot anything!
        return None

    # List of runs to scan over
    all_runs_index = freqlike_table.columns.get_level_values("set").values
    if run_num == -1:
        logger.debug(f"Looping over all runs")
        scanned_runs_index = all_runs_index
    else:
        scanned_runs_index = [run_num]

    for run_num in scanned_runs_index:
        # Grab single run, single dimx
        table: pd.DataFrame = freqlike_table.loc[
            (dimx_val, slice(None), slice(None)), run_num
        ]
        assert isinstance(table, pd.Series)  # This is now a single run
        init_table: pd.DataFrame = table.unstack(level=["numsols"]).droplevel("dimx")
        for numsol, df in init_table.groupby(
            axis="columns", level="numsols"
        ):  # Loop over all numsols
            pretty_label = f"run_num={run_num}, numsol={int(numsol)}"
            logger.debug(f"Creating initial plot for '%s'", pretty_label)
            se: pd.Series = df.loc[:, numsol]
            assert isinstance(se, pd.Series)
            se.name = pretty_label
            if initial_plot:
                color = colors[numsol]
                style = linestyles[run_num]
                se.plot(
                    ax=ax,
                    marker="o",
                    color=color,
                    style=style,
                    legend=True,
                )
            else:
                logger.debug(f"Found {len(ax.lines)} existing lines")
                for line in ax.lines:
                    # Find our line
                    label = line.get_label()
                    if label == pretty_label:
                        line.set_ydata(se)
                        # values = values.replace(0, np.nan)  # TODO: Not sure if we need this
                        ax.relim()
                        ax.autoscale(axis="y")
                        ax.figure.canvas.draw_idle()

    ax = sort_axis_labels(ax)
    return ax


def build_plot(
    datasets,
    flux_type,
    normalization,
    instability_tag="total",
    sum_hydrogen=True,
    drop_non_hydrogen=True,
    lowwave_boundry=2,
    names=None,
    interactive=True,
    myslice=None,
):
    if instability_tag == "total":
        # In QuaLiKiz lingo, total flux suffix is just ""
        instability_tag = ""

    if myslice is None:
        myslice = {}

    if len(myslice) != 0:
        fltrd = []
        # TODO: Make quicker for larger datasets; maybe move it to after pandaization
        for dataset in datasets:
            for name, val in myslice.items():
                logger.info(f"Filtering data with {name}={val}")
                match = re.match("(\D*)(\d+)", name)
                if match is None:
                    # No numbers at the end. This is either dimx-only or
                    # a unknown name handeled later
                    ion_value = None
                    rname = name
                else:
                    # A string of numbers, this is probably an ion variable
                    grps = match.groups()
                    assert len(grps) == 2, "Que? Ask Karel"
                    ion_value = int(grps[1])
                    rname = grps[0]
                    # To avoid Dimensions vs Coordinates vs Data Vars trouble,
                    # we re-index the dataset here
                    newvar = dataset[rname].sel(nions=ion_value)
                    dataset[name] = newvar
                    dataset = dataset.drop_vars(rname)
                    dataset = dataset.set_coords(name)

                if name in dataset:
                    if (dataset[name].dims) != ("dimx",) and ion_value is None:
                        raise NotImplementedError(
                            "Slice dimension does not depend on only 'dimx', "
                            "please use a number-suffix to slice on ion "
                            "dimensions, e.g. Ati0 for main ion temperature "
                            "gradient"
                        )
                    try:
                        oned_filter_var: xr.DataArray = dataset[name]
                        is_eq = lambda x: np.isclose(
                            x, float(val), atol=1e-5, rtol=1e-3
                        )
                        fltr = oned_filter_var.where(is_eq, False).astype(bool)
                        dataset = dataset.where(fltr)
                    except IndexError:
                        logger.error("Requested %s=%s not found in dataset", rname, val)
                        raise
                else:
                    logger.warning(
                        "Requested {0!s}={1!s}, but {0!s} not in dataset, not "
                        "filtering on that but trying to continue".format(rname, val)
                    )
            fltrd.append(dataset)
        logger.info("Filtering done, start squeezing")
    else:
        logger.info("No filtering, start squeezing")
        fltrd = datasets

    filtered = []
    for filterd in fltrd:
        filtered.append(filterd.dropna("dimx"))

    squeezed = []
    for dataset in filtered:
        squeeze = squeeze_dataset(dataset, no_squeeze=["Zi"])
        if "nions" not in squeeze["Zi"].dims:
            squeeze.coords["Zi"] = xr.DataArray(
                np.tile(squeeze["Zi"], (1, len(squeeze["nions"]))),
                dims=["set", "nions"],
            )
        if "dimx" not in squeeze["Zi"].dims:
            squeeze.coords["Zi"] = xr.DataArray(
                [np.tile(squeeze["Zi"].data, (len(squeeze["dimx"]), 1))],
                dims=["set", "dimx", "nions"],
            )
        squeeze = squeeze.reset_coords("Zi")
        squeezed.append(squeeze)
    try:
        ds = xr.concat(squeezed, dim="set")
    except KeyError:
        raise Exception("Incompatible datasets for simple plotting")

    # Determine scan dimension
    scan_dim_array: xr.DataArray = determine_scandim(ds)
    scan_dim: pd.Series = scan_dim_array.to_pandas()
    scan_dim.name = scan_dim_array.name

    # Prepare dataset
    dfs: dict = xarray_to_pandas(ds)
    dimx_set = ("set", "dimx")
    efelike: pd.DataFrame = dfs[dimx_set].unstack(level=("set"))
    efilike: pd.DataFrame = dfs[("set", "dimx", "nions")].unstack(
        level=("set", "nions")
    )
    freqlike: pd.DataFrame = dfs[("set", "dimx", "kthetarhos", "numsols")].unstack(
        level=("set")
    )

    # Build plot target names
    target_efelike = "".join([flux_type, "e", instability_tag, "_", normalization])
    target_efilike = "".join([flux_type, "i", instability_tag, "_", normalization])

    freq_name = "ome_" + normalization
    grow_name = "gam_" + normalization

    # Build plot area
    fig = plt.figure()
    gs = gridspec.GridSpec(
        22, 4, hspace=20, left=0.05, right=0.95, bottom=0.05, top=0.95
    )
    if len(myslice) == 0:
        lbound = 22
    else:
        lbound = 20
    axes = {
        "flux": plt.subplot(gs[:lbound, :2]),
        "freq_low": plt.subplot(gs[:10, 2]),
        "freq_high": plt.subplot(gs[:10, 3]),
        "grow_low": plt.subplot(gs[10:-2, 2]),
        "grow_high": plt.subplot(gs[10:-2, 3]),
        "dimx_slider": plt.subplot(gs[-2:, 2:]),
    }
    if len(myslice) != 0:
        ax = axes["myslice"] = plt.subplot(gs[lbound:, :2])
        table = ax.table(
            cellText=[list(i) for i in zip(*[(k,v) for k,v in myslice.items()])],
            cellLoc="center",
            loc="upper center",
        )
        ax.axis("off")

    axes["freq_low"].set_ylabel("freq_low")
    axes["freq_high"].set_ylabel("freq_high")
    axes["grow_low"].set_ylabel("grow_low")
    axes["grow_high"].set_ylabel("grow_high")

    for ax in axes.values():
        ax.ticklabel_format(style="sci", scilimits=(-2, 2), axis="y")

    # freq_ax.set_ylabel(freq_name)
    # grow_ax.set_ylabel(grow_name)

    # Initialize dimx slider
    dimx = dfs[("set", "dimx")].index.get_level_values(level="dimx")
    dimx_slider = plt.Slider(
        axes["dimx_slider"],
        "dimx",
        dimx[0],
        dimx[-1],
        valstep=dimx,
    )

    # For rangeslider
    # assert isinstance(dimx_val, np.ndarray), "Input to update_grow should be a numpy array!"
    # assert dimx_val.shape == (2,), "Shape of input to update_grow should by (2,)!"

    # Extract part of dataset and plot it
    idx = pd.IndexSlice
    cmap = plt.get_cmap("tab10")
    colors = {
        set: color
        for set, color in zip(chain([-1], ds.nions.values), cycle(cmap.colors))
    }
    linestyles = {
        set: style
        for set, style in zip(chain(ds.set.values), cycle(["-", "--", "-.", ":"]))
    }
    try:
        indexed_table = efelike[[target_efelike]]
        scan_dim_df = scan_dim.to_frame()
        # Add the set level to the scan dim
        scan_dim_df.columns = pd.MultiIndex.from_arrays(
            [scan_dim_df.columns, ["all"] * len(scan_dim_df.columns)],
            names=[None, "set"],
        )
        efelike_table = indexed_table.join(scan_dim_df)
    except KeyError:
        print("No electron data found for {!s}".format(target_efelike))
    else:
        efelike_table.plot(
            x=(scan_dim.name, "all"),
            ax=axes["flux"],
            marker="o",
            color=[colors[-1] for col in efelike_table if isinstance(col[1], int)],
            style=[
                linestyles[col[1]] for col in efelike_table if isinstance(col[1], int)
            ],
        )

    pd.options.mode.chained_assignment = None  # Ignore pandas warnings
    try:
        efilike_table = efilike[[target_efilike]]
    except KeyError:
        print("No ion data found for {!s}".format(target_efilike))
    else:
        if drop_non_hydrogen:
            non_hydrogen = efilike_table.loc[
                :, (slice(None), (efilike["Zi"] != 1).all())
            ]

            efilike_table.loc[:, pd.Index(non_hydrogen.columns)] = np.nan

        if sum_hydrogen:
            # Find hydrogen ions
            hydrogen = efilike_table.loc[:, (slice(None), (efilike["Zi"] == 1).all())]
            hydro_sum = hydrogen.groupby(axis="columns", level="set").sum()

            # Remembed variable and set of hydrogen summation
            hydro_sum.columns = hydrogen.loc[
                :, (slice(None), slice(None), slice(0, 0))
            ].columns

            # Drop hydrogen atoms from the efilike_table
            efilike_table.loc[:, pd.Index(hydrogen.columns)] = np.nan

            # Add the summed hydrogen to efilike_table
            efilike_table = efilike_table.combine_first(hydro_sum)

        efilike_table.dropna(axis="columns", inplace=True)
        pd.options.mode.chained_assignment = "warn"  # Turn pandas warnings on

        # Add the set and nions levels to the scan dim
        scan_dim_df = scan_dim.to_frame()
        scan_dim_df.columns = pd.MultiIndex.from_arrays(
            [
                scan_dim_df.columns,
                ["all"] * len(scan_dim_df.columns),
                ["all"] * len(scan_dim_df.columns),
            ],
            names=[None, "set", "nions"],
        )
        efilike_table = efilike_table.join(scan_dim_df)
        efilike_table.plot(
            x=(scan_dim.name, "all", "all"),
            ax=axes["flux"],
            marker="o",
            color=[colors[col[2]] for col in efilike_table if isinstance(col[1], int)],
            style=[
                linestyles[col[1]] for col in efilike_table if isinstance(col[1], int)
            ],
        )

    freqlike_tables = {
        "freq_high": freqlike["ome_" + normalization].loc[
            (idx[:, lowwave_boundry:, :]), :
        ],
        "grow_high": freqlike["gam_" + normalization].loc[
            (idx[:, lowwave_boundry:, :]), :
        ],
        "freq_low": freqlike["ome_" + normalization].loc[
            (idx[:, :lowwave_boundry, :]), :
        ],
        "grow_low": freqlike["gam_" + normalization].loc[
            (idx[:, :lowwave_boundry, :]), :
        ],
    }

    colors = {set: color for set, color in zip(ds.numsols.values, cycle(cmap.colors))}
    for type in ["freq", "grow"]:
        for part in ["high", "low"]:
            # Create the "initial plot", this will be updated by the slider
            name = type + "_" + part
            ax = axes[name]
            freqlike_table = freqlike_tables[name]

            # Each column in freqlike_table is a different run.
            # The index is dimx, kthetarhos, and numsols
            # Try to find a spectum in any of the runs to set colors and style

            ax_spec: plt.Axes = plot_any_spectrum(
                freqlike_table, name, colors, linestyles, ax
            )

            assert isinstance(ax_spec, plt.Axes)

            if len(freqlike_table.index.get_level_values("dimx")) >= 1:
                initial_dimx = freqlike_table.index.get_level_values("dimx")[0]
            else:
                initial_dimx = 0
            logger.warning(f"No initial dimx found for name={name}")
            assert isinstance(ax, plt.Axes)

    # Define the event triggered by slider update
    def update_specific_grow(dimx_val):
        assert isinstance(dimx_val, float), "Input to update_grow should be float!"
        # From matplotlib on_changed
        # https://matplotlib.org/stable/api/widgets_api.html#matplotlib.widgets.Slider.on_changed
        # Function to call when slider is changed. The function must accept
        # a single float
        for type in ["freq", "grow"]:
            for part in ["high", "low"]:
                name = type + "_" + part
                ax = axes[name]
                freqlike_table = freqlike_tables[name]
                update_grow(
                    dimx_val,
                    name,
                    colors,
                    linestyles,
                    ax=ax,
                    freqlike_table=freqlike_table,
                    run_num=-1,
                    initial_plot=False,
                )

    dimx_slider.on_changed(update_specific_grow)

    # table = freqlike_table.loc[run_num]
    # if len(table) != 0:
    #    # Find a spectrum with non-zero values
    #    if part == "low":
    #        # Sum all dimxs and sets together
    #        sums = (
    #            freqlike_table
    #            .unstack(level=["dimx", "numsols"])
    #            .sum("columns")
    #        )
    #        kthetarhos_max = sums.ne(0)[::-1].argmax()
    #        axes[name].set_xlim(left=0, right=kthetarhos_max)
    #    break
    # Set the plots to the initial value, which is the 0'th plot
    # It'll be set by the slider later

    # freq_table = freqlike[freq_name].loc[0].unstack()
    # freq_low_table = freq_table[freq_table.index<2]
    # freq_high_table = freq_table[freq_table.index>=2]
    # freq_low_table.plot(ax=freq_low_ax, marker='o')
    # freq_high_table.plot(ax=freq_high_ax, marker='o')

    ##############
    # Formatting #
    ##############
    ax = axes["flux"]
    xlabel = ax.get_xlabel()

    analyzed_base = pretty_names.analyze_basename(target_efelike)
    efelike_title: str = pretty_names.create_title_from_analyzed_base(analyzed_base)

    analyzed_base = pretty_names.analyze_basename(target_efilike)
    efilike_title: str = pretty_names.create_title_from_analyzed_base(analyzed_base)

    ax.set_ylabel(" and ".join([efelike_title, efilike_title]))
    ax.set_xlabel(scan_dim.name)
    for ax in axes["freq_low"], axes["freq_high"], axes["grow_low"], axes["grow_high"]:
        ax.set_ylabel(" ".join([ax.get_ylabel(), f"[{normalization}]"]))

    fmt = "({!s}) {!s} " * len(ds.set)
    if names is None:
        names = list(range(len(ds.set)))
    tuples = [(linestyles[set], names[ii]) for ii, set in enumerate(range(len(ds.set)))]
    flat_list = [item for sublist in tuples for item in sublist]
    fig.suptitle(fmt.format(*flat_list))
    if interactive:
        plt.show()
    return fig


if __name__ == "__main__":
    ds = xr.open_dataset("./mini.nc")
    ds2 = xr.open_dataset("./mini_mult_iso.nc")

    flux_type = "ef"
    instability_tag = "ITG"
    normalization = "SI"
    build_plot([ds, ds2], flux_type, normalization, instability_tag)
# slider_ax = plt.subplot(gs[1,0])
# kthetarhos = dfs[('kthetarhos',)]['kthetarhos'].values
