""" This file contains helper classes

    Typical usage:
        dict_ = {'cow': 'moo'}
        ods = ODS(dict_)
"""
import logging
from collections import OrderedDict
from itertools import zip_longest, chain
import copy
import os
import glob

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def _gen_dict_extract(var, key, verbosity=0):
    """Find keys in deeply nested structures
    From https://stackoverflow.com/a/60261620/3613853
    Best performance from tested implementations
    Adjusted for Python3 duck-typing style

    Args:
        var: The variable to search in. Recurses deeper inward.
        key: The key to search for

    Keyword Args:
        verbosity: Verbosity of this function [0-inf]

    Yields:
        The found fields matching the key requested. Of the same
        type of the given var.

    Raises:
        IOError: You are not on a computer
    """
    if verbosity >= 3:
        logger.setLevel(logging.TRACE)
    elif verbosity >= 2:
        logger.setLevel(logging.DEBUG)
    elif verbosity >= 1:
        logger.setLevel(logging.INFO)
    elif verbosity >= 0:
        logger.setLevel(logging.WARNING)

    if hasattr(var, "items"):
        for kk, vv in var.items():
            logger.trace("Checking key=%r of %s", kk, type(vv))
            if kk == key:
                logger.debug("Yielding key %r of %s", kk, type(vv))
                yield vv
            else:
                logger.trace("Key not found, recurse into key={!r}".format(kk))

            # This is not the key, recurse
            if hasattr(vv, "items"):
                # We have items, probably a dict or something
                for result in _gen_dict_extract(vv, key, verbosity=verbosity):
                    yield result
            elif isinstance(vv, list):
                # We do not have items, try to loop normally
                for el in vv:
                    for result in _gen_dict_extract(key, el, verbosity=verbosity):
                        yield result


# Even though dicts are sorted in recent Python,
# Use SortedDicts for backwards compat
class OrdinaryOrderedDictionaryStructure(OrderedDict):
    """Structure to save searchable OrderedDicts

    Attributes:
        _dict_like_convert_classes: A list of dict-like classes we can convert
        _list_like_convert_classes: A list of list-like classes we can convert
    """

    _dict_like_convert_classes = (dict, OrderedDict)
    _list_like_convert_classes = (list,)
    """ Defines a wrapper for regular Python dicts """

    def __init__(self, init_structure):
        """Inits OrdinaryOrderedDictionaryStructure with an existing structure

        Args:
            init_structure: The dictionary to initialize with. Can be
                list-like or dict-like.
        """
        super().__init__()
        # This is a very protective way of initialization, so only allow
        # specific classes
        if isinstance(init_structure, self._dict_like_convert_classes):
            for key in init_structure.keys():
                self[key] = init_structure[key]
        elif isinstance(init_structure, self._list_like_convert_classes):
            for el in init_structure:
                if len(el) != 2:
                    raise ValueError(
                        'When passing a list-like, make sure all elements are iteratables of ("key_name", value)'
                    )
                self[el[0]] = el[1]
        else:
            raise TypeError("You deal with it! (pass an allowed class)")

    def recursive_find(self, keyname, verbosity=0):
        """Find a key recursively in ODS structure"""
        return _gen_dict_extract(self, keyname, verbosity=verbosity)

    def values_middle_first(self, remove_none=True):
        """Like .values() but with middle values first"""
        for value in self.values():
            if len(value) > 2:
                # Value is something list-like
                mid_index = int(len(value) / 2)
                # For a 10-length list, this is splitting in 5 and 5
                # value[mid_index::]
                # value[mid_index-1::-1]
                iteratable = chain.from_iterable(
                    zip_longest(value[mid_index::], value[mid_index - 1 :: -1])
                )
                sort = list(iteratable)
                # Contains None if value was of uneven length
                # The last value should be the _first_ value in the iteratable given
                if sort[-1] != value[0]:
                    del sort[-1]
                # Sanity checks
                if len(sort) != len(value):
                    raise Exception(
                        "Tried to sort, but sorted return value has different length"
                    )
                if set(sort) != set(value):
                    raise Exception(
                        "Tried to sort, but sorted return value has different values"
                    )
                yield sort
            else:
                yield value

    def __deepcopy__(self, memo):
        return OrdinaryOrderedDictionaryStructure(copy.deepcopy(OrderedDict(self)))


OODS = OrdinaryOrderedDictionaryStructure  # Define a useful short name


def next_path(path_pattern, search_path=None):
    """Finds the next free path in an sequentially named list of files

    Adapted from https://stackoverflow.com/a/47087513/3613853
    e.g. path_pattern = 'file-%s.txt':

    file-1.txt
    file-2.txt
    file-3.txt

    Runs in log(n) time where n is the number of existing files in sequence
    """
    i = 1
    if search_path is not None:
        path_pattern = os.path.join(search_path, path_pattern)

    # First do an exponential search
    while any(os.path.exists(path) for path in glob.glob(path_pattern % i)):
        i = i * 2

    # Result lies somewhere in the interval (i/2..i]
    # We call this interval (a..b] and narrow it down until a + 1 = b
    a, b = (i // 2, i)
    while a + 1 < b:
        c = (a + b) // 2  # interval midpoint
        a, b = (
            (c, b)
            if any(os.path.exists(path) for path in glob.glob(path_pattern % c))
            else (a, c)
        )

    return path_pattern % b
