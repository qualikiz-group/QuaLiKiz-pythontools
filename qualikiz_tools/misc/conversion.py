import logging

import numpy as np
from scipy.special import lambertw
import pandas as pd

root_logger = logging.getLogger("qualikiz_tools")
logger = root_logger
logger.setLevel(logging.WARNING)


try:
    from scipy.optimize import root_scalar
except ImportError:
    logger.warning(
        "'root_scalar' not found in 'scipy.optimize', trying to import 'root' instead. Alternatively, upgrade your scipy to at least 1.2.0"
    )
    from scipy.optimize import root

    def root_scalar(
        fun,
        x0,
        x1=None,
        args=(),
        method="hybr",
        jac=None,
        tol=None,
        callback=None,
        options=None,
        maxiter=None,
    ):
        """root_scalar mask

        This build a quasi-root_scalar interface from scipy-1.2.0 for older
        scipy versions. Tested with scipy-1.1.0.
        """
        # Get options with scipy.optimize.show_options(solver='root', method='hybr')
        if method == "hybr":
            options = {"maxfev": maxiter}
        else:
            raise ValueError("Unexpected method {!s}".format(method))
        # From scipy root_scalar
        if x1:
            logger.info("Ignoring passed x1 argument")
        opt_res = root(
            fun,
            x0,
            args=args,
            method=method,
            jac=jac,
            tol=tol,
            callback=callback,
            options=options,
        )
        # Fake a scipy.optimize.RootResults object: https://docs.scipy.org/doc/scipy-1.5.2/reference/generated/scipy.optimize.root_scalar.html
        # Take values from the scipy.optimize.OptimizeResult object: https://docs.scipy.org/doc/scipy-1.1.0/reference/generated/scipy.optimize.OptimizeResult.html
        # https://stackoverflow.com/questions/2827623/how-can-i-create-an-object-and-add-attributes-to-it
        # All functions can have arbitrary attributes
        root_res = lambda: None
        setattr(root_res, "root", float(opt_res.x))
        setattr(root_res, "iterations", None)  # int
        setattr(root_res, "function_calls", int(opt_res.nfev))
        setattr(root_res, "converged", bool(opt_res.success))
        setattr(root_res, "flag", str(opt_res.message))
        if not root_res.converged:
            logger.warning("Root finding algorithm did not converge")
        return root_res


qe = 1.602176565e-19  # electron charge [C]
me = 9.10938291e-31  # electron mass [kg]
mp = 1.672621777e-27  # proton mass [kg]


def calc_c1(zeff, ne, q, Ro, Rmin, x):
    """Calculate part of the normalized collision frequency :math:`\nu^*`

    Args:
        zeff: Effective ion charge [-]
        ne: Electron density [1e19 m^-3]
        q: Safety factor [-]
        Ro: Midplane-averaged major radius of last-closed-flux-surface, QuaLiKiz internal [m]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        x: Normalized radius [m] :math:`\frac{r_{\text{out}} + r_{\text{in}}}{r_{\text{out,LCFS}} + r_{\text{in,LCFS}}}`
    """
    c1 = 6.9224e-5 * zeff * ne * q * Ro * (Rmin * x / Ro) ** -1.5
    return c1


def calc_c2(ne):
    """Calculate part of the normalized collision frequency :math:`\nu^*`

    Args:
        ne: Electron density [1e19 m^-3]
    """
    c2 = 15.2 - 0.5 * np.log(0.1 * ne)
    return c2


def calc_lambda_e(ne, Te):
    """Calculate the Coulomb constant :math:`\Lambda_e`

    Args:
        ne: Electron density [1e19 m^-3]
        Te: Electron temperature [keV]
    """
    # From QLK source:
    # Lambe(:) = 15.2_DBL - 0.5_DBL*LOG(0.1_DBL*Nex(:)) + LOG(Tex(:))  !Coulomb constant and collisionality. Wesson 2nd edition p661-663
    c2 = calc_c2(ne)
    return c2 + np.log(Te)


def calc_nu_e(ne, Te, zeff, collmult=1):
    """Calculate the collision frequency :math:`\nu_e`

    Args:
        ne: Electron density [1e19 m^-3]
        Te: Electron temperature [keV]
        zeff: Effective ion charge [-]
    Kwargs:
        collmult: Collision strength multiplier [-] (QuaLiKiz debugging)
    """
    # From QLK source:
    # Nue(:) = 1._DBL/(1.09d-3) *Zeffx(:)*Nex(:)*Lambe(:)/(Tex(:))**1.5_DBL*collmult
    lambda_e = calc_lambda_e(ne, Te)
    return 1 / 1.09e-3 * zeff * ne * lambda_e / Te ** 1.5 * collmult


# More text [keV]  [-]  [m] [m] [m]
#  :math:`\Lambda_e`
#  :math:`\frac{r_{\text{out}} + r_{\text{in}}}{r_{\text{out,LCFS}} + r_{\text{in,LCFS}}}`
def calc_tau_bounce(Te, q, Ro, Rmin, x):
    """Calculate the QuaLiKiz bounce frequency :math:`\Lambda_e`

    x is in QuaLiKiz units: :wiki:`Input-and-output-variables#geometry`

    .. math:: \\frac{r_{out} + r_{out,LCFS}}{r_{out,LCFS} + r_{in,LCFS}}

    Args:
        Te: Electron temperature [keV]
        q: Safety factor [-]
        Ro: Midplane-averaged major radius of last-closed-flux-surface, QuaLiKiz internal [m]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        x: Normalized radius [m]
    """
    # From QLK source:
    # Nustar(:) = Nue(:)*qx(:)*Ro(:)/epsilon(:)**1.5/(SQRT(Tex(:)*1.d3*qe/me))
    epsilon = Rmin * x / Ro
    return q * Ro / (epsilon ** 1.5 * np.sqrt(Te * 1e3 * qe / me))


def calc_nustar_from_parts_qlkstyle(zeff, ne, Te, q, Ro, Rmin, x):
    """Calculate normalized collision frequency as in QuaLiKiz internals :math:`\nu^*`

    Args:
        zeff: Effective ion charge [-]
        ne: Electron density [1e19 m^-3]
        Te: Electron temperature [keV]
        q: Safety factor [-]
        Ro: Midplane-averaged major radius of last-closed-flux-surface, QuaLiKiz internal [m]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        x: Normalized radius [m] :math:`\frac{r_{\text{out}} + r_{\text{in}}}{r_{\text{out,LCFS}} + r_{\text{in,LCFS}}}`
    """
    tau_bounce = calc_tau_bounce(Te, q, Ro, Rmin, x)
    nu_e = calc_nu_e(ne, Te, zeff)
    return nu_e * tau_bounce


def calc_te_from_nustar(zeff, ne, nustar, q, Ro, Rmin, x):
    """Calculate electron temperature as function of normalized collision frequency

    Args:
        zeff: Effective ion charge [-]
        ne: Electron density [1e19 m^-3]
        nustar: QuaLiKiz-style normalized collision frequency [-]
        q: Safety factor [-]
        Ro: Midplane-averaged major radius of last-closed-flux-surface, QuaLiKiz internal [m]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        x: Normalized radius [m] :math:`\frac{r_{\text{out}} + r_{\text{in}}}{r_{\text{out,LCFS}} + r_{\text{in,LCFS}}}`
    """
    c1 = calc_c1(zeff, ne, q, Ro, Rmin, x)
    c2 = calc_c2(ne)

    z = np.array(-2 * np.exp(-2 * c2) * nustar / c1, ndmin=1)
    # If z is real, the lambert W function (z = f^-1(z.e^z) = W(z.e^z))
    # has a solution for z >= -1/e, and two solutions for z E (-1/e, 0)
    # See https://en.wikipedia.org/wiki/Lambert_W_function
    real_branches = []
    # For z -> 0, W_0(z) ~= x - x^2 + O(x^3), so do not use the 0th branch for now
    # if any(z > - 1/np.e):
    #    real_branches.append(0)
    if any((-1 / np.e < z) & (z < 0)):
        real_branches.append(-1)
    if len(real_branches) == 0:
        raise Exception("No real solution")

    for branch in real_branches:
        sol = np.array(
            1j * np.sqrt(c1) * np.sqrt(lambertw(z, branch)) / np.sqrt(2 * nustar)
        )
        sol = sol.real  # Solution only has a real part
        # -sol and sol are both solutions, but Te is > 0
        sol[sol < 0] = -sol[sol < 0]
        calced_nustar = calc_nustar_from_c1_c2(c1, c2, sol)
        if all(np.isclose(calced_nustar, nustar)):
            Te = sol
            break

    return Te


def calc_nustar_from_c1_c2(c1, c2, Te):
    nustar = c1 / Te ** 2 * (c2 + np.log(Te))
    return nustar


def calc_nustar_from_parts(zeff, ne, Te, q, Ro, Rmin, x):
    """Calculate normalized collision frequency as in Pythontools :math:`\nu^*`

    Inverse of `calc_te_from_nustar`

    Args:
        zeff: Effective ion charge [-]
        ne: Electron density [1e19 m^-3]
        Te: Electron temperature [keV]
        q: Safety factor [-]
        Ro: Midplane-averaged major radius of last-closed-flux-surface, QuaLiKiz internal [m]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        x: Normalized radius [m] :math:`\frac{r_{\text{out}} + r_{\text{in}}}{r_{\text{out,LCFS}} + r_{\text{in,LCFS}}}`
    """
    c1 = calc_c1(zeff, ne, q, Ro, Rmin, x)
    c2 = calc_c2(ne)
    nustar = calc_nustar_from_c1_c2(c1, c2, Te)
    return nustar


def calc_zeff(ionlist):
    if len(ionlist) < 1:
        raise ValueError("Ionlist empty. Cannot calculate Zeff")
    zeff = sum(ion["n"] * ion["Z"] ** 2 for ion in ionlist)
    return zeff


# In pure toroidal calculations we ignore the Machtor*\epsilon^2
# term of gammaE, consistent with QLK ordering
# See https://gitlab.com/qualikiz-group/QuaLiKiz-pythontools/-/merge_requests/22#note_369206049


def calc_puretor_Machpar_from_Machtor(Machtor, epsilon, q):
    if np.all(Machtor == 0):
        logger.warning("Machtor is zero! Machpar will be zero too")
    Machpar = Machtor / np.sqrt(1 + (epsilon / q) ** 2)
    return Machpar


def calc_puretor_Machtor_from_Machpar(Machpar, epsilon, q):
    if np.all(Machpar == 0):
        logger.warning("Machtor is zero! Machpar will be zero too")
    Machtor = Machpar * np.sqrt(1 + (epsilon / q) ** 2)
    return Machtor


def calc_puretor_Autor_from_gammaE(gammaE, epsilon, q):
    if np.all(gammaE == 0):
        logger.warning("gammaE is zero! Autor will be zero too")
    Autor = -gammaE * np.sqrt(1 + (q / epsilon) ** 2)
    return Autor


def calc_puretor_Aupar_from_gammaE(gammaE, epsilon, q):
    if np.all(gammaE == 0):
        logger.warning("gammaE is zero! Autor will be zero too")
    Aupar = -gammaE * q / epsilon
    return Aupar


def calc_puretor_Aupar_from_Autor(Autor, epsilon, q):
    if np.all(Autor == 0):
        logger.warning("Autor is zero! Aupar will be zero too")
    Aupar = Autor / np.sqrt(1 + (epsilon / q) ** 2)
    return Aupar


def calc_puretor_gammaE_from_Autor(Autor, epsilon, q):
    if np.all(Autor == 0):
        logger.warning("Autor is zero! gammaE will zero too")
    gammaE = -Autor / np.sqrt(1 + (q / epsilon) ** 2)
    return gammaE


def calc_puretor_Autor_from_Aupar(Aupar, epsilon, q):
    if np.all(Aupar == 0):
        logger.warning("Aupar is zero! Aupar will be zero too")
    Autor = Aupar * np.sqrt(1 + (epsilon / q) ** 2)
    return Autor


def calc_puretor_gammaE_from_Aupar(Aupar, epsilon, q):
    if np.all(Aupar == 0):
        logger.warning("Aupar is zero! gammaE will be zero too!")
    gammaE = -Aupar * epsilon / q
    return gammaE


def calc_puretor_absolute(epsilon, q, Machtor=np.nan, Machpar=np.nan):
    if np.sum([np.all(~np.isnan(x)) for x in [Machpar, Machtor]]) != 1:
        raise ValueError(
            "Need to supply either Machpar or Machtor. "
            "Got Machpar={!s} and Machtor={!s}".format(Machtor, Machpar)
        )
    if ~np.all(np.isnan(Machtor)):
        Machpar = calc_puretor_Machpar_from_Machtor(Machtor, epsilon, q)
    elif ~np.all(np.isnan(Machpar)):
        Machtor = calc_puretor_Machtor_from_Machpar(Machpar, epsilon, q)
    return [Machtor, Machpar]


def calc_puretor_gradient(epsilon, q, Aupar=np.nan, Autor=np.nan, gammaE=np.nan):
    if np.sum([np.all(~np.isnan(x)) for x in [Aupar, Autor, gammaE]]) != 1:
        raise ValueError(
            "Need to supply either Aupar, Autor or gammaE. "
            "Got Aupar={!s}, Autor={!s} and gammaE={!s}".format(Aupar, Autor, gammaE)
        )
    if ~np.all(np.isnan(Autor)):
        Aupar = calc_puretor_Aupar_from_Autor(Autor, epsilon, q)
        gammaE = calc_puretor_gammaE_from_Autor(Autor, epsilon, q)
    elif ~np.all(np.isnan(Aupar)):
        Autor = calc_puretor_Autor_from_Aupar(Aupar, epsilon, q)
        gammaE = calc_puretor_gammaE_from_Aupar(Aupar, epsilon, q)
    elif ~np.all(np.isnan(gammaE)):
        Aupar = calc_puretor_Aupar_from_gammaE(gammaE, epsilon, q)
        Autor = calc_puretor_Autor_from_gammaE(gammaE, epsilon, q)
    return [Aupar, Autor, gammaE]


def calc_puretor_Aupar_from_Autor_Machtor(Autor, Machtor, epsilon, q, smag):
    if np.all(Autor == 0):
        logger.warning("Autor is zero! Aupar will be zero too")
    Aupar = Autor / np.sqrt(1 + (epsilon / q) ** 2) + Machtor * q * epsilon * (
        smag - 1
    ) / ((q ** 2 + epsilon ** 2) ** 1.5)
    return Aupar


def calc_no_pol_gammaE(eps, q, smag, Bo, r, Ro, Ani, Ati, pi, ne, Ane, Mtor, Autor):
    """Equation to calculate gammaE as a function of Machtor and Autor

    For this, equation (31) from
    https://gitlab.com/qualikiz-group/QuaLiKiz-documents/-/blob/main/reports/rotation_variables.pdf
    is used. It only holds when assuming there is no poloidal rotation and no curvature in ion
    density and temperature.

    Args:
        eps: inverse aspect ratio
        q: safety factor
        smag: magnetic shear
        Bo: magnetic field at magnetic axis
        r: radial position
        Ani: Logarithmic ion density gradient, in array form containing all ions
        Ati: Logarithmic ion temperature gradient, in array form containing all ions
        pi: ion pressure, in array form containing all ions
        ne: electron density
        Ane: Logarithmic electron density gradient
        Mtor: Toroidal velocity
        Autor: Toroidal velocity gradient
    """

    cref = 3.094969e5
    if np.all(Mtor == 0) and np.all(Autor == 0):
        logger.warning(
            "Machtor and Autor are zero! gammaE will only have a contribution from the second derivative of pressure!"
        )

    pi = pi * qe  # Apply the last unit conversion
    s_Aniti = [sum(x) for x in zip(Ani, Ati)]
    Antp_i = [a * b for a, b in zip(s_Aniti, pi)]
    p_Aniti = [a * b for a, b in zip(Ani, Ati)]
    p_Aneni = [Ane * x for x in Ani]
    p_Aneti = [Ane * x for x in Ati]
    cross = [2 * a - 1 * b - 1 * c for a, b, c in zip(p_Aniti, p_Aneni, p_Aneti)]
    sdp = [a * b for a, b in zip(cross, pi)]

    gammaE = (
        Mtor
        / np.sqrt(1.0 + (eps / q) ** 2.0)
        * (smag - 1.0)
        / q
        / (1.0 + (q / eps) ** 2.0)
        - Autor / np.sqrt(1 + (q / eps) ** 2)
        - np.sum(Antp_i) / (ne * qe) * (smag - 1) / (Bo * r * cref)
        + np.sum(sdp) / (ne * qe) / (Bo * Ro * cref)
    )

    return gammaE


def calc_no_pol_Machtor(eps, q, smag, Bo, r, Ro, Ani, Ati, pi, ne, Ane, gammaE, Autor):
    """Equation to calculate Machtor as a function of gammaE and Autor

    For this, equation (31) from
    https://gitlab.com/qualikiz-group/QuaLiKiz-documents/-/blob/main/reports/rotation_variables.pdf
    is rewritten. It only holds when assuming there is no poloidal rotation and no curvature in ion
    density and temperature.

    Args:
        eps: inverse aspect ratio
        q: safety factor
        smag: magnetic shear
        Bo: magnetic field at magnetic axis
        r: radial position
        Ani: Logarithmic ion density gradient, in array form containing all ions
        Ati: Logarithmic ion temperature gradient, in array form containing all ions
        pi: ion pressure, in array form containing all ions
        ne: electron density
        Ane: Logarithmic electron density gradient
        gammaE: Perpendicular ExB flow shear
        Autor: Toroidal velocity gradient
    """

    cref = 3.094969e5
    if np.all(gammaE == 0) and np.all(Autor == 0):
        logger.warning(
            "gammaE and Autor are zero! Machtor will only have a contribution from the second derivative of pressure!"
        )

    pi = pi * qe  # Apply the last unit conversion
    s_Aniti = [sum(x) for x in zip(Ani, Ati)]
    Antp_i = [a * b for a, b in zip(s_Aniti, pi)]
    p_Aniti = [a * b for a, b in zip(Ani, Ati)]
    p_Aneni = [Ane * x for x in Ani]
    p_Aneti = [Ane * x for x in Ati]
    cross = [2 * a - 1 * b - 1 * c for a, b, c in zip(p_Aniti, p_Aneni, p_Aneti)]
    sdp = [a * b for a, b in zip(cross, pi)]

    Mtor = (
        -np.sqrt(1 + (eps / q) ** 2)
        / (smag - 1)
        * q
        * (1 + (q / eps) ** 2)
        * (
            -gammaE
            - Autor / np.sqrt(1 + (q / eps) ** 2)
            - np.sum(Antp_i) / (ne * qe) * (smag - 1) / (Bo * r * cref)
            + np.sum(sdp) / (ne * qe) / (Bo * Ro * cref)
        )
    )
    return Mtor


def calc_no_pol_Autor(eps, q, smag, Bo, r, Ro, Ani, Ati, pi, ne, Ane, Mtor, gammaE):
    """Equation to calculate Autor as a function of Machtor and gammaE

    For this, equation (31) from
    https://gitlab.com/qualikiz-group/QuaLiKiz-documents/-/blob/main/reports/rotation_variables.pdf
    is rewritten. It only holds when assuming there is no poloidal rotation and no curvature in ion
    density and temperature.

    Args:
        eps: inverse aspect ratio
        q: safety factor
        smag: magnetic shear
        Bo: magnetic field at magnetic axis
        r: radial position
        Ani: Logarithmic ion density gradient, in array form containing all ions
        Ati: Logarithmic ion temperature gradient, in array form containing all ions
        pi: ion pressure, in array form containing all ions
        ne: electron density
        Ane: Logarithmic electron density gradient
        Mtor: Toroidal velocity
        gammaE: Perpendicular ExB flow shear
    """

    cref = 3.094969e5
    if np.all(Mtor == 0) and np.all(gammaE == 0):
        logger.warning(
            "Machtor and gammaE are zero! Autor will only have a contribution from the second derivative of pressure!"
        )

    pi = pi * qe  # Apply the last unit conversion
    s_Aniti = [sum(x) for x in zip(Ani, Ati)]
    Antp_i = [a * b for a, b in zip(s_Aniti, pi)]
    p_Aniti = [a * b for a, b in zip(Ani, Ati)]
    p_Aneni = [Ane * x for x in Ani]
    p_Aneti = [Ane * x for x in Ati]
    cross = [2 * a - 1 * b - 1 * c for a, b, c in zip(p_Aniti, p_Aneni, p_Aneti)]
    sdp = [a * b for a, b in zip(cross, pi)]

    Autor = np.sqrt(1 + (q / eps) ** 2) * (
        -gammaE
        + Mtor / np.sqrt(1 + (eps / q) ** 2) * (smag - 1) / q / (1 + (q / eps) ** 2)
        - np.sum(Antp_i) / (ne * qe) * (smag - 1) / (Bo * r * cref)
        + np.sum(sdp) / (ne * qe) / (Bo * Ro * cref)
    )
    return Autor


def calc_epsilon_from_parts(x, Rmin, Ro):
    epsilon = x * Rmin / Ro
    return epsilon


def calc_GB_scale(Ai0, Te, Bo, Rmin):
    """Calculate GB scaling constant

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
    """
    # From QLK source:
    # chi_GB(ir)=SQRT(Ai(ir,1)*mp)/(qe**2*Bo(ir)**2)*((Tex(ir)*1e3*qe)**1.5)/Rmin(ir)  !GyroBohm normalisation in m^2/s based on main ion
    Te_SI = qe * 1e3 * Te
    return np.sqrt(Ai0 * mp) / (qe ** 2 * Bo ** 2) * Te_SI ** 1.5 / Rmin


def calc_ef_fac(Ai0, Te, Bo, Rmin, ns, Ts):
    """Calculate GB heat flux scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        ns: Density of species of interest [1e19 m^-3]
        Ts: Temperature of species of interest [keV]
    """
    # From QLK source:
    # defe(ir) = (efe(ir)/(Nex(ir)*1e19*Tex(ir)*1e3*qe/Rmin(ir)))/chi_GB(ir)
    # defi(ir,ion) = (efi(ir,ion)/(Nix(ir,ion)*1e19*Tix(ir,ion)*1e3*qe/Rmin(ir)))/chi_GB(ir)
    Ts_SI = qe * 1e3 * Ts
    GB_scale = calc_GB_scale(Ai0, Te, Bo, Rmin)
    return Rmin / (1e19 * ns * Ts_SI * GB_scale)


def calc_pf_fac(Ai0, Te, Bo, Rmin, ns):
    """Calculate GB particle flux scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        ns: Density of species of interest [1e19 m^-3]
    """
    # From QLK source:
    # dpfe(ir) = (pfe(ir)/(Nex(ir)*1e19/Rmin(ir)))/chi_GB(ir)
    # dpfi(ir,ion) = (pfi(ir,ion)/(Nix(ir,ion)*1e19/Rmin(ir)))/chi_GB(ir)
    GB_scale = calc_GB_scale(Ai0, Te, Bo, Rmin)
    return Rmin / (1e19 * ns * GB_scale)


def calc_vx_fac(Ai0, Te, Bo, Rmin, ns):
    """Calculate GB particle pinches scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        ns: Density of species of interest [1e19 m^-3]
    """
    # From QLK source:
    # vte_GB(ir) = vte_SI(ir)*Rmin(ir)/chi_GB(ir)
    # vti_GB(ir,:) = vti_SI(ir,:)*Rmin(ir)/chi_GB(ir)
    GB_scale = calc_GB_scale(Ai0, Te, Bo, Rmin)
    return Rmin / GB_scale


def calc_vex_fac(Ai0, Te, Bo, Rmin, ns):
    """Calculate GB heat pinches scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        ns: Density of species of interest [1e19 m^-3]
    """
    # From QLK source:
    # vene_GB(ir) = vene_SI(ir)*Rmin(ir)/chi_GB(ir)
    # veni_GB(ir,:) = veni_SI(ir,:)*Rmin(ir)/chi_GB(ir)
    return calc_vx_fac(Ai0, Te, Bo, Rmin, ns)


def calc_chie_fac(Ai0, Te, Bo, Rmin):
    """Calculate GB heat conductivity scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
    """
    # From QLK source:
    # chiee_GB(ir) = chiee_SI(ir)/chi_GB(ir)
    # chiei_GB(ir,:) = chiei_SI(ir,:)/chi_GB(ir)
    GB_scale = calc_GB_scale(Ai0, Te, Bo, Rmin)
    return 1 / GB_scale


def calc_df_fac(Ai0, Te, Bo, Rmin):
    """Calculate GB particle diffusivity scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
    """
    # From QLK source:
    # dfe_GB(ir) = dfe_SI(ir)/chi_GB(ir)
    # dfi_GB(ir,:) = dfi_SI(ir,:)/chi_GB(ir)
    return calc_chie_fac(Ai0, Te, Bo, Rmin)


def calc_cth(Ts, ms):
    """Calculate species thermal velocity

    Args:
        Ts: Temperature of species of interest [keV]
        ms: Mass of the species of interest [kg]
    """
    # From QLK source:
    # cthe(:) = SQRT(2._DBL*qe*Tex(:)*1.d3/me)
    # cthi(:,i) = SQRT(2._DBL*qe*Tix(:,i)*1.d3/mi(:,i)) !Thermal velocities
    return np.sqrt(2 * qe * Ts * 1e3 / ms)


def calc_vf_fac(Ai0, Te, Bo, Rmin, ns, Ts, Ro, ms):
    """Calculate GB angular momentum flux scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Bo: Magnetic field (with plasma included) at magnetic axis [T]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
        ns: Density of species of interest [1e19 m^-3]
        Ts: Temperature of species of interest [keV]
        Ro: Midplane-averaged major radius of last-closed-flux-surface [m]
        ms: Mass of the species of interest [kg]
    """
    # From QLK source:
    # dvfi(ir,ion) = (vfi(ir,ion)/(Nix(ir,ion)*1e19*Ai(ir,ion)*mp*cthi(ir,ion)*Ro/Rmin(ir)))/chi_GB(ir)
    GB_scale = calc_GB_scale(Ai0, Te, Bo, Rmin)
    cthi = calc_cth(Ts, ms)
    return Rmin / (ns * 1e19 * ms * cthi * Ro * GB_scale)


def calc_csou(Ai0, Te):
    """Calculate (electron) sound speed

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
    """
    # From QLK source:
    # csou(:) = SQRT(qe*Tex(:)*1.d3/mi(:,1)) !Calculated with respect to main ions (assumed index 1)
    mi0 = Ai0 * mp
    return np.sqrt(qe * Te * 1e3 / mi0)


def calc_gam_fac(Ai0, Te, Rmin):
    """Calculate GB growthrate scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
    """
    # From QLK source:
    # gamGB(:)=csou(:)/Rmin(:) !gyrobohm 1/s unit
    # gam_GB(ir,j,k)=AIMAG(sol(ir,j,k))*nwgmat(ir,j)/gamGB(ir)
    # gam_SI(ir,j,k)=AIMAG(sol(ir,j,k))*nwgmat(ir,j)
    csou = calc_csou(Ai0, Te)
    gamGB = csou / Rmin
    return 1 / gamGB


def calc_ome_fac(Ai0, Te, Rmin):
    """Calculate GB frequency scaling

    Args:
        Ai0: Main ion mass [amu]
        Te: Electron temperature [keV]
        Rmin: Midplane-averaged minor radius of last-closed-flux-surface [m]
    """
    # From QLK source:
    # gamGB(:)=csou(:)/Rmin(:) !gyrobohm 1/s unit
    # ome_GB(ir,j,k)=REAL(sol(ir,j,k))*nwgmat(ir,j)/gamGB(ir)
    # ome_SI(ir,j,k)=REAL(sol(ir,j,k))*nwgmat(ir,j)
    return calc_gam_fac(Ai0, Te, Rmin)


def dimensionless_to_physical(
    inp,
    zi_in=None,
    te_in=None,
    lref_in=None,
    ro_in=None,
    rlcfs_in=None,
    autor_in=None,
    pure_tor=False,
    qn_ion=None,
    grad_qn_ion=None,
    zeff_ion=None,
    grad_zeff_ion=None,
    verbose=0,
):
    """Converts dimensionless parameters to equivalent physical parameters

    Args:
        inp: Input data, only tested as pandas DataFrame
        zi_in: Charge numbers to be used, set to None to use values in inp, list
        te_in: Reference electron temperature to use, set to None to use values in inp, float [eV]
        lref_in: Reference length to use, set to None to use values in inp, float [m]
        ro_in: Point midplane averaged major radius to use, set to None to use values in inp, float [m]
        rlcfs_in: Reference midplane averaged minor radius to use, set to None to use values in inp, float [m]
        autor_in: Dimensionless flow shear gradient to use, set to None to use values in inp, float
        pure_tor: Apply purely toroidal flow assumption when calculating output values, bool
        qn_ion: Toggle enforcing of quasineutrality, integer value chooses which ion is modified, int
        grad_qn_ion: Toggle enforcing of gradient quasineutrality, integer value chooses which ion is modified, int
        zeff_ion: Toggle enforcing of Zeff if provided in inp, integer value chooses which ion is modified, int
        grad_zeff_ion: Toggle enforcing of grad_Zeff if provided in inp, integer value chooses which ion is modified, int
        verbose: Set verbosity (not much verbosity is implemented)

    Returns:
        outp: Converted data, pandas DataFrame
    """

    outp = pd.DataFrame()
    if not inp.empty:

        # Determine number of ion species in input data
        #    Charge number of zero reserved for identifying species for which data should be taken from input
        #    Useful when there is scan-dependent charge numbers
        zi = []
        if isinstance(zi_in, list):
            for ii in range(0, len(zi_in)):
                itag = "{:d}".format(ii)
                zi.append(zi_in[ii])
                if zi_in[ii] == 0 and "Zi" + itag in inp:
                    if len(inp["Zi" + itag]) > 1 and all(
                        np.isclose(inp["Zi" + itag][1:], inp["Zi" + itag][:-1])
                    ):
                        zi[ii] = float(inp["Zi" + itag][0])
                    elif len(inp["Zi" + itag]) == 1:
                        zi[ii] = float(inp["Zi" + itag][0])
                elif zi_in == 0 and "Zi" in inp and ii == 0:
                    if len(inp["Zi"]) > 1 and all(
                        np.isclose(inp["Zi"][1:], inp["Zi"][:-1])
                    ):
                        zi[ii] = float(inp["Zi"][0])
                    elif len(inp["Zi"]) == 1:
                        zi[ii] = float(inp["Zi"][0])
        numions = len(zi)
        itag = "{:d}".format(numions)
        while "Zi" + itag in inp:
            zi.append(0.0)
            if len(inp["Zi" + itag]) > 1 and all(
                np.isclose(inp["Zi" + itag][1:], inp["Zi" + itag][:-1])
            ):
                zi[-1] = float(inp["Zi" + itag][0])
            elif len(inp["Zi" + itag]) == 1:
                zi[-1] = float(inp["Zi" + itag][0])
            numions += 1
            itag = "{:d}".format(numions)
        if len(zi) < numions or numions == 0:
            raise ValueError("Insufficient number of ion charges provided, aborting!")
        for ii in range(0, len(zi)):
            if zi[ii] < 0.0:
                raise ValueError("Negative ion charge number provided, aborting!")
        numions = len(zi)

        # Set flags to enforce quasineutrality, integer value sets which ion species is adjusted to enforce QN
        iqn = qn_ion if qn_ion is not None and qn_ion < len(zi) else None
        if iqn is not None and iqn < 0:
            iqn = numions - 1
        igqn = (
            grad_qn_ion if grad_qn_ion is not None and grad_qn_ion < len(zi) else None
        )
        if igqn is not None and igqn < 0:
            igqn = numions - 1
        # Set flags to enforce effective charge, integer value sets which ion species is adjusted to enforce QN
        izf = zeff_ion if zeff_ion is not None and zeff_ion < len(zi) else None
        if izf is not None and izf < 0:
            izf = numions - 1
        igzf = (
            grad_zeff_ion
            if grad_zeff_ion is not None and grad_zeff_ion < len(zi)
            else None
        )
        if igzf is not None and igzf < 0:
            igzf = numions - 1
        if izf is not None and iqn is None:
            raise ValueError(
                "Effective charge cannot be enforced without also enforcing quasineutrality, aborting!"
            )
        if igzf is not None and igqn is None:
            raise ValueError(
                "Effective charge gradient cannot be enforced without also enforcing gradient quasineutrality, aborting!"
            )

        # Transfer physical parameter values already in input data to output
        target_vars = [
            "r",
            "grad_q",
            "ne",
            "grad_ne",
            "grad_Te",
            "Bo",
            "utor",
            "grad_utor",
            "dpi",
            "grad_dpi",
        ]
        target_ivars = ["Ti", "grad_Ti", "ni", "grad_ni", "Zi", "Ai"]
        for var in target_vars:
            if var in inp:
                outp[var] = inp[var] * 1.0
        for ii in range(0, numions):
            itag = "{:d}".format(ii)
            for var in target_ivars:
                if var + itag in inp:
                    outp[var + itag] = inp[var + itag] * 1.0
                elif var in inp and ii == 0:
                    outp[var + itag] = inp[var] * 1.0

        # Set general constants, those not explicitly given in input data must be passed in during function call
        eom = 1.75882e11
        cref = 3.0949691e5
        lref = None
        if "Ro" in inp:
            lref = inp["Ro"]
        if lref_in is not None:
            lref = lref_in
        if lref is None:
            raise ValueError(
                "Reference gradient length for unnormalization not provided, aborting!"
            )
        else:
            outp["Ro"] = lref
        ro = None
        if "Ro" in inp:
            ro = inp["Ro"]
        if ro_in is not None:
            ro = ro_in
        if ro is None:
            ro = lref
        if ro is not None:
            outp["Ro"] = ro
        rlcfs = None
        if "Rmin" in inp:
            rlcfs = inp["Rmin"]
        if rlcfs_in is not None:
            rlcfs = rlcfs_in
        if rlcfs is None:
            raise ValueError(
                "Reference minor radius for unnormalization not provided, aborting!"
            )
        else:
            outp["Rmin"] = rlcfs
        te = None
        if "Te" in inp:
            te = inp["Te"] * 1.0e3
        if te_in is not None:
            te = te_in
        if te is None:
            raise ValueError("Base electron temperature not provided, aborting!")
        if np.all(te > 1.0e5):
            logger.warning(
                "Dimensionless electron temperatures should be in keV, they seem a bit large..."
            )

        # Start converting dimensionless parameters to physical parameters
        if "x" in inp:
            outp["r"] = inp["x"] * rlcfs
        if "r" in outp:
            print("Calculation of midplane-average minor radius complete!")
        else:
            raise ValueError("Normalized radial coordinate not provided, aborting!")
        if "q" in inp:
            outp["q"] = inp["q"]
        if "q" in outp:
            print("Calculation of safety factor complete!")
        if "smag" in inp and "q" in outp and "r" in outp:
            outp["grad_q"] = inp["smag"] * outp["q"] / outp["r"]
        if "grad_q" in outp:
            print("Calculation of safety factor gradient complete!")
        bpolbyb = None
        btorbyb = None
        grad_bpolbyb = None
        grad_btorbyb = None
        if "r" in outp and "q" in outp:
            eps = outp["r"] / ro
            denom = 1.0 / (np.power(outp["q"], 2.0) + np.power(eps, 2.0))
            bpolbyb = np.sqrt(np.power(eps, 2.0) * denom)
            btorbyb = np.sqrt(np.power(outp["q"], 2.0) * denom)
            if "grad_q" in outp:
                # These assume grad_ro = 0.0, which is true since ro is global for given equilibrium
                grad_bpolbyb = np.sqrt(denom) / ro - bpolbyb * denom * (
                    outp["q"] * outp["grad_q"] + eps / ro
                )
                grad_btorbyb = np.sqrt(denom) * outp["grad_q"] - btorbyb * denom * (
                    outp["q"] * outp["grad_q"] + eps / ro
                )
            else:
                grad_bpolbyb = outp["x"] * 0.0
                grad_btorbyb = outp["x"] * 0.0
        nu = None
        if "logNustar" in inp:
            nu = np.power(10.0, inp["logNustar"])
        elif "Nustar" in inp:
            nu = inp["Nustar"] * 1.0
        if nu is not None and "Zeff" in inp and "q" in outp and "r" in outp:
            # The constants in this calculation are consistent with QuaLiKiz, may differ from other literature
            tbounce = (
                outp["q"] * ro * np.power(outp["r"] / ro, -1.5) / np.sqrt(eom * te)
            )
            collei = nu / tbounce
            inv_nustar = pd.DataFrame()
            inv_nustar["te"] = outp["q"] * 0.0 + te * 1.0e-3
            inv_nustar["bal"] = collei / (
                9174.3119266 * inp["Zeff"] / np.power(inv_nustar["te"], 1.5)
            )
            # Uses root_scalar function to solve for ne, convergence and unique solution not guaranteed
            func_ne20 = lambda row: root_scalar(
                lambda ne: (15.2 - np.log(row["te"]) + 0.5 * np.log(ne)) * ne
                - row["bal"],
                x0=0.01,
                x1=1.0,
                maxiter=100,
            )
            sol_ne20 = inv_nustar.apply(func_ne20, axis=1)
            outp["ne"] = sol_ne20.apply(lambda sol: 1.0e20 * sol.root)
        elif "ne" in inp:
            outp["ne"] = inp["ne"] * 1.0e19
        # if np.any(~np.isfinite(outp['ne'])):
        #    raise ValueError("Non-finite electron densities generated by direct conversion, aborting!")
        # if np.any(outp['ne'] < -1.0e-10):
        #    raise ValueError("Negative electron densities generated by direct conversion, aborting!")
        if "ne" in outp:
            print("Calculation of electron density complete!")
        if "Ane" in inp and "ne" in outp:
            outp["grad_ne"] = -inp["Ane"] * outp["ne"] / lref
        # if np.any(~np.isfinite(outp['grad_ne'])):
        #    raise ValueError("Non-finite electron density gradients generated by direct conversion, aborting!")
        if "grad_ne" in outp:
            print("Calculation of electron density gradient complete!")
        if "r" in outp:
            outp["Te"] = outp["r"] * 0.0 + te
        # if np.any(~np.isfinite(outp['Te'])):
        #    raise ValueError("Non-finite electron temperatures generated by direct conversion, aborting!")
        # if np.any(outp['Te'] < -1.0e-10):
        #    raise ValueError("Negative electron temperatures generated by direct conversion, aborting!")
        if "Te" in outp:
            print("Calculation of electron temperature complete!")
        if "Ate" in inp:
            outp["grad_Te"] = -inp["Ate"] * te / lref
        # if np.any(~np.isfinite(outp['grad_Te'])):
        #    raise ValueError("Non-finite electron temperature gradients generated by direct conversion, aborting!")
        if "grad_Te" in outp:
            print("Calculation of electron temperature gradient complete!")

        # This branch is taken if ion data is not enumerated in input data, can specify 2 ion species if Z is given
        if "Ati0" not in inp and "Ti_Te0" not in inp:
            if len(zi) > 0 and zi[0] > 0.0:
                outp["Zi0"] = outp["r"] * 0.0 + zi[0]
            elif "Zi" in inp:
                outp["Zi0"] = outp["r"] * 0.0 + inp["Zi"]
            if "Ai" in inp:
                outp["Ai0"] = outp["r"] * 0.0 + inp["Ai"]
            if "Ti_Te" in inp:
                outp["Ti0"] = inp["Ti_Te"] * te
            # if np.any(~np.isfinite(outp['Ti0'])):
            #    raise ValueError("Non-finite electron temperatures generated by direct conversion, aborting!")
            # if np.any(outp['Ti0'] < -1.0e-10):
            #    raise ValueError("Negative electron temperatures generated by direct conversion, aborting!")
            if "Ati" in inp and "Ti0" in outp:
                outp["grad_Ti0"] = -inp["Ati"] * outp["Ti0"] / lref
            if "grad_Ti0" not in outp and "grad_Te" in outp and "Ti0" in outp:
                outp["grad_Ti0"] = outp["grad_Te"] * outp["Ti0"] / te
            # if np.any(~np.isfinite(outp['grad_Ti0'])):
            #    raise ValueError("Non-finite electron temperature gradients generated by direct conversion, aborting!")
            if "normni" in inp and "ne" in outp:
                outp["ni0"] = inp["normni"] * outp["ne"]
                if "Zeff" in inp:
                    logger.warning(
                        "Over-constrained problem for ion densities, Zeff match not guaranteed",
                    )
            elif "Zeff" in inp and "ne" in outp and len(zi) > 1:
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["ni0"] = outp["ne"] * (zi1 - inp["Zeff"]) / ((zi1 - zi0) * zi0)
            if "ni0" not in outp and "ne" in outp:
                outp["ni0"] = outp["ne"]
            # if np.any(~np.isfinite(outp['ni0'])):
            #    raise ValueError("Non-finite ion densities generated by direct conversion, aborting!")
            # if np.any(outp['ni0'] < -1.0e-10):
            #    raise ValueError("Negative ion densities generated by direct conversion, aborting!")
            if "Ani" in inp:
                outp["grad_ni0"] = -inp["Ani"] * outp["ni0"] / lref
                if "grad_Zeff" in inp:
                    logger.warning(
                        "Over-constrained problem for ion density gradients, grad_Zeff match not guaranteed",
                    )
            elif (
                "grad_Zeff" in inp
                and "Zeff" in inp
                and "grad_ne" in outp
                and "ne" in outp
                and len(zi) > 1
            ):
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["grad_ni0"] = (
                    outp["grad_ne"] * (zi1 - inp["Zeff"])
                    - outp["ne"] * inp["grad_Zeff"]
                ) / ((zi1 - zi0) * zi0)
            if (
                "grad_ni0" not in outp
                and "ni0" in outp
                and "ne" in outp
                and "grad_ne" in outp
            ):
                outp["grad_ni0"] = outp["ni0"] * outp["grad_ne"] / outp["ne"]
            # if np.any(~np.isfinite(outp['grad_ni0'])):
            #    raise ValueError("Non-finite ion density gradients generated by direct conversion, aborting!")
            numions = 1

            # Apply second ion via two-ion quasineutrality if a second charge number is given
            if "grad_Ti0" in outp and "grad_ni0" in outp and len(zi) > 1:
                if "Zi1" not in outp and zi[1] > 0.0:
                    outp["Zi1"] = outp["r"] * 0.0 + zi[1]
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["Ti1"] = outp["Ti0"]
                outp["grad_Ti1"] = outp["grad_Ti0"]
                outp["ni1"] = (outp["ne"] - zi0 * outp["ni0"]) / zi1
                outp["grad_ni1"] = (outp["grad_ne"] - zi0 * outp["grad_ni0"]) / zi1
                # if np.any(~np.isfinite(outp['ni1'])):
                #    raise ValueError("Non-finite ion densities generated by two-ion quasineutrality, aborting!")
                # if np.any(outp['ni1'] < -1.0e-10):
                #    raise ValueError("Negative ion densities generated by two-ion quasineutrality, aborting!")
                # if np.any(~np.isfinite(outp['grad_ni1'])):
                #    raise ValueError("Non-finite ion density gradients generated by two-ion quasineutrality, aborting!")
                numions = 2

        # This branch is taken if ion data is enumerated in input data, can specify N ions if Z is given
        else:
            ni_missing = []
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if ii < len(zi) and zi[ii] > 0.0:
                    outp["Zi" + itag] = outp["r"] * 0.0 + zi[ii]
                elif "Zi" + itag in inp:
                    outp["Zi" + itag] = outp["r"] * 0.0 + inp["Zi" + itag]
                if "Ai" in inp:
                    outp["Ai" + itag] = outp["r"] * 0.0 + inp["Ai" + itag]
                if "Ti_Te" + itag in inp:
                    outp["Ti" + itag] = inp["Ti_Te" + itag] * te
                elif "Ti_Te0" in inp:
                    outp["Ti" + itag] = inp["Ti_Te0"] * te
                elif "Ti_Te" in inp:
                    outp["Ti" + itag] = inp["Ti_Te"] * te
                # if np.any(~np.isfinite(outp['Ti'+itag])):
                #    raise ValueError("Non-finite electron temperatures generated by direct conversion, aborting!")
                # if np.any(outp['Ti'+itag] < -1.0e-10):
                #    raise ValueError("Negative electron temperatures generated by direct conversion, aborting!")
                if "Ati" + itag in inp and "Ti" + itag in outp:
                    outp["grad_Ti" + itag] = (
                        -inp["Ati" + itag] * outp["Ti" + itag] / lref
                    )
                elif "Ati0" in inp and "Ti" + itag in outp:
                    outp["grad_Ti" + itag] = -inp["Ati0"] * outp["Ti" + itag] / lref
                elif "Ati" in inp and "Ti" + itag in outp:
                    outp["grad_Ti" + itag] = -inp["Ati"] * outp["Ti" + itag] / lref
                if (
                    "grad_Ti" + itag not in outp
                    and "grad_Te" in outp
                    and "Ti" + itag in outp
                ):
                    outp["grad_Ti" + itag] = outp["grad_Te"] * outp["Ti" + itag] / te
                # if np.any(~np.isfinite(outp['grad_Ti'+itag])):
                #    raise ValueError("Non-finite electron temperature gradients generated by direct conversion, aborting!")
                if "normni" + itag in inp and "ne" in outp:
                    outp["ni" + itag] = inp["normni" + itag] * outp["ne"]
                if "ni" + itag not in outp:
                    ni_missing.append(ii)
                # else:
                #    if np.any(~np.isfinite(outp['ni'+itag])):
                #        raise ValueError("Non-finite ion densities generated by direct conversion, aborting!")
                #    if np.any(outp['ni'+itag] < -1.0e-10):
                #        raise ValueError("Negative ion densities generated by direct conversion, aborting!")

            # Setup required data for enforcing effective charge, if specified
            if "Zeff" in inp:
                if iqn is not None and iqn < numions:
                    if len(ni_missing) == 0:
                        ni_missing.append(iqn)
                        logger.warning(
                            "Quasineutrailty enforced on ion %d, densities may no longer represent input data",
                            iqn,
                        )
                if izf is not None and izf < numions:
                    if len(ni_missing) == 1:
                        ni_missing.append(izf)
                        logger.warning(
                            "Effective charge enforced on ion %d, densities may no longer represent input data",
                            izf,
                        )
                    else:
                        logger.warning(
                            "Issue detected in ion density consistency, chosen ion not modified to match Zeff",
                        )
                if len(ni_missing) < 2:
                    logger.warning(
                        "Over-constrained problem for ion densities, Zeff match not guaranteed",
                    )
            # Enforcing effective charge
            if len(ni_missing) == 2 and "Zeff" in inp and "ne" in outp:
                atag = "{:d}".format(ni_missing[0])
                btag = "{:d}".format(ni_missing[1])
                zi_missing = (
                    outp["Zi" + btag] if "Zi" + btag in outp else zi[ni_missing[1]]
                )
                zi_other = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_zeff = outp["ne"] * (zi_other - inp["Zeff"])
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ni_missing and "ni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_zeff = sol_zeff - zii * (zi_other - zii) * outp["ni" + itag]
                outp["ni" + btag] = sol_zeff / (zi_missing * (zi_other - zi_missing))
                if np.any(~np.isfinite(outp["ni" + btag])):
                    raise ValueError(
                        "Non-finite ion densities generated in enforcing Zeff, aborting!"
                    )
                if np.any(outp["ni" + btag] < -1.0e-10):
                    raise ValueError(
                        "Negative ion densities generated in enforcing Zeff, aborting!"
                    )
                ni_missing.pop(1)
            # Setup required data for enforcing quasineutrality, if specified (already set up if effective charge also enforced)
            if len(ni_missing) == 0:
                if iqn is not None and iqn < numions:
                    ni_missing.append(iqn)
                    logger.warning(
                        "Quasineutrailty enforced on ion %d, densities may no longer represent input data",
                        iqn,
                    )
                else:
                    logger.warning(
                        "Over-constrained problem for ion densities, quasineutrality not guaranteed",
                    )
            # Enforcing quasineutrality
            if len(ni_missing) == 1:
                atag = "{:d}".format(ni_missing[0])
                zi_missing = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_neut = outp["ne"] * 1.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ni_missing and "ni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_neut = sol_neut - zii * outp["ni" + itag]
                outp["ni" + atag] = sol_neut / zi_missing
                if np.any(~np.isfinite(outp["ni" + atag])):
                    raise ValueError(
                        "Non-finite ion densities generated in enforcing quasineutrality, aborting!"
                    )
                if np.any(outp["ni" + atag] < -1.0e-10):
                    raise ValueError(
                        "Negative ion densities generated in enforcing quasineutrality, aborting!"
                    )
                ni_missing.pop(0)
            if len(ni_missing) > 0:
                raise ValueError(
                    "Under-constrained problem for ion densities, aborting!"
                )

            # Calculate final effective charge after density constraints are applied, required for gradient constraints
            zeff = inp["Zeff"] if "Zeff" in inp else None
            if zeff is None:
                zeff = outp["r"] * 0.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                    zeff = zeff + outp["ni" + itag] * zii * zii / outp["ne"]
                zeff.loc[zeff < 1.0] = 1.0

            ani_missing = []
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if "Ani" + itag in inp and "ni" + itag in outp:
                    outp["grad_ni" + itag] = (
                        -inp["Ani" + itag] * outp["ni" + itag] / lref
                    )
                if "grad_ni" + itag not in outp:
                    ani_missing.append(ii)
            if len(ani_missing) == numions:
                ii = 0
                while ii < numions:
                    ii += 1
                    itag = "{:d}".format(ani_missing[-ii])
                    if "grad_ne" in outp and "ni" + itag in outp:
                        outp["grad_ni" + itag] = (
                            outp["grad_ne"] * outp["ni" + itag] / outp["ne"]
                        )
                        ani_missing.pop(-ii)
            # Setup required data for enforcing effective charge gradient, if specified
            if igzf is not None and igzf < numions:
                if igqn is not None and igqn < numions:
                    if len(ani_missing) == 0:
                        ani_missing.append(igqn)
                        logger.warning(
                            "Gradient quasineutrailty enforced on ion %d, density gradients may no longer represent input data",
                            iqn,
                        )
                if igzf is not None and igzf < numions:
                    if len(ani_missing) == 1:
                        ani_missing.append(igzf)
                        logger.warning(
                            "Effective charge gradient enforced on ion %d}, density gradients may no longer represent input data",
                            izf,
                        )
                    else:
                        logger.warning(
                            "Issue detected in ion density gradient consistency, chosen ion not modified to match grad_Zeff",
                        )
                if len(ani_missing) < 2:
                    logger.warning(
                        "Over-constrained problem for ion density gradients, grad_Zeff match not guaranteed",
                    )
            # Enforcing effective charge gradient
            if (
                len(ani_missing) == 2
                and "grad_Zeff" in inp
                and "grad_ne" in outp
                and "ne" in outp
            ):
                atag = "{:d}".format(ani_missing[0])
                btag = "{:d}".format(ani_missing[1])
                zi_missing = (
                    outp["Zi" + btag] if "Zi" + btag in outp else zi[ni_missing[1]]
                )
                zi_other = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_zeff = (
                    outp["grad_ne"] * (zi_other - zeff) - outp["ne"] * inp["grad_Zeff"]
                )
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ani_missing and "grad_ni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_zeff = (
                            sol_zeff - zii * (zi_other - zii) * outp["grad_ni" + itag]
                        )
                outp["grad_ni" + btag] = sol_zeff / (
                    zi_missing * (zi_other - zi_missing)
                )
                if np.any(~np.isfinite(outp["grad_ni" + btag])):
                    raise ValueError(
                        "Non-finite ion density gradients generated in enforcing grad_Zeff, aborting!"
                    )
                ani_missing.pop(1)
            # Setup required data for enforcing gradient quasineutrality, if specified (already set up if effective charge gradient also enforced)
            if len(ani_missing) == 0:
                if igqn is not None and igqn < numions:
                    ani_missing.append(igqn)
                    logger.warning(
                        "Gradient quasineutrailty enforced on ion %d, density gradients may no longer represent input data",
                        iqn,
                    )
                else:
                    logger.warning(
                        "Over-constrained problem for ion density gradients, gradient quasineutrality not guaranteed"
                    )
            # Enforcing gradient quasineutrality
            if len(ani_missing) == 1:
                atag = "{:d}".format(ani_missing[0])
                zi_missing = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ani_missing[0]]
                )
                sol_neut = outp["grad_ne"] * 1.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ani_missing and "grad_ni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_neut = sol_neut - zii * outp["grad_ni" + itag]
                outp["grad_ni" + atag] = sol_neut / zi_missing
                if np.any(~np.isfinite(outp["grad_ni" + atag])):
                    raise ValueError(
                        "Non-finite ion density gradients generated in enforcing gradient quasineutrality, aborting!"
                    )
                ani_missing.pop(0)
            if len(ani_missing) > 0:
                raise ValueError(
                    "Under-constrained problem for ion density gradients, aborting!"
                )

        # Continuing converting dimensionless parameters to physical parameters
        if "Ti0" in outp and "grad_Ti0" in outp:
            print("Calculation of ion temperatures and gradients complete!")
        if "ni0" in outp and "grad_ni0" in outp:
            print("Calculation of ion densities and gradients complete!")
        if "alpha" in inp and "grad_Te" in outp and "grad_ne" in outp and "q" in outp:
            # This algorithm starts having noticeable deviation when alpha < 0.01
            ee = 1.60217662e-19
            mu0 = 1.25663706e-6
            betac = -2.0 * mu0 * np.power(outp["q"], 2.0) * lref
            grad_p = ee * (outp["grad_ne"] * outp["Te"] + outp["ne"] * outp["grad_Te"])
            grad_pi = outp["r"] * 0.0
            sum_nz = outp["r"] * 0.0
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if "grad_Ti" + itag in outp and "grad_ni" + itag in outp:
                    grad_p = grad_p + ee * (
                        outp["grad_ni" + itag] * outp["Ti" + itag]
                        + outp["ni" + itag] * outp["grad_Ti" + itag]
                    )
                    grad_pi = grad_pi + ee * (
                        outp["grad_ni" + itag] * outp["Ti" + itag]
                        + outp["ni" + itag] * outp["grad_Ti" + itag]
                    )
                if "ni" + itag in outp:
                    zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                    sum_nz = sum_nz + ee * outp["ni" + itag] * zii
            outp["Bo"] = np.sqrt(betac * grad_p / inp["alpha"])
            outp["dpi"] = grad_pi / sum_nz
            if "dpi" in inp and verbose >= 2:
                print(inp["dpi"] - outp["dpi"])
        if "Bo" in outp:
            print("Calculation of magnetic field on axis complete!")
        if "dpi" in outp:
            print("Calculation of total ion pressure gradient complete!")
        uper = None
        grad_uper = None
        if "Machtor" in inp:
            outp["utor"] = inp["Machtor"] * cref
            uper = inp["Machtor"] * cref * bpolbyb
            grad_uper = inp["Machtor"] * cref * grad_bpolbyb
            # Scans in Te effectively change Machtor due to normalization with cref instead of csound
            outp["Machtor_eff"] = inp["Machtor"] / np.sqrt(te * 1.0e-3)
        elif "r" in outp:
            outp["utor"] = outp["r"] * 0.0
            uper = outp["r"] * 0.0
            grad_uper = outp["r"] * 0.0
        if "utor" in outp:
            print("Calculation of toroidal flow velocity complete!")
        if "Machpar" in inp and not pure_tor:
            unpol = inp["Machpar"] * cref
            if "utor" in outp:
                unpol = unpol - outp["utor"] * btorbyb
            outp["upol"] = unpol / bpolbyb
            uper = (
                uper - outp["upol"] * btorbyb
                if uper is not None
                else -outp["upol"] * btorbyb
            )
            grad_uper = (
                grad_uper - outp["upol"] * grad_btorbyb
                if grad_uper is not None
                else -outp["upol"] * grad_btorbyb
            )
            # Scans in Te effectively change Machpar due to normalization with cref instead of csound
            outp["Machpar_eff"] = inp["Machpar"] / np.sqrt(te * 1.0e-3)
        if "upol" in outp:
            # Note that upol is NOT printed into output by default, but only if specific conditions are met
            print("Calculation of poloidal flow velocity complete!")
        if autor_in is not None:
            outp["grad_utor"] = -autor_in * cref / lref
            # Scans in Te effectively change Autor due to normalization with cref instead of csound
            outp["Autor_eff"] = outp["r"] * 0.0 + autor_in / np.sqrt(te * 1.0e-3)
        elif "Autor" in inp:
            outp["grad_utor"] = -inp["Autor"] * cref / lref
            # Scans in Te effectively change Autor due to normalization with cref instead of csound
            outp["Autor_eff"] = inp["Autor"] / np.sqrt(te * 1.0e-3)
        if "grad_utor" in outp:
            grad_uper = (
                grad_uper + outp["grad_utor"] * bpolbyb
                if grad_uper is not None
                else outp["grad_utor"] * bpolbyb
            )
        if "Aupar" in inp and not pure_tor:
            grad_unpol = -inp["Aupar"] * cref / lref
            if "utor" in outp:
                grad_unpol = grad_unpol - outp["utor"] * grad_btorbyb
            if "grad_utor" in outp:
                grad_unpol = grad_unpol - outp["grad_utor"] * btorbyb
            if "upol" in outp:
                grad_unpol = grad_unpol - outp["upol"] * grad_bpolbyb
            outp["grad_upol"] = grad_unpol / bpolbyb
            grad_uper = (
                grad_uper - outp["grad_upol"] * btorbyb
                if grad_uper is not None
                else outp["grad_upol"] * btorbyb
            )
            # Scans in Te effectively change Aupar due to normalization with cref instead of csound
            outp["Aupar_eff"] = inp["Aupar"] / np.sqrt(te * 1.0e-3)
        gammaE = None
        if (
            "gammaE" in inp
            and "Bo" in outp
            and "grad_q" in outp
            and "q" in outp
            and "r" in outp
        ):
            grad_fgeo_byfgeo = (outp["grad_q"] / outp["q"]) - (
                1.0 / outp["r"]
            )  # Division by r could cause problems
            gammaE = inp["gammaE"] * 1.0
            grad_dpi = inp["gammaE"] * cref / lref
            if pure_tor:
                outp["grad_dpi"] = outp["r"] * 0.0
                if "grad_utor" in outp:
                    gammaE = outp["grad_utor"] * bpolbyb * lref / cref
                    if "utor" in outp:
                        gammaE = (
                            gammaE
                            + outp["utor"]
                            * (grad_bpolbyb + btorbyb * grad_fgeo_byfgeo)
                            * lref
                            / cref
                        )
                    logger.warning(
                        "Over-constrained problem for rotation variables with pure toroidal assumption, ignoring input gammaE",
                        UserWarning,
                    )
                else:
                    # Calculate contribution from flow gradient term assuming it is the only contribution
                    if uper is not None:
                        grad_dpi = grad_dpi - uper * grad_fgeo_byfgeo
                    if "utor" in outp:
                        grad_dpi = grad_dpi - outp["utor"] * grad_bpolbyb
                    outp["grad_utor"] = grad_dpi / bpolbyb
            else:
                # Calculate contribution from ion pressure term if flow gradient is given
                if grad_uper is not None:
                    grad_dpi = grad_dpi - grad_uper
                if uper is not None:
                    grad_dpi = grad_dpi - uper * grad_fgeo_byfgeo
                if "dpi" in outp:
                    # Assumes grad_bo = 0.0, reasonable but not strictly true due to flux-surface dependence
                    grad_dpi = grad_dpi - outp["dpi"] * grad_fgeo_byfgeo / outp["Bo"]
                outp["grad_dpi"] = grad_dpi * outp["Bo"]
                if "grad_utor" not in outp:
                    outp["grad_utor"] = outp["r"] * 0.0
        if gammaE is not None:
            # Scans in Te effectively change gammaE due to normalization with cref instead of csound
            outp["gammaE_eff"] = gammaE / np.sqrt(te * 1.0e-3)
        if "grad_utor" in outp:
            print("Calculation of toroidal flow velocity gradient complete!")
        if "grad_upol" in outp:
            # Note that grad_upol is NOT printed into output by default, but only if specific conditions are met
            print("Calculation of parallel flow velocity gradient complete!")
        if "grad_dpi" in outp:
            print("Calculation of derivative of total ion pressure gradient complete!")

    return outp


def physical_to_dimensionless(
    inp,
    zi_in=None,
    te_in=None,
    lref_in=None,
    ro_in=None,
    rlcfs_in=None,
    autor_in=None,
    pure_tor=False,
    qn_ion=None,
    grad_qn_ion=None,
    zeff_ion=None,
    grad_zeff_ion=None,
    verbose=0,
):
    """Converts physical parameters to equivalent dimensionless parameters

    Args:
        inp: Input data, only tested as pandas DataFrame
        zi_in: Charge numbers to be used, set to None to use values in inp, list
        te_in: Reference electron temperature to use, set to None to use values in inp, float [eV]
        lref_in: Reference length to use, set to None to use values in inp, float [m]
        ro_in: Point midplane averaged major radius to use, set to None to use values in inp, float [m]
        rlcfs_in: Reference midplane averaged minor radius to use, set to None to use values in inp, float [m]
        autor_in: Dimensionless flow shear gradient to use, set to None to use values in inp, float
        pure_tor: Apply purely toroidal flow assumption when calculating output values, bool
        qn_ion: Toggle enforcing of quasineutrality, integer value chooses which ion is modified, int
        grad_qn_ion: Toggle enforcing of gradient quasineutrality, integer value chooses which ion is modified, int
        zeff_ion: Toggle enforcing of Zeff if provided in inp, integer value chooses which ion is modified, int
        grad_zeff_ion: Toggle enforcing of grad_Zeff if provided in inp, integer value chooses which ion is modified, int
        verbose: Set verbosity (not much verbosity is implemented)

    Returns:
        outp: Converted data, pandas DataFrame
    """

    outp = pd.DataFrame()
    if not inp.empty:

        # Determine number of ion species in input data and their charge numbers
        #    Charge number of zero reserved for identifying species for which data should be taken from input
        #    Useful when there is scan-dependent charge numbers
        zi = []
        numions = len(zi)
        itag = "{:d}".format(numions)
        if "Zi" in inp:
            zi.append(0.0)
            if len(inp["Zi"]) > 1 and all(np.isclose(inp["Zi"][1:], inp["Zi"][:-1])):
                zi[-1] = float(inp["Zi"][0])
            elif len(inp["Zi"]) == 1:
                zi[-1] = float(inp["Zi"][0])
            numions += 1
            itag = "{:d}".format(numions)
        while "Zi" + itag in inp:
            zi.append(0.0)
            if len(inp["Zi" + itag]) > 1 and all(
                np.isclose(inp["Zi" + itag][1:], inp["Zi" + itag][:-1])
            ):
                zi[-1] = float(inp["Zi" + itag][0])
            elif len(inp["Zi" + itag]) == 1:
                zi[-1] = float(inp["Zi" + itag][0])
            numions += 1
            itag = "{:d}".format(numions)
        if isinstance(zi_in, list):
            for ii in range(0, len(zi_in)):
                itag = "{:d}".format(ii)
                if ii < len(zi) and zi_in[ii] > 0.0:
                    zi[ii] = zi_in[ii]
                    if "Zi" + itag in outp:
                        outp["Zi" + itag] = outp["Zi" + itag] * 0.0 + zi[ii]
        if len(zi) < numions or numions == 0:
            raise ValueError("Insufficient number of ion charges provided, aborting!")
        for ii in range(0, len(zi)):
            if zi[ii] < 0.0:
                raise ValueError("Negative ion charge number provided, aborting!")
        numions = len(zi)

        # Set flags to enforce quasineutrality, integer value sets which ion species is adjusted to enforce QN
        iqn = qn_ion if qn_ion is not None and qn_ion < len(zi) else None
        if iqn is not None and iqn < 0:
            iqn = numions - 1
        igqn = (
            grad_qn_ion if grad_qn_ion is not None and grad_qn_ion < len(zi) else None
        )
        if igqn is not None and igqn < 0:
            igqn = numions - 1
        izf = zeff_ion if zeff_ion is not None and zeff_ion < len(zi) else None
        if izf is not None and izf < 0:
            izf = numions - 1
        igzf = (
            grad_zeff_ion
            if grad_zeff_ion is not None and grad_zeff_ion < len(zi)
            else None
        )
        if igzf is not None and igzf < 0:
            igzf = numions - 1
        if izf is not None and iqn is None:
            raise ValueError(
                "Effective charge cannot be enforced without also enforcing quasineutrality, aborting!"
            )
        if igzf is not None and igqn is None:
            raise ValueError(
                "Effective charge gradient cannot be enforced without also enforcing gradient quasineutrality, aborting!"
            )

        # Transfer dimensionless parameter values already in input data to output
        target_vars = [
            "x",
            "smag",
            "logNustar",
            "Nustar",
            "Ane",
            "Ate",
            "alpha",
            "Machtor",
            "Autor",
            "dpi",
            "gammaE",
        ]
        target_ivars = ["Ti_Te", "Ati", "normni", "Ani", "Zi", "Ai"]
        for var in target_vars:
            if var in inp:
                outp[var] = inp[var] * 1.0
        for ii in range(0, numions):
            itag = "{:d}".format(ii)
            for var in target_ivars:
                if var + itag in inp:
                    outp[var + itag] = inp[var + itag] * 1.0
                elif var in inp and ii == 0:
                    outp[var + itag] = inp[var] * 1.0

        # Set general constants, all of them should be explicitly given in input data
        eom = 1.75882e11
        cref = 3.0949691e5
        lref = None
        if "Ro" in inp:
            lref = inp["Ro"]
        if lref_in is not None:
            lref = lref_in
        if lref is None:
            raise ValueError(
                "Reference gradient length for normalization not provided, aborting!"
            )
        else:
            outp["Ro"] = lref
        ro = None
        if "Ro" in inp:
            ro = inp["Ro"]
        if ro_in is not None:
            ro = ro_in
        if ro is None:
            ro = lref
        if ro is not None:
            outp["Ro"] = ro
        rlcfs = None
        if "Rmin" in inp:
            rlcfs = inp["Rmin"]
        if rlcfs_in is not None:
            rlcfs = rlcfs_in
        if rlcfs is None:
            raise ValueError(
                "Reference minor radius for normalization not provided, aborting!"
            )
        else:
            outp["Rmin"] = rlcfs
        te = None
        if "Te" in inp:
            te = inp["Te"]
        if te_in is not None:
            te = te_in
        if te is None:
            raise ValueError("Base electron temperature not provided, aborting!")
        if np.all(te < 1.0e2):
            logger.warning(
                "Physical electron temperatures should be in eV, they seem a bit small..."
            )
        ne = None
        if "ne" in inp:
            ne = inp["ne"]
        if ne is None:
            raise ValueError("Base electron density not provided, aborting!")
        if np.all(ne < 1.0e6):
            logger.warning(
                "Physical electron densities should be in m**-3, they seem a bit small..."
            )

        # Start converting dimensionless parameters to physical parameters
        if "r" in inp:
            outp["x"] = inp["r"] / rlcfs
        if "x" in outp:
            print("Calculation of normalized radial coordinate complete!")
        else:
            raise ValueError("Midplane-averaged minor radius not provided, aborting!")
        if "q" in inp:
            outp["q"] = inp["q"]
        if "q" in outp:
            print("Calculation of safety factor complete!")
        if "grad_q" in inp and "q" in outp and "x" in outp:
            outp["smag"] = inp["grad_q"] * outp["x"] * rlcfs / outp["q"]
        if "smag" in outp:
            print("Calculation of magnetic shear complete!")
        bpolbyb = None
        btorbyb = None
        grad_bpolbyb = None
        grad_btorbyb = None
        if "x" in outp and "q" in outp:
            eps = outp["x"] * rlcfs / ro
            denom = 1.0 / (np.power(outp["q"], 2.0) + np.power(eps, 2.0))
            bpolbyb = np.sqrt(np.power(eps, 2.0) * denom)
            btorbyb = np.sqrt(np.power(outp["q"], 2.0) * denom)
            if "smag" in outp:
                # These assume grad_ro = 0.0, which is true since ro is global for given equilibrium
                grad_q = outp["smag"] * outp["q"] / (outp["x"] * rlcfs)
                grad_bpolbyb = np.sqrt(denom) / ro - bpolbyb * denom * (
                    outp["q"] * grad_q + eps / ro
                )
                grad_btorbyb = np.sqrt(denom) * grad_q - btorbyb * denom * (
                    outp["q"] * grad_q + eps / ro
                )
            else:
                grad_bpolbyb = outp["x"] * 0.0
                grad_btorbyb = outp["x"] * 0.0
        if "x" in outp:
            outp["ne"] = outp["x"] * 0.0 + ne * 1.0e-19
        # if np.any(~np.isfinite(outp['ne'])):
        #    raise ValueError("Non-finite electron densities generated by direct conversion, aborting!")
        # if np.any(outp['ne'] < -1.0e-10):
        #    raise ValueError("Negative electron densities generated by direct conversion, aborting!")
        if "ne" in outp:
            print("Calculation of reference electron density complete!")
        if "grad_ne" in inp:
            outp["Ane"] = -inp["grad_ne"] * lref / ne
        # if np.any(~np.isfinite(outp['Ane'])):
        #    raise ValueError("Non-finite normalized logarithmic electron density gradients generated by direct conversion, aborting!")
        if "Ane" in outp:
            print(
                "Calculation of normalized logarithmic electron density gradient complete!"
            )
        if "x" in outp:
            outp["Te"] = outp["x"] * 0.0 + te * 1.0e-3
        # if np.any(~np.isfinite(outp['Te'])):
        #    raise ValueError("Non-finite electron temperatures generated by direct conversion, aborting!")
        # if np.any(outp['Te'] < -1.0e-10):
        #    raise ValueError("Negative electron temperatures generated by direct conversion, aborting!")
        if "Te" in outp:
            print("Calculation of reference electron temperature complete!")
        if "grad_Te" in inp:
            outp["Ate"] = -inp["grad_Te"] * lref / te
        # if np.any(~np.isfinite(outp['Ate'])):
        #    raise ValueError("Non-finite normalized logarithmic electron temperature gradients generated by direct conversion, aborting!")
        if "Ate" in outp:
            print(
                "Calculation of normalized logarithmic electron temperature gradient complete!"
            )

        # This branch is taken if ion data is not enumerated in input data, can specify 2 ion species if Z is given
        if "grad_Ti0" not in inp and "Ti0" not in inp:
            if len(zi) > 0 and zi[0] > 0.0:
                outp["Zi0"] = outp["x"] * 0.0 + zi[0]
            elif "Zi" in inp:
                outp["Zi0"] = outp["x"] * 0.0 + inp["Zi"]
            if "Ai" in inp:
                outp["Ai0"] = outp["x"] * 0.0 + inp["Ai"]
            if "Ti" in inp:
                outp["Ti_Te0"] = inp["Ti"] / te
            if "Ti_Te0" not in outp:
                outp["Ti_Te0"] = te * 0.0 + 1.0
            # if np.any(~np.isfinite(outp['Ti_Te0'])):
            #    raise ValueError("Non-finite ion-to-electron temperature ratios generated by direct conversion, aborting!")
            # if np.any(outp['Ti_Te0'] < -1.0e-10):
            #    raise ValueError("Negative ion-to-electron temperature ratios generated by direct conversion, aborting!")
            if "grad_Ti" in inp and "Ti_Te0" in outp:
                outp["Ati0"] = -inp["grad_Ti"] * lref / (outp["Ti_Te0"] * te)
            if "Ati0" not in outp and "Ate" in outp and "Ti_Te0" in outp:
                outp["Ati0"] = outp["Ate"] / outp["Ti_Te0"]
            # if np.any(~np.isfinite(outp['Ati0'])):
            #    raise ValueError("Non-finite normalized logarithmic ion temperature gradients generated by direct conversion, aborting!")
            if "ni" in inp:
                outp["normni0"] = inp["ni"] / ne
                if "Zeff" in inp:
                    logger.warning(
                        "Over-constrained problem for ion density fractions, Zeff match not guaranteed",
                        UserWarning,
                    )
            if "normni0" not in inp and "Zeff" in inp and len(zi) > 1:
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["normni0"] = (zi[1] - inp["Zeff"]) / (zi[0] * (zi[1] - zi[0]))
            if "normni0" not in outp:
                outp["normni0"] = outp["x"] * 0.0 + (1.0 / zi[0])
            # if np.any(~np.isfinite(outp['normni0'])):
            #    raise ValueError("Non-finite ion density fractions generated by direct conversion, aborting!")
            # if np.any(outp['normni0'] < -1.0e-10):
            #    raise ValueError("Negative ion density fractions generated by direct conversion, aborting!")
            if "grad_ni" in inp and "normni0" in outp:
                outp["Ani0"] = -inp["grad_ni"] * lref / (outp["normni0"] * ne)
                if "grad_Zeff" in inp:
                    logger.warning(
                        "Over-constrained problem for ion density gradients, grad_Zeff match not guaranteed",
                        UserWarning,
                    )
            elif (
                "grad_Zeff" in inp
                and "Zeff" in inp
                and "Ane" in outp
                and "normni0" in outp
                and len(zi) > 1
            ):
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["Ani0"] = (
                    outp["Ane"] * (zi1 - inp["Zeff"]) + lref * inp["grad_Zeff"]
                ) / (outp["normni0"] * (zi1 - zi0) * zi0)
            if "Ani0" not in outp and "Ane" in outp:
                outp["Ani0"] = outp["Ane"]
            # if np.any(~np.isfinite(outp['Ani0'])):
            #    raise ValueError("Non-finite normalized logarithmic ion density gradients generated by direct conversion, aborting!")
            numions = 1

            # Apply second ion via two-ion quasineutrality if a second charge number is given
            if "Ati0" in outp and "Ani0" in outp and len(zi) > 1:
                if "Zi1" not in outp:
                    outp["Zi1"] = outp["x"] * 0.0 + zi1
                zi0 = outp["Zi0"] if "Zi0" in outp else zi[0]
                zi1 = outp["Zi1"] if "Zi1" in outp else zi[1]
                outp["Ti_Te1"] = outp["Ti_Te0"]
                outp["Ati1"] = outp["Ati0"]
                outp["normni1"] = (1.0 - zi0 * outp["normni0"]) / zi1
                outp["Ani1"] = (outp["Ane"] - zi0 * outp["normni0"] * outp["Ani0"]) / (
                    zi1 * outp["normni1"]
                )
                # if np.any(~np.isfinite(outp['normni1'])):
                #    raise ValueError("Non-finite ion density fractions generated by two-ion quasineutrality, aborting!")
                # if np.any(outp['normni1'] < -1.0e-10):
                #    raise ValueError("Negative ion density fractions generated by two-ion quasineutrality, aborting!")
                # if np.any(~np.isfinite(outp['Ani1'])):
                #    raise ValueError("Non-finite normalized logarithmic ion density gradients generated by two-ion quasineutrality, aborting!")
                numions = 2

        # This branch is taken if ion data is enumerated in input data, can specify N ions if Z is given
        else:
            ni_missing = []
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if ii < len(zi) and zi[ii] > 0.0:
                    outp["Zi" + itag] = outp["x"] * 0.0 + zi[ii]
                elif "Zi" + itag in inp:
                    outp["Zi" + itag] = outp["x"] * 0.0 + inp["Zi" + itag]
                if "Ai" + itag in inp:
                    outp["Ai" + itag] = outp["x"] * 0.0 + inp["Ai" + itag]
                if "Ti" + itag in inp:
                    outp["Ti_Te" + itag] = inp["Ti" + itag] / te
                elif "Ti0" in inp:
                    outp["Ti_Te" + itag] = inp["Ti0"] / te
                elif "Ti" in inp:
                    outp["Ti_Te" + itag] = inp["Ti"] / te
                if "Ti_Te" + itag not in outp:
                    outp["Ti_Te" + itag] = outp["x"] * 0.0 + 1.0
                # if np.any(~np.isfinite(outp['Ti_Te'+itag])):
                #    raise ValueError("Non-finite ion-to-electron temperature ratios generated by direct conversion, aborting!")
                # if np.any(outp['Ti_Te'+itag] < -1.0e-10):
                #    raise ValueError("Negative ion-to-electron temperature ratios generated by direct conversion, aborting!")
                if "grad_Ti" + itag in inp and "Ti_Te" + itag in outp:
                    outp["Ati" + itag] = (
                        -inp["grad_Ti" + itag] * lref / (outp["Ti_Te" + itag] * te)
                    )
                elif "grad_Ti0" in inp and "Ti_Te" + itag in outp:
                    outp["Ati" + itag] = (
                        -inp["grad_Ti0"] * lref / (outp["Ti_Te" + itag] * te)
                    )
                elif "grad_Ti" in inp and "Ti_Te" + itag in outp:
                    outp["Ati" + itag] = (
                        -inp["grad_Ti"] * lref / (outp["Ti_Te" + itag] * te)
                    )
                if (
                    "Ati" + itag not in outp
                    and "Ate" in outp
                    and "Ti_Te" + itag in outp
                ):
                    outp["Ati" + itag] = outp["Ate"] / outp["Ti_Te" + itag]
                # if np.any(~np.isfinite(outp['Ati'+itag])):
                #    raise ValueError("Non-finite normalized logarithmic electron temperature gradients generated by direct conversion, aborting!")
                if "ni" + itag in inp:
                    outp["normni" + itag] = inp["ni" + itag] / ne
                if "normni" + itag not in outp:
                    ni_missing.append(ii)
                # else:
                #    if np.any(~np.isfinite(outp['normni'+itag])):
                #        raise ValueError("Non-finite ion density fractions generated by direct conversion, aborting!")
                #    if np.any(outp['ni'+itag] < -1.0e-10):
                #        raise ValueError("Negative ion density fractions generated by direct conversion, aborting!")

            # Setup required data for enforcing effective charge, if specified
            if "Zeff" in inp:
                if iqn is not None and iqn < numions:
                    if len(ni_missing) == 0:
                        ni_missing.append(iqn)
                        logger.warning(
                            "Quasineutrailty enforced on ion {:d}, density fractions may no longer represent input data".format(
                                iqn
                            ),
                            UserWarning,
                        )
                if izf is not None and izf < numions:
                    if len(ni_missing) == 1:
                        ni_missing.append(izf)
                        logger.warning(
                            "Effective charge enforced on ion {:d}, density fractions may no longer represent input data".format(
                                izf
                            ),
                            UserWarning,
                        )
                    else:
                        logger.warning(
                            "Issue detected in ion density fraction consistency, chosen ion not modified to match Zeff",
                            UserWarning,
                        )
                if len(ni_missing) < 2:
                    logger.warning(
                        "Over-constrained problem for ion density fractions, Zeff match not guaranteed",
                        UserWarning,
                    )
            # Enforcing effective charge
            if len(ni_missing) == 2 and "Zeff" in inp:
                atag = "{:d}".format(ni_missing[0])
                btag = "{:d}".format(ni_missing[1])
                zi_missing = (
                    outp["Zi" + btag] if "Zi" + btag in outp else zi[ni_missing[1]]
                )
                zi_other = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_zeff = zi_other - inp["Zeff"]
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ni_missing and "normni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_zeff = (
                            sol_zeff - zii * (zi_other - zii) * outp["normni" + itag]
                        )
                outp["normni" + btag] = sol_zeff / (
                    zi_missing * (zi_other - zi_missing)
                )
                if np.any(~np.isfinite(outp["normni" + btag])):
                    raise ValueError(
                        "Infinite ion density fractions generated in enforcing Zeff, aborting!"
                    )
                if np.any(outp["normni" + btag] < -1.0e-10):
                    raise ValueError(
                        "Negative ion density fractions generated in enforcing Zeff, aborting!"
                    )
                ni_missing.pop(1)
            # Setup required data for enforcing quasineutrality, if specified (already set up if effective charge also enforced)
            if len(ni_missing) == 0:
                if iqn is not None and iqn < numions:
                    ni_missing.append(iqn)
                    logger.warning(
                        "Quasineutrailty enforced on ion {:d}, density fractions may no longer represent input data".format(
                            iqn
                        ),
                        UserWarning,
                    )
                else:
                    logger.warning(
                        "Over-constrained problem for ion density fractions, quasineutrality not guaranteed",
                        UserWarning,
                    )
            # Enforcing quasineutrality
            if len(ni_missing) == 1:
                atag = "{:d}".format(ni_missing[0])
                zi_missing = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_neut = outp["x"] * 0.0 + 1.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if ii not in ni_missing and "normni" + itag in outp:
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_neut = sol_neut - zii * outp["normni" + itag]
                outp["normni" + atag] = sol_neut / zi_missing
                if np.any(~np.isfinite(outp["normni" + atag])):
                    raise ValueError(
                        "Infinite ion density fractions generated in enforcing quasineutrality, aborting!"
                    )
                if np.any(outp["normni" + atag] < -1.0e-10):
                    raise ValueError(
                        "Negative ion density fractions generated in enforcing quasineutrality, aborting!"
                    )
                ni_missing.pop(0)
            if len(ni_missing) > 0:
                raise ValueError(
                    "Under-constrained problem for ion density fractions, aborting!"
                )

            # Calculate final effective charge after density constraints are applied, required for gradient constraints
            zeff = inp["Zeff"] if "Zeff" in inp else None
            if zeff is None:
                zeff = outp["x"] * 0.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                    zeff = zeff + outp["normni" + itag] * zii * zii
                zeff.loc[zeff < 1.0] = 1.0

            ani_missing = []
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if "grad_ni" + itag in inp and "normni" + itag in outp:
                    outp["Ani" + itag] = (
                        -inp["grad_ni" + itag] * lref / (outp["normni" + itag] * ne)
                    )
                if "Ani" + itag not in outp:
                    ani_missing.append(ii)
            if len(ani_missing) == numions:
                ii = 0
                while ii < numions:
                    ii += 1
                    itag = "{:d}".format(ani_missing[-ii])
                    if "Ane" in outp:
                        outp["Ani" + itag] = outp["Ane"]
                        ani_missing.pop(-ii)
            # Setup required data for enforcing effective charge gradient, if specified
            if igzf is not None and igzf < numions:
                if igqn is not None and igqn < numions:
                    if len(ani_missing) == 0:
                        ani_missing.append(igqn)
                        logger.warning(
                            "Gradient quasineutrailty enforced on ion {:d}, normalized logarithmic density gradients may no longer represent input data".format(
                                iqn
                            ),
                            UserWarning,
                        )
                if igzf is not None and igzf < numions:
                    if len(ani_missing) == 1:
                        ani_missing.append(igzf)
                        logger.warning(
                            "Effective charge gradient enforced on ion {:d}, normalized logarithmic density gradients may no longer represent input data".format(
                                izf
                            ),
                            UserWarning,
                        )
                    else:
                        logger.warning(
                            "Issue detected in normalized logarithmic ion density gradient consistency, chosen ion not modified to match grad_Zeff",
                            UserWarning,
                        )
                if len(ani_missing) < 2:
                    logger.warning(
                        "Over-constrained problem for normalized logarithmic ion density gradients, grad_Zeff match not guaranteed",
                        UserWarning,
                    )
            # Enforcing effective charge gradient
            if len(ani_missing) == 2 and "grad_Zeff" in inp and "Ane" in outp:
                atag = "{:d}".format(ani_missing[0])
                btag = "{:d}".format(ani_missing[1])
                if "normni" + btag not in outp:
                    raise ValueError(
                        "Unable to find ion density fraction for ion {:d} for enforcing grad_Zeff, aborting!".format(
                            ani_missing[1]
                        )
                    )
                zi_missing = (
                    outp["Zi" + btag] if "Zi" + btag in outp else zi[ni_missing[1]]
                )
                zi_other = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ni_missing[0]]
                )
                sol_zeff = outp["Ane"] * (zi_other - zeff) + lref * inp["grad_Zeff"]
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if (
                        ii not in ani_missing
                        and "Ani" + itag in outp
                        and "normni" + itag in outp
                    ):
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_zeff = (
                            sol_zeff
                            - zii
                            * (zi_other - zii)
                            * outp["Ani" + itag]
                            * outp["normni" + itag]
                        )
                outp["Ani" + btag] = sol_zeff / (
                    zi_missing * (zi_other - zi_missing) * outp["normni" + btag]
                )
                if np.any(~np.isfinite(outp["Ani" + btag])):
                    raise ValueError(
                        "Non-finite normalized logarithmic ion density gradients generated in enforcing grad_Zeff, aborting!"
                    )
                ani_missing.pop(1)
            # Setup required data for enforcing gradient quasineutrality, if specified (already set up if effective charge gradient also enforced)
            if len(ani_missing) == 0:
                if igqn is not None and igqn < numions:
                    ani_missing.append(igqn)
                    logger.warning(
                        "Gradient quasineutrailty enforced on ion {:d}, normalized logarithmic ion density gradients may no longer represent input data".format(
                            igqn
                        ),
                        UserWarning,
                    )
                else:
                    logger.warning(
                        "Over-constrained problem for normalized logarithmic ion density gradients, gradient quasineutrality not guaranteed",
                        UserWarning,
                    )
            # Enforcing gradient quasineutrality
            if len(ani_missing) == 1:
                atag = "{:d}".format(ani_missing[0])
                zi_missing = (
                    outp["Zi" + atag] if "Zi" + atag in outp else zi[ani_missing[0]]
                )
                sol_neut = outp["Ane"] * 1.0
                for ii in range(0, numions):
                    itag = "{:d}".format(ii)
                    if (
                        ii not in ani_missing
                        and "Ani" + itag in outp
                        and "normni" + itag in outp
                    ):
                        zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                        sol_neut = (
                            sol_neut - zii * outp["Ani" + itag] * outp["normni" + itag]
                        )
                outp["Ani" + atag] = sol_neut / (zi_missing * outp["normni" + atag])
                if np.any(~np.isfinite(outp["Ani" + atag])):
                    raise ValueError(
                        "Non-finite normalized logarithmic ion density gradients generated in enforcing gradient quasineutrality, aborting!"
                    )
                ani_missing.pop(0)
            if len(ani_missing) > 0:
                raise ValueError(
                    "Under-constrained problem for normalized logarithmic ion density gradients, aborting!"
                )

        # Continuing converting dimensionless parameters to physical parameters
        if "Ti_Te0" in outp and "Ati0" in outp:
            print("Calculation of normalized ion temperatures and gradients complete!")
        if "normni0" in outp and "Ani0" in outp:
            print("Calculation of normalized ion densities and gradients complete!")
        if "normni0" in outp and "Zi0" in outp:
            itag = "0"
            zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
            sum_normnz = outp["normni" + itag] * zii
            sum_normnzz = outp["normni" + itag] * zii * zii
            for ii in range(1, numions):
                itag = "{:d}".format(ii)
                if "normni" + itag in outp:
                    zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                    sum_normnz = sum_normnz + outp["normni" + itag] * zii
                    sum_normnzz = sum_normnzz + outp["normni" + itag] * zii * zii
            outp["Zeff"] = sum_normnzz
            if not np.all(np.isclose(sum_normnz, 1.0)):
                logger.warning(
                    "Quasineutrality not satisfied for every point", UserWarning
                )
                if verbose >= 2:
                    print(sum_normnz - 1.0)
        if "Zeff" in outp:
            print("Calculation of effective charge complete!")
        bo = None
        if "Bo" in inp and "Ate" in outp and "Ane" in outp and "q" in outp:
            bo = inp["Bo"] * 1.0
            ee = 1.60217662e-19
            mu0 = 1.25663706e-6
            betac = -2.0 * mu0 * np.power(outp["q"], 2.0) * ee * ne * te
            ap = outp["Ane"] + outp["Ate"]
            api = outp["x"] * 0.0
            sum_normnz = outp["x"] * 0.0
            for ii in range(0, numions):
                itag = "{:d}".format(ii)
                if "Ati" + itag in outp and "Ani" + itag in outp:
                    ap = (
                        ap
                        + (outp["Ani" + itag] + outp["Ati" + itag])
                        * outp["normni" + itag]
                        * outp["Ti_Te" + itag]
                    )
                    api = (
                        api
                        + (outp["Ani" + itag] + outp["Ati" + itag])
                        * outp["normni" + itag]
                        * outp["Ti_Te" + itag]
                    )
                if "normni" + itag in outp:
                    zii = outp["Zi" + itag] if "Zi" + itag in outp else zi[ii]
                    sum_normnz = sum_normnz + outp["normni" + itag] * zii
            outp["alpha"] = -betac * ap / np.power(inp["Bo"], 2.0)
            outp["dpi"] = -api * te / (sum_normnz * lref)
        if "dpi" not in outp and "dpi" in inp:
            outp["dpi"] = outp["x"] * 0.0 + inp["dpi"]
        if "alpha" in outp:
            print("Calculation of normalized pressure gradient complete!")
        if "dpi" in outp:
            print("Calculation of total ion pressure gradient complete!")
        if "Zeff" in outp and "q" in outp and "x" in outp:
            # The constants in this calculation are consistent with QuaLiKiz, may differ from other literature
            nu_te = te * 1.0e-3
            nu_ne = ne * 1.0e-20
            tbounce = (
                outp["q"]
                * ro
                * np.power(outp["x"] * rlcfs / ro, -1.5)
                / np.sqrt(eom * te)
            )
            collei = (
                (9174.3119266 * outp["Zeff"] / np.power(nu_te, 1.5))
                * (15.2 - np.log(nu_te) + 0.5 * np.log(nu_ne))
                * nu_ne
            )
            outp["logNustar"] = np.log10(collei * tbounce)
        if "logNustar" in outp:
            print("Calculation of normalized logarithmic collisionality complete!")
        uper = None
        grad_uper = None
        if "utor" in inp:
            outp["Machtor"] = inp["utor"] / cref
            uper = inp["utor"] * bpolbyb
            grad_uper = inp["utor"] * grad_bpolbyb
        if "Machtor" not in outp and "x" in outp:
            outp["Machtor"] = outp["x"] * 0.0
            uper = outp["x"] * 0.0
            grad_uper = outp["x"] * 0.0
        if "Machtor" in outp:
            # Scans in Te effectively change Machtor due to normalization with cref instead of csound
            outp["Machtor_eff"] = outp["Machtor"] / np.sqrt(te * 1.0e-3)
            print("Calculation of toroidal flow velocity Mach number complete!")
        if pure_tor:
            outp["Machpar"] = outp["x"] * 0.0
        elif "upol" in inp:
            outp["Machpar"] = (
                inp["upol"] * bpolbyb / cref
            )  # Only contains poloidal contribution at this point
            uper = (
                uper - inp["upol"] * btorbyb
                if uper is not None
                else -inp["upol"] * btorbyb
            )
            grad_uper = (
                grad_uper - inp["upol"] * grad_btorbyb
                if grad_uper is not None
                else -inp["upol"] * grad_btorbyb
            )
        if "Machpar" in outp and "Machtor" in outp:
            outp["Machpar"] = outp["Machpar"] + outp["Machtor"] * btorbyb
        if "Machpar" in outp:
            # Scans in Te effectively change Machpar due to normalization with cref instead of csound
            outp["Machpar_eff"] = outp["Machpar"] / np.sqrt(te * 1.0e-3)
            # Note that Machpar is NOT printed into output by default, but only if specific conditions are met
            print("Calculation of parallel flow velocity Mach number complete!")
        if "grad_utor" in inp:
            outp["Autor"] = -inp["grad_utor"] * lref / cref
        if autor_in is not None:
            outp["Autor"] = outp["x"] * 0.0 + autor_in
        if "Autor" in outp:
            grad_uper = (
                grad_uper - outp["Autor"] * cref * bpolbyb / lref
                if grad_uper is not None
                else -outp["Autor"] * cref * bpolbyb / lref
            )
        if "Autor" in outp:
            # Scans in Te effectively change Autor due to normalization with cref instead of csound
            outp["Autor_eff"] = outp["Autor"] / np.sqrt(te * 1.0e-3)
            print("Calculation of normalized toroidal flow velocity gradient complete!")
        if pure_tor and "Autor" in outp:
            outp["Aupar"] = outp["Autor"] * btorbyb
            if "Machtor" in outp:
                outp["Aupar"] = outp["Aupar"] - lref * outp["Machtor"] * grad_btorbyb
        elif "grad_upol" in inp:
            grad_upar = inp["grad_upol"] * bpolbyb
            grad_uper = (
                grad_uper - inp["grad_upol"] * btorbyb
                if grad_uper is not None
                else -inp["grad_upol"] * btorbyb
            )
            if "upol" in inp:
                grad_upar = grad_upar + inp["upol"] * grad_bpolbyb
            if "Autor" in outp:
                grad_upar = grad_upar - outp["Autor"] * cref * btorbyb / lref
            if "Machtor" in outp:
                grad_upar = grad_upar + outp["Machtor"] * cref * grad_btorbyb
            outp["Aupar"] = -grad_upar * lref / cref
        if "Aupar" in outp:
            # Scans in Te effectively change Autor due to normalization with cref instead of csound
            outp["Aupar_eff"] = outp["Aupar"] / np.sqrt(te * 1.0e-3)
            # Note that Aupar is NOT printed into output by default, but only if specific conditions are met
            print("Calculation of normalized parallel flow velocity gradient complete!")
        if "grad_dpi" in inp:
            outp["grad_dpi"] = inp["grad_dpi"] * 1.0
        elif "gammaE" in inp:
            print(inp["gammaE"])
            if pure_tor:
                outp["grad_dpi"] = outp["x"] * 0.0
                logger.warning(
                    "Over-constrained problem for rotation variables with pure toroidal assumption, overwriting input gammaE",
                    UserWarning,
                )
            elif "x" in outp and "q" in outp and "smag" in outp:
                grad_fgeo_byfgeo = (outp["smag"] - 1.0) / (
                    outp["x"] * rlcfs
                )  # Division by x could cause problems
                # Poloidal flow contribution is neglected if not explicitly provided in input
                #   pure_tor flag causes parallel velocity calculation to happen earlier but is identical to calculation here
                aterm = outp["x"] * 0.0
                mterm = outp["x"] * 0.0
                pterm = outp["x"] * 0.0
                if grad_uper is not None:
                    aterm = grad_uper * bo
                if uper is not None:
                    mterm = uper * grad_fgeo_byfgeo * bo
                if "dpi" in outp:
                    # Assumes grad_bo = 0.0, reasonable but not strictly true due to flux-surface dependence
                    pterm = outp["dpi"] * grad_fgeo_byfgeo
                # Calculate contribution from ion pressure term if flow gradient and ion pressure gradient is given
                outp["grad_dpi"] = (
                    inp["gammaE"] * bo * cref / lref - aterm - mterm - pterm
                )
        if grad_uper is not None:
            grad_fgeo_byfgeo = (outp["smag"] - 1.0) / (
                outp["x"] * rlcfs
            )  # Division by x could cause problems
            grad_uper_tot = grad_uper * 1.0
            if uper is not None:
                grad_uper_tot = grad_uper_tot + uper * grad_fgeo_byfgeo
            # Calculate contribution from ion pressure gradient terms in vperp equation if information is given
            if "dpi" in outp and not pure_tor:
                # Assumes grad_bo = 0.0, reasonable but not strictly true due to flux-surface dependence
                grad_uper_tot = grad_uper_tot + outp["dpi"] * grad_fgeo_byfgeo / bo
            if "grad_dpi" in outp and not pure_tor:
                grad_uper_tot = grad_uper_tot + outp["grad_dpi"] / bo
            if "grad_dpi" not in outp:
                outp["grad_dpi"] = outp["x"] * 0.0
            outp["gammaE"] = grad_uper_tot * lref / cref
        if "grad_dpi" in outp:
            print("Calculation of derivative of total ion pressure gradient complete!")
        if "gammaE" in outp:
            # Scans in Te effectively change gammaE due to normalization with cref instead of csound
            outp["gammaE_eff"] = outp["gammaE"] / np.sqrt(te * 1.0e-3)
            print("Calculation of normalized ExB shearing rate complete!")

    return outp


def calc_dilution(normnis, Zis, imain_ion=0):
    nions = len(normnis)
    assert len(Zis) == nions
    if calc_qn(normnis, Zis) != 0:
        raise Exception("Given normni and Zi is not QuasiNeutral, caveat emptor!")
    if imain_ion != 0:
        raise Exception("Main ion is not 0, caveat emptor!")

    # z for impurity
    # m for main
    # i for all ions
    # Split main ions and not main ions
    Zm: float = Zis[imain_ion]
    normnm: float = normnis[imain_ion]
    Zz: list = [Zi for ix, Zi in enumerate(Zis) if ix != imain_ion]
    normnz: list = [normn for ix, normn in enumerate(normnis) if ix != imain_ion]
    logger.debug("Got Zm={!s}, Zi={!s}, Zz={!s}, normnz={!s}", Zm, normnm, Zz, normnz)

    # This function is big as it's very verbosely written
    nz_charge: float = sum(Zi * normni for Zi, normni in zip(normnz, Zz))
    nm_charge: float = Zm * normnm
    logger.debug("Got nz_charge={!s}, nm_charge={!s}", nz_charge, nm_charge)
    return nm_charge


def calc_qn(normnis, Zis):
    nions = len(normnis)
    assert len(Zis) == nions
    Zi_tot = sum(normni * Zi for normni, Zi in zip(normnis, Zis))
    return Zi_tot - 1
