from pathlib import Path

from IPython import embed  # pylint: disable=unused-import # noqa: F401
import xarray as xr


def is_qlk_netcdf(path: Path):
    """Relatively lowlevel function to check if something is a qlk-style netCDF file"""
    if path.is_file():
        # Just try to open the file; should be quick even for large files as
        # loading is lazy
        try:
            ds = xr.open_dataset(path)
            ds.close()
        except:
            return False
        # If could open open without trouble, it is a netCDF
        return True
    else:
        return False
