""" Set up multiple runs in a single batch """
import sys
from collections import OrderedDict
from pathlib import Path
from os.path import relpath

import numpy as np
from IPython import embed  # pylint: disable=unused-import # noqa: F401

import qualikiz_tools
from qualikiz_tools.machine_specific.bash import Batch, Run
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

tools_root = Path(qualikiz_tools.__path__[0])
qlk_root = (tools_root / "../..").resolve()

# Grab rundir name from CLI or use a default
# We assume the QuaLiKiz_pythontoolsdir lives inside the QuaLiKiz root
runsdir = qlk_root / "runs"
if len(sys.argv) == 2:
    if sys.argv[1] != "":
        runsdir = Path(sys.argv[1]).resolve()

# Find some of the paths we need
# The folder we're in, we save our batch as a subfolder of this one
batch_parent = runsdir
# The binary is usually located one level above the qualikiz_tools root
qlk_binpath = qlk_root / "QuaLiKiz"

# Use the default JSON as template for this batch
templatepath = tools_root / "qualikiz_io/parameters_template.json"

# Load the default QuaLiKiz plan we will use as base
qualikiz_plan_base = QuaLiKizPlan.from_json(templatepath)

# We will use a physically relevant base scan
# We change our base plan programatically instead of with the JSON
qualikiz_plan_base["scan_dict"] = OrderedDict(
    [
        ("Ati", [2.75, 4.25, 5.75, 6.5, 8.0]),
        ("Ate", [2.75, 4.25, 5.0, 6.5, 8.0]),
        ("Ane", np.linspace(-1, 3, 8).tolist()),
        ("q", [1.0, 2.0, 2.5, 3.0, 4.0]),
    ]
)
qualikiz_plan_base["scan_type"] = "hyperrect"  # Scan all points in the hyperrectangle
qualikiz_plan_base["xpoint_base"]["kthetarhos"] = [
    0.1,
    0.175,
    0.25,
    0.325,
    0.4,
    0.5,
    0.6,
    0.8,
    1.0,
    2.0,
    3.5,
    6.0,
    10.5,
    15.0,
    19.5,
    24.0,
    30.0,
    36.0,
]  # Use a nicer kthetarhos grid

# Set up multiple runs. For example, scanning over maxpoints:
maxpoints = [5e5, 5e4, 5e3, 1e3]
# Let's use only 4 cores for now
cores = tasks = 4
# And build our list of runs
runlist = []
batch_name = "batch"
for maxpts in maxpoints:
    name = "maxpts" + str(maxpts)

    # Because we will overwrite this dict every loop, no need to deepcopy
    qualikiz_plan_base["xpoint_base"]["maxpts"] = maxpts

    # Now the binary dir relative to the rundir (not the batchdir)
    run_parent = batch_parent / batch_name
    run_dir = run_parent / name
    binreldir = Path(relpath(qlk_binpath, start=run_dir))
    run = Run(
        run_parent, name, binreldir, qualikiz_plan=qualikiz_plan_base, tasks=tasks
    )
    runlist.append(run)

# Implementation detail, we pass the directory the batch _lives in_
# and the name of the batch
batch = Batch(batch_parent, batch_name, runlist)

# Prepare our batches, e.g. build the folder structure
batch.prepare()
batch.generate_input(dotprint=True)
print("\n")
resp = input("Submit all jobs in runsdir to queue? [Y/n]")
if resp in ["", "Y", "y"]:
    batch.launch()
