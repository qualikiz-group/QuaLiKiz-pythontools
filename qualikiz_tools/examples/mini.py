""" Set up a small QuaLiKiz run """
import os
import sys
from pathlib import Path
from os.path import relpath

from IPython import embed  # pylint: disable=unused-import # noqa: F401

import qualikiz_tools
from qualikiz_tools.machine_specific.bash import Batch, Run

tools_root = Path(qualikiz_tools.__path__[0])
qlk_root = (tools_root / "../..").resolve()

# Grab rundir name from CLI or use a default
runsdir = qlk_root / "runs"
if len(sys.argv) == 2:
    if sys.argv[1] != "":
        runsdir = Path(sys.argv[1]).resolve()

# We'll make a folder 'mini' inside the 'runs' dir with our example
# First, we need to know where the binary lives relative to the folder
name = "mini"
qlk_binpath = qlk_root / "QuaLiKiz"
run_dir = runsdir / name
binreldir = Path(relpath(qlk_binpath, start=run_dir))

# Create the run. By not passing it a QuaLiKizPlan, it will use the
# parameters_template.json file
run = Run(runsdir, name, binreldir)
runlist = [run]

# Let's also create a batch script:
batch = Batch(runsdir, name, runlist)

batch.prepare()
os.chdir(run_dir)
batch.generate_input()

resp = input("Run job? [Y/n]")
if resp in ["", "Y", "y"]:
    batch.launch()
