netcdf4_engine = None

try:
    import netCDF4 as nc4  # pylint: disable=preferred-module # noqa: F401

    HAS_NETCDF4 = True
except ModuleNotFoundError:
    HAS_NETCDF4 = False


__all__ = ["commands", "fs_manipulation", "machine_specific", "misc", "qualikiz_io"]
# Set up logging
import qualikiz_tools.setup_logging

from . import _version
__version__ = _version.get_versions()['version']
