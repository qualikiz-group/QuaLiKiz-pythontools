Bare bone QuaLiKizBatch based on
[QuaLiKiz]https://gitlab.com/qualikiz-group/QuaLiKiz/-/commit/15ebf21fb8cc2c06788d840fd218a9595e87d7d9)
[quicktest_parameters](https://gitlab.com/qualikiz-group/QuaLiKiz/-/blob/main/reference_parameters/quicktest_parameters.json)


Got results by running
``` bash
qualikiz_tools input generate .
./qualikiz.batch
```
