from itertools import product
from pathlib import Path
import logging
from os.path import relpath
import copy

from IPython import embed  # pylint: disable=unused-import # noqa: F401

import qualikiz_tools
from qualikiz_tools.machine_specific.bash import Run, Batch

# Import other needed QuaLiKiz tools
from qualikiz_tools import __file__ as root_init
from qualikiz_tools.misc.helper_classes import OODS
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan

# Set up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# The binary is located one level up from the package (editable install)
root = Path(root_init).parent.resolve()
qlk_bindir = (root / "../..").resolve()
qlk_binpath = qlk_bindir / "QuaLiKiz"
if not qlk_binpath.is_file():
    logger.warning('QuaLiKiz binary "{!s}" does not exist'.format(qlk_binpath))


# Use the a JSON as template for this batch
templatepath = "./template.json"

# Load the default QuaLiKiz plan we will use as base
qualikiz_plan_base = QuaLiKizPlan.from_json(templatepath)

# We will use a physically relevant base scan
# We change our base plan programatically instead of with the JSON
qualikiz_plan_base["scan_dict"] = OODS(
    [
        ("Ati", [0, 2.75, 4.25, 7.5, 10, 16]),
        ("Ate", [0, 2.75, 4.25, 7.5, 10, 16]),
    ]
)
qualikiz_plan_base["scan_type"] = "hyperrect"  # Scan all points in the hyperrectangle
qualikiz_plan_base["xpoint_base"]["kthetarhos"] = [
    0.1,
    0.25,
    0.325,
    0.4,
    0.7,
    1.0,
    1.8,
    3,
    9,
    21,
    27,
]  # JETTO grid

# Define the "programatically defined parameters"
# Inner cube
batch_cube = OODS(
    [
        ("q", [1.66, 2]),
    ]
)

# Set up multiple batches.
rest_cube = OODS(
    [
        ("smag", (1,)),
        ("x", (0.55,)),
        ("Ti_Te_rel", (1,)),
        (
            "dilution",
            (
                0,
                0.2,
            ),
        ),
    ]
)

# Manually create the 'megacube' over multiple batches
mega_cube = OODS(
    [
        ("Nustar", (0.001,)),
    ]
)

outer_var = "Nustar"
outer_vals = mega_cube.get(outer_var)
outer_val = outer_vals[0]  # The first megacube

# Save out batches as a subfolder of this one
batch_parent = Path("_".join([outer_var, str(outer_val)])).absolute()
if batch_parent.exists():
    raise Exception(
        "Folder in which we generate already exists. This is probably not what you meant! Please remove it and try again"
    )

# Use single node
nodes = 1

# And build our list of runs
batchlist = []
for ii, subcube_values in enumerate(product(*rest_cube.values_middle_first())):
    # Create name for the batch
    val_strings = list("{:0.2f}".format(v) for v in subcube_values)
    named_strings = list("_".join(foo) for foo in zip(rest_cube.keys(), val_strings))
    batch_name = "_".join(named_strings)
    batch_dir = batch_parent / batch_name  # For convenience

    runlist = []
    for ii, runcube_values in enumerate(product(*batch_cube.values())):
        # Create name for the run
        val_strings = list("{:0.2f}".format(v) for v in runcube_values)
        named_strings = list(
            "_".join(foo) for foo in zip(batch_cube.keys(), val_strings)
        )
        run_name = "_".join(named_strings)

        # Fill what changes every run
        # Set the order _explicitly_, so we know what sets what
        # Note that Ati, Ate, Ane, Ani0, and Nustar are set from the scan_dict itself
        # Use the explicit interfaces to be 100% sure1
        batch_qualikiz_plan = copy.deepcopy(qualikiz_plan_base)
        batch_qualikiz_plan["xpoint_base"]["q"] = runcube_values[
            list(batch_cube.keys()).index("q")
        ]
        batch_qualikiz_plan["xpoint_base"]["smag"] = subcube_values[
            list(rest_cube.keys()).index("smag")
        ]
        batch_qualikiz_plan["xpoint_base"]["x"] = subcube_values[
            list(rest_cube.keys()).index("x")
        ]
        tite = subcube_values[list(rest_cube.keys()).index("Ti_Te_rel")]
        dilution = subcube_values[list(rest_cube.keys()).index("dilution")]

        # Dilution is a bit tricky
        batch_qualikiz_plan["xpoint_base"]["ni0"] = 1 - dilution
        batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_normni_ion"] = 1
        if dilution == 0:
            batch_qualikiz_plan["xpoint_base"]["ni1"] = 0
            batch_qualikiz_plan["xpoint_base"]["Ani1"] = 0
            batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_An_ion"] = 0
        else:
            batch_qualikiz_plan["xpoint_base"]["options"]["set_qn_An_ion"] = 1
            batch_qualikiz_plan["xpoint_base"].set_qn_normni_ion_n()
        # batch_qualikiz_plan['xpoint_base'].match_dilution(dilution)
        # Now set the vars that depend on other vars
        batch_qualikiz_plan["xpoint_base"]["Nustar"] = outer_val
        batch_qualikiz_plan["xpoint_base"]["Ti_Te_rel"] = tite

        run_parent = batch_name
        run_dir = batch_dir / run_name  # For convenience
        binreldir = Path(relpath(qlk_binpath, start=run_dir))
        run = Run(
            batch_dir,
            run_name,
            binreldir,
            qualikiz_plan=batch_qualikiz_plan,
            nodes=nodes,
        )
        runlist.append(run)
    # Implementation detail, we pass the directory the batch
    # _lives in_ and the name of the batch
    logger.info("Dimx of run is", run.qualikiz_plan.calculate_dimx())
    logger.info(
        "If runs are sequential, means {!s} MPI tasks taking {:.2f} minutes".format(
            run.tasks, len(runlist) * run.estimate_walltime(run.tasks) / 60
        )
    )
    batch = Batch(batch_parent, batch_name, runlist)
    batchlist.append(batch)

print(
    "If runs are sequential, means {!s} MPI tasks taking {:.2f} minutes per batch".format(
        run.tasks, len(runlist) * run.estimate_walltime(run.tasks) / 60
    )
)

run_dimx = run.qualikiz_plan.calculate_dimxn() / len(
    run.qualikiz_plan["xpoint_base"]["kthetarhos"]
)  # Single run dimx
batch_dimx = len(runlist) * run_dimx
print(
    "dimx={:.0f} per batch, for a total dimx={:.0f} for {:.0f} batches".format(
        batch_dimx, len(batchlist) * batch_dimx, len(batchlist)
    )
)

cpu_time_single_batch_single_core = len(runlist) * run.estimate_cputime(
    1
)  # In CPU seconds
cpu_time_single_batch_single_node = (
    cpu_time_single_batch_single_core / Run.defaults["cores_per_node"]
)  # In node-seconds

node_hours = len(batchlist) * cpu_time_single_batch_single_node / 3600
cpu_hours = node_hours * Run.defaults["cores_per_node"]
print(
    "If runs are sequential, meaning {:.2f} node hours, or {:.2f} CPU hours".format(
        node_hours, cpu_hours
    )
)

# Prepare our batches, e.g. build the folder structure
for batch in batchlist:
    batch.prepare()

# Generate input
for batch in batchlist:
    print("Generating input files", end="", flush=True)
    batch.generate_input(dotprint=True)
    print("\n")
