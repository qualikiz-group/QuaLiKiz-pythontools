import os
import sys
from subprocess import PIPE, Popen as popen
import shutil
import re
import importlib
from datetime import datetime
from collections import OrderedDict
import json
from pathlib import Path
import importlib.machinery
import importlib.util
import logging

import pytest
from pytest import TempPathFactory
import xarray as xr
from IPython import embed

from qualikiz_tools.machine_specific.bash import Run, Batch
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizPlan
from qualikiz_tools.qualikiz_io.compare_versions import process_case
from qualikiz_tools.qualikiz_io.outputfiles import (
    determine_sizes,
    convert_debug,
    convert_output,
    convert_primitive,
)
from qualikiz_tools.qualikiz_io.qualikizrun import find_qualikiz_binary
from qualikiz_tools.qualikiz_io.reference_runs import generate_file_list
from qualikiz_tools.qualikiz_io.reference_runs import *

this_file = Path(__file__).resolve()  # Normalized path

try:
    binpath = find_qualikiz_binary(this_file)
except FileNotFoundError:
    # Fall back to default path
    binpath = (this_file / "../../../../QuaLiKiz").resolve()

ref_version = "reference"


@pytest.fixture
def generated_files(request):
    test_list = request.getfixturevalue("test_spec")[1]
    paths = generate_file_list(test_list)
    yield paths

    def remove_factory(file, verbose=False):
        def remove():
            if verbose:
                print(file)
            os.remove(file)

        return remove

    for file in paths:
        request.addfinalizer(remove_factory(file))


@pytest.fixture
def generate_all_base_cases():
    generate_all_base_cases_standalone()


@pytest.fixture
def clean_report(request):
    yield
    case_name = get_case_name_in_fixture(request)
    os.remove(case_name + ".pdf")


@pytest.fixture(params=all_tests)
def run_qualikiz_test(request, get_reference_dataset, generate_all_base_cases):
    case_name = request.param
    root_dir = request.config.getoption("--target-dir")
    print(
        "Starting test",
        all_tests.index(case_name) + 1,
        "of",
        len(all_tests),
        ":",
        case_name,
    )
    casefile_path = find_reference_folder(this_file, "reference_parameters")
    qualikiz_plan = QuaLiKizPlan.from_json(
        os.path.join(casefile_path, case_name + ".json")
    )
    # Change CLI-given settings in the QuaLiKiz plan
    simple_mpi_only = request.config.getoption("--simple-mpi-only")
    qualikiz_plan["xpoint_base"]["meta"]["simple_mpi_only"] = simple_mpi_only

    # Needs to be relative to the run folder
    binrelpath = os.path.relpath(binpath, os.path.join(root_dir, case_name))
    tasks = request.config.getoption("--mpi-tasks")
    run = Run(
        root_dir,
        case_name,
        binrelpath,
        nodes=1,  # Specify amount of nodes
        tasks=tasks,  # Reproducibility of multicore QLK in testing
        qualikiz_plan=qualikiz_plan,
    )
    if request.config.getoption("--allow-run-as-root"):
        run.runstring += " --allow-run-as-root"

    batch = Batch(root_dir, case_name, [run])
    batch.prepare(overwrite_batch=True)
    batch.generate_input()
    run_n_times = request.config.getoption("--run-n-times")
    kwargs = {}
    for ii in range(run_n_times):
        print("QLK run", ii + 1, "/", run_n_times)
        if ii == 1:
            kwargs = {"clean": False}
            # Hacky way to change parameters mid-test
            if case_name == "full_flags_run_twice":
                print("Re-generating input")
                case_name = "full_flags"
                qualikiz_plan = QuaLiKizPlan.from_json(
                    os.path.join(casefile_path, case_name + ".json")
                )
                # Change CLI-given settings in the QuaLiKiz plan
                simple_mpi_only = request.config.getoption("--simple-mpi-only")
                qualikiz_plan["xpoint_base"]["meta"][
                    "simple_mpi_only"
                ] = simple_mpi_only
                run = batch.runlist[0]
                run.qualikiz_plan = qualikiz_plan
                run.qualikiz_plan.to_json(os.path.join(run.rundir, run.parameterspath))
                run.generate_input()
        batch.launch(**kwargs)
    # Restore case name
    case_name = request.param
    batch_dir = os.path.join(root_dir, case_name)

    # Convert to netCDF
    sizes = determine_sizes(batch_dir)
    ds = convert_debug(sizes, batch_dir)
    ds = convert_output(ds, sizes, batch_dir)
    ds = convert_primitive(ds, sizes, batch_dir)
    yield case_name, ds
    if not request.config.getoption("--no-clean"):
        shutil.rmtree(batch_dir)


class TestPrerequisites:
    pytestmark = [pytest.mark.need_casefiles]

    @pytest.mark.parametrize("test_spec", generated_tests.items())
    def test_generated_tests(self, test_spec, generated_files):
        generator_name, test_list = test_spec
        generator_module = import_generation_file(generator_name)
        generator_module.generate_base_cases()
        for file in generated_files:
            assert os.path.isfile(file)

    def test_all_import(self):
        casefile_path = find_reference_folder(this_file, "reference_parameters")
        generate_all_base_cases_standalone()
        counter = 0
        for file in os.listdir(casefile_path):
            if file.endswith(".json"):
                counter += 1
                plan = QuaLiKizPlan.from_json(os.path.join(casefile_path, file))
        assert counter == len(all_tests) + len(generated_tests)


class TestBaseCases:
    pytestmark = [pytest.mark.run_qualikiz, pytest.mark.need_casefiles]

    def test_compare_cases(
        self, request, caplog, get_reference_dataset, run_qualikiz_test
    ):
        root_dir = request.config.getoption("--target-dir")
        equiv_rel_diff_bound = request.config.getoption("--equiv-rel-diff-bound")

        case_name, ds_result = run_qualikiz_test
        dss = OrderedDict(
            [
                (ref_version, get_reference_dataset),
                ("current", ds_result),
            ]
        )
        with caplog.at_level(
            logging.DEBUG, logger="qualikiz_tools.qualikiz_io.compare_versions"
        ):
            result = process_case(
                case_name,
                dss,
                ref_version,
                save_pdf=True,
                pdf_dir=root_dir,
                equiv_rel_diff_bound=equiv_rel_diff_bound,
            )
        is_numerically_equivalent = result[1]
        is_binary_equivalent = result[0]
        if equiv_rel_diff_bound != 0:
            assert is_numerically_equivalent[(ref_version, "current")]
        else:
            assert is_binary_equivalent[(ref_version, "current")]


@pytest.fixture
def data_folder(tmp_path_factory: TempPathFactory):
    # Start in a tmp dir and make sure there is no QuaLiKiz binary up the tree
    factory: TempPathFactory = tmp_path_factory
    root: Path = factory.getbasetemp()
    data: Path = factory.mktemp("data")

    with pytest.raises(
        FileNotFoundError,
        match="Reached file system root, but no reference cases folder found",
    ):
        binary: Path = find_reference_folder(
            startdir=data, folder_name="reference_parameters"
        )

    # We know the binary does not exist. We can now pass this to other tests
    return data


@pytest.fixture
def fake_folder(data_folder: Path, tmp_path_factory: TempPathFactory):
    fake_folder: Path = data_folder / "reference_parameters"
    fake_folder.mkdir()
    yield fake_folder
    try:
        shutil.rmtree(fake_folder)
    except FileNotFoundError:
        pass


def test_find_reference_folder(data_folder: Path, fake_folder: Path):
    # In principle this method is pretty dumb. It looks for a QuaLiKiz binary,
    # which is just defined as a file (Path.is_file) that exists and is called
    # `folder_name`. If it doesn't get a `start_dir` it just starts in
    # `Path.cwd()`. In any case, it walks up the tree until a file is found or
    # it raises and exception.

    # Try to see if we can find a simple one
    binary: Path = find_reference_folder(
        startdir=data_folder, folder_name=fake_folder.name
    )
    assert fake_folder == binary, "Different binary found!"


def test_find_reference_folder_link(data_folder: Path, fake_folder: Path):
    # Try to see if we can find a linked binary
    fake_link: Path = Path(data_folder / "QuaLiKiz_link")
    fake_link.symlink_to(fake_folder, target_is_directory=False)
    target: Path = fake_link.readlink()
    assert fake_folder == target
    binary: Path = find_reference_folder(
        startdir=data_folder, folder_name="QuaLiKiz_link"
    )
    assert binary == fake_link
    assert binary != fake_folder
    fake_link.unlink()


def test_find_reference_folder_tree(data_folder: Path, fake_folder: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    binary: Path = find_reference_folder(startdir=subfolder)
    assert fake_folder == binary


def test_find_reference_folder_no_exist(data_folder: Path, fake_folder: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    shutil.rmtree(fake_folder)
    with pytest.raises(
        FileNotFoundError,
        match="Reached file system root, but no reference cases folder found",
    ):
        binary: Path = find_reference_folder(startdir=subfolder)


def test_find_reference_folder_default_startdir(data_folder: Path, fake_folder: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    binary: Path = find_reference_folder(folder_name="reference_parameters")
    assert fake_folder == binary
    os.chdir(cwd)


def test_find_reference_folder_default_all(data_folder: Path, fake_folder: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    with pytest.raises(
        FileNotFoundError,
        match="Either define startdir and folder_name, or define the 'QUALIKIZ_REFERENCE' environment variable",
    ):
        binary: Path = find_reference_folder()
    os.chdir(cwd)


def test_find_reference_folder_env_variable(data_folder: Path, fake_folder: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    assert "QUALIKIZ_REFERENCE" not in os.environ
    os.environ["QUALIKIZ_REFERENCE"] = str(fake_folder)
    binary: Path = find_reference_folder()
    assert fake_folder == binary
    os.environ.pop("QUALIKIZ_REFERENCE")
    assert "QUALIKIZ_REFERENCE" not in os.environ
    os.chdir(cwd)
