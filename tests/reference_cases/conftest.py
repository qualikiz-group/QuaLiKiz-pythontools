# See https://docs.python.org/3.7/library/optparse.html?highlight=add_option#optparse.OptionParser.add_option
import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--no-clean", action="store_true", help="Do not remove files on test cleanup"
    )
    parser.addoption(
        "--allow-run-as-root",
        action="store_true",
        help="Allow using mpirun as root user",
    )
    parser.addoption(
        "--mpi-tasks",
        type=int,
        default=1,
        help="Amount of MPI tasks to use for QuaLiKiz run",
    )
    parser.addoption(
        "--target-dir",
        action="store",
        default="tmp",
        help="Root folder to save QuaLiKizBatch",
    )
    parser.addoption(
        "--simple-mpi-only",
        action="store_true",
        help="Use the simple mpi job distribution scheme inside QuaLiKiz",
    )
    parser.addoption(
        "--equiv-rel-diff-bound",
        type=float,
        default=0,
        help="Bound on max numerical relative difference using `rel_difference`"
        "in compare_versions.py",
    )
    parser.addoption(
        "--run-n-times",
        type=int,
        default=1,
        help="Run QuaLiKiz multiple times, might matter depending on runtimes",
    )
