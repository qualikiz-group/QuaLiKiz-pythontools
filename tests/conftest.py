# pylint: disable=missing-function-docstring,missing-class-docstring,missing-module-docstring # noqa: E501
import shutil
from pathlib import Path
from subprocess import check_output, PIPE
import collections
import os

from IPython import embed  # pylint: disable=unused-import # noqa: F401
import pytest

this_file = Path(__file__).resolve()
datadir = (this_file / "../../testdata").resolve()


##################################
# Common useful pytest functions #
##################################
@pytest.fixture
def tmp_wd(tmp_path):
    """Changes working directory and returns to previous on exit.

    Mixes tmp_path from pytest and a cleanup that automatically moves
    back to where you started in cleanup.
    """
    prev_cwd = Path.cwd()
    os.chdir(tmp_path)
    try:
        yield tmp_path
    finally:
        os.chdir(prev_cwd)


def copy_with_relink(tmp_root: Path, case_name: str):
    """Copy a basecase and relink QuaLikiz binary link.

    This is very lowlevel on purpose, to test the very lowlevel tools manually.

    Args:
        tmp_root: Path instance pointing to the folder where we will copy our
            QuaLiKiz data in. Equivalent to cannonical ``runs`` directory
        case_name: Name of the case to copy from ``datadir``

    Returns:
        A Path instance pointing to the root of the copied QuaLiKiz data.
        Will usually live in ``/tmp/pytest-of-*/pytest-*/*``
    """
    assert isinstance(tmp_root, Path)
    assert isinstance(case_name, str)

    template_dir = datadir / case_name
    target_dir = tmp_root / template_dir.name
    # Copy the full tree, copy_function will _not_ be used for symlinks
    copied_dir = shutil.copytree(
        template_dir,
        target_dir,
        symlinks=True,
        ignore=shutil.ignore_patterns("PLACEHOLDER*"),
    )
    assert copied_dir == target_dir

    # Find all QuaLiKiz binary links. Canonically start with QuaLiKiz-*
    for path in target_dir.rglob("QuaLiKiz*"):
        if path.is_symlink():
            # Relink binary
            binary_path = (template_dir / os.readlink(path)).resolve()
            path.unlink()
            path.symlink_to(binary_path)

    # Move into the newly created path
    # Assume wrapping function will move us back with tmp_wd
    os.chdir(target_dir)
    return target_dir

@pytest.fixture
def prepare_qlk_run(tmp_wd):
    """ A typical QuaLiKizRun folder structure. """
    return copy_with_relink(tmp_wd, "quicktest_run")


@pytest.fixture
def prepare_qlk_batch(tmp_wd):
    """ A typical QuaLiKizBatch folder structure. """
    return copy_with_relink(tmp_wd, "quicktest_batch")


@pytest.fixture
def regression_qlk_multibatch(tmp_wd):
    """ A typical folder with multiple QuaLiKizBatch folders. """
    return copy_with_relink(tmp_wd, "cubetest_multibatch")


@pytest.fixture(params=["this_regression"], ids=["regression_folder_name"])
def regression_qlk_batch(request, prepare_qlk_batch):
    regression_folder_name_idx = request.param_index
    assert regression_folder_name_idx == 0

    extract_to_dir = Path(request.param)
    # Extract for a regression case for dual-run comparisons
    cwd = Path.cwd()
    cmd = "qualikiz_tools output extract_regression {!s} {!s}".format(
        cwd, extract_to_dir
    )
    output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
    regression_dir = extract_to_dir.resolve()

    return prepare_qlk_batch, regression_dir


@pytest.fixture(params=[40], ids=["shell_timeout"])
def run_in_shell_wrapper(request):
    if isinstance(request.param, int):
        shell_timeout = request.param

    def run_in_shell(cmd):
        cmd = str(cmd)
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=shell_timeout)
        return output

    return run_in_shell


@pytest.fixture
def qlk_batch_netcdf_path():
    return datadir / "quicktest_batch.nc"


@pytest.fixture
def qlk_dir(request):
    return (request.config.rootpath / "..").resolve()


def get_case_name_in_fixture(request):
    import re

    case_name = None
    if (
        "run_qualikiz_test" in request.fixturenames
        or "get_reference_dataset" in request.fixturenames
    ):
        node_name = request.node.name
        search = re.search("\[(.+)\]", node_name)
        if search is not None:
            case_name = search.group(1)
    elif "case_name" in request.fixturenames:
        case_name = request.getfixturevalue("case_name")
    return case_name


@pytest.fixture
def get_reference_dataset(request):
    from qualikiz_tools.qualikiz_io.reference_runs import find_reference_folder
    from qualikiz_tools.qualikiz_io.outputfiles import (
        determine_sizes,
        convert_debug,
        convert_output,
        convert_primitive,
    )
    import os
    import xarray as xr

    case_name = get_case_name_in_fixture(request)
    reference_case_path = find_reference_folder(this_file, "reference")
    case_reference_path = os.path.join(reference_case_path, case_name)
    # Workaround to get non-confusing printed fixture statements in tests
    xr.Dataset.__str__ = lambda self: case_reference_path
    if os.path.exists(case_reference_path):
        sizes = determine_sizes(case_reference_path)
        ds = convert_debug(sizes, case_reference_path)
        ds = convert_output(ds, sizes, case_reference_path)
        ds = convert_primitive(ds, sizes, case_reference_path)
        # ref_netcdf_path = os.path.join(reference_case_path, case_name + '.nc')
        # ds_ref = xr.open_dataset(ref_netcdf_path)
    else:
        raise Exception("Could not find reference for case {!s}".format(case_name))
    return ds


DefaultPlotcase = collections.namedtuple(
    "DefaultPlotcase", "tmpdir data_paths script_path"
)


@pytest.fixture(params=["example_extra_impurities", "no_ETG"])
def default_plotcase(request, tmpdir, qlk_dir):
    case_name = request.param
    test_root: LocalPath = request.fspath.realpath()  # Path to this file
    assert test_root.exists()
    plot_script_path: LocalPath = test_root.join(
        "../../../qualikiz_tools/plottools/plot_fluxlike.py"
    ).realpath()
    assert plot_script_path.exists()
    data_path: LocalPath = test_root.join(f"../../../testdata/{case_name}").realpath()
    assert data_path.exists()
    data2_name = f"{case_name}_two"
    data2_path = tmpdir.mkdir(data2_name)
    data_path.copy(data2_path)
    case = DefaultPlotcase(tmpdir, [data_path, data2_path], plot_script_path)
    yield case
    tmpdir.remove()
