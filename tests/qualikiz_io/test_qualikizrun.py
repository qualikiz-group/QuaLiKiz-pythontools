# pylint: disable=no-self-use,missing-function-docstring,missing-class-docstring,missing-module-docstring # noqa
import copy
import logging
import os
import re

import pytest  # pylint: disable=unused-import # noqa: F401
from pytest import approx, TempPathFactory
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.fs_manipulation.compare import bin_to_np
from qualikiz_tools.qualikiz_io.qualikizrun import *  # noqa: F403

logger_name = "qualikiz_tools.qualikiz_io.qualikizrun"
logger = logging.getLogger(logger_name)
qlkbin = (root / "../../QuaLiKiz").resolve()


class TestFromSubdirFilter:
    def test_print_warning(self, caplog):
        caplog.set_level(logging.WARNING, logger=logger_name)
        logger.warning("A test warning %s", "here")
        assert caplog.record_tuples == [
            (logger_name, logging.WARNING, "A test warning here")
        ]

    def test_from_dir_undefined(self, caplog):
        caplog.set_level(logging.WARNING, logger=logger_name)
        logger.warning(SPECIAL_FROM_DIR_UNDEFINED_MSG)
        assert caplog.record_tuples == [
            (logger_name, logging.WARNING, "Specialized from_dir method not defined")
        ]

    def test_from_dir_undefined_mask(self, caplog):
        caplog.set_level(logging.WARNING, logger=logger_name)
        fil = FromSubdirFilter()
        logger.addFilter(fil)
        logger.warning(SPECIAL_FROM_DIR_UNDEFINED_MSG)
        assert caplog.record_tuples == []
        logger.removeFilter(fil)
        logger.warning(SPECIAL_FROM_DIR_UNDEFINED_MSG)
        assert caplog.record_tuples == [
            (logger_name, logging.WARNING, "Specialized from_dir method not defined")
        ]

    def test_from_file_undefined(self, caplog):
        caplog.set_level(logging.WARNING, logger=logger_name)
        logger.warning(SPECIAL_FROM_FILE_UNDEFINED_STARTMSG)
        assert caplog.record_tuples == [
            (logger_name, logging.WARNING, "No from_file function defined for ")
        ]

    def test_from_file_undefined_mask(self, caplog):
        caplog.set_level(logging.WARNING, logger=logger_name)
        fil = FromSubdirFilter()
        logger.addFilter(fil)
        logger.warning(SPECIAL_FROM_FILE_UNDEFINED_STARTMSG)
        assert caplog.record_tuples == []
        logger.removeFilter(fil)
        logger.warning(SPECIAL_FROM_FILE_UNDEFINED_STARTMSG)
        assert caplog.record_tuples == [
            (logger_name, logging.WARNING, "No from_file function defined for ")
        ]


class TestQuaLiKizRun:
    def test_initialize(self):
        cwd = Path.cwd()
        run = QuaLiKizRun(cwd, "testrun", qlkbin)
        assert isinstance(run, QuaLiKizRun)

    def test_shallow_copy(self):
        cwd = Path.cwd()
        run = QuaLiKizRun(cwd, "testrun", qlkbin)
        newrun = copy.copy(run)
        assert newrun == run
        assert newrun is not run
        assert run.metadata is newrun.metadata

    def test_deepcopy(self):
        cwd = Path.cwd()
        run = QuaLiKizRun(cwd, "testrun", qlkbin)
        newrun = copy.deepcopy(run)
        assert newrun == run
        assert newrun is not run
        assert run.metadata is not newrun.metadata

    def test_prepare(self, tmpdir):
        """Preparing a run should create a skeleton folder structure"""
        with tmpdir.as_cwd():
            cwd = Path.cwd()
            run = QuaLiKizRun(cwd, "testrun", qlkbin)
            run.prepare()
            assert run.rundir.is_dir()
            assert (run.rundir / "input").is_dir()
            assert (run.rundir / "debug").is_dir()
            assert (run.rundir / "meta").is_dir()
            assert (run.rundir / "output/primitive").is_dir()
            assert (run.rundir / "output_qlflux").is_dir()
            assert (run.rundir / "debug_qlflux").is_dir()
            assert (run.rundir / "metadata.json").is_file()
            assert (run.rundir / "parameters.json").is_file()
            assert (run.rundir / "impurities_qlflux.json").is_file()
            assert (run.rundir / qlkbin.name).is_symlink()

    def test_generate_input(self, tmpdir):
        with tmpdir.as_cwd():
            cwd = Path.cwd()
            run = QuaLiKizRun(cwd, "testrun", qlkbin)
            run.prepare()
            run.generate_input()
            input_dir = run.rundir / "input"
            assert len(list(input_dir.iterdir())) == 53

    def test_inputbinaries_exist(self, tmpdir):
        with tmpdir.as_cwd():
            cwd = Path.cwd()
            run = QuaLiKizRun(cwd, "testrun", qlkbin)
            run.prepare()
            assert not run.inputbinaries_exist()
            run.generate_input()
            assert run.inputbinaries_exist()

    def test_from_dir(self, tmpdir):
        with tmpdir.as_cwd():
            cwd = Path.cwd()
            run = QuaLiKizRun(cwd, "testrun", qlkbin)
            run.prepare()
            other_run = QuaLiKizRun.from_dir(cwd / "testrun")
            assert other_run == run
            assert other_run is not run

    def test_clean(self, tmpdir):
        with tmpdir.as_cwd():
            cwd = Path.cwd()
            run = QuaLiKizRun(cwd, "testrun", qlkbin)
            outputdir = run.rundir / run.outputdir
            primitivedir = run.rundir / run.primitivedir
            debugdir = run.rundir / run.debugdir
            stdout = run.rundir / run.stdout
            stderr = run.rundir / run.stderr
            runcounter = run.rundir / "runcounter.dat"
            testfiles = [
                outputdir / "test.dat",
                primitivedir / "test.dat",
                debugdir / "test.dat",
                stdout,
                stderr,
                runcounter,
            ]

            run.prepare()
            for testfile in testfiles:
                if testfile == runcounter:
                    testfile.write_text("0")
                testfile.touch()
            for testfile in testfiles:
                assert testfile.is_file()
            run.clean()
            for testfile in testfiles:
                assert not testfile.is_file()

    def test_reconstruct_run(self, prepare_qlk_run):
        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        assert run.qualikiz_plan["scan_dict"] == {"smag": [0.7, 1]}
        assert run.metadata == {"mark": ["foo", "bar"]}
        assert run.impurities_qlflux == {
            "ion_index": 0,
            "Ai": [4, 14, 19],
            "Zi": [2, 7, 9],
        }

    def test_reconstruct_run_missing_metadata(self, caplog, prepare_qlk_run):
        meta_path = prepare_qlk_run / QuaLiKizRun.metadatapath
        meta_path.unlink()
        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        assert "Could not find metadata JSON" in caplog.text

    def test_reconstruct_run_missing_qlflux(self, caplog, prepare_qlk_run):
        ql_path = prepare_qlk_run / QuaLiKizRun.qlfluxpath
        ql_path.unlink()
        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        assert "Could not find impurities_qlflux JSON" in caplog.text

    def test_generate_qlscan_input(self, prepare_qlk_run):
        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        inputdir = run.rundir / run.inputdir
        debugdir = run.rundir / run.debug_qlfluxdir
        outputdir = run.rundir / run.output_qlfluxdir
        primitivedir = run.rundir / run.primitive_qlfluxdir
        # TODO: Use a "modern" QuaLiKiz Run
        # Create missing folders during testing phase
        debugdir.mkdir()
        primitivedir.mkdir(parents=True)

        # Check our folder is empty at the start
        non_prim = set(outputdir.iterdir()) - set(
            outputdir.glob(Path(run.primitive_qlfluxdir).name)
        )
        assert len(non_prim) == 0
        assert len(list(primitivedir.iterdir())) == 0
        assert len(list(debugdir.iterdir())) == 0

        # Check if we have inputfiles, we should have them and
        # should be able to overwrite them
        assert len(list(inputdir.iterdir())) != 0

        # Double check input variables
        assert run.impurities_qlflux["ion_index"] == 0
        assert run.impurities_qlflux["Ai"] == [4, 14, 19]
        assert run.impurities_qlflux["Zi"] == [2, 7, 9]
        # Test if QLFlux will generate something new
        assert run.qualikiz_plan["xpoint_base"]["Ai"] != [4, 14, 19]
        assert run.qualikiz_plan["xpoint_base"]["Zi"] != [2, 7, 9]
        assert run.qualikiz_plan["xpoint_base"]["typei"] != [0, 0, 0]
        assert run.qualikiz_plan["xpoint_base"]["options"]["check_qn"]
        assert run.qualikiz_plan["xpoint_base"]["options"]["set_qn_normni"]
        assert run.qualikiz_plan["xpoint_base"]["options"]["set_qn_An"]

        # Generate input
        old_plan = copy.deepcopy(run.qualikiz_plan)
        run.generate_input(qlflux=True)

        # Check generated binaries
        Ai = bin_to_np(inputdir / "Ai.bin")
        Zi = bin_to_np(inputdir / "Zi.bin")
        typei = bin_to_np(inputdir / "typei.bin")

        # 2 point scan, list extended with impurities_qlflux
        assert Ai == approx([2, 2, 4, 4, 14, 14, 19, 19])
        assert Zi == approx([1, 1, 2, 2, 7, 7, 9, 9])
        assert all(typei == [1, 1, 0, 0, 0, 0, 0, 0])

        # Double check special kind of Pythontools options
        # Implementations changes temporarely check_qn, set_qn_normni, and set_qn_An
        normni = bin_to_np(inputdir / "normni.bin")
        Ani = bin_to_np(inputdir / "Ani.bin")

        assert all(ni == 1e-5 for ni in normni[2:])
        assert normni[:1] == approx(1, 1)
        assert all(An == 3 for An in Ani)

        # Check that the plan didn't change
        assert old_plan is not run.qualikiz_plan
        assert old_plan == run.qualikiz_plan

        # Double check as I do not trust myself
        assert run.qualikiz_plan["xpoint_base"]["options"]["set_qn_An"]


class TestQuaLiKizBatch:
    @pytest.fixture
    def base_batch(self, tmpdir):
        run0 = QuaLiKizRun(tmpdir / "testbatch", "testrun0", qlkbin)
        run1 = QuaLiKizRun(tmpdir / "testbatch", "testrun1", qlkbin)
        batch = QuaLiKizBatch(tmpdir, "testbatch", [run0, run1])
        return batch

    def test_initialize(self, base_batch):
        assert isinstance(base_batch, QuaLiKizBatch)

    def test_prepare(self, base_batch):
        base_batch.prepare()
        batch_dir = base_batch.parent_dir / base_batch.name
        assert (batch_dir / base_batch.scriptname).is_file()
        assert (batch_dir / base_batch.runlist[0].rundir / "parameters.json").is_file()
        assert (batch_dir / base_batch.runlist[1].rundir / "parameters.json").is_file()

    def test_generate_input(self, base_batch):
        base_batch.prepare()
        base_batch.generate_input()
        assert base_batch.runlist[0].inputbinaries_exist()
        assert base_batch.runlist[1].inputbinaries_exist()

    def test_runlist_from_subdirs(self, base_batch):
        base_batch.prepare()
        batch_dir = base_batch.parent_dir / base_batch.name
        runlist = QuaLiKizBatch.runlist_from_subdirs(batch_dir)
        assert_equal_ignore_order(runlist, base_batch.runlist)

    def test_from_subdirs(self, base_batch):
        base_batch.prepare()
        batch_dir = base_batch.parent_dir / base_batch.name
        other_batch = QuaLiKizBatch.from_subdirs(batch_dir)
        assert other_batch == base_batch

    def test_from_dir_recursive(self, base_batch):
        base_batch.prepare()
        batch_dir = base_batch.parent_dir / base_batch.name
        other_batches = QuaLiKizBatch.from_dir_recursive(batch_dir)
        assert other_batches[0] == base_batch

    def test_clean(self, base_batch):
        run = base_batch.runlist[0]
        outputdir = run.rundir / run.outputdir
        primitivedir = run.rundir / run.primitivedir
        debugdir = run.rundir / run.debugdir
        stdout = run.rundir / run.stdout
        stderr = run.rundir / run.stderr
        runcounter = run.rundir / "runcounter.dat"
        testfiles = [
            outputdir / "test.dat",
            primitivedir / "test.dat",
            debugdir / "test.dat",
            stdout,
            stderr,
            runcounter,
        ]

        base_batch.prepare()
        for testfile in testfiles:
            if testfile == runcounter:
                testfile.write_text("0")
            testfile.touch()
        for testfile in testfiles:
            assert testfile.is_file()
        base_batch.clean()
        for testfile in testfiles:
            assert not testfile.is_file()


def assert_equal_ignore_order(a, b):
    """Use only when elements are neither hashable nor sortable!"""
    unmatched = list(b)
    for element in a:
        try:
            unmatched.remove(element)
        except ValueError:
            return False
    assert not unmatched


class TestToNetCDF4:
    def test_run_to_netcdf(self, prepare_qlk_run):
        run_name = prepare_qlk_run.name
        ds = run_to_netcdf(prepare_qlk_run)
        netcdf_path = prepare_qlk_run / (run_name + ".nc")
        assert netcdf_path.is_file(), "Reference netCDF does not exist"
        # Not identical, as we convert to float32
        ds2 = run_to_netcdf(prepare_qlk_run, overwrite=False)
        assert id(ds) != id(ds2)
        xr.testing.assert_allclose(ds, ds2)

    def test_qlk_run_to_netcdf(self, prepare_qlk_run):
        run_name = prepare_qlk_run.name
        netcdf_path = prepare_qlk_run / (run_name + ".nc")
        assert not netcdf_path.is_file(), "Pre-existing netCDF found!"

        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        run.to_netcdf()
        assert netcdf_path.is_file(), "No netCDF created"

    multibatch_name = "Nustar_0.001"
    batch_name0 = "smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.20"
    batch_name1 = "smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.00"

    def test_qlk_batch_to_netcdf(self, caplog, regression_qlk_multibatch):
        # "default" settings. Glue runs together to a final Batch
        # and clean the left-behind run netCDFs. Use dimx as glue dimension
        # This is a regular case; runfolders inside a batchfolder
        batch_dir = regression_qlk_multibatch / self.multibatch_name / self.batch_name0
        batch = QuaLiKizBatch.from_dir(batch_dir)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            batch.to_netcdf(verbosity=2)

        # Check if the right mode is selected
        glue_msg = next((s for s in caplog.messages if "Gluing" in s), None)
        assert "matrixy" in glue_msg
        assert "glue_dimx" in glue_msg

        using_msg = next((s for s in caplog.messages if "Using" in s), None)
        n_processes_used = int(re.findall(r"Using (\d) processes", using_msg)[0])
        assert n_processes_used == 1

        assert_netCDF_exists(batch)

    def test_qlk_runbatch_to_netcdf(self, caplog, prepare_qlk_batch):
        # This is a special case; runfolder == batchfolder, so no gluing needed
        batch = QuaLiKizBatch.from_dir(prepare_qlk_batch)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            dss = batch.to_netcdf(
                verbosity=2
            )  # pylint: disable=unused-variable # noqa: F841,E501

        # Check if the right mode is selected
        glue_msg = next((s for s in caplog.messages if "Gluing" in s), None)
        assert "Gluing is not needed" in glue_msg

        os.chdir(prepare_qlk_batch / "..")
        assert_netCDF_exists(batch, run_should_exist=True)

    def test_qlk_batch_to_netcdf_noglue(self, caplog, regression_qlk_multibatch):
        # Do not glue, but just keep the original runfiles
        # Should happen automatically
        batch_dir = regression_qlk_multibatch / self.multibatch_name / self.batch_name0
        batch = QuaLiKizBatch.from_dir(batch_dir)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            batch.to_netcdf(mode="noglue", verbosity=2)

        # Check if the right mode is selected
        glue_msg = next((s for s in caplog.messages if "Gluing" in s), None)
        assert "return all generated runs" in glue_msg

        assert_netCDF_exists(batch, batch_should_exist=False, run_should_exist=True)

    def test_qlk_batch_to_netcdf_dual_core(self, caplog, regression_qlk_multibatch):
        # Test multi-processing with 2 processes
        batch_dir = regression_qlk_multibatch / self.multibatch_name / self.batch_name0
        batch = QuaLiKizBatch.from_dir(batch_dir)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            batch.to_netcdf(n_processes=2, verbosity=2)

        using_msg = next((s for s in caplog.messages if "Using" in s), None)
        n_processes_used = int(re.findall(r"Using (\d) processes", using_msg)[0])
        assert n_processes_used == 2

        assert_netCDF_exists(batch)

    def test_qlk_batch_to_netcdf_max_core(self, caplog, regression_qlk_multibatch):
        # Test multi-processing with the max amount of processes
        batch_dir = regression_qlk_multibatch / self.multibatch_name / self.batch_name0
        batch = QuaLiKizBatch.from_dir(batch_dir)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            batch.to_netcdf(n_processes="max", verbosity=2)

        using_msg = next((s for s in caplog.messages if "Using" in s), None)
        n_processes_used = int(re.findall(r"Using (\d+) processes", using_msg)[0])
        mp_msg = next((s for s in caplog.messages if "Multiprocessing" in s), None)
        n_cpus_detected = int(
            re.findall(r"Multiprocessing detected (\d+) CPUs", mp_msg)[0]
        )
        assert_netCDF_exists(batch)

    def test_qlk_batch_to_netcdf_loopy(self, caplog, regression_qlk_multibatch):
        batch_dir = regression_qlk_multibatch / self.multibatch_name / self.batch_name0
        batch = QuaLiKizBatch.from_dir(batch_dir)
        with caplog.at_level(logging.DEBUG, logger=logger_name):
            batch.to_netcdf(loopy=True, verbosity=2)

        assert_netCDF_exists(batch)


def assert_netCDF_exists(batch, batch_should_exist=True, run_should_exist=False):
    # Check batch netCDF
    netcdf_path = batch.parent_dir / batch.name / f"{batch.name}.nc"
    if batch_should_exist:
        assert netcdf_path.is_file()
    else:
        assert not netcdf_path.is_file()

    # Check run netCDFs
    run_netcdf_paths = [run.rundir / f"{run.rundir.name}.nc" for run in batch.runlist]
    for netcdf_path in run_netcdf_paths:
        if run_should_exist:
            assert netcdf_path.is_file()
        else:
            assert not netcdf_path.is_file()


@pytest.fixture
def data_folder(tmp_path_factory: TempPathFactory):
    # Start in a tmp dir and make sure there is no QuaLiKiz binary up the tree
    factory: TempPathFactory = tmp_path_factory
    root: Path = factory.getbasetemp()
    data: Path = factory.mktemp("data")

    with pytest.raises(
        FileNotFoundError, match="Reached file system root, but no binary found"
    ):
        binary: Path = find_qualikiz_binary(startdir=data, binary_name="QuaLiKiz")

    # We know the binary does not exist. We can now pass this to other tests
    return data


@pytest.fixture
def fake_binary(data_folder: Path, tmp_path_factory: TempPathFactory):
    fake_binary: Path = data_folder / "QuaLiKiz"
    fake_binary.touch()
    yield fake_binary
    fake_binary.unlink(missing_ok=True)


def test_find_qualikiz_binary(data_folder: Path, fake_binary: Path):
    # In principle this method is pretty dumb. It looks for a QuaLiKiz binary,
    # which is just defined as a file (Path.is_file) that exists and is called
    # `binary_name`. If it doesn't get a `start_dir` it just starts in
    # `Path.cwd()`. In any case, it walks up the tree until a file is found or
    # it raises and exception.

    # Try to see if we can find a simple one
    binary: Path = find_qualikiz_binary(
        startdir=data_folder, binary_name=fake_binary.name
    )
    assert fake_binary == binary, "Different binary found!"


def test_find_qualikiz_binary_link(data_folder: Path, fake_binary: Path):
    # Try to see if we can find a linked binary
    fake_link: Path = Path(data_folder / "QuaLiKiz_link")
    fake_link.symlink_to(fake_binary, target_is_directory=False)
    target: Path = fake_link.readlink()
    assert fake_binary == target
    binary: Path = find_qualikiz_binary(
        startdir=data_folder, binary_name="QuaLiKiz_link"
    )
    assert binary == fake_link
    assert binary != fake_binary
    fake_link.unlink()


def test_find_qualikiz_binary_tree(data_folder: Path, fake_binary: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    binary: Path = find_qualikiz_binary(startdir=subfolder)
    assert fake_binary == binary


def test_find_qualikiz_binary_no_exist(data_folder: Path, fake_binary: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    fake_binary.unlink()
    with pytest.raises(
        FileNotFoundError, match="Reached file system root, but no binary found"
    ):
        binary: Path = find_qualikiz_binary(startdir=subfolder)


def test_find_qualikiz_binary_default_startdir(data_folder: Path, fake_binary: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    binary: Path = find_qualikiz_binary(binary_name="QuaLiKiz")
    assert fake_binary == binary
    os.chdir(cwd)


def test_find_qualikiz_binary_default_all(data_folder: Path, fake_binary: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    with pytest.raises(
        FileNotFoundError,
        match="Either define startdir and binary_name, or define the 'QUALIKIZ_BIN' environment variable",
    ):
        binary: Path = find_qualikiz_binary()
    os.chdir(cwd)


def test_find_qualikiz_binary_env_variable(data_folder: Path, fake_binary: Path):
    subfolder: Path = data_folder / "subfolder"
    subfolder.mkdir()
    cwd = Path.cwd()
    os.chdir(subfolder)
    assert "QUALIKIZ_BIN" not in os.environ
    os.environ["QUALIKIZ_BIN"] = str(fake_binary)
    binary: Path = find_qualikiz_binary()
    assert fake_binary == binary
    os.environ.pop("QUALIKIZ_BIN")
    assert "QUALIKIZ_BIN" not in os.environ
    os.chdir(cwd)
