# pylint: disable=missing-module-docstring
import pytest  # pylint: disable=unused-import # noqa: F401
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.static_analysis import *  # pylint: disable=unused-wildcard-import, wildcard-import # noqa: F403, F401, E501

from qualikiz_tools.qualikiz_io.static_analysis import (
    _dump_qlk_datamap,
    _get_qlk_datamap,
)  # pylint: disable=unused-wildcard-import, wildcard-import # noqa: F403, F401, E501

from qualikiz_tools.qualikiz_io.outputfiles import variables as output_variables


@pytest.mark.qualikiz_src
class TestQuaLiKizHardcoded:
    def test_get_qlk_datamap(self, qlk_dir):
        assert qlk_dir.exists()
        variables = _get_qlk_datamap(qlk_dir)
        assert len(variables) == 189

    def test_dump_qlk_datamap(self, qlk_dir, tmpdir):
        target_path = _dump_qlk_datamap(target_path=tmpdir.join("datamap.json"))
        assert target_path.exists()
        genned_variables = json.load(target_path.open())
        del genned_variables["_metadata"]  # Delete generation metadata
        assert genned_variables == output_variables
