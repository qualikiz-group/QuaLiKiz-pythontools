# pylint: disable=no-self-use,missing-function-docstring,missing-class-docstring,missing-module-docstring,redefined-outer-name # noqa
# from subprocess import PIPE, Popen as popen
import copy
from pathlib import Path

import pytest  # pylint: disable=unused-import # noqa: F401
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.inputfiles import *  # pylint: disable=unused-wildcard-import, wildcard-import # noqa: F403, F401, E501


@pytest.fixture
def part_dict():
    """ Dictionairy with variables needed for Particles, so Ions and Electron """
    part = {
        "T": 8.0,
        "n": 0.1,
        "At": 5.0,
        "An": 5.0,
        "type": 1,
        "anis": 1.0,
        "danisdr": 0.0,
    }
    return part


@pytest.fixture
def ion_0_dict(part_dict):
    ion_0 = copy.deepcopy(part_dict)
    ion_0["n"] = 0.9
    ion_0["A"] = 2.0
    ion_0["Z"] = 1.0
    ion_0["type"] = 1
    return ion_0


@pytest.fixture
def ion_1_dict(part_dict):
    ion_1 = copy.deepcopy(part_dict)
    ion_1["A"] = 12.0
    ion_1["Z"] = 6.0
    ion_1["type"] = 2
    return ion_1


@pytest.fixture
def ion_2_dict(part_dict):
    ion_2 = copy.deepcopy(part_dict)
    ion_2["A"] = 9.0
    ion_2["Z"] = 4.0
    ion_2["type"] = 3
    return ion_2


@pytest.fixture
def ions(ion_0_dict, ion_1_dict, ion_2_dict):
    ions = IonList(ion_0_dict, ion_1_dict)
    ions.append(Ion(**ion_2_dict))
    return ions


@pytest.fixture
def particles(ions, part_dict):
    elec_dict = copy.deepcopy(part_dict)
    elec = Electron(**elec_dict)
    return elec, ions


@pytest.fixture
def meta_dict():
    meta = {
        "phys_meth": 2,
        "coll_flag": True,
        "rot_flag": False,
        "verbose": True,
        "separateflux": False,
        "numsols": 3,
        "relacc1": 1e-3,
        "relacc2": 2e-2,
        "absacc1": 0,
        "absacc2": 0,
        "maxruns": 1,
        "maxpts": 5e5,
        "timeout": 60,
    }
    return meta


@pytest.fixture
def meta(meta_dict):
    return Meta(meta_dict)


@pytest.fixture
def kthetarhos_lst():
    kthetarhos = [
        0.1,
        2.2,
        4.4,
        6.6,
        8.8,
        11.0,
        13.2,
        15.4,
        17.6,
        19.8,
        22.0,
        24.2,
        26.4,
        28.6,
        30.8,
        33.0,
    ]
    return kthetarhos


@pytest.fixture
def special(kthetarhos_lst):
    return Special(kthetarhos=kthetarhos_lst)


@pytest.fixture
def geometric_dict():
    geometric = {
        "x": 0.45,
        "rho": 0.45,
        "Ro": 3,
        "Rmin": 1,
        "Bo": 3,
        "q": 3.0,
        "smag": 2.0,
        "alpha": 0.0,
        "gammaE": 0.0,
        "Machpar": 0.0,
        "Machtor": 0.0,
        "Aupar": 0.0,
        "Autor": 0.0,
    }
    return geometric


@pytest.fixture
def geometric(geometric_dict):
    return Geometric(**geometric_dict)


@pytest.fixture
def base_x_point(particles, kthetarhos_lst, meta_dict, geometric_dict):
    elec, ions = particles
    options = {
        "set_qn_normni": False,
        "set_qn_normni_ion": 0,
        "set_qn_An": False,
        "set_qn_An_ion": 0,
        "check_qn": False,
        "x_eq_rho": False,
        "recalc_Nustar": False,
        "recalc_Ti_Te_rel": False,
        "assume_tor_rot": False,
        "recalc_Nustar": False,
        "recalc_Ti_Te_rel": False,
        "assume_tor_rot": False,
    }
    defaults = {}
    defaults.update(options)
    baseXpoint = QuaLiKizXpoint(
        kthetarhos=kthetarhos_lst,
        electrons=elec,
        ions=ions,
        **meta_dict,
        **geometric_dict,
        **options,
    )
    return baseXpoint


class TestIonList:
    def test_set_ionlist(self, ions):
        ions["At"] = 10
        for ion in ions:
            assert ion["At"] == 10


class TestQuaLiKizXpoint:
    def test_initialize(self, base_x_point):
        """ Test if hardcoded variables have not changed """
        assert base_x_point["Ai0"] == 2.0
        assert base_x_point["Ai1"] == 12.0
        assert base_x_point["Ai2"] == 9.0
        base_x_point["Ai2"] = 200.0

    def test_initialize_isolation(self, base_x_point):
        """ Test if fixtures are isolated """
        assert base_x_point["Ai2"] == 9.0

    def test_set_qn_normni_ion_n(self, base_x_point):
        """ Test setting normni on different ions in the QuaLiKizXpoint """
        base_x_point["options"]["set_qn_normni"] = True
        base_x_point["options"]["set_qn_normni_ion"] = 0

        base_x_point["ions"][0]["Z"] = 2
        base_x_point["ions"][1]["Z"] = 6
        base_x_point["ions"][2]["Z"] = 4
        n0s = [0.05, 0.2, 0.365, 0.497]
        n1s = [0.15, 0.1, 0.045, 0.001]
        print("test set_qn")
        for n0, n1 in zip(n0s, n1s):
            base_x_point["ions"][0]["n"] = 0.0
            base_x_point["ions"][1]["n"] = n1
            base_x_point.set_qn_normni_ion_n()
            assert n0 == pytest.approx(base_x_point["ions"][0]["n"])

        base_x_point["ions"][2]["type"] = 1
        for n0, n1 in zip(n0s, n1s):
            base_x_point["ions"][0]["n"] = 0.0
            base_x_point["ions"][1]["n"] = n1 / 3
            base_x_point["ions"][2]["n"] = n1
            base_x_point.set_qn_normni_ion_n()
            assert n0 == pytest.approx(base_x_point["ions"][0]["n"])

    def test_set_qn_An_ion_n(self, base_x_point):
        """ Test setting An on different ions in the QuaLiKizXpoint """
        base_x_point["options"]["set_qn_An"] = True
        base_x_point["options"]["set_qn_An_ion"] = 0

        base_x_point["ions"][0]["Z"] = 2
        An0s = [
            (5 - 0.09) / 1.8,
            (5 - 0.06) / 1.8,
            (5 - 0.027) / 1.8,
            (5 - 0.0006) / 1.8,
        ]
        An1s = [0.15, 0.1, 0.045, 0.001]
        for An0, An1 in zip(An0s, An1s):
            base_x_point["ions"][0]["An"] = 0.0
            base_x_point["ions"][1]["An"] = An1
            base_x_point.set_qn_An_ion_n()
            assert An0 == pytest.approx(base_x_point["ions"][0]["An"])

        base_x_point["ions"][2]["type"] = 1
        for An0, An1 in zip(An0s, An1s):
            base_x_point["ions"][0]["An"] = 0.0
            base_x_point["ions"][1]["An"] = An1 / 3
            base_x_point["ions"][2]["An"] = 3 * An1 / 3
            base_x_point.set_qn_An_ion_n()
            assert An0 == pytest.approx(base_x_point["ions"][0]["An"])

    def test_check_quasi(self, base_x_point):
        """ Test Exceptions raised by quasineutrality violations """
        base_x_point["options"]["check_qn"] = True
        with pytest.raises(Exception) as ex:
            base_x_point.check_quasi()
        assert "Quasineutrality violated!" in str(ex.value)
        base_x_point["ions"][0]["n"] = 0.4
        base_x_point.check_quasi()
        base_x_point["ions"][1]["An"] = 4.0
        with pytest.raises(Exception) as ex:
            base_x_point.check_quasi()
        assert "Quasineutrality gradient violated!" in str(ex.value)
        base_x_point["ions"][1]["An"] = 5.0
        base_x_point.check_quasi()

    def test_setitem_singles(self, base_x_point):
        """ Test settings of single values, e.g. floats and ints """
        test_params = [
            "Te",
            "Ti1",
            "ne",
            "ni1",
            "Ate",
            "Ati1",
            "Ane",
            "Ani1",
            "typee",
            "typei1",
            "anise",
            "anisi1",
            "danisdre",
            "danisdri1",
            "Ai1",
            "Zi1",
        ]
        test_params += (
            list(QuaLiKizXpoint.Meta.in_args.keys())
            + QuaLiKizXpoint.Geometry.in_args
            + list(QuaLiKizXpoint.Options.in_args.keys())
        )

        for param in test_params:
            base_x_point[param] = 50
            assert base_x_point[param] == 50, param

    def test_getset_nonexisting(self, base_x_point):
        """ Test raising of errors on settings/getting made up quantities """
        with pytest.raises(NotImplementedError) as ex:
            base_x_point["made up"] = 50
        assert "setting of made up" in str(ex.value)

        with pytest.raises(NotImplementedError) as ex:
            base_x_point["made up"]
        assert "getting of made up" in str(ex.value)

        base_x_point["options"]["set_qn_normni"] = True
        base_x_point.__setitem__("ni2", 5)
        base_x_point["options"]["set_qn_An"] = True
        base_x_point.__setitem__("ni2", 5)
        base_x_point["options"]["check_qn"] = True
        base_x_point.__setitem__("ni2", 5)

    def test_setitem_arrays(self, base_x_point):
        """ Test setting of arrays + raising of errors on settings/getting made up quantities """
        test_params = [
            "Ti",
            "ni",
            "Ati",
            "Ani",
            "typei",
            "anisi",
            "danisdri",
            "Ai",
            "Zi",
        ]
        for param in test_params:
            base_x_point[param] = 50
            assert base_x_point[param] == 50, param

        with pytest.raises(NotImplementedError) as ex:
            base_x_point["ions"]["made up"]
        assert "getting of made up" in str(ex.value)

    def test_setitem_kthetarhos(self, base_x_point):
        base_x_point["kthetarhos"] = [1, 4, 8]
        assert base_x_point["kthetarhos"], [1, 4, 8]

    def test_getitem_kthetarhos(self, base_x_point):
        base_x_point["kthetarhos"]

    def test_setitem_unknown(self, base_x_point):
        with pytest.raises(NotImplementedError) as ex:
            base_x_point["testi"] = 5

    def test_match_zeff(self, base_x_point):
        base_x_point.match_zeff(1.3)
        assert 0.94 == pytest.approx(base_x_point["ions"][0]["n"])
        assert 0.01 == pytest.approx(base_x_point["ions"][1]["n"])
        assert base_x_point["ions"][2]["n"] == 0.1
        base_x_point.match_zeff(1.7)
        assert 0.86 == pytest.approx(base_x_point["ions"][0]["n"])
        assert 0.02333333333333 == pytest.approx(base_x_point["ions"][1]["n"])
        assert base_x_point["ions"][2]["n"] == 0.1
        with pytest.raises(Exception) as ex:
            base_x_point.match_zeff(0.1)
        assert str(ex.value).startswith("Zeff= 0.1 results in unphysical")

    def test_calc_zeff(self, base_x_point):
        base_x_point.match_zeff(1.3)
        assert 1.3 == pytest.approx(base_x_point.calc_zeff())

    def test_setitems_zeff(self, base_x_point):
        base_x_point["Zeff"] = 1.1
        assert 1.1 == pytest.approx(base_x_point["Zeff"])

    def test_match_nustar(self, base_x_point):
        assert base_x_point["elec"]["n"] == 0.1
        assert base_x_point["geometry"]["q"] == 3.0
        assert base_x_point["geometry"]["Ro"] == 3
        assert base_x_point["geometry"]["Rmin"] == 1
        assert base_x_point["geometry"]["x"] == 0.45

        base_x_point.match_nustar(0.1)
        assert 0.91676417086826256 == pytest.approx(base_x_point["elec"]["T"])

    def test_calc_nustar(self, base_x_point):
        assert base_x_point["elec"]["n"] == 0.1
        assert base_x_point["geometry"]["q"] == 3.0
        assert base_x_point["geometry"]["Ro"] == 3
        assert base_x_point["geometry"]["Rmin"] == 1
        assert base_x_point["geometry"]["x"] == 0.45

        base_x_point.match_nustar(0.1)
        assert 0.1 == pytest.approx(base_x_point.calc_nustar())

    def test_setitems_nustar(self, base_x_point):
        assert base_x_point["elec"]["n"] == 0.1
        assert base_x_point["geometry"]["q"] == 3.0
        assert base_x_point["geometry"]["Ro"] == 3
        assert base_x_point["geometry"]["Rmin"] == 1
        assert base_x_point["geometry"]["x"] == 0.45

        base_x_point["Nustar"] = 0.1
        assert 0.1 == pytest.approx(base_x_point["Nustar"])
        assert 0.91676417086826256 == pytest.approx(base_x_point["elec"]["T"])

    def test_match_tite(self, base_x_point):
        assert base_x_point["elec"]["T"] == 8.0
        base_x_point.match_tite(0.1)

        assert base_x_point["ions"][0]["T"] == 0.8
        assert base_x_point["ions"][1]["T"] == 0.8
        assert base_x_point["ions"][2]["T"] == 0.8

    def test_calc_tite(self, base_x_point):
        TiTe = base_x_point.calc_tite()
        base_x_point["ions"][0]["T"] = 5
        with pytest.raises(Exception) as ex:
            base_x_point.calc_tite()
        assert str(ex.value) == "Ions have non-equal temperatures"

    def test_setitem_tite(self, base_x_point):
        assert base_x_point["elec"]["T"] == 8.0
        base_x_point["Ti_Te_rel"] = 0.1

        assert base_x_point["ions"][0]["T"] == 0.8
        assert base_x_point["ions"][1]["T"] == 0.8
        assert base_x_point["ions"][2]["T"] == 0.8

    def test_getitem_tite(self, base_x_point):
        assert base_x_point["elec"]["T"] == 8.0
        assert base_x_point["ions"]["T"] == 8.0

        assert base_x_point["Ti_Te_rel"] == 1

    @pytest.mark.skip("Removed from mainline for now")
    def test_equalize_gradient(self, base_x_point):
        base_x_point["ions"]["An"] = 12.0
        base_x_point["elec"]["An"] = 8.0
        base_x_point.equalize_gradient()
        assert base_x_point["ions"]["An"] == 8

    def test_match_epsilon(self, base_x_point):
        base_x_point["geometry"]["Rmin"] = 2.0
        assert base_x_point["geometry"]["Ro"] == 3.0
        assert base_x_point["geometry"]["Rmin"] == 2.0

        base_x_point.match_epsilon(0.5)
        assert base_x_point["geometry"]["x"], 0.75

    def test_setitem_epsilon(self, base_x_point):
        base_x_point["geometry"]["Rmin"] = 2.0
        assert base_x_point["geometry"]["Ro"] == 3.0
        assert base_x_point["geometry"]["Rmin"] == 2.0

        base_x_point["epsilon"] = 0.5
        assert base_x_point["geometry"]["x"] == 0.75

    def test_calc_epsilon(self, base_x_point):
        assert base_x_point["geometry"]["Ro"] == 3.0
        assert base_x_point["geometry"]["Rmin"] == 1.0
        assert base_x_point["geometry"]["x"] == 0.45

        assert base_x_point.calc_epsilon() == 0.15

    def test_getitem_epsilon(self, base_x_point):
        assert base_x_point["geometry"]["Ro"] == 3.0
        assert base_x_point["geometry"]["Rmin"] == 1.0
        assert base_x_point["geometry"]["x"] == 0.45

        assert base_x_point["epsilon"] == 0.15

    @pytest.mark.skip(reason="pending recheck of calc_rot method")
    def test_recalc_rot_zerorot(self, caplog, base_x_point):
        assert base_x_point["Machtor"] == 0
        assert base_x_point["Autor"] == 0
        assert base_x_point["gammaE"] == 0

        rec_rot_val, rec_rot_var = base_x_point.calc_rot()
        assert rec_rot_val is None
        assert rec_rot_var is None
        captured_tuple = caplog.record_tuples[0]
        assert (
            captured_tuple[2]
            == "Machtor, gammaE and Autor are zero! Skipping recalculation of rotation."
        )

    @pytest.mark.skip(reason="pending recheck of calc_rot method")
    def test_recalc_rot_missinggammaE(self, base_x_point):
        base_x_point["Machtor"] = 0.1
        base_x_point["Autor"] = 0.1
        base_x_point["gammaE"] = None
        assert base_x_point["Machtor"] is not None
        assert base_x_point["Autor"] is not None
        assert base_x_point["gammaE"] is None
        rec_rot_val, rec_rot_var = base_x_point.calc_rot()
        assert rec_rot_var == "gammaE"
        assert -0.006225305372078396 == pytest.approx(rec_rot_val)

    @pytest.mark.skip(reason="pending recheck of calc_rot method")
    def test_recalc_rot_missingMachtor(self, base_x_point):
        base_x_point["gammaE"] = 0.1
        base_x_point["Autor"] = 0.1
        base_x_point["Machtor"] = None
        assert base_x_point["Machtor"] is None
        assert base_x_point["Autor"] is not None
        assert base_x_point["gammaE"] is not None
        rec_rot_val, rec_rot_var = base_x_point.calc_rot()
        assert rec_rot_var == "Machtor"
        assert 128.04867895497355 == pytest.approx(rec_rot_val)

    @pytest.mark.skip(reason="pending recheck of calc_rot method")
    def test_recalc_rot_missingAutor(self, base_x_point):
        base_x_point["gammaE"] = 0.1
        base_x_point["Machtor"] = 0.1
        base_x_point["Autor"] = None
        assert base_x_point["Machtor"] is not None
        assert base_x_point["Autor"] is None
        assert base_x_point["gammaE"] is not None
        rec_rot_val, rec_rot_var = base_x_point.calc_rot()
        assert rec_rot_var == "Autor"
        assert -2.027160082376951 == pytest.approx(rec_rot_val)

    @pytest.mark.skip(reason="pending recheck of calc_rot method")
    def test_recalc_rot_recalc_all_nonzero(self, base_x_point):
        base_x_point["Machtor"] = 0.1
        base_x_point["Autor"] = 0.1
        base_x_point["gammaE"] = 0.1
        assert base_x_point["Machtor"] != 0
        assert base_x_point["Autor"] != 0
        assert base_x_point["gammaE"] != 0
        rec_rot_val, rec_rot_var = base_x_point.calc_rot()
        assert rec_rot_var == "gammaE"
        assert -0.006225305372078396 == pytest.approx(rec_rot_val)


@pytest.fixture
def plan_hyperedge(base_x_point):
    scan_dict = OrderedDict()
    keys = ["Ati", "Ate", "Ane", "q", "smag", "x", "Ti_Te_rel", "Zeff"]
    values = [
        np.linspace(0, 1, 1),
        np.linspace(0, 1, 2),
        np.linspace(0, 1, 3),
        np.linspace(0, 1, 4),
        np.linspace(0, 1, 5),
        np.linspace(0, 1, 6),
        np.linspace(0, 1, 7),
        np.linspace(1, 3, 8),
    ]
    for key, value in zip(keys, values):
        scan_dict[key] = value.tolist()

    qualikizplan = QuaLiKizPlan(
        scan_dict=scan_dict,
        scan_type="hyperedge",
        xpoint_base=base_x_point,
    )
    return qualikizplan


class TestQuaLiKizPlan_hyperedge:
    def test_initialize(self, plan_hyperedge):
        kthetarhos = plan_hyperedge["xpoint_base"]["special"]["kthetarhos"]
        assert len(kthetarhos) == 16

    def test_calculate_dimx(self, plan_hyperedge):
        assert plan_hyperedge.calculate_dimx() == 36

    def test_calculate_dimxn(self, plan_hyperedge):
        assert plan_hyperedge.calculate_dimxn() == 576

    def test_unknown_plan(self, plan_hyperedge):
        plan_hyperedge["scan_type"] = "test"
        with pytest.raises(Exception) as ex:
            plan_hyperedge.setup()
        assert str(ex.value).startswith("Unknown scan_type")
        with pytest.raises(Exception) as ex:
            plan_hyperedge.calculate_dimx()
        assert str(ex.value).startswith("Unknown scan_type")

    def test_setup(self, plan_hyperedge):
        scan_dict = OrderedDict(
            [("Ati", [0, 2, 4]), ("Ate", [1, 3, 5]), ("Ane", [6, 9, 12])]
        )
        plan_hyperedge["scan_dict"] = scan_dict
        byte_arrays, meta = plan_hyperedge.setup()
        expected = [
            0, 2, 4, 0, 0, 0, 0, 0, 0,
            0, 2, 4, 0, 0, 0, 0, 0, 0,
            0, 2, 4, 0, 0, 0, 0, 0, 0,
        ]
        assert byte_arrays["Ati"] == array.array("d", expected)
        expected = [
            1, 1, 1, 1, 3, 5, 1, 1, 1,
        ]
        assert byte_arrays["Ate"] == array.array("d", expected)
        expected = [
            6, 6, 6, 6, 6, 6, 6, 9, 12,
        ]
        assert byte_arrays["Ane"] == array.array("d", expected)


class TestQuaLiKizPlan_hyperedge_files:
    def test_to_json(self, tmpdir, plan_hyperedge):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_hyperedge.to_json(json_path)
            assert json_path.is_file()

    def test_from_json(self, tmpdir, plan_hyperedge):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_hyperedge.to_json(json_path)
            newplan = QuaLiKizPlan.from_json(json_path)
            assert id(plan_hyperedge) != id(newplan)
            assert plan_hyperedge == newplan


@pytest.fixture
def plan_hyperrect(plan_hyperedge):
    plan_hyperedge["scan_type"] = "hyperrect"
    return plan_hyperedge


class TestQuaLiKizPlan_hyperrect:
    def test_initialize(self, plan_hyperrect):
        kthetarhos = plan_hyperrect["xpoint_base"]["special"]["kthetarhos"]
        assert len(kthetarhos) == 16

    def test_calculate_dimx(self, plan_hyperrect):
        assert plan_hyperrect.calculate_dimx() == 40320

    def test_calculate_dimxn(self, plan_hyperrect):
        assert plan_hyperrect.calculate_dimxn() == 645120

    def test_setup(self, plan_hyperrect):
        scan_dict = OrderedDict(
            [("Ati", [0, 2, 4]), ("Ate", [1, 3, 5]), ("Ane", [6, 9, 12])]
        )
        plan_hyperrect["scan_dict"] = scan_dict
        byte_arrays, meta = plan_hyperrect.setup()
        expected = [
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            4, 4, 4, 4, 4, 4, 4, 4, 4,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            4, 4, 4, 4, 4, 4, 4, 4, 4,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            4, 4, 4, 4, 4, 4, 4, 4, 4,
        ]
        assert byte_arrays["Ati"] == array.array("d", expected)
        expected = [
            1, 1, 1, 3, 3, 3, 5, 5, 5,
            1, 1, 1, 3, 3, 3, 5, 5, 5,
            1, 1, 1, 3, 3, 3, 5, 5, 5,
        ]
        assert byte_arrays["Ate"] == array.array("d", expected)
        expected = [
            6, 9, 12, 6, 9, 12, 6, 9, 12,
            6, 9, 12, 6, 9, 12, 6, 9, 12,
            6, 9, 12, 6, 9, 12, 6, 9, 12,
        ]
        assert byte_arrays["Ane"] == array.array("d", expected)

    def test_nonscannable(self, plan_hyperrect):
        scan_dict = OrderedDict([("relacc2", [0.1, 0.01, 0.001])])
        plan_hyperrect["scan_dict"] = scan_dict
        with pytest.raises(Exception) as ex:
            plan_hyperrect.setup()
        assert "relacc2" in str(ex.value)
        assert "cannot be scanned" in str(ex.value)


class TestQuaLiKizPlan_hyperrect_files:
    def test_to_json(self, tmpdir, plan_hyperrect):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_hyperrect.to_json(json_path)
            assert json_path.is_file()

    def test_from_json(self, tmpdir, plan_hyperrect):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_hyperrect.to_json(json_path)
            newplan = QuaLiKizPlan.from_json(json_path)
            assert id(plan_hyperrect) != id(newplan)
            assert plan_hyperrect == newplan


@pytest.fixture
def plan_mixed(base_x_point):
    scan_dict = OrderedDict()
    scan_dict["smag"] = [0.5, 1.5, 2.5]
    scan_dict["q"] = [1.0, 2.0, 3.0]
    scan_dict["ne"] = [5.0, 3.0, 1.0]
    scan_dict["Bo"] = [1.0, 2.0, 3.0]
    mixed_dict = {
        "q": ["ne", "Bo"],
        "smag": None,
    }
    qualikizplan = QuaLiKizPlan(
        scan_dict=scan_dict,
        scan_type="mixed",
        mixed_dict=mixed_dict,
        xpoint_base=base_x_point,
    )
    return qualikizplan


class TestQuaLiKizPlan_mixed:
    def test_initialize(self, plan_mixed):
        kthetarhos = plan_mixed["xpoint_base"]["special"]["kthetarhos"]
        assert len(kthetarhos) == 16

    def test_calculate_dimx(self, plan_mixed):
        assert plan_mixed.calculate_dimx() == 9

    def test_calculate_dimxn(self, plan_mixed):
        assert plan_mixed.calculate_dimxn() == 144

    def test_setup(self, plan_mixed):
        scan_dict = OrderedDict(
            [("Ati", [0, 2, 4]), ("Ate", [1, 3, 5]), ("Ane", [6, 9, 12])]
        )
        mixed_dict = {"Ati": ["Ate"], "Ane": None}
        plan_mixed["scan_dict"] = scan_dict
        plan_mixed["mixed_dict"] = mixed_dict
        byte_arrays, meta = plan_mixed.setup()
        expected = [
            0, 0, 0, 2, 2, 2, 4, 4, 4,
            0, 0, 0, 2, 2, 2, 4, 4, 4,
            0, 0, 0, 2, 2, 2, 4, 4, 4,
        ]
        assert byte_arrays["Ati"] == array.array("d", expected)
        expected = [
            1, 1, 1, 3, 3, 3, 5, 5, 5,
        ]
        assert byte_arrays["Ate"] == array.array("d", expected)
        expected = [
            6, 9, 12, 6, 9, 12, 6, 9, 12,
        ]
        assert byte_arrays["Ane"] == array.array("d", expected)


class TestQuaLiKizPlan_mixed_files:
    def test_to_json(self, tmpdir, plan_mixed):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_mixed.to_json(json_path)
            assert json_path.is_file()

    def test_from_json(self, tmpdir, plan_mixed):
        with tmpdir.as_cwd():
            json_path = Path("test.json")
            plan_mixed.to_json(json_path)
            newplan = QuaLiKizPlan.from_json(json_path)
            assert id(plan_mixed) != id(newplan)
            assert plan_mixed == newplan


class TestQuaLiKizQLScan:
    def test_from_json(self, prepare_qlk_run):
        data = QuaLiKizQLFluxScan.from_json("impurities_qlflux.json")
        assert data["ion_index"] == 0
        assert data["Ai"] == [4, 14, 19]
        assert data["Zi"] == [2, 7, 9]

    def test_to_json(self, prepare_qlk_run):
        data = QuaLiKizQLFluxScan.from_json("impurities_qlflux.json")

        data.to_json("test.json")
        data2 = QuaLiKizQLFluxScan.from_json("impurities_qlflux.json")
        assert data == data2
