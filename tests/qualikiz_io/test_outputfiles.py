# pylint: disable=missing-module-docstring
import pytest  # pylint: disable=unused-import # noqa: F401
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.qualikiz_io.outputfiles import *  # pylint: disable=unused-wildcard-import, wildcard-import # noqa: F403, F401, E501
from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun
from pathlib import Path

class TestQuaLiKizFromDisk:
    def test_determine_sizes(self, prepare_qlk_run):
        sizes = determine_sizes(prepare_qlk_run)
        assert sizes == OrderedDict(
            [
                ("dimx", 2),
                ("dimn", 2),
                ("nions", 1),
                ("numsols", 3),
                ("phys_meth", 2),
                ("separateflux", 1),
                ("ntheta", 64),
                ("numecoefs", 13),
                ("numicoefs", 7),
            ]
        )

    def test_convert_debug(self, caplog, prepare_qlk_run):
        sizes = determine_sizes(prepare_qlk_run)
        with caplog.at_level(logging.WARNING):
            ds = convert_debug(sizes, prepare_qlk_run)
        assert len(caplog.messages) == 0

    def test_convert_output(self, caplog, prepare_qlk_run):
        sizes = determine_sizes(prepare_qlk_run)
        ds = convert_debug(sizes, prepare_qlk_run)
        with caplog.at_level(logging.WARNING):
            ds = convert_output(ds, sizes, prepare_qlk_run)
        assert len(caplog.messages) == 0

    def test_convert_primitive(self, caplog, prepare_qlk_run):
        sizes = determine_sizes(prepare_qlk_run)
        ds = convert_debug(sizes, prepare_qlk_run)
        ds = convert_output(ds, sizes, prepare_qlk_run)
        with caplog.at_level(logging.WARNING):
            ds = convert_primitive(ds, sizes, prepare_qlk_run)
        assert len(caplog.messages) == 0


class TestToNetCDF4:
    def test_qualikiz_folder_to_xarray(self, prepare_qlk_run):
        run = QuaLiKizRun.from_dir(prepare_qlk_run)
        run_name = prepare_qlk_run.name
        cwd = Path.cwd()
        ds = qualikiz_folder_to_xarray(cwd)
        check_missing_debug(run, ds)
        # ds = run_to_netcdf(prepare_qlk_run)


def dbgfiles(run):
    dbgdir = run.rundir / run.debugdir
    stems = [file.stem for file in dbgdir.iterdir()]
    return stems


def check_missing_debug(run, ds):
    # compile info is read in attrs
    # size_names are saved implicitly
    not_in_ds = (
        set(dbgfiles(run)) - set(ds.coords) - set(["compile_info"]) - set(size_names)
    )
    not_in_dbgfiles = (
        set(ds.coords)
        - set(dbgfiles(run))
        - set(size_names)
        - set(["ecoefs", "ntheta", "numicoefs"])
    )
    assert len(not_in_ds) == 0, f"{not_in_ds} found in debug folder, but not in ds!"
    assert (
        len(not_in_dbgfiles) == 0
    ), f"{not_in_dbgfiles} found in ds folder, but not in debug folder!"


class TestOrthogonalize:
    def test_squeeze_coords(self, regression_qlk_multibatch):
        pass

    def test_add_dims(self):
        pass

    def test_find_nonmatching_coords(self, regression_qlk_multibatch):
        pass
        # embed()

    def test_merge_orthogonal(self):
        pass

    def test_merge_many_orthogonal(self):
        pass
