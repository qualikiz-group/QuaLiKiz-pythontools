# pylint: disable=missing-function-docstring,missing-class-docstring,missing-module-docstring # noqa: E501
import shutil
from pathlib import Path
from subprocess import check_output, PIPE
import sys

from IPython import embed  # pylint: disable=unused-import # noqa: F401
import pytest
import xarray as xr


from qualikiz_tools.qualikiz_io.outputfiles import load_file
from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizRun, QuaLiKizBatch
from qualikiz_tools.qualikiz_io.compare_versions import *  # pylint: disable=wildcard-import, unused-wildcard-import # noqa: F403, E501

this_file = Path(__file__).resolve()
datadir = (this_file / "../../testdata").resolve()


def test_rel_difference():
    ds = xr.Dataset(data_vars={"y": ("dimx", [0.95, 1.05])})
    ds_ref = xr.Dataset(data_vars={"y": ("dimx", [1, 1])})
    diff = rel_difference(ds, ds_ref)
    assert diff == [0.05, 0.05]

    # The rel_difference is not symmetric, so only the same with .5% or so
    diff = rel_difference(ds, ds_ref)
    assert [0.05, 0.05] == pytest.approx(diff["y"].values, abs=0.005)


def test_collect_reference_run(prepare_qlk_run):
    run_name = prepare_qlk_run.name
    run = QuaLiKizRun.from_dir(prepare_qlk_run)
    tmpdir = Path("tmp")

    collect_reference_run(prepare_qlk_run, tmpdir)
    # We assume if the file is there, the copy worked
    # Check if some special cases and the first and last output file are there

    assert not (tmpdir / "runcounter.dat").exists()  # Should not be shared

    assert (tmpdir / "debug/dimx.dat").exists()  # First QLK debug file
    assert (tmpdir / "debug/rhomax.dat").exists()  # Last QLK debug file

    assert (tmpdir / "debug/phys_meth.dat").exists()
    phys_meth = load_file(tmpdir, "debug", "phys_meth")
    assert phys_meth == 2
    assert (tmpdir / "debug/separateflux.dat").exists()
    sepflux = load_file(tmpdir, "debug", "separateflux")
    assert sepflux == 1
    # As phys_meth is 2 and seplufux is 1, all variables we possibly want
    # to share should be copied to tmp

    # Of primitives, output_primi_meth_0 writes called first in outputascii
    assert not (tmpdir / "output/primitive/phys_meth.dat").exists()
    # Of primitives, output_primi_meth_2 is called last in outputascii
    assert not (tmpdir / "output/primitive/Lepiegci.dat").exists()

    # Check if the four special primitives *modeshift.dat and *modewidth.dat are there
    for prim in [
        "imodeshift.dat",
        "rmodeshift.dat",
        "imodewidth.dat",
        "rmodewidth.dat",
    ]:
        assert (tmpdir / "output/primitive" / prim).exists()

    # Check if the special primitive cftrans is there
    # assert (tmpdir / "output/primitives/cftrans.dat").exists()

    # Of outputs, output_output_meth_0 is called first in outputascii
    assert (tmpdir / "output/pfe_cm.dat").exists()
    # Of outputs, output_output_meth_2_sep_1 is called last in outputascii
    assert (tmpdir / "output/veceITG_GB.dat").exists()

    # Check if the special compile_info file is copied over
    assert (tmpdir / "debug/compile_info.csv").exists()


@pytest.fixture(params=["full_flags"])
def datasets(request, get_reference_dataset):
    case_name = request.param
    ds = get_reference_dataset
    ds_ref = ds.copy(deep=True)
    dss = {"ref": ds_ref, "other": ds}
    return case_name, dss


class TestProcessCase:
    pytestmark = [pytest.mark.need_reffiles]

    def test_basic(self, datasets):
        case_name, dss = datasets
        bin_equiv, num_equiv = process_case(case_name, dss, "ref")
        assert bin_equiv[("ref", "other")] is True
        assert num_equiv[("ref", "other")] is True

    def test_pdf(self, datasets, tmpdir, capsys):
        # Change one "fluxlike" variable, should produce a plot
        case_name, dss = datasets
        dss["other"]["efe_GB"] = 0.95 * dss["other"]["efe_GB"]

        with tmpdir.as_cwd():
            bin_equiv, num_equiv = process_case(
                case_name, dss, "ref", equiv_rel_diff_bound=1e-2, save_pdf=True
            )
            captured = capsys.readouterr()
            assert "is not numerically equivalent" in captured.out
            assert "efe_GB" in captured.out
            assert bin_equiv[("ref", "other")] is False
            assert num_equiv[("ref", "other")] is False
            pdf_file = Path(case_name + ".pdf")
            assert pdf_file.exists()

    def test_binary(self, datasets, tmpdir, capsys):
        # Change binary change a variable
        case_name, dss = datasets
        dss["other"]["efe_GB"] = dss["other"]["efe_GB"] + sys.float_info.epsilon

        with tmpdir.as_cwd():
            bin_equiv, num_equiv = process_case(
                case_name, dss, "ref", equiv_rel_diff_bound=1e-2
            )
            captured = capsys.readouterr()
            assert (
                "other has non-binary equivalent variables ['efe_GB']" in captured.out
            )
            assert bin_equiv[("ref", "other")] is False
            assert num_equiv[("ref", "other")] is True


    def test_non_physics(self, datasets, tmpdir, capsys):
        case_name, dss = datasets
        dss["other"]["efe_GB"] = dss["other"]["efe_GB"] + sys.float_info.epsilon
        assert dss["other"]["simple_mpi_only"] == 1
        dss["other"]["simple_mpi_only"] = 0

        with tmpdir.as_cwd():
            bin_equiv, num_equiv = process_case(
                case_name, dss, "ref", equiv_rel_diff_bound=1e-2
            )
            captured = capsys.readouterr()
            assert (
                "other has non-binary equivalent variables ['efe_GB']" in captured.out
            )
            assert bin_equiv[("ref", "other")] is False
            assert num_equiv[("ref", "other")] is True
