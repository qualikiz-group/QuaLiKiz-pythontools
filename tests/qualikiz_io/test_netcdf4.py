# pylint: disable=no-self-use,missing-function-docstring,missing-class-docstring,missing-module-docstring # noqa

import pytest  # pylint: disable=unused-import # noqa: F401
from IPython import embed  # pylint: disable=unused-import # noqa: F401

# netCDF4 = pytest.importorskip("netCDF4")

from qualikiz_tools.qualikiz_io.outputfiles import _possible_dims
from qualikiz_tools.qualikiz_io.netcdf4 import *  # pylint: disable=wildcard-import, unused-wildcard-import # noqa: F403, E501


def test_get_variables_first(qlk_batch_netcdf_path):
    ds_nc = Dataset(qlk_batch_netcdf_path)
    var = get_variables(ds_nc, var_return_mode="first", raise_on_missing_dim=True)
    missing_dims = set(_possible_dims) - set(var.keys())
    assert len(missing_dims) == 0
    for kk, vv in var.items():
        assert len(vv) == 1


def test_get_variables_all(qlk_batch_netcdf_path):
    ds_nc = Dataset(qlk_batch_netcdf_path)
    var = get_variables(ds_nc, var_return_mode="all", raise_on_missing_dim=True)
    missing_dims = set(_possible_dims) - set(var.keys())
    assert len(missing_dims) == 0
    for kk, vv in var.items():
        assert len(vv) >= 1


def test_get_chunks(qlk_batch_netcdf_path):
    ds_nc = Dataset(qlk_batch_netcdf_path)
    var = get_chunks(ds_nc, raise_on_missing_dim=True)
    missing_dims = set(_possible_dims) - set(var.keys())
    assert len(missing_dims) == 0


def test_get_shapes(qlk_batch_netcdf_path):
    ds_nc = Dataset(qlk_batch_netcdf_path)
    var = get_shapes(ds_nc, raise_on_missing_dim=True)
    missing_dims = set(_possible_dims) - set(var.keys())
    assert len(missing_dims) == 0


def test_get_bytes(qlk_batch_netcdf_path):
    ds_nc = Dataset(qlk_batch_netcdf_path)
    var = get_bytes(ds_nc, raise_on_missing_dim=True)
    missing_dims = set(_possible_dims) - set(var.keys())
    assert len(missing_dims) == 0


def test_get_info_first(qlk_batch_netcdf_path):
    var = get_info(
        qlk_batch_netcdf_path, var_return_mode="first", raise_on_missing_dim=True
    )
    assert len(var) == 4


def test_get_info_all(qlk_batch_netcdf_path):
    var = get_info(
        qlk_batch_netcdf_path, var_return_mode="all", raise_on_missing_dim=True
    )
    assert len(var) == 4
