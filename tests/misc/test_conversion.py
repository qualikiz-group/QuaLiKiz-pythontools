import unittest
from unittest import TestCase, skip
import copy
import os

from IPython import embed  # pylint: disable=unused-import # noqa: F401
import pytest
from numpy.testing import assert_almost_equal

from qualikiz_tools.misc.conversion import *


class TestZeff(TestCase):
    def test_calc_zeff(self):
        ions = [
            {"n": 0.94, "Z": 1},
            {"n": 0.01, "Z": 6},
        ]
        zeff = calc_zeff(ions)
        self.assertAlmostEqual(zeff, 1.3)

        ions = [
            {"n": 0.9, "Z": 1},
            {"n": 0.1, "Z": 6},
        ]
        zeff = calc_zeff(ions)
        self.assertAlmostEqual(zeff, 4.5)


class TestNustar(TestCase):
    def test_calc_c2(self):
        ne = 0.1
        c2 = calc_c2(ne)
        self.assertAlmostEqual(c2, 17.502585093)

    def test_calc_c1(self):
        ne = 0.1
        q = 3
        Ro = 3
        Rmin = 1
        x = 0.45
        zeff = 4.5
        c1 = calc_c1(zeff, ne, q, Ro, Rmin, x)
        self.assertAlmostEqual(c1, 0.00482586118484152)

    def test_calc_nustar_from_c1_c2(self):
        ne = 0.1
        q = 3
        Ro = 3
        Rmin = 1
        x = 0.45
        Te = 0.91676417086826256
        zeff = 4.5
        c1 = calc_c1(zeff, ne, q, Ro, Rmin, x)
        c2 = calc_c2(ne)
        nustar_calc = calc_nustar_from_c1_c2(c1, c2, Te)
        self.assertAlmostEqual(nustar_calc, 0.1)

    def test_calc_nustar_from_parts(self):
        ne = 0.1
        q = 3
        Ro = 3
        Rmin = 1
        x = 0.45
        Te = 0.91676417086826256
        Te = 0.9167642
        zeff = 4.5
        nustar_calc = calc_nustar_from_parts(zeff, ne, Te, q, Ro, Rmin, x)
        self.assertAlmostEqual(nustar_calc, 0.9993235e-001, places=3)
        nustar_calc2 = calc_nustar_from_parts_qlkstyle(zeff, ne, Te, q, Ro, Rmin, x)
        self.assertAlmostEqual(nustar_calc2, 0.9993235e-001)

        ne = 5
        q = 0.66
        Ro = 3
        Rmin = 1
        x = 0.33
        Te = 7.488360
        zeff = 1.7
        nustar_calc = calc_nustar_from_parts(zeff, ne, Te, q, Ro, Rmin, x)
        self.assertAlmostEqual(nustar_calc, 0.9993257e-002, places=4)
        nustar_calc2 = calc_nustar_from_parts_qlkstyle(zeff, ne, Te, q, Ro, Rmin, x)
        self.assertAlmostEqual(nustar_calc2, 0.9993257e-002)

    def test_calc_nustar_from_parts_array(self):
        ne = np.array([0.1, 5])
        q = np.array([3, 0.66])
        Ro = np.array([3, 3])
        Rmin = np.array([1, 1])
        x = np.array([0.45, 0.33])
        Te = np.array([0.91676417086826256, 7.4883686])
        zeff = np.array([4.5, 1.7])
        nustar_calc = calc_nustar_from_parts(zeff, ne, Te, q, Ro, Rmin, x)

    def test_calc_te_from_nustar(self):
        ne = 0.1
        q = 3
        Ro = 3
        Rmin = 1
        x = 0.45
        nustar = 0.1
        zeff = 4.5
        te_calc = calc_te_from_nustar(zeff, ne, nustar, q, Ro, Rmin, x)
        assert_almost_equal(te_calc, 0.91676417086826256)

        ne = 5
        q = 0.66
        Ro = 3
        Rmin = 1
        x = 0.33
        nustar = 0.01
        zeff = 1.7
        te_calc = calc_te_from_nustar(zeff, ne, nustar, q, Ro, Rmin, x)
        assert_almost_equal(te_calc, 7.4883686)

    def test_calc_nustar_from_parts_array(self):
        ne = np.array([0.1, 5])
        q = np.array([3, 0.66])
        Ro = np.array([3, 3])
        Rmin = np.array([1, 1])
        x = np.array([0.45, 0.33])
        nustar = np.array([0.1, 0.01])
        zeff = np.array([4.5, 1.7])
        te_calc = calc_te_from_nustar(zeff, ne, nustar, q, Ro, Rmin, x)
        te = np.array([0.91676417086826256, 7.4883686])
        assert_almost_equal(te_calc, te)


class TestPuretor(TestCase):
    def test_calc_puretor_Machpar_from_Machtor(self):
        eps = 0.28
        q = 0.66
        Machtor = 0.67
        Machpar = calc_puretor_Machpar_from_Machtor(Machtor, eps, q)
        self.assertAlmostEqual(Machpar, 0.616789793856422)

    def test_calc_puretor_Autor_from_gammaE(self):
        eps = 0.28
        q = 0.66
        gammaE = -0.7
        Autor = calc_puretor_Autor_from_gammaE(gammaE, eps, q)
        self.assertAlmostEqual(Autor, 1.792344832893492)

    def test_calc_puretor_Aupar_from_Autor(self):
        eps = 0.28
        q = 0.66
        Autor = 5
        Aupar = calc_puretor_Aupar_from_Autor(Autor, eps, q)
        self.assertAlmostEqual(Aupar, 4.602908909376283)

    def test_calc_puretor_gammaE_from_Autor(self):
        eps = 0.28
        q = 0.66
        Autor = 5
        gammaE = calc_puretor_gammaE_from_Autor(Autor, eps, q)
        self.assertAlmostEqual(gammaE, -1.9527492342808472)

    def test_calc_puretor_absolute_Machtor(self):
        eps = 0.28
        q = 0.66
        Machtor = 0.67
        [Machtor_calc, Machpar_calc] = calc_puretor_absolute(eps, q, Machtor=Machtor)
        self.assertAlmostEqual(Machpar_calc, 0.616789793856422)
        self.assertEqual(Machtor_calc, Machtor)

    def test_calc_puretor_absolute_Machpar(self):
        eps = 0.28
        q = 0.66
        Machpar = 0.616789793856422
        [Machtor_calc, Machpar_calc] = calc_puretor_absolute(eps, q, Machpar=Machpar)
        self.assertAlmostEqual(Machtor_calc, 0.67)
        self.assertEqual(Machpar_calc, Machpar)

    def test_calc_puretor_absolute_none(self):
        eps = 0.28
        q = 0.66
        self.assertRaises(ValueError, calc_puretor_absolute, eps, q)

    def test_calc_puretor_absolute_two(self):
        eps = 0.28
        q = 0.66
        Machtor = 0.67
        Machpar = 0.616789793856422
        self.assertRaises(
            ValueError, calc_puretor_absolute, eps, q, Machtor=Machtor, Machpar=Machpar
        )

    def test_calc_puretor_gradient_Autor(self):
        eps = 0.28
        q = 0.66
        Autor = 5
        [Aupar_calc, Autor_calc, gammaE_calc] = calc_puretor_gradient(
            eps, q, Autor=Autor
        )
        self.assertAlmostEqual(gammaE_calc, -1.9527492342808472)
        self.assertAlmostEqual(Aupar_calc, 4.602908909376283)
        self.assertEqual(Autor_calc, Autor)

    def test_calc_puretor_gradient_Aupar(self):
        eps = 0.28
        q = 0.66
        Aupar = 4.602908909376283
        [Aupar_calc, Autor_calc, gammaE_calc] = calc_puretor_gradient(
            eps, q, Aupar=Aupar
        )
        self.assertAlmostEqual(Autor_calc, 5)
        self.assertAlmostEqual(gammaE_calc, -1.9527492342808472)
        self.assertEqual(Aupar_calc, Aupar)

    def test_calc_puretor_gradient_gammaE(self):
        eps = 0.28
        q = 0.66
        gammaE = -2.121212121212121
        [Aupar_calc, Autor_calc, gammaE_calc] = calc_puretor_gradient(
            eps, q, gammaE=gammaE
        )
        self.assertAlmostEqual(Autor_calc, 5.431347978465127)
        self.assertAlmostEqual(Aupar_calc, 4.999999999999999)
        self.assertEqual(gammaE_calc, gammaE)

    def test_calc_puretor_gradient_none(self):
        eps = 0.28
        q = 0.66
        self.assertRaises(ValueError, calc_puretor_absolute, eps, q)

    def test_calc_puretor_gradient_two(self):
        eps = 0.28
        q = 0.66
        Autor = 5
        Aupar = 4.602908909376283
        self.assertRaises(
            ValueError, calc_puretor_gradient, eps, q, Aupar=Autor, Autor=Autor
        )

    def test_calc_puretor_gradient_three(self):
        eps = 0.28
        q = 0.66
        Autor = 5
        Aupar = 4.602908909376283
        gammaE = -2.121212121212121
        self.assertRaises(
            ValueError,
            calc_puretor_gradient,
            eps,
            q,
            Aupar=Autor,
            Autor=Autor,
            gammaE=gammaE,
        )


class TestGyroBohmScaling:
    # Parameters based on example JSON with different Ti/Te
    qe = 1.602176565e-19  # C
    mp = 1.672621777e-27  # kg
    Ai0 = 2  # amu
    Te = 8  # keV
    Bo = 3  # T
    Rmin = 1  # m
    Ro = 3  # m
    normni = 0.6  # ni / ne
    ne = 5  # 10^19 m^-3
    ns = normni * ne  # 10^19 m^-3
    Ts = 9  # keV
    As = 2  # Amu
    ms = As * mp  # kg

    def test_calc_efe_fac(self):
        fac = calc_ef_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ne, self.Te)
        fac_qlk = 0.9495038 / 699066.1  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_efi_fac(self):
        fac = calc_ef_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ns, self.Ts)
        fac_qlk = 0.4083720 / 202946.4  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_pfe_fac(self):
        fac = calc_pf_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ne)
        fac_qlk = 0.3118541 / 0.1791318e021  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_pfi_fac(self):
        fac = calc_pf_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ns)
        fac_qlk = 0.2105131 / 0.7255235e020  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_vce_fac(self):
        fac = calc_vx_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ne)
        fac_qlk = -0.3636184 / -4.177312  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_vci_fac(self):
        fac = calc_vx_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ns)
        fac_qlk = 0.2626748e-002 / 0.3017655e-001  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_vece_fac(self):
        fac = calc_vex_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ne)
        fac_qlk = -0.6107052 / -7.015890  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_veci_fac(self):
        fac = calc_vex_fac(self.Ai0, self.Te, self.Bo, self.Rmin, self.ns)
        fac_qlk = -0.2448951e-002 / -0.2813398e-001  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_chiee_fac(self):
        fac = calc_chie_fac(self.Ai0, self.Te, self.Bo, self.Rmin)
        fac_qlk = 0.2335897 / 2.683521  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_chiei_fac(self):
        fac = calc_chie_fac(self.Ai0, self.Te, self.Bo, self.Rmin)
        fac_qlk = -0.2100565e-002 / -0.2413167e-001  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    # Now a few tests with GASTD-like rotation
    def test_calc_dfe_fac(self):
        fac = calc_df_fac(self.Ai0, self.Te, self.Bo, self.Rmin)
        fac_qlk = 1.905202 / 21.88730  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_dfi_fac(self):
        fac = calc_df_fac(self.Ai0, self.Te, self.Bo, self.Rmin)
        fac_qlk = 0.3240568 / 3.722822  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_vfi_fac(self):
        fac = calc_vf_fac(
            self.Ai0, self.Te, self.Bo, self.Rmin, self.ns, self.Ts, self.Ro, self.ms
        )
        fac_qlk = 0.3036090e-001 / 0.9750203e-001  # GB / SI 2nd point
        assert fac == pytest.approx(fac_qlk)

    def test_calc_gam_fac(self):
        fac = calc_gam_fac(self.Ai0, self.Te, self.Rmin)
        fac_qlk = 0.2210739e-001 / 13684.34  # GB / SI 1st row, 2nd column
        assert fac == pytest.approx(fac_qlk)

    def test_calc_ome_fac(self):
        fac = calc_ome_fac(self.Ai0, self.Te, self.Rmin)
        fac_qlk = 0.1706970 / 105660.4  # GB / SI 1st row, 2nd column
        assert fac == pytest.approx(fac_qlk)


class TestQuasiNeutrality:
    def test_calc_qn(self):
        normnis = [0.5, 0.125]  # QLK units (ni/ne)
        Zis = [1, 4]  # Charge number
        qn = calc_qn(normnis, Zis)
        assert qn == 0


class TestDilution:
    def test_calc_dilution(self):
        normnis = [0.2, 0.6 / 4]  # QLK units (ni/ne)
        Zis = [2, 4]  # Charge number
        dil = calc_dilution(normnis, Zis)
        assert dil == 0.4
