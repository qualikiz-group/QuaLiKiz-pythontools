from IPython import embed  # pylint: disable=unused-import # noqa: F401
import pytest

from qualikiz_tools.misc.fs_manipulation import *


def test_is_qlk_netcdf(qlk_batch_netcdf_path):
    assert isinstance(qlk_batch_netcdf_path, Path)
    assert is_qlk_netcdf(qlk_batch_netcdf_path)


def test_is_not_qlk_netcdf(tmp_path):
    test_dir = tmp_path / "test"
    test_dir.mkdir()
    assert is_qlk_netcdf(test_dir) is False
    test_file = tmp_path / "test.nc"
    test_file.touch()
    assert is_qlk_netcdf(test_file) is False
