from subprocess import check_output, PIPE
from unittest import TestCase, skip
import os
import shutil
from pathlib import Path
import filecmp

from IPython import embed
import pytest

this_file = Path(__file__).resolve()


def test_returns_usage_information():
    cmd = "qualikiz_tools output -h"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")

    cmd = "qualikiz_tools output --help"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")

    cmd = "qualikiz_tools output help"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")


##############################
# to_netcdf subcommand tests #
##############################
def test_to_netcdf_batch(run_in_shell_wrapper, prepare_qlk_batch):
    cmd = "qualikiz_tools output to_netcdf --only-runs ."
    cwd = Path.cwd()
    run_in_shell_wrapper(cmd)


def test_to_netcdf_run(run_in_shell_wrapper, prepare_qlk_run):
    cmd = "qualikiz_tools output to_netcdf --only-runs ."
    cwd = Path.cwd()
    run_in_shell_wrapper(cmd)


#######################################
# extract_regression subcommand tests #
#######################################


def is_regression_complete(orig_path, regress_path):
    """Check if the regression case is fully finished."""
    print("Testing {!s} subfolders".format(regress_path))
    orig_path = Path(orig_path)
    regress_path = Path(regress_path)

    assert orig_path.is_dir()
    assert regress_path.is_dir()

    # Check output
    regress_out = regress_path / "output"
    assert regress_out.is_dir()
    # Last output from output_ascii is output_output_meth_2_sep_1
    # Last output from output_output_meth_2_sep_1 is veceITG

    ref_out_filename = "veceITG_GB.dat"
    print("Testing {!s}".format(ref_out_filename))
    orig_file = orig_path / "output" / ref_out_filename
    regress_file = regress_out / ref_out_filename
    assert orig_file.is_file()
    assert regress_file.is_file()
    assert filecmp.cmp(orig_file, regress_file)

    # Check the four important primives
    primitives = [
        "imodeshift.dat",
        "imodewidth.dat",
        "rmodeshift.dat",
        "rmodewidth.dat",
    ]
    regress_prim = regress_out / "primitive"
    orig_prim = regress_out / "primitive"
    for prim in primitives:
        print("Testing {!s}".format(prim))
        regress_file = regress_prim / prim
        orig_file = orig_prim / prim
        assert orig_file.is_file()
        assert regress_file.is_file()
        assert filecmp.cmp(orig_file, regress_file)


def test_extract_regression_batch(run_in_shell_wrapper, prepare_qlk_batch):
    # qualikiz_tools output [-v | -vv] extract_regression [--overwrite] <target_path> [<regression_path>]
    extract_to_dir = Path("this_regression")
    cwd = Path.cwd()
    cmd = "qualikiz_tools output extract_regression {!s} {!s}".format(
        cwd, extract_to_dir
    )
    run_in_shell_wrapper(cmd)
    extract_result_dir = extract_to_dir
    is_regression_complete(prepare_qlk_batch, extract_result_dir)


def test_extract_regression_run(run_in_shell_wrapper, prepare_qlk_run):
    # qualikiz_tools output [-v | -vv] extract_regression [--overwrite] <target_path> [<regression_path>]
    extract_to_dir = Path("this_regression")
    cwd = Path.cwd()
    cmd = "qualikiz_tools output extract_regression {!s} {!s}".format(
        cwd, extract_to_dir
    )
    run_in_shell_wrapper(cmd)
    extract_result_dir = extract_to_dir
    is_regression_complete(prepare_qlk_run, extract_result_dir)


#################################
# compare_runs subcommand tests #
#################################


def test_compare_runs_batch(run_in_shell_wrapper, regression_qlk_batch):
    # qualikiz_tools output [-v | -vv] [--save-pdf] [--show-plot] compare_runs <target_path> <other_paths>...
    cwd = Path.cwd()
    cmd = "qualikiz_tools output compare_runs {!s} {!s}".format(
        cwd, regression_qlk_batch[1]
    )
    run_in_shell_wrapper(cmd)
