import collections
import os
import shutil
from pathlib import Path
from pathlib import Path as LocalPath
from subprocess import check_output, PIPE
from subprocess import CalledProcessError
from unittest import TestCase, skip

from IPython import embed
import pytest

this_file = Path(__file__).resolve()


def test_returns_usage_information():
    cmd = "qualikiz_tools plot -h"
    output = check_output(cmd, stderr=PIPE, shell=True, text=True)
    assert output.startswith("usage: qualikiz_tools plot")

    cmd = "qualikiz_tools plot --help"
    output = check_output(cmd, stderr=PIPE, shell=True, text=True)
    assert output.startswith("usage: qualikiz_tools plot")

    # TODO: Implement sub-subcommand
    # cmd = "qualikiz_tools plot help"
    # output = check_output(cmd, stderr=PIPE, shell=True)
    # assert output.startswith(b"Usage:")


def test_returns_multiple_lines():
    cmd = "qualikiz_tools plot --help"
    output = check_output(cmd, stderr=PIPE, shell=True, text=True)
    parsed_output = output.split("\n")
    assert len(parsed_output) > 1

    # TODO: Implement sub-subcommand
    # cmd = "qualikiz_tools plot help"
    # output = check_output(cmd, stderr=PIPE, shell=True)
    # assert len(output.decode("UTF-8").split("\n")) > 1


class TestFluxlikePlottingCLI:
    class TestHeadlessPlotting:
        def test_help(self, default_plotcase, run_in_shell_wrapper):
            cmd = "qualikiz_tools plot fluxlike --help"
            with default_plotcase.tmpdir.as_cwd():
                output = run_in_shell_wrapper(cmd)
            assert output.startswith(b"usage: qualikiz_tools plot fluxlike [-h]")

        def test_empty_runlist(self, default_plotcase, run_in_shell_wrapper):
            cmd = "qualikiz_tools plot --outname='test' fluxlike'"
            with default_plotcase.tmpdir.as_cwd():
                with pytest.raises(CalledProcessError):
                    output = run_in_shell_wrapper(cmd)

        def test_one_runlist(self, default_plotcase, run_in_shell_wrapper):
            data_path = default_plotcase.data_paths[1]
            outname = "test"
            cmd = f"qualikiz_tools plot --outname='{outname}' fluxlike {data_path}"
            with default_plotcase.tmpdir.as_cwd():
                output = run_in_shell_wrapper(cmd)
                outfile = default_plotcase.tmpdir.join(f"fig1-{outname}.png")
                assert outfile.exists()

        def test_one_netcdf(self, default_plotcase, run_in_shell_wrapper):
            data_path = default_plotcase.data_paths[1]
            netcdf_path = data_path / data_path.basename + ".nc"
            outname = "test"
            with default_plotcase.tmpdir.as_cwd():
                # Create to-be-plotted output file to netCDF
                cmd = f"qualikiz_tools output to_netcdf {data_path}"
                output = run_in_shell_wrapper(cmd)
                modification_time = netcdf_path.stat().mtime
                # Plot created netCDF file
                cmd = (
                    f"qualikiz_tools plot --outname='{outname}' fluxlike {netcdf_path}"
                )
                output = run_in_shell_wrapper(cmd)
                outfile = default_plotcase.tmpdir.join(f"fig1-{outname}.png")
                assert outfile.exists()
                assert (
                    modification_time == netcdf_path.stat().mtime
                ), "netCDF got re-created!"

        def test_slice_Ate_one_netcdf(
            self, regression_qlk_multibatch, run_in_shell_wrapper
        ):
            data_path = (
                regression_qlk_multibatch
                / "Nustar_0.001/smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.20/q_2.00/"
            )
            netcdf_path = data_path / f"{data_path.name}.nc"
            outname = "test"
            myslice = '{"Ate": 7.5}'

            os.chdir(data_path)
            # Create to-be-plotted output file to netCDF
            cmd = f"qualikiz_tools output to_netcdf {data_path}"
            output = run_in_shell_wrapper(cmd)
            modification_time = netcdf_path.stat().st_mtime
            # Plot created netCDF file
            cmd = f"qualikiz_tools plot --myslice='{myslice}' --outname='{outname}' fluxlike {netcdf_path}"
            output = run_in_shell_wrapper(cmd)
            outfile = data_path / f"fig1-{outname}.png"
            assert outfile.exists()
            assert (
                modification_time == netcdf_path.stat().st_mtime
            ), "netCDF got re-created!"

        def test_slice_Ati_one_netcdf(
            self,
            regression_qlk_multibatch,
            run_in_shell_wrapper,
        ):
            data_path = (
                regression_qlk_multibatch
                / "Nustar_0.001/smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.20/q_2.00/"
            )
            netcdf_path = data_path / f"{data_path.name}.nc"
            outname = "test"
            myslice = '{"Ati": 10}'

            os.chdir(data_path)
            # Create to-be-plotted output file to netCDF
            cmd = f"qualikiz_tools output to_netcdf {data_path}"
            output = run_in_shell_wrapper(cmd)
            modification_time = netcdf_path.stat().st_mtime
            # Plot created netCDF file
            cmd = f"qualikiz_tools plot --myslice='{myslice}' --outname='{outname}' fluxlike {netcdf_path}"
            try:
                output = run_in_shell_wrapper(cmd)
            except CalledProcessError as cpe:
                assert (
                    "Slice dimension does not depend on only 'dimx'"
                    in cpe.stderr.decode()
                )
            except Exception as ee:
                raise Exception(
                    f"Wrong Exception raised by cmd '{cmd}', should be CalledProcessError, was {type(ee)}"
                ) from ee

        def test_slice_Ati0_one_netcdf(
            self, regression_qlk_multibatch, run_in_shell_wrapper
        ):
            data_path = (
                regression_qlk_multibatch
                / "Nustar_0.001/smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.20/q_2.00/"
            )
            netcdf_path = data_path / f"{data_path.name}.nc"
            outname = "test"
            myslice = '{"Ati1": 10}'

            os.chdir(data_path)
            # Create to-be-plotted output file to netCDF
            cmd = f"qualikiz_tools output to_netcdf {data_path}"
            output = run_in_shell_wrapper(cmd)
            modification_time = netcdf_path.stat().st_mtime
            # Plot created netCDF file
            cmd = f"qualikiz_tools plot --myslice='{myslice}' --outname='{outname}' fluxlike {netcdf_path}"
            output = run_in_shell_wrapper(cmd)
            outfile = data_path / f"fig1-{outname}.png"
            assert outfile.exists()
            assert (
                modification_time == netcdf_path.stat().st_mtime
            ), "netCDF got re-created!"
