import shutil
import subprocess
import json

import pytest
from docopt import docopt
from numpy.testing import assert_allclose


@pytest.fixture
def default_rundir(request, tmpdir):
    test_file = request.fspath
    test_root = test_file.join("../../../")
    parameters_file = test_root.join(
        "tests/commands/parameters_dimensionality_check.json"
    )
    with tmpdir.as_cwd():
        shutil.copy(str(parameters_file), str(tmpdir.join("parameters.json")))
    return tmpdir


class TestInputPhysicalization:
    def test_default_train(request, default_rundir):
        with default_rundir.as_cwd():
            print("Running in tmpdir {!s}".format(default_rundir))
            physical_filename = "param_physical.json"
            roundtrip_physical_filename = "param_physical_round.json"
            dimensionless_filename = "param_dimensionless.json"

            cmd = [
                "qualikiz_tools",
                "input",
                "to_physical",
                "--output=" + physical_filename,
                "parameters.json",
            ]
            print('Test suite running "{!s}"'.format(" ".join(cmd)))
            subprocess.run(cmd)

            cmd = [
                "qualikiz_tools",
                "input",
                "to_dimensionless",
                "--output=" + dimensionless_filename,
                physical_filename,
            ]
            print('Test suite running "{!s}"'.format(" ".join(cmd)))
            subprocess.run(cmd)

            cmd = [
                "qualikiz_tools",
                "input",
                "to_physical",
                "--output=" + roundtrip_physical_filename,
                dimensionless_filename,
            ]
            print('Test suite running "{!s}"'.format(" ".join(cmd)))
            subprocess.run(cmd)

            with open(physical_filename, "r") as physfile, open(
                roundtrip_physical_filename, "r"
            ) as phys_round_file:
                phys = json.load(physfile)
                rnd = json.load(phys_round_file)
                for key in rnd.keys():
                    assert_allclose(phys[key], rnd[key], err_msg="field=" + key)
