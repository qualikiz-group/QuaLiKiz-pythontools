from subprocess import check_output, PIPE
from unittest import TestCase, skip
import os
import shutil
from pathlib import Path

from IPython import embed
import pytest

this_file = Path(__file__).resolve()


def test_returns_usage_information():
    cmd = "qualikiz_tools create -h"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")

    cmd = "qualikiz_tools create --help"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")

    cmd = "qualikiz_tools create help"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert output.startswith(b"Usage:")


def test_returns_multiple_lines():
    cmd = "qualikiz_tools create help"
    output = check_output(cmd, stderr=PIPE, shell=True)
    assert len(output.decode("UTF-8").split("\n")) > 1


def test_create_example(tmpdir):
    cmd = "qualikiz_tools create example"
    with tmpdir.as_cwd():
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
        assert Path("example").is_dir()
        assert Path("example/parameters.json").is_file()


def test_create_example_batch(tmpdir):
    cmd = "qualikiz_tools create example_batch"
    with tmpdir.as_cwd():
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
        assert Path("example_batch").is_dir()
        assert Path("example_batch/parameters_template.json").is_file()
        assert Path("example_batch/multirun.py").is_file()


def test_create_example_from_json(tmpdir):
    templatepath = (
        this_file / "../../../qualikiz_tools/qualikiz_io/parameters_template.json"
    ).resolve()
    cmd = "qualikiz_tools create from_json {!s}".format(templatepath)
    with tmpdir.as_cwd():
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
        assert Path("parameters_template").is_dir()
        assert Path("parameters_template/parameters.json").is_file()


def test_create_example_from_json_batch(tmpdir):
    templatepath = (
        this_file / "../../../qualikiz_tools/qualikiz_io/parameters_template.json"
    ).resolve()
    cmd = "qualikiz_tools create from_json {!s} --as_batch=bash".format(templatepath)
    with tmpdir.as_cwd():
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
        assert Path("parameters_template").is_dir()
        assert Path("parameters_template/parameters.json").is_file()
        assert Path("parameters_template/qualikiz.batch").is_file()


@pytest.mark.need_casefiles
def test_create_regression(tmpdir):
    cmd = "qualikiz_tools create regression --as_batch=bash"
    with tmpdir.as_cwd():
        output = check_output(cmd, stderr=PIPE, shell=True, timeout=60)
        assert Path("regression").is_dir()
        assert Path("regression/quicktest_parameters").is_dir()
        assert Path("regression/quicktest_parameters/parameters.json").is_file()
        assert Path("regression/qualikiz.batch").is_file()
