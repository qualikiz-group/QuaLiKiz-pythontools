""" Test internal pytest functions """
import os
from pathlib import Path

import pytest
from IPython import embed  # pylint: disable=unused-import # noqa: F401


@pytest.fixture
def chdirred_back(request):
    """ Test if the cwd is the same directory we called pytest from """
    yield
    assert Path.cwd() == request.session.startdir, "We are not back where we started"


def test_recursive_relink_copy(request, chdirred_back, prepare_qlk_run):
    """ Test recursively copying a QuaLiKiz run directory with the pytest fixture mechanisms"""
    startdir = request.session.startdir
    os.chdir("/tmp")  # Move to something we are for sure never living
    # Check if we even have to move
    assert Path.cwd() != startdir
    assert Path.cwd() != prepare_qlk_run
