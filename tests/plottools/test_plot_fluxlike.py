import os
from subprocess import CalledProcessError
from pathlib import Path
from pathlib import Path as LocalPath

from matplotlib.pyplot import Figure
import xarray as xr
import pytest
from pytest import approx
from IPython import embed

from qualikiz_tools.qualikiz_io.qualikizrun import QuaLiKizBatch
from qualikiz_tools.plottools.plot_fluxlike import *


class TestDetermineScandim:
    def test_determine_scandim_default(self, request, default_plotcase):
        data_path: LocalPath = default_plotcase.data_paths[1]
        batch: QuaLiKizBatch = QuaLiKizBatch.from_dir(data_path)
        dss: list = batch.to_netcdf()
        ds: xr.Dataset = dss[0]
        da: xr.DataArray = determine_scandim(ds)
        assert isinstance(da, xr.DataArray)
        assert ("dimx",) == da.dims
        # Float32 comparisons
        dimx_file = default_plotcase.data_paths[0] / "/debug/dimx.dat"
        assert dimx_file.exists()
        dimx_data = np.genfromtxt(dimx_file)
        reference = np.arange(0, dimx_data, dtype=da.dtype)
        assert reference == approx(da.values)
        assert reference == approx(da["dimx"].values)


class TestBuildPlotNonInteractive:
    @pytest.fixture
    def default_dss(self, default_plotcase):
        data_path: LocalPath = default_plotcase.data_paths[1]
        batch: QuaLiKizBatch = QuaLiKizBatch.from_dir(data_path)
        dss: list = batch.to_netcdf()
        return data_path, dss

    @pytest.fixture
    def default_ds(self, default_dss):
        batch_path: LocalPath = default_dss[0]
        ds: xr.Dataset = default_dss[1][0]
        return batch_path, ds

    def test_defaults(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(dss, flux_type, normalization, interactive=False)
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()

    def test_instability_tag_total(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(
                dss,
                flux_type,
                normalization,
                interactive=False,
                instability_tag="total",
            )
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()

    def test_sum_hydrogen_true(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(
                dss, flux_type, normalization, interactive=False, sum_hydrogen=True
            )
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()

    def test_drop_non_hydrogen_true(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(
                dss, flux_type, normalization, interactive=False, drop_non_hydrogen=True
            )
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()

    def test_lowwave_boundary_two(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(
                dss, flux_type, normalization, interactive=False, lowwave_boundry=2
            )
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()

    def test_names_list(self, default_dss):
        # Boilerplate
        batch_path: LocalPath = default_dss[0]
        dss: list = default_dss[1]
        flux_type = "ef"
        normalization = "GB"
        with batch_path.as_cwd() as cwd:
            # The test
            fig: Figure = build_plot(
                dss, flux_type, normalization, interactive=False, names=["testname1"]
            )
            figname = "".join([str(hash(self)), ".png"])
            assert isinstance(fig, Figure)
            fig.savefig(figname)
            assert Path(figname).exists()
