import os

from IPython import embed
import pytest

from qualikiz_tools.plottools.pretty_names import *


@pytest.fixture
def this_file(request, tmpdir, qlk_dir):
    test_root: LocalPath = request.fspath.realpath()  # Path to this file
    pretty_script_path: LocalPath = test_root.join(
        "../../../qualikiz_tools/plottools/plot_fluxlike.py"
    ).realpath()
    return pretty_script_path


def test_import():
    from qualikiz_tools.plottools.pretty_names import plot_styles

    assert len(plot_styles) > 1
    from qualikiz_tools.plottools.pretty_names import prims

    assert len(prims) > 1


def test_get_file_list(default_plotcase):
    data_path = default_plotcase.data_paths[1]
    output_path = data_path.join("output")
    primitive_path = data_path.join("output/primitive")
    assert output_path.exists()
    assert primitive_path.exists()
    with default_plotcase.tmpdir.as_cwd():
        cwd = os.getcwd()
        parsed_file_list: dict = parse_file_list_dir(output_path)
    assert len(list(Path(output_path).glob("*"))) == len(parsed_file_list)
