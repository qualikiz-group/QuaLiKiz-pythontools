# Generated with a pip freeze after
# . /u/sim/sh/grpdef
# module load jintrac
# module load jintrac-pythontools
# Disabled anaconda-only packages
# Disabled not-comiling packages
# Disabled heimdall-only packages

alabaster==0.7.12
#anaconda-client==1.7.2 # Anaconda specific
#anaconda-navigator==1.9.6 # Anaconda specific
#anaconda-project==0.8.2 # Anaconda specific
appdirs==1.4.3
apptools==4.4.0
asciitable==0.8.0
asn1crypto==0.24.0
asteval==0.9.13
astroid==2.1.0
astropy==3.2.1
astroquery==0.3.9
atomicwrites==1.2.1
attrs==18.2.0
Automat==0.7.0
Babel==2.6.0
backcall==0.1.0
backports.os==0.1.1
backports.shutil-get-terminal-size==1.0.0
beautifulsoup4==4.6.3
bitarray==0.8.3
bkcharts==0.2
black==20.8b1
#blaze==0.11.3 # Not available from PyPI. closest is 0.10.1, newest is 0.10.1
blaze==0.10.1
bleach==3.0.2
blessings==1.7
blinker==1.4
bokeh==1.0.2
boto==2.49.0
boto3==1.9.66
botocore==1.12.67
Bottleneck==1.2.1
bpython==0.17.1
breathe==4.11.1
Cerberus==1.3.2
certifi==2020.6.20
cffi==1.11.5
cftime==1.0.3.4
chardet==3.0.4
click==7.1.2
cloudpickle==0.6.1
#clyent==1.2.2 # Not available from PyPI. Closest is 1.2.1, newest is 1.2.1
clyent==1.2.1
colorama==0.4.1
# conda==4.6.14 # Not available from PyPI. Closest is 4.3.16, newest is 4.3.16
# conda-build==3.17.6 # Not available from PyPI. Closest is 2.1.5, newest is 2.1.5
configobj==5.0.6
constantly==15.1.0
contextlib2==0.5.5
control==0.8.1
coverage==5.3
cryptography==2.4.2
curtsies==0.3.0
cycler==0.10.0
#Cython==0.29.2 # This is a build dependency for many packages
cytoolz==0.9.0.1
dask==1.0.0
#datashape==0.5.4 # Not available from PyPI. Closest is 0.5.2, newest is 0.5.2
datashape==0.5.2
decorator==4.3.0
defusedxml==0.5.0
distributed==1.25.1
Django==2.1.4
docopt==0.6.2
docutils==0.14
entrypoints==0.2.3
et-xmlfile==1.0.1
f90nml==1.2
fastcache==1.0.2
feedgenerator==1.9
filelock==3.0.10
Flask==1.0.2
Flask-Cors==3.0.7
Flask-RESTful==0.3.7
fortranformat==0.2.5
future==0.17.1
fuzzywuzzy==0.17.0
gevent==1.3.7
glob2==0.6
Glymur==0.8.16
# gmpy2==2.0.8 # This fails in the 'build' step without 'mpfr.h'
greenlet==0.4.15
grip==4.5.2
gunicorn==19.9.0
h5py==2.8.0
heapdict==1.0.0
html5lib==1.0.1
hyperlink==18.0.0
#idlbridge==1.1.0 # We don't have IDL
idna==2.10
imageio==2.4.1
imagesize==1.1.0
iminuit==1.3.3
importlib-metadata==2.0.0
imutils==0.5.2
incremental==17.5.0
iniconfig==1.0.1
ipykernel==5.1.0
ipython==7.2.0
ipython-genutils==0.2.0
ipywidgets==7.4.2
isort==4.3.4
itsdangerous==1.1.0
jdcal==1.4
jedi==0.13.2
jeepney==0.4
# JET specific packages
#jet.data.cpf==1.1
#jet.data.dataclasses==1.0.1
#jet.data.sal==1.3.2
#jetto-tools @ file:///home/sim/jintrac-pythontools/jetto-pythontools/dist/jetto_tools-0.2.8-py3-none-any.whl
Jinja2==2.10
jmespath==0.9.3
jsonschema==2.6.0
jupyter==1.0.0
jupyter-client==5.2.4
jupyter-console==6.0.0
jupyter-core==4.4.0
jupyterlab==0.35.3
jupyterlab-launcher==0.13.1
jupyterlab-server==0.2.0
keyring==17.0.0
kiwisolver==1.0.1
lazy-object-proxy==1.3.1
ldap3==2.5.1
libarchive-c==2.8
#lief==0.9.0 # This fails in the 'build' step without access to '/root/lief-0.9.0-py3.7-linux.egg'
#line-profiler==2.1.1 # Fails building wheel because of Python versions?
llvmlite==0.26.0
lmfit==0.9.12
locket==0.2.0
logilab-common==1.4.2
lxml==4.2.5
Markdown==3.0.1
MarkupSafe==1.1.0
matplotlib==3.0.2
mccabe==0.6.1
mistune==0.8.4
#mkl-fft==1.0.6 # Not available on PyPI
#mkl-random==1.0.2 # Not available on PyPI
mock==2.0.0
more-itertools==4.3.0
mpldatacursor==0.6.2
mpmath==1.1.0
msgpack==0.5.6
multipledispatch==0.6.0
natsort==5.5.0
#navigator-updater==0.2.1 # Anaconda only
nbconvert==5.3.1
nbformat==4.4.0
netCDF4==1.4.2
networkx==2.2
nltk==3.4
nose==1.3.7
notebook==5.7.4
numba==0.39.0
numexpr==2.6.8
#numpy==1.15.4 # This is a build dependency for many packages
numpydoc==0.8.0
oct2py==4.0.6
#odo==0.5.1 # Not available from PyPI. Closest is 0.5.0, newest is 0.5.0
odo==0.5.0
olefile==0.46
#opencv-python==4.0.0.21  # Not available for Python 3.8
opencv-python==4.1.2.30
openpyxl==2.5.12
packaging==18.0
pam==0.1.4
pandas==1.1.3
pandocfilters==1.4.2
parso==0.3.1
partd==0.3.9
path-and-address==2.0.1
path.py==11.5.0
pathlib2==2.3.3
pathspec==0.8.0
patsy==0.5.1
pbr==5.1.1
pelican==4.0.1
pep8==1.7.1
pexpect==4.6.0
pickleshare==0.7.5
Pillow==5.3.0
pkginfo==1.4.2
plotly==3.4.2
pluggy==0.13.1
ply==3.11
pockets==0.7.2
prometheus-client==0.5.0
prominence==0.7.0
prompt-toolkit==2.0.7
psutil==5.4.8
psycopg2==2.7.6.1
ptyprocess==0.6.0
py==1.9.0
pyasn1==0.4.4
pyasn1-modules==0.2.2
pycodestyle==2.4.0
pycosat==0.6.3
pycparser==2.19
pycrypto==2.6.1
pycurl==7.43.0.2
pyface==6.0.0
pyFFTW==0.11.1
pyflakes==2.0.0
Pygments==2.3.1
PyHamcrest==1.9.0
PyJWT==1.7.1
pykerberos==1.2.1
pylint==2.2.2
pymssql==2.1.4
#pyodbc==4.0.25 # Fails building without sql.h
PyOpenGL==3.1.1a1
pyOpenSSL==18.0.0
pyparsing==2.3.0
#pyqtgraph==0.10.0ukaea0 # JET only
pyqtgraph==0.10.0 # Closest version
PySide2==5.12.0
PySocks==1.6.8
pytest==6.1.0
pytest-arraydiff==0.3
pytest-astropy==0.5.0
pytest-cov==2.10.1
pytest-doctestplus==0.2.0
pytest-mock==3.3.1
pytest-openfiles==0.3.1
pytest-remotedata==0.3.1
python-dateutil==2.7.5
pytz==2018.7
PyWavelets==1.0.1
PyYAML==3.13
pyzmq==17.1.2
QtAwesome==0.5.3
qtconsole==4.4.3
QtPy==1.5.2
quantities==0.12.2
regex==2020.10.23
requests==2.24.0
requests-kerberos==0.12.0
retrying==1.3.3
rope==0.11.0
ruamel-yaml==0.15.46
s3transfer==0.1.13
sal==1.2.2
scikit-image==0.14.1
scikit-learn==0.20.1
#scikit-sparse==0.4.4 # Fails building without cholmod.h
scipy==1.1.0
seaborn==0.9.0
SecretStorage==3.1.0
Send2Trash==1.5.0
service-identity==18.1.0
sh==1.12.14
#Shapely==1.6.4.post1 # Not available on PyPI
simplegeneric==0.8.1
singledispatch==3.4.0.3
six==1.12.0
snowballstemmer==1.2.1
sortedcollections==1.0.1
sortedcontainers==2.1.0
soupsieve==1.7.1
Sphinx==1.8.2
sphinx-readable-theme==1.3.0
sphinx-rtd-theme==0.4.2
sphinxcontrib-napoleon==0.7
sphinxcontrib-websupport==1.1.0
spyder==3.3.2
spyder-kernels==0.3.0
SQLAlchemy==1.2.15
statsmodels==0.9.0
sympy==1.3
#tables==3.4.4 # Needs HDF installed. Handle explicitly
tblib==1.3.2
terminado==0.8.1
testpath==0.4.2
tkcolorpicker==2.1.3
tkfilebrowser==2.3.2
toml==0.10.1
toolz==0.9.0
tornado==5.1.1
tqdm==4.28.1
traitlets==4.3.2
traits==4.6.0
traitsui==6.0.0
Twisted==18.9.0
typed-ast==1.4.1
typing-extensions==3.7.4.3
uncertainties==3.0.3
unicodecsv==0.14.1
#urllib3==1.25.10 # botocore 1.12.67 depends on urllib3=1.20; python_version >= "3.4"
wcwidth==0.1.7
webencodings==0.5.1
Werkzeug==0.14.1
widgetsnbextension==3.4.2
wrapt==1.10.11
wurlitzer==1.0.2
xarray==0.16.1
xlrd==1.2.0
XlsxWriter==1.1.2
xlwt==1.3.0
xmltodict==0.11.0
zict==0.1.3
zipp==3.2.0
zope.interface==4.6.0
