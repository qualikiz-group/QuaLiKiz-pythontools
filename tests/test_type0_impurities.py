# pylint: disable=no-self-use,missing-function-docstring,missing-class-docstring,missing-module-docstring,redefined-outer-name # noqa
from pathlib import Path
from pathlib import Path as LocalPath

import numpy as np
import pytest
from IPython import embed  # pylint: disable=unused-import # noqa: F401

from qualikiz_tools.machine_specific.bash import Run, Batch

pytestmark = pytest.mark.run_qualikiz

this_file = Path(__file__).resolve()  # Normalized path

binpath = (this_file / "../../../QuaLiKiz").resolve()


@pytest.fixture
def qlflux_mode_run(regression_qlk_multibatch) -> Run:
    batch_dir = (
        regression_qlk_multibatch
        / "Nustar_0.001/smag_1.00_x_0.55_Ti_Te_rel_1.00_dilution_0.20"
    )
    with regression_qlk_multibatch.as_cwd():
        batch = Batch.from_dir(batch_dir)
        assert batch.is_done()

        # Remove one QLK run to save time
        del batch.runlist[1]
        script_path = batch.parent_dir / batch.name / batch.scriptname
        batch.to_batch_file(script_path, overwrite_batch_script=True)

        for run in batch.runlist:
            # Relink binary
            run.relink_binary(binpath)

            # Modify plan in memory to batch in qlflux_mode
            run.qualikiz_plan["xpoint_base"]["ions"][1]["type"] = 0

            # Overwrite the old plan
            run.qualikiz_plan.to_json(run.rundir / run.parameterspath)

            (run.rundir / "debug_qlflux").mkdir()
            (run.rundir / "output_qlflux" / Path(run.primitivedir).name).mkdir(
                parents=True
            )

        # Regenerate input with new ion type
        batch.generate_input()
        batch.launch(clean=False)
        return LocalPath(batch.runlist[0].rundir)


def test_qlflux_mode_regress(qlflux_mode_run: LocalPath):
    with qlflux_mode_run.as_cwd():
        # Check if the run is done
        assert qlflux_mode_run / "output_qlflux/vfi_GB.dat"
        efiITG = np.loadtxt(qlflux_mode_run / "output_qlflux/efiITG_GB.dat")
        assert pytest.approx(efiITG[:, 1], 0)
        efiITG_orig = np.loadtxt(qlflux_mode_run / "output/efiITG_GB.dat")
        assert pytest.approx(efiITG[:, 0], efiITG_orig[:, 0])


def test_qlflux_mode_scan(prepare_qlk_run: LocalPath):
    # We test the nitty gritty IO in TestQuaLiKizRun
    # Be a bit rough here
    run = Run.from_dir(prepare_qlk_run)
    inputdir = run.rundir / run.inputdir
    debugdir = run.rundir / run.debug_qlfluxdir
    outputdir = run.rundir / run.output_qlfluxdir
    primitivedir = run.rundir / run.primitive_qlfluxdir
    debugdir.mkdir()
    primitivedir.mkdir(parents=True)
    with prepare_qlk_run.as_cwd():
        # Check our folder is empty at the start
        non_prim = set(outputdir.iterdir()) - set(
            outputdir.glob(Path(run.primitive_qlfluxdir).name)
        )
        assert len(non_prim) == 0
        assert len(list(primitivedir.iterdir())) == 0
        assert len(list(debugdir.iterdir())) == 0

        batch = Batch(run.rundir.parent, run.rundir.name, [run])
        batch.generate_input(qlflux=True)
        batch.launch(qlflux=True)
