# makefile for qualikiz-pythontools library
PYTHON?=python
PIP?=pip
ECHO?=$(shell which echo) -e
PRINTF?=$(shell which printf)

# Variables required for release and bugfix options
PROJECT=qualikiz-pythontools
PROJECT_ID=10189354
PACKAGE=qualikiz_tools
PACKAGE_HOST=gitlab

# Other variables
SUBDIRS:=

#####################################################

.PHONY: help clean sdist bdist wheel install_package_jintrac upload_package_to_gitlab install_package_from_gitlab docs realclean

help:
	@$(ECHO) "Recipes:"
	@$(ECHO) "  clean                       - remove all build, test, doc, and Python artifacts"
	@$(ECHO) "  sdist                       - build package for source distribution"
	@$(ECHO) "  bdist                       - build package for binary distribution"
	@$(ECHO) "  wheel                       - build package for binary wheel distribution"
	@$(ECHO) "  install_package_jintrac     - build wheel and install into JINTRAC_PYTHON_INSTALL_DIR"
	@$(ECHO) "  upload_package_to_gitlab    - build wheel and sdist + upload to GitLab "
	@$(ECHO) "  install_package_from_gitlab - install package from GitLabs simple repository"
	@$(ECHO) "  docs                        - build documentation for package"
	@$(ECHO) "  realclean                   - clean and remove build distributions "
	@$(ECHO) ""
	@$(ECHO) "Environment variables:"
	@$(ECHO) "  PYTHON                      - Python binary to use for commands [default: "$(shell grep -e PYTHON?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  PIP                         - Pip binary to use for commands [default: "$(shell grep -e PIP?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  JINTRAC_PYTHON_INSTALL_DIR  - JINTRAC install directory [default: "$(shell grep -e JINTRAC_PYTHON_INSTALL_DIR?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  PYTHONTOOLS_EXTRAS          - Extras to install [default: "$(shell grep -e PYTHONTOOLS_EXTRAS?\= Makefile | cut -d\= -f2)"]"

clean:
	@echo 'Cleaning $(PROJECT)...'
	$(PYTHON) setup.py clean --all
	$(MAKE) -C docs $@

# Build a 'sdist' or 'installable source distribution' with setuptools
# This creates a sdist package installable with pip
# On pip install this will re-compile from source compiled components
sdist:
	$(PYTHON) setup.py sdist

# Build a 'bdist' or 'installable binary distribution' with setuptools
# This creates a bdist package installable with pip
# This should be pip-installable on the system it was compiled on
# Not recommended to use this! Use wheels instead
bdist:
	$(PYTHON) setup.py bdist

# Build a 'wheel' or 'installable universial binary distribution' with setuptools
# This creates a wheel that can be used with pip
wheel:
	$(PYTHON) setup.py bdist_wheel

# Get the current version.
# The name will be generated from git, i.e.
# 1.4.1+0.ge21d73f.dirty
# See https://github.com/python-versioneer/python-versioneer?tab=readme-ov-file
VERSION_STRING=$(shell $(PYTHON) setup.py --version)
WHEEL_NAME:=$(PACKAGE)-$(VERSION_STRING)-py3-none-any.whl
SDIST_NAME:=$(PACKAGE)-$(VERSION_STRING).tar.gz

# Install package in the common JINTRAC Python library directory
# As we know that we use py3 and do not have compiled components
# We can guess our generated wheel name
# EasyBuild pip installs like so: AL_CYTHON_VERSION=0.1.0 FORCE_USE_CYTHON=True pip install --prefix=/home/ITER/vandepk/.local/easybuild/software/AL-Cython/0.1.0-foss-2020a-Python-3.8.2  --no-deps  --ignore-installed  --no-build-isolation  .⋅
JINTRAC_PYTHON_INSTALL_DIR?=install
PYTHONTOOLS_EXTRAS?='gui,test'
install_package_jintrac: wheel
	@echo [INFO] Installing version '$(VERSION_STRING)'
	@echo [INFO] With extras $(PYTHONTOOLS_EXTRAS)
	@echo [INFO] to $(JINTRAC_PYTHON_INSTALL_DIR)
	@echo [INFO] Assuming generated wheel is '$(WHEEL_NAME)'
	@echo [DEBUG] setuptools version=$(shell $(PYTHON) -c "import setuptools; print(setuptools.__version__)")
	@echo [DEBUG] setuptools SCM detected version=$(shell $(PYTHON) -c "from setuptools_scm import get_version; print(get_version(root='.'));")
	@echo [DEBUG] wheel version=$(shell $(PYTHON) -c "import wheel; print(wheel.__version__)")
	PYTHONUSERBASE=$(JINTRAC_PYTHON_INSTALL_DIR) pip install dist/$(WHEEL_NAME)[$(PYTHONTOOLS_EXTRAS)] --cache-dir=$(JINTRAC_PYTHON_INSTALL_DIR)/pip_cache --user --upgrade

# You need write permission the the package repository to do this. We use deploy tokens for this
# that are tracked by pip. See https://docs.gitlab.com/ee/user/packages/pypi_repository/
# This involves:
#   - Getting a deploy key for the repository
#   - Setting up your default pip settings with ~/.pypirc
#   - (optional) build an encryped password store with e.g. KGpg
upload_package_to_gitlab: wheel sdist
	$(PYTHON) -m twine upload --verbose --repository $(PACKAGE)_$(PACKAGE_HOST) dist/$(WHEEL_NAME) dist/$(SDIST_NAME)

install_package_from_gitlab:
	$(PIP) install $(PACKAGE)[$(PYTHONTOOLS_EXTRAS)] --extra-index-url https://$(PACKAGE_HOST).com/api/v4/projects/$(PROJECT_ID)/packages/pypi/simple

docs:
	$(MAKE) -C docs html

realclean: clean
	@echo 'Real cleaning $(PROJECT)...'
	rm -f dist/*
	$(MAKE) -C docs $@
