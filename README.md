QuaLiKiz-pythontools
====================

*A collection of tools for QuaLiKiz in Python.*

This is a collection of Python modules to be used for working with QuaLiKiz,
a quasi-linear gyrokinetic code. QuaLiKiz can be found
[on GitLab](https://gitlab.com/qualikiz-group/QuaLiKiz).
This repository contains the source for the Python package qualikiz_tools.
Use the qualikiz_tools CLI (Command Line Interface) to generate, run and
analyze QuaLiKiz runs. For more advanced usage scenarios the modules
themselves can be used in other python scripts. For example, the QuaLiKiz neural
network [QLKNN-10D](https://gitlab.com/Karel-van-de-Plassche/QLKNN-develop)
training set was generated in this way.  You can find more "doc-like docs" on
[GitLab Pages](https://qualikiz-group.gitlab.io/QuaLiKiz-pythontools/).


Install
-------
The recommended way to install qualikiz_tools is to use pip. Although
installation is not strictly necessary to use the python modules,
it is advised to install anyway for the full power of the CLI.

1. [Set up your SSH keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
2. [Clone the repository from GitLab](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html).
     If you want to install as submodule of QuaLiKiz (preferred)

        git clone git@gitlab.com:QuaLiKiz-group/QuaLiKiz.git
        cd QuaLiKiz
        git submodule init
        git submodule update

     Or **NOT RECOMMENDED** if you instead want to install standalone, clone using

        git clone git@gitlab.com:QuaLiKiz-group/QuaLiKiz-pythontools.git

3. We assume you are installing into a [virtual environment](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/)
     This command will install the pythontools with all its optional 
     dependencies, e.g. for QLKNN _et al._

        cd QuaLiKiz-pythontools
        pip install --upgrade .[all]

4. Point the tools to the QuaLiKiz binary using the `QUALIKIZ_BIN` environment variable. If you do
     not set this, historic heuristics will be used to find the binary instead. Note that the name
     of the binary depends on the flags given to TUBS when building QuaLiKiz. See the
     [TUBS documentation](https://gitlab.com/tci-dev/tubs/-/blob/master/readme.md) for more
     information

       export QUALIKIZ_BIN="../bin/QuaLiKiz-gcc-release-default-mpi.exe"

Usage
-----

Examples scripts can be found in qualikiz_tools/examples. A workflow example is
given below:

1. Generate a template QuaLiKiz run directory using the CLI

       qualikiz_tools create example
       cd runs/example

2. Adjust the `parameters.json` to your liking (optional)
3. Generate the input binaries using the CLI

       qualikiz_tools input generate .

4. Submit the job. We assume the [machine specific](qualikiz_tools/machine_specific) files exist. Use 'bash' if they do not, which assumes you can run an mpi program using `mpirun`

       qualikiz_tools launcher launch bash .

5. After the job is done, convert the output to netCDF.

       qualikiz_tools output to_netcdf .

6. Plot your netCDF file, for example:

       qualikiz_tools plot --flux ef .

FAQ
---

* How do I find out what each CLI command does?

      qualikiz_tools help
      qualikiz_tools <command> help
