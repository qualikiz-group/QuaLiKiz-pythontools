# This file is part of QuaLiKiz-pythontools.
#
# QuaLiKiz-pythontools is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# QuaLiKiz-pythontools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with QuaLiKiz-pythontools.  If not, see <https://www.gnu.org/licenses/>.
import sys

if sys.version_info <= (3, 6):
    sys.exit(
        "Sorry, Python <= 3.6 is not supported. Use a different"
        " python e.g. `module swap python python/3.7`"
    )

import logging
import site
import versioneer

from setuptools import find_packages, setup

qlk_tools_logger = logging.getLogger("qualikiz_tools")
logger = qlk_tools_logger
logger.setLevel(logging.INFO)

logger.info("pyproject.toml support got added in pip 10." " Assuming it is available")

# Workaround for https://github.com/pypa/pip/issues/7953
# Cannot install into user site directory with editable source
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

setup(
    packages=find_packages(exclude=["docs", "tests*"]),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
