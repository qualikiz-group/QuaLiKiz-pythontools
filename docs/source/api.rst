.. currentmodule:: qualikiz_tools

API reference
=============

This page provides an auto-generated summary of xarray's API. For more details
and examples, refer to the relevant chapters in the main part of the
documentation.

.. See also: :ref:`public api`

QuaLiKiz IO manipulation
------------------------

Input
*****

.. autosummary::
   :toctree: generated/
   :template: custom-module-template.rst
   :recursive:

   qualikiz_io.inputfiles
   qualikiz_io.inputfiles.QuaLiKizXpoint
   qualikiz_io.inputfiles.QuaLiKizPlan

Output (basic)
**************

We use :std:term:`xarray's Dataset abstraction <xarray:Dataset>`, see :py:class:`xarray:xarray.Dataset`. Prefer using :py:class:`netCDF4 backend <xarray:xarray.backends.NetCDF4DataStore>`, which uses the :netcdf4:`netCDF4 library <>`. Internally, this uses the :netcdf4:`nc4.Dataset <#netCDF4.Dataset>` class and the :netcdf4:`nc4.Dataset.createVariable <#netCDF4.Dataset.createVariable>` method. Test :py:meth:`xarray:xarray.Dataset.to_netcdf`

.. autosummary::
   :toctree: generated/
   :template: custom-module-template.rst
   :recursive:

   qualikiz_io.outputfiles
   qualikiz_io.outputfiles.split_ion_coordinates
   qualikiz_io.outputfiles.to_input_json
   qualikiz_io.outputfiles.xarray_to_pandas
   qualikiz_io.outputfiles.qualikiz_folder_to_xarray


.. autosummary::
   :toctree: generated/
   :template: custom-module-template.rst
   :recursive:

   qualikiz_io.qualikizrun
   qualikiz_io.qualikizrun.QuaLiKizRun.to_netcdf
   qualikiz_io.qualikizrun.QuaLiKizBatch.to_netcdf
   qualikiz_io.qualikizrun.run_to_netcdf

Output (hyperslabs)
*******************
.. autosummary::
   :toctree: generated/
   :template: custom-module-template.rst
   :recursive:

   qualikiz_io.outputfiles.squeeze_coords
   qualikiz_io.outputfiles.squeeze_dataset
   qualikiz_io.outputfiles.remove_dependent_axes
   qualikiz_io.outputfiles.orthogonalize_dataset
   qualikiz_io.outputfiles.add_dims
   qualikiz_io.outputfiles.merge_many_lazy_snakes
   qualikiz_io.outputfiles.merge_many_orthogonal
   qualikiz_io.outputfiles.merge_orthogonal
   qualikiz_io.outputfiles.append_metadata_to_xarray
