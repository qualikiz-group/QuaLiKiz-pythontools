.. Generate API reference pages, but don't display these in tables.
.. This extra page is a work around for sphinx not having any support for
.. hiding an autosummary table.

API autosummary
===============

.. Explicitly list submodules here
.. autosummary::
    :toctree: generated/
    :recursive:
    :template: custom-module-template.rst

    qualikiz_tools.qualikiz_io
    qualikiz_tools.cli
    qualikiz_tools.commands
    qualikiz_tools.fs_manipulation
    qualikiz_tools.machine_specific
    qualikiz_tools.misc
    qualikiz_tools.plottools
    qualikiz_tools.scheduling
    qualikiz_tools.setup_logging
