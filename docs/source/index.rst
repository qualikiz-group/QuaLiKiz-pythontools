.. Master "index". This will be converted to a landing index.html by sphinx. We define TOC here, but it'll be put in the sidebar by the theme

===========================
QuaLiKiz-pythontools Manual
===========================
Welcome! This is the documentation manual for the Python tools for
`QuaLiKiz <http://qualikiz.com>`_. These are tools to run QuaLiKiz
in standalone mode, as well as analyse and set up QuaLiKiz runs.
QuaLiKiz-pythontools is an open source project under the L-GPL license.
Please contribute by opening a feature request or issues
`on GitLab <https://gitlab.com/qualikiz-group/QuaLiKiz-pythontools/-/issues>`_
or open a `merge request <https://gitlab.com/qualikiz-group/QuaLiKiz-pythontools/-/merge_requests>`_.

For users
=========
* :qlksrc:`Compiling QuaLiKiz from source<README.md#install>`
* :src:`Installing the Pythontools<README.md#install>`
* :wiki:`Running standalone QuaLiKiz with the Pythontools<Running-with-Pythontools-in-standalone>`
* QuaLiKiz is also included in :jintrac:`JINTRAC QuaLiKiz pages<JETTO_QuaLiKiz.html>`
* Documentation is WIP! :merge:`44`
* This issue is closed! :issue:`1`
* A schematic design schematic can be found here: :doc:`pythontools_design_schematic`

.. toctree::
   :hidden:
   :caption: Getting Started

   self

For developers
==============

* :doc:`api`
* :doc:`api-hidden`

.. toctree::
   :hidden:
   :caption: API docs

   api
   api-hidden



LICENSE
-------
.. literalinclude:: ../../LICENSE


README copy
-----------
This README is best read on :src:`#qualikiz-pythontools`. You can also read a re-render here:

.. toctree::
   :hidden:
   QuaLiKiz-Pythontools README <README>

Sitemap
-------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

